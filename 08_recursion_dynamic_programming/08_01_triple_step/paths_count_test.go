package paths

import "testing"

func TestCount(t *testing.T) {
	testCases := []struct {
		name      string
		inSteps   int
		wantCount int
	}{
		{
			name:      "negative steps",
			inSteps:   -6,
			wantCount: 0,
		},
		{
			name:      "zero steps",
			inSteps:   0,
			wantCount: 1,
			/*
				0
			*/
		},
		{
			name:      "one step",
			inSteps:   1,
			wantCount: 1,
			/*
				0, 1
			*/
		},
		{
			name:      "two steps",
			inSteps:   2,
			wantCount: 2,
			/*
				0, 1, 2
				0, 2
			*/
		},
		{
			name:      "three steps",
			inSteps:   3,
			wantCount: 4,
			/*
				0, 1, 2, 3
				0, 1, 3
				0, 2, 3
				0, 3
			*/
		},
		{
			name:      "four steps",
			inSteps:   4,
			wantCount: 7,
			/*
				0, 1, 2, 3, 4
				0, 1, 2, 4
				0, 1, 3, 4
				0, 1, 4
				0, 2, 3, 4
				0, 2, 4
				0, 3, 4
			*/
		},
		{
			name:      "five steps",
			inSteps:   5,
			wantCount: 13,
			/*
				0, 1, 2, 3, 4, 5
				0, 1, 2, 3, 5
				0, 1, 2, 4, 5
				0, 1, 2, 5
				0, 1, 3, 4, 5
				0, 1, 3, 5
				0, 1, 4, 5
				0, 2, 3, 4, 5
				0, 2, 3, 5
				0, 2, 4, 5
				0, 2, 5
				0, 3, 4, 5
				0, 3, 5
			*/
		},
		{
			name:      "six steps",
			inSteps:   6,
			wantCount: 24,
			/*
				0, 1, 2, 3, 4, 5, 6
				0, 1, 2, 3, 4, 6
				0, 1, 2, 3, 5, 6
				0, 1, 2, 3, 6
				0, 1, 2, 4, 5, 6
				0, 1, 2, 4, 6
				0, 1, 2, 5, 6
				0, 1, 3, 4, 5, 6
				0, 1, 3, 4, 6
				0, 1, 3, 5, 6
				0, 1, 3, 6
				0, 1, 4, 5, 6
				0, 1, 4, 6
				0, 2, 3, 4, 5, 6
				0, 2, 3, 4, 6
				0, 2, 3, 5, 6
				0, 2, 3, 6
				0, 2, 4, 5, 6
				0, 2, 4, 6
				0, 2, 5, 6
				0, 3, 4, 5, 6
				0, 3, 4, 6
				0, 3, 5, 6
				0, 3, 6
			*/
		},
	}
	for _, testCase := range testCases {
		testCase := testCase
		t.Run(testCase.name, func(pt *testing.T) {
			gotCount := Count(testCase.inSteps)
			if gotCount != testCase.wantCount {
				pt.Errorf("Count(%d) = %d, want %d", testCase.inSteps, gotCount, testCase.wantCount)
			}
		})
	}
}
