package paths

// Count returns the number of ways to climb a ladder with that many steps.
// The rules are:
// - at each step, we can advance exactly 1 step, 2 steps, or 3 steps at a time (if the target step exists).
// - climbing starts on the floor (before the first step)
// - climbing ends right on the last step, not before or after.
func Count(ladderSteps int) int {
	switch {
	case ladderSteps < 0:
		return 0
	case ladderSteps == 0:
		return 1
	case ladderSteps == 1:
		return 1
	case ladderSteps == 2:
		return 2
	}
	threeBackCount := 1
	twoBackCount := 1
	oneBackCount := 2
	newCount := 0
	for currentStep := 3; currentStep <= ladderSteps; currentStep++ {
		newCount = threeBackCount + twoBackCount + oneBackCount
		threeBackCount = twoBackCount
		twoBackCount = oneBackCount
		oneBackCount = newCount
	}
	return newCount
}
