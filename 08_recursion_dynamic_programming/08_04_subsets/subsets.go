package subsets

import (
	"math/big"

	"gitlab.com/ogv/golang-coding-interview/00_utils/sliceutils"
)

// AllSubsets returns all the subsets of a set (including the whole set and the empty set).
func AllSubsets[E any](input []E) [][]E {
	if input == nil {
		return nil
	}
	var (
		currentSet []E
		result     = [][]E{{}}
	)
	for i, element := range input {
		upperLimit := 1 << i
		for subsetIndex := 0; subsetIndex < upperLimit; subsetIndex++ {
			currentSet = make([]E, len(result[subsetIndex])+1)
			copy(currentSet, result[subsetIndex])
			currentSet[len(currentSet)-1] = element
			result = append(result, currentSet)
		}
	}
	return result
}

// AllSubsetsB returns all the subsets of a set (including the whole set and the empty set).
func AllSubsetsB[E any](input []E) [][]E {
	if input == nil {
		return nil
	}
	var (
		previousSet    = make([]E, 0)
		currentSet     []E
		elementIndices = big.NewInt(0)
		includedIndex  int
		setsCount      = int64(1 << len(input))
		result         = [][]E{previousSet}
	)
	for setNumber := int64(2); setNumber <= setsCount; setNumber++ {
		currentSet = sliceutils.ReverseCopy(previousSet)
		for includedIndex = 0; elementIndices.Bit(includedIndex) == 1; includedIndex++ {
			elementIndices.SetBit(elementIndices, includedIndex, 0)
		}
		currentSet = currentSet[:len(currentSet)-includedIndex]

		elementIndices.SetBit(elementIndices, includedIndex, 1)
		currentSet = append(currentSet, input[includedIndex])
		sliceutils.Reverse(currentSet)
		result = append(result, currentSet)
		previousSet = currentSet
	}
	return result
}

// AllSubsetsC returns all the subsets of a set (including the whole set and the empty set).
func AllSubsetsC[E any](input []E) [][]E {
	if input == nil {
		return nil
	}
	var (
		currentSet     []E
		elementIndices = big.NewInt(0)
		includedIndex  int
		setsCount      = int64(1 << len(input))
		result         = [][]E{{}}
	)
	for setNumber := int64(2); setNumber <= setsCount; setNumber++ {
		currentSet = make([]E, 0, 1)

		for includedIndex = 0; elementIndices.Bit(includedIndex) == 1; includedIndex++ {
			elementIndices.SetBit(elementIndices, includedIndex, 0)
		}

		elementIndices.SetBit(elementIndices, includedIndex, 1)

		for i := 0; i < len(input); i++ {
			if elementIndices.Bit(i) == 1 {
				currentSet = append(currentSet, input[i])
			}
		}
		result = append(result, currentSet)
	}
	return result
}

// AllSubsetsD returns all the subsets of a set (including the whole set and the empty set).
func AllSubsetsD[E any](input []E) [][]E {
	if input == nil {
		return nil
	}
	var (
		previousSet    = make([]E, 0)
		currentSet     []E
		elementIndices = big.NewInt(0)
		includedIndex  int
		setsCount      = int64(1 << len(input))
		result         = [][]E{previousSet}
	)
	for setNumber := int64(2); setNumber <= setsCount; setNumber++ {
		currentSet = make([]E, len(previousSet))
		copy(currentSet, previousSet)

		for includedIndex = 0; elementIndices.Bit(includedIndex) == 1; includedIndex++ {
			elementIndices.SetBit(elementIndices, includedIndex, 0)
			currentSet = currentSet[1:]
		}
		sliceutils.Reverse(currentSet)

		elementIndices.SetBit(elementIndices, includedIndex, 1)
		currentSet = append(currentSet, input[includedIndex])
		sliceutils.Reverse(currentSet)
		result = append(result, currentSet)
		previousSet = currentSet
	}
	return result
}
