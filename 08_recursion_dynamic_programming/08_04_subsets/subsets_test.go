package subsets

import (
	"fmt"
	"math"
	"reflect"
	"testing"
)

func TestAllSubsets_Int(t *testing.T) {
	testAllSubsetsGenerator(t, AllSubsets[int], generateAllSubsetsIntTestCases())
}

func testAllSubsetsGenerator[E any](t *testing.T, generator subsetsGenerator[E], testCases []subsetsTestCase[E]) {
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			gotSubsets := generator(testCase.in)
			if !reflect.DeepEqual(testCase.wantSubsets, gotSubsets) {
				pt.Errorf("expected subsets:\n%+v\ngot subsets:\n%+v", testCase.wantSubsets, gotSubsets)
			}
		})
	}
}

type subsetsGenerator[E any] func(input []E) [][]E

func TestAllSubsetsB_Int(t *testing.T) {
	testAllSubsetsGenerator(t, AllSubsetsB[int], generateAllSubsetsIntTestCases())
}

func TestAllSubsetsC_Int(t *testing.T) {
	testAllSubsetsGenerator(t, AllSubsetsC[int], generateAllSubsetsIntTestCases())
}

func TestAllSubsetsD_Int(t *testing.T) {
	testAllSubsetsGenerator(t, AllSubsetsD[int], generateAllSubsetsIntTestCases())
}

func generateAllSubsetsIntTestCases() []subsetsTestCase[int] {
	return []subsetsTestCase[int]{
		{
			name:        "nil set",
			in:          nil,
			wantSubsets: nil,
		},
		{
			name:        "empty set",
			in:          []int{},
			wantSubsets: [][]int{{}},
		},
		{
			name:        "one element set",
			in:          []int{1},
			wantSubsets: [][]int{{}, {1}},
		},
		{
			name:        "two element set",
			in:          []int{1, 2},
			wantSubsets: [][]int{{}, {1}, {2}, {1, 2}},
		},
		{
			name:        "three element set",
			in:          []int{1, 2, 3},
			wantSubsets: [][]int{{}, {1}, {2}, {1, 2}, {3}, {1, 3}, {2, 3}, {1, 2, 3}},
		},
		{
			name:        "four element set",
			in:          []int{1, 2, 3, 4},
			wantSubsets: [][]int{{}, {1}, {2}, {1, 2}, {3}, {1, 3}, {2, 3}, {1, 2, 3}, {4}, {1, 4}, {2, 4}, {1, 2, 4}, {3, 4}, {1, 3, 4}, {2, 3, 4}, {1, 2, 3, 4}},
		},
		{
			name:        "three element extreme values set",
			in:          []int{math.MinInt, 0, math.MaxInt},
			wantSubsets: [][]int{{}, {math.MinInt}, {0}, {math.MinInt, 0}, {math.MaxInt}, {math.MinInt, math.MaxInt}, {0, math.MaxInt}, {math.MinInt, 0, math.MaxInt}},
		},
	}
}

type subsetsTestCase[T any] struct {
	name        string
	in          []T
	wantSubsets [][]T
}

var subsets [][]int

var allSubsetsElementCounts = []int{5, 10, 20, 22}

func BenchmarkAllSubsets_Int(b *testing.B) {
	for _, count := range allSubsetsElementCounts {
		elements := make([]int, count)
		b.Run(fmt.Sprintf("%d_elements", count), func(sb *testing.B) {
			for i := 0; i < sb.N; i++ {
				subsets = AllSubsets(elements)
			}
		})
	}
}

func BenchmarkAllSubsetsB_Int(b *testing.B) {
	for _, count := range allSubsetsElementCounts {
		elements := make([]int, count)
		b.Run(fmt.Sprintf("%d_elements", count), func(sb *testing.B) {
			for i := 0; i < sb.N; i++ {
				subsets = AllSubsetsB(elements)
			}
		})
	}
}

func BenchmarkAllSubsetsC_Int(b *testing.B) {
	for _, count := range allSubsetsElementCounts {
		elements := make([]int, count)
		b.Run(fmt.Sprintf("%d_elements", count), func(sb *testing.B) {
			for i := 0; i < sb.N; i++ {
				subsets = AllSubsetsC(elements)
			}
		})
	}
}

func BenchmarkAllSubsetsD_Int(b *testing.B) {
	for _, count := range allSubsetsElementCounts {
		elements := make([]int, count)
		b.Run(fmt.Sprintf("%d_elements", count), func(sb *testing.B) {
			for i := 0; i < sb.N; i++ {
				subsets = AllSubsetsD(elements)
			}
		})
	}
}
