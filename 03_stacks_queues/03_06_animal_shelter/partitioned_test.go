package partitioned

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/03_stacks_queues/types"
)

func TestNewSliceQueue(t *testing.T) {

	testCases := []struct {
		name      string
		inValues  []TaggedValue
		wantQueue *SliceQueue
	}{
		{
			name:      "success, create queue with no values",
			inValues:  []TaggedValue{},
			wantQueue: &SliceQueue{},
		},
		{
			name:      "success, ignore values with the invalid tag",
			inValues:  []TaggedValue{{Tag: InvalidTag, Value: 31}},
			wantQueue: &SliceQueue{},
		},
		{
			name:      "success, ignore values with an unsupported tag",
			inValues:  []TaggedValue{{Tag: TagB + 1, Value: 31}},
			wantQueue: &SliceQueue{},
		},

		{
			name:     "success, create queue with one A nil value",
			inValues: []TaggedValue{{Tag: TagA, Value: nil}},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: nil}, sequenceNumber: 0},
				}),
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 1,
			},
		},
		{
			name:     "success, create queue with one A value",
			inValues: []TaggedValue{{Tag: TagA, Value: 31}},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: 31}, sequenceNumber: 0},
				}),
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 1,
			},
		},
		{
			name: "success, create queue with several A values",
			inValues: []TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
			},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 0},
					{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 1},
					{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 2},
				}),
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 3,
			},
		},

		{
			name: "success, create queue with A, B, and invalid values",
			inValues: []TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: InvalidTag, Value: 32},
				{Tag: TagA, Value: 34},
				{Tag: TagB, Value: 36},
				{Tag: TagA, Value: 35},
				{Tag: TagB + 1, Value: 41},
				{Tag: TagB, Value: 37},
				{Tag: TagB, Value: 38},
				{Tag: TagA, Value: 39},
			},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 0},
					{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 1},
					{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 3},
					{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 6},
				}),
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 2},
					{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 4},
					{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 5},
				}),
				sequenceNumber: 7,
			},
		},

		{
			name:     "success, create queue with one B nil value",
			inValues: []TaggedValue{{Tag: TagB, Value: nil}},
			wantQueue: &SliceQueue{
				aElements: types.SliceQueue[numberedEntry]{},
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: nil}, sequenceNumber: 0},
				}),
				sequenceNumber: 1,
			},
		},
		{
			name:     "success, create queue with one B value",
			inValues: []TaggedValue{{Tag: TagB, Value: 32}},
			wantQueue: &SliceQueue{
				aElements: types.SliceQueue[numberedEntry]{},
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: 32}, sequenceNumber: 0},
				}),
				sequenceNumber: 1,
			},
		},
		{
			name: "success, create queue with several B values",
			inValues: []TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagB, Value: 38},
			},
			wantQueue: &SliceQueue{
				aElements: types.SliceQueue[numberedEntry]{},
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 0},
					{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 1},
					{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 2},
				}),
				sequenceNumber: 3,
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualQueue := NewSliceQueue(testCase.inValues)
			if !reflect.DeepEqual(actualQueue, testCase.wantQueue) {
				t.Errorf("expected new queue to be %#v but got %#v", testCase.wantQueue, actualQueue)
			}
		})
	}
}

func TestSliceQueueAdd(t *testing.T) {
	testCases := []struct {
		name      string
		inQueue   *SliceQueue
		inValues  []TaggedValue
		wantQueue *SliceQueue
	}{
		{
			name:    "success, add A value to empty queue",
			inQueue: &SliceQueue{},
			inValues: []TaggedValue{
				{Tag: TagA, Value: -6},
			},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: -6}},
				}),
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 1,
			},
		},
		{
			name: "success, add A value to queue of As",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 40},
				{Tag: TagA, Value: 41},
			}),
			inValues: []TaggedValue{{Tag: TagA, Value: -6}},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: 40}, sequenceNumber: 0},
					{TaggedValue: TaggedValue{Tag: TagA, Value: 41}, sequenceNumber: 1},
					{TaggedValue: TaggedValue{Tag: TagA, Value: -6}, sequenceNumber: 2},
				}),
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 3,
			},
		},
		{
			name: "success, add A value to queue of Bs",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 40},
				{Tag: TagB, Value: 41},
			}),
			inValues: []TaggedValue{{Tag: TagA, Value: -6}},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: -6}, sequenceNumber: 2},
				}),
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: 40}, sequenceNumber: 0},
					{TaggedValue: TaggedValue{Tag: TagB, Value: 41}, sequenceNumber: 1},
				}),
				sequenceNumber: 3,
			},
		},
		{
			name: "success, add A value to mixed queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 40},
				{Tag: TagA, Value: 41},
			}),
			inValues: []TaggedValue{{Tag: TagA, Value: -6}},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: 41}, sequenceNumber: 1},
					{TaggedValue: TaggedValue{Tag: TagA, Value: -6}, sequenceNumber: 2},
				}),
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: 40}, sequenceNumber: 0},
				}),
				sequenceNumber: 3,
			},
		},

		{
			name: "success, add B values to queue of As",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 40},
				{Tag: TagA, Value: 41},
			}),
			inValues: []TaggedValue{
				{Tag: TagB, Value: -6},
				{Tag: TagB, Value: -5},
			},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: 40}, sequenceNumber: 0},
					{TaggedValue: TaggedValue{Tag: TagA, Value: 41}, sequenceNumber: 1},
				}),
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: -6}, sequenceNumber: 2},
					{TaggedValue: TaggedValue{Tag: TagB, Value: -5}, sequenceNumber: 3},
				}),
				sequenceNumber: 4,
			},
		},
		{
			name: "success, add B valueS to queue of Bs",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 40},
				{Tag: TagB, Value: 41},
			}),
			inValues: []TaggedValue{
				{Tag: TagB, Value: -6},
				{Tag: TagB, Value: -5},
			},
			wantQueue: &SliceQueue{
				aElements: types.SliceQueue[numberedEntry]{},
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: 40}, sequenceNumber: 0},
					{TaggedValue: TaggedValue{Tag: TagB, Value: 41}, sequenceNumber: 1},
					{TaggedValue: TaggedValue{Tag: TagB, Value: -6}, sequenceNumber: 2},
					{TaggedValue: TaggedValue{Tag: TagB, Value: -5}, sequenceNumber: 3},
				}),
				sequenceNumber: 4,
			},
		},
		{
			name: "success, add B value to mixed queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 40},
				{Tag: TagA, Value: 41},
			}),
			inValues: []TaggedValue{{Tag: TagB, Value: -6}},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagA, Value: 41}, sequenceNumber: 1},
				}),
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: 40}, sequenceNumber: 0},
					{TaggedValue: TaggedValue{Tag: TagB, Value: -6}, sequenceNumber: 2},
				}),
				sequenceNumber: 3,
			},
		},

		{
			name:    "success, add B value to empty queue",
			inQueue: &SliceQueue{},
			inValues: []TaggedValue{
				{Tag: TagB, Value: -6},
			},
			wantQueue: &SliceQueue{
				aElements: types.SliceQueue[numberedEntry]{},
				bElements: *types.NewSliceQueue([]numberedEntry{
					{TaggedValue: TaggedValue{Tag: TagB, Value: -6}},
				}),
				sequenceNumber: 1,
			},
		},

		{
			name:      "ignore invalid tag value for empty queue",
			inQueue:   &SliceQueue{},
			inValues:  []TaggedValue{{Tag: InvalidTag, Value: 10}},
			wantQueue: &SliceQueue{},
		},
		{
			name:    "ignore wrong tag values for empty queue",
			inQueue: &SliceQueue{},
			inValues: []TaggedValue{
				{Tag: TagB + 1, Value: 10},
				{Tag: TagB + 2, Value: 11},
			},
			wantQueue: &SliceQueue{},
		},
		{
			name: "ignore invalid tag value",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 23},
				{Tag: TagB, Value: 14},
			}),
			inValues: []TaggedValue{{Tag: InvalidTag, Value: 10}},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 23},
				{Tag: TagB, Value: 14},
			}),
		},
		{
			name: "ignore wrong tag values",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 23},
				{Tag: TagB, Value: 14},
			}),
			inValues: []TaggedValue{
				{Tag: TagB + 1, Value: 10},
				{Tag: TagB + 2, Value: 11},
			},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 23},
				{Tag: TagB, Value: 14},
			}),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			for _, value := range testCase.inValues {
				testCase.inQueue.Add(value)
			}
			if !reflect.DeepEqual(testCase.inQueue, testCase.wantQueue) {
				t.Errorf("expected queue to be %+v after Add but got %+v", testCase.wantQueue, testCase.inQueue)
			}
		})
	}
}

func TestSliceQueueRemove(t *testing.T) {
	emptiedSlice := types.NewSliceQueue([]numberedEntry{{
		TaggedValue: TaggedValue{}}})
	_, _ = emptiedSlice.Remove()

	firstARemovedSlice := types.NewSliceQueue(
		[]numberedEntry{
			{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 0},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 3},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 4},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 5},
		})
	_, _ = firstARemovedSlice.Remove()

	firstBRemovedSlice := types.NewSliceQueue(
		[]numberedEntry{
			{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 0},
			{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 2},
			{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 6},
		})
	_, _ = firstBRemovedSlice.Remove()

	firstOfBsRemovedSlice := types.NewSliceQueue(
		[]numberedEntry{
			{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 0},
			{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 1},
			{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 2},
		})
	_, _ = firstOfBsRemovedSlice.Remove()

	firstOfAsRemovedSlice := types.NewSliceQueue(
		[]numberedEntry{
			{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 0},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 1},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 2},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 3},
		})
	_, _ = firstOfAsRemovedSlice.Remove()

	testCases := []struct {
		name      string
		inQueue   *SliceQueue
		wantValue TaggedValue
		wantQueue *SliceQueue
		wantAnErr bool
	}{
		{
			name: "success, remove the only A element in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 40},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 40},
			wantQueue: &SliceQueue{
				aElements:      *emptiedSlice,
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 0,
			},
			wantAnErr: false,
		},
		{
			name: "success, remove the only B element in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 41},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 41},
			wantQueue: &SliceQueue{
				aElements:      types.SliceQueue[numberedEntry]{},
				bElements:      *emptiedSlice,
				sequenceNumber: 0,
			},
			wantAnErr: false,
		},
		{
			name: "success, remove the oldest A element in an A queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 33},
			wantQueue: &SliceQueue{
				aElements:      *firstOfAsRemovedSlice,
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 4,
			},
			wantAnErr: false,
		},
		{
			name: "success, remove the oldest A element in a mixed queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 33},
			wantQueue: &SliceQueue{
				aElements: *firstARemovedSlice,
				bElements: *types.NewSliceQueue(
					[]numberedEntry{
						{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 1},
						{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 2},
						{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 6},
					}),
				sequenceNumber: 7,
			},
			wantAnErr: false,
		},

		{
			name: "success, remove the oldest B element in a mixed queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagA, Value: 33},
				{Tag: TagB, Value: 37},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 36},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue(
					[]numberedEntry{
						{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 1},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 3},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 4},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 5},
					}),
				bElements:      *firstBRemovedSlice,
				sequenceNumber: 7,
			},
			wantAnErr: false,
		},
		{
			name: "success, remove the oldest B element in a B queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 36},
			wantQueue: &SliceQueue{
				aElements:      types.SliceQueue[numberedEntry]{},
				bElements:      *firstOfBsRemovedSlice,
				sequenceNumber: 3,
			},
			wantAnErr: false,
		},
		{
			name:      "failure, remove from empty queue",
			inQueue:   &SliceQueue{},
			wantValue: TaggedValue{},
			wantQueue: &SliceQueue{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inQueue.Remove()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to remove value %+v but got %+v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inQueue, testCase.wantQueue) {
				t.Errorf("expected queue to be %+v after Remove but got %+v", testCase.wantQueue, testCase.inQueue)
			}
		})
	}
}

func TestSliceQueueRemoveA(t *testing.T) {
	emptiedSlice := types.NewSliceQueue([]numberedEntry{{
		TaggedValue: TaggedValue{}}})
	_, _ = emptiedSlice.Remove()

	firstARemovedSlice := types.NewSliceQueue(
		[]numberedEntry{
			{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 0},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 3},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 4},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 5},
		})
	_, _ = firstARemovedSlice.Remove()

	firstOfAsRemovedSlice := types.NewSliceQueue(
		[]numberedEntry{
			{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 0},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 1},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 2},
			{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 3},
		})
	_, _ = firstOfAsRemovedSlice.Remove()

	testCases := []struct {
		name      string
		inQueue   *SliceQueue
		wantValue TaggedValue
		wantQueue *SliceQueue
		wantAnErr bool
	}{
		{
			name: "success, remove the only A element in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 40},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 40},
			wantQueue: &SliceQueue{
				aElements:      *emptiedSlice,
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 0,
			},
			wantAnErr: false,
		},
		{
			name: "success, remove the oldest A element in an A queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 33},
			wantQueue: &SliceQueue{
				aElements:      *firstOfAsRemovedSlice,
				bElements:      types.SliceQueue[numberedEntry]{},
				sequenceNumber: 4,
			},
			wantAnErr: false,
		},
		{
			name: "success, remove the oldest A element that's the oldest in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 33},
			wantQueue: &SliceQueue{
				aElements: *firstARemovedSlice,
				bElements: *types.NewSliceQueue(
					[]numberedEntry{
						{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 1},
						{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 2},
						{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 6},
					}),
				sequenceNumber: 7,
			},
			wantAnErr: false,
		},

		{
			name: "success, remove the oldest A element that isn't the oldest in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagA, Value: 33},
				{Tag: TagB, Value: 37},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 33},
			wantQueue: &SliceQueue{
				aElements: *firstARemovedSlice,
				bElements: *types.NewSliceQueue(
					[]numberedEntry{
						{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 0},
						{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 2},
						{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 6},
					}),
				sequenceNumber: 7,
			},
			wantAnErr: false,
		},

		{
			name: "failure, only Bs in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagB, Value: 38},
			}),
			wantAnErr: true,
		},
		{
			name: "failure, only one B in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 41},
			}),
			wantValue: TaggedValue{},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 41},
			}),
			wantAnErr: true,
		},
		{
			name:      "failure, remove from empty queue",
			inQueue:   &SliceQueue{},
			wantValue: TaggedValue{},
			wantQueue: &SliceQueue{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inQueue.RemoveA()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to remove value %+v but got %+v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inQueue, testCase.wantQueue) {
				t.Errorf("expected queue to be %+v after RemoveA but got %+v", testCase.wantQueue, testCase.inQueue)
			}
		})
	}
}

func TestSliceQueueRemoveB(t *testing.T) {
	emptiedSlice := types.NewSliceQueue([]numberedEntry{{
		TaggedValue: TaggedValue{}}})
	_, _ = emptiedSlice.Remove()

	firstBRemovedSlice := types.NewSliceQueue(
		[]numberedEntry{
			{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 0},
			{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 2},
			{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 6},
		})
	_, _ = firstBRemovedSlice.Remove()

	firstOfBsRemovedSlice := types.NewSliceQueue(
		[]numberedEntry{
			{TaggedValue: TaggedValue{Tag: TagB, Value: 36}, sequenceNumber: 0},
			{TaggedValue: TaggedValue{Tag: TagB, Value: 37}, sequenceNumber: 1},
			{TaggedValue: TaggedValue{Tag: TagB, Value: 38}, sequenceNumber: 2},
		})
	_, _ = firstOfBsRemovedSlice.Remove()

	testCases := []struct {
		name      string
		inQueue   *SliceQueue
		wantValue TaggedValue
		wantQueue *SliceQueue
		wantAnErr bool
	}{
		{
			name: "success, remove the only B element in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 41},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 41},
			wantQueue: &SliceQueue{
				aElements:      types.SliceQueue[numberedEntry]{},
				bElements:      *emptiedSlice,
				sequenceNumber: 0,
			},
			wantAnErr: false,
		},
		{
			name: "success, remove the oldest B element in a mixed queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagA, Value: 33},
				{Tag: TagB, Value: 37},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 36},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue(
					[]numberedEntry{
						{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 1},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 3},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 4},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 5},
					}),
				bElements:      *firstBRemovedSlice,
				sequenceNumber: 7,
			},
			wantAnErr: false,
		},
		{
			name: "success, remove the oldest B element in a B queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 36},
			wantQueue: &SliceQueue{
				aElements:      types.SliceQueue[numberedEntry]{},
				bElements:      *firstOfBsRemovedSlice,
				sequenceNumber: 3,
			},
			wantAnErr: false,
		},

		{
			name: "success, remove the oldest B element that isn't the oldest in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 36},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue(
					[]numberedEntry{
						{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 0},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 3},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 4},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 5},
					}),
				bElements:      *firstBRemovedSlice,
				sequenceNumber: 7,
			},
			wantAnErr: false,
		},

		{
			name: "success, remove the oldest B element that is the oldest in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagA, Value: 33},
				{Tag: TagB, Value: 37},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
				{Tag: TagB, Value: 38},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 36},
			wantQueue: &SliceQueue{
				aElements: *types.NewSliceQueue(
					[]numberedEntry{
						{TaggedValue: TaggedValue{Tag: TagA, Value: 33}, sequenceNumber: 1},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 34}, sequenceNumber: 3},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 35}, sequenceNumber: 4},
						{TaggedValue: TaggedValue{Tag: TagA, Value: 39}, sequenceNumber: 5},
					}),
				bElements:      *firstBRemovedSlice,
				sequenceNumber: 7,
			},
			wantAnErr: false,
		},

		{
			name: "failure, only As in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
			}),
			wantValue: TaggedValue{},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
				{Tag: TagA, Value: 39},
			}),
			wantAnErr: true,
		},
		{
			name: "failure, only one A in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 40},
			}),
			wantValue: TaggedValue{},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 40},
			}),
			wantAnErr: true,
		},
		{
			name:      "failure, remove from empty queue",
			inQueue:   &SliceQueue{},
			wantValue: TaggedValue{},
			wantQueue: &SliceQueue{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inQueue.RemoveB()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to remove value %+v but got %+v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inQueue, testCase.wantQueue) {
				t.Errorf("expected queue to be %+v after RemoveB but got %+v", testCase.wantQueue, testCase.inQueue)
			}
		})
	}
}

func TestSliceQueuePeek(t *testing.T) {
	testCases := []struct {
		name      string
		inQueue   *SliceQueue
		wantValue TaggedValue
		wantQueue *SliceQueue
		wantAnErr bool
	}{
		{
			name: "success, peek at the only A element in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 4},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 4},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 4},
			}),
			wantAnErr: false,
		},
		{
			name: "success, peek at the only B element in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 9},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 9},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 9},
			}),
			wantAnErr: false,
		},
		{
			name: "success, peek at the oldest A element in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 4},
				{Tag: TagB, Value: 3},
			}),
			wantValue: TaggedValue{Tag: TagA, Value: 4},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 4},
				{Tag: TagB, Value: 3},
			}),
			wantAnErr: false,
		},
		{
			name: "success, peek at the oldest A element in the queue",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 3},
				{Tag: TagA, Value: 4},
			}),
			wantValue: TaggedValue{Tag: TagB, Value: 3},
			wantQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 3},
				{Tag: TagA, Value: 4},
			}),
			wantAnErr: false,
		},
		{
			name:      "failure, peek at empty queue",
			inQueue:   &SliceQueue{},
			wantValue: TaggedValue{},
			wantQueue: &SliceQueue{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inQueue.Peek()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to peek at value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inQueue, testCase.wantQueue) {
				t.Errorf("expected queue to be %+v after Peek but got %+v", testCase.wantQueue, testCase.inQueue)
			}
		})
	}
}

func TestSliceQueueIsEmpty(t *testing.T) {
	testCases := []struct {
		name      string
		inQueue   *SliceQueue
		wantEmpty bool
	}{
		{
			name:      "zero value SliceQueue is empty",
			inQueue:   &SliceQueue{},
			wantEmpty: true,
		},
		{
			name:      "empty SliceQueue",
			inQueue:   NewSliceQueue([]TaggedValue{}),
			wantEmpty: true,
		},

		{
			name: "all invalid tag elements leave SliceQueue empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: InvalidTag, Value: 31},
				{Tag: InvalidTag, Value: 32}}),
			wantEmpty: true,
		},
		{
			name: "all invalid tag elements leave SliceQueue empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB + 1, Value: 31},
				{Tag: TagB + 2, Value: 32}}),
			wantEmpty: true,
		},

		{
			name: "one nil A Value means SliceQueue not empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: nil},
			}),
			wantEmpty: false,
		},
		{
			name: "one A value means SliceQueue not empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 31},
			}),
			wantEmpty: false,
		},
		{
			name: "several A values means SliceQueue not empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: TagA, Value: 34},
				{Tag: TagA, Value: 35},
			}),
			wantEmpty: false,
		},

		{
			name: "A, B, and invalid values means SliceQueue not empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagA, Value: 33},
				{Tag: InvalidTag, Value: 32},
				{Tag: TagA, Value: 34},
				{Tag: TagB, Value: 36},
				{Tag: TagA, Value: 35},
				{Tag: TagB + 1, Value: 41},
				{Tag: TagB, Value: 37},
				{Tag: TagB, Value: 38},
				{Tag: TagA, Value: 39},
			}),
			wantEmpty: false,
		},

		{
			name: "one nil B Value means SliceQueue not empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: nil},
			}),
			wantEmpty: false,
		},
		{
			name: "one B Value means SliceQueue not empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 42},
			}),
			wantEmpty: false,
		},
		{
			name: "several B values means SliceQueue not empty",
			inQueue: NewSliceQueue([]TaggedValue{
				{Tag: TagB, Value: 36},
				{Tag: TagB, Value: 37},
				{Tag: TagB, Value: 38},
			}),
			wantEmpty: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue := testCase.inQueue.IsEmpty()
			if !reflect.DeepEqual(actualValue, testCase.wantEmpty) {
				t.Errorf("expected queue empty: %v but got %v", testCase.wantEmpty, actualValue)
			}
		})
	}
}
