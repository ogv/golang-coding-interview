package partitioned

import (
	"errors"

	"gitlab.com/ogv/golang-coding-interview/03_stacks_queues/types"
)

// Make sure this implements the types.Queue interface.
var _ types.Queue[TaggedValue] = new(SliceQueue)

// numberedEntry attaches a sequence number to an element stored in a queue,
// so that if X was added to the queue before Y, X's sequence number is smaller than Y's.
type numberedEntry struct {
	TaggedValue
	sequenceNumber uint
}

// TaggedValue stores a value to be stored in a partitioned queue.
type TaggedValue struct {
	Tag   Tag
	Value any
}

// Tag is used to differentiate between different types of values stored in a partitioned queue.
type Tag uint

// Known Tag values
const (
	InvalidTag = iota
	TagA
	TagB
)

// SliceQueue is a Queue implementation whose operations take constant time
// and supports dequeueing each of the 2 different kinds of values it stores, also in constant time.
type SliceQueue struct {
	aElements      types.SliceQueue[numberedEntry]
	bElements      types.SliceQueue[numberedEntry]
	sequenceNumber uint // the number to assign to the next value to be enqueued
}

// NewSliceQueue creates a slice-based queue whose elements store the input values,
// added in the order they are given in.
func NewSliceQueue(taggedValues []TaggedValue) *SliceQueue {
	result := new(SliceQueue)
	for _, taggedValue := range taggedValues {
		result.Add(taggedValue)
	}
	return result
}

// Add from the Queue interface.
func (s *SliceQueue) Add(taggedValue TaggedValue) {
	switch taggedValue.Tag {
	case TagA:
		s.addTo(&s.aElements, taggedValue)
	case TagB:
		s.addTo(&s.bElements, taggedValue)
	default:
		// do nothing
	}
}

// addTo is a utility function for adding to one of the queues of the receiver
func (s *SliceQueue) addTo(elements *types.SliceQueue[numberedEntry], taggedValue TaggedValue) {
	elements.Add(numberedEntry{TaggedValue: taggedValue, sequenceNumber: s.sequenceNumber})
	s.sequenceNumber++
}

// Remove from the Queue interface.
func (s *SliceQueue) Remove() (TaggedValue, error) {
	if s.IsEmpty() {
		return TaggedValue{}, errors.New("cannot remove from empty queue")
	}

	if s.aElements.IsEmpty() {
		return s.RemoveB()
	}
	if s.bElements.IsEmpty() {
		return s.RemoveA()
	}

	indexedEntryA, _ := s.aElements.Peek()
	indexedEntryB, _ := s.bElements.Peek()
	if indexedEntryA.sequenceNumber < indexedEntryB.sequenceNumber {
		return s.RemoveA()
	}
	return s.RemoveB()
}

// RemoveA eliminates and returns the oldest element of type A from the Queue, if any.
func (s *SliceQueue) RemoveA() (TaggedValue, error) {
	return s.removeFrom(&s.aElements)
}

// removeFrom is a utility function for removing from one of the queues of the receiver
func (s *SliceQueue) removeFrom(elements *types.SliceQueue[numberedEntry]) (TaggedValue, error) {
	defer func() {
		if s.IsEmpty() {
			s.sequenceNumber = 0
		}
	}()

	element, err := elements.Remove()
	if err != nil {
		return TaggedValue{}, err
	}
	return element.TaggedValue, nil
}

// RemoveB eliminates and returns the oldest element of type B from the Queue, if any.
func (s *SliceQueue) RemoveB() (TaggedValue, error) {
	return s.removeFrom(&s.bElements)
}

// Peek from the Queue interface.
func (s *SliceQueue) Peek() (TaggedValue, error) {
	if s.aElements.IsEmpty() {
		if s.bElements.IsEmpty() {
			return TaggedValue{}, errors.New("cannot peek at empty queue")
		}

		bElement, _ := s.bElements.Peek()
		return bElement.TaggedValue, nil
	}

	aElement, _ := s.aElements.Peek()
	if s.bElements.IsEmpty() {
		return aElement.TaggedValue, nil
	}

	bElement, _ := s.bElements.Peek()
	if aElement.sequenceNumber < bElement.sequenceNumber {
		return aElement.TaggedValue, nil
	}
	return bElement.TaggedValue, nil
}

// IsEmpty from the Queue interface.
func (s *SliceQueue) IsEmpty() bool {
	return s.aElements.IsEmpty() && s.bElements.IsEmpty()
}
