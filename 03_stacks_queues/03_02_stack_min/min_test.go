package min

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/03_stacks_queues/types"
)

func TestNewSliceStack(t *testing.T) {

	testCases := []struct {
		name      string
		inValues  []int
		wantStack *SliceStack[int]
	}{
		{
			name:     "success, create stack with no values",
			inValues: []int{},
			wantStack: &SliceStack[int]{
				stack: types.SliceStack[int]{},
				mins:  nil,
			},
		},
		{
			name:     "success, create stack with one value",
			inValues: []int{-6},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{-6}),
				mins:  []int{-6},
			},
		},
		{
			name:     "success, create stack with various values",
			inValues: []int{4, 1, 34, -2, -1},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{4, 1, 34, -2, -1}),
				mins:  []int{4, 1, 1, -2, -2},
			},
		},
		{
			name:     "success, create stack with equal values",
			inValues: []int{4, 4, 4, 4, 4},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{4, 4, 4, 4, 4}),
				mins:  []int{4, 4, 4, 4, 4},
			},
		},
		{
			name:     "success, create stack with increasing values",
			inValues: []int{-2, -1, 1, 4, 34},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{-2, -1, 1, 4, 34}),
				mins:  []int{-2, -2, -2, -2, -2},
			},
		},
		{
			name:     "success, create stack with decreasing values",
			inValues: []int{34, 4, 1, -1, -2},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{34, 4, 1, -1, -2}),
				mins:  []int{34, 4, 1, -1, -2},
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualStack := NewSliceStack(testCase.inValues)
			if !reflect.DeepEqual(actualStack, testCase.wantStack) {
				t.Errorf("expected new min stack to be %#v but got %#v", testCase.wantStack, actualStack)
			}
		})
	}
}

func TestPush(t *testing.T) {
	testCases := []struct {
		name      string
		inStack   *SliceStack[int]
		inValues  []int
		wantStack *SliceStack[int]
	}{
		{
			name:     "success, push one value on empty stack",
			inStack:  &SliceStack[int]{},
			inValues: []int{-6},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{-6}),
				mins:  []int{-6},
			},
		},
		{
			name:     "success, push one value",
			inStack:  NewSliceStack([]int{4}),
			inValues: []int{-6},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{4, -6}),
				mins:  []int{4, -6},
			},
		},
		{
			name:     "success, push several values on empty stack",
			inStack:  &SliceStack[int]{},
			inValues: []int{4, 52, -3},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{4, 52, -3}),
				mins:  []int{4, 4, -3},
			},
		},
		{
			name:     "success, push several values on stack",
			inStack:  NewSliceStack([]int{3, -2, 0}),
			inValues: []int{4, -373, 52},
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{3, -2, 0, 4, -373, 52}),
				mins:  []int{3, -2, -2, -2, -373, -373},
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			for _, value := range testCase.inValues {
				testCase.inStack.Push(value)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Push but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestPeek(t *testing.T) {

	testCases := []struct {
		name      string
		inStack   *SliceStack[int]
		wantValue int
		wantStack *SliceStack[int]
		wantAnErr bool
	}{
		{
			name:      "success, peek at the only element in the stack",
			inStack:   NewSliceStack([]int{4}),
			wantValue: 4,
			wantStack: NewSliceStack([]int{4}),
			wantAnErr: false,
		},
		{
			name:      "success, peek at the stack",
			inStack:   NewSliceStack([]int{4, 52, -373}),
			wantValue: -373,
			wantStack: NewSliceStack([]int{4, 52, -373}),
			wantAnErr: false,
		},
		{
			name:      "failure, peek at empty stack",
			inStack:   &SliceStack[int]{},
			wantValue: 0,
			wantStack: &SliceStack[int]{},
			wantAnErr: true,
		},
		{
			name:      "failure, peek at empty built stack",
			inStack:   NewSliceStack([]int{}),
			wantValue: 0,
			wantStack: NewSliceStack([]int{}),
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inStack.Peek()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to peek at value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Peek but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestMin(t *testing.T) {
	emptiedStack := types.NewSliceStack([]int{1})
	_, _ = emptiedStack.Pop()

	testCases := []struct {
		name      string
		inStack   *SliceStack[int]
		wantValue int
		wantStack *SliceStack[int]
		wantAnErr bool
	}{
		{
			name:      "success, min is the only element in the stack",
			inStack:   NewSliceStack([]int{4}),
			wantValue: 4,
			wantStack: NewSliceStack([]int{4}),
			wantAnErr: false,
		},
		{
			name:      "success, min is the oldest element in the stack",
			inStack:   NewSliceStack([]int{4, 52, 37}),
			wantValue: 4,
			wantStack: NewSliceStack([]int{4, 52, 37}),
			wantAnErr: false,
		},
		{
			name:      "success, min is some element in the stack",
			inStack:   NewSliceStack([]int{4, -52, 37}),
			wantValue: -52,
			wantStack: NewSliceStack([]int{4, -52, 37}),
			wantAnErr: false,
		},
		{
			name:      "success, min is the latest element in the stack",
			inStack:   NewSliceStack([]int{4, 52, -373}),
			wantValue: -373,
			wantStack: NewSliceStack([]int{4, 52, -373}),
			wantAnErr: false,
		},
		{
			name:      "failure, min from empty stack",
			inStack:   &SliceStack[int]{},
			wantValue: 0,
			wantStack: &SliceStack[int]{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inStack.Min()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected min value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Min but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestPop(t *testing.T) {
	emptiedStack := types.NewSliceStack([]int{1})
	_, _ = emptiedStack.Pop()

	testCases := []struct {
		name      string
		inStack   *SliceStack[int]
		wantValue int
		wantStack *SliceStack[int]
		wantAnErr bool
	}{
		{
			name:      "success, pop the only element in the stack",
			inStack:   NewSliceStack([]int{4}),
			wantValue: 4,
			wantStack: &SliceStack[int]{
				stack: *emptiedStack,
				mins:  []int{},
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop an element in the stack",
			inStack:   NewSliceStack([]int{4, 52, -373}),
			wantValue: -373,
			wantStack: &SliceStack[int]{
				stack: *types.NewSliceStack([]int{4, 52}),
				mins:  []int{4, 4},
			},
			wantAnErr: false,
		},
		{
			name:      "failure, pop from empty stack",
			inStack:   &SliceStack[int]{},
			wantValue: 0,
			wantStack: &SliceStack[int]{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inStack.Pop()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to pop value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Pop but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestIsEmpty(t *testing.T) {

	testCases := []struct {
		name      string
		inStack   *SliceStack[int]
		wantEmpty bool
	}{
		{
			name:      "zero value IntSliceStack is empty",
			inStack:   &SliceStack[int]{},
			wantEmpty: true,
		},
		{
			name:      "empty stack",
			inStack:   NewSliceStack([]int{}),
			wantEmpty: true,
		},
		{
			name:      "one element stack isn't empty",
			inStack:   NewSliceStack([]int{52}),
			wantEmpty: false,
		},
		{
			name:      "multi element stack isn't empty",
			inStack:   NewSliceStack([]int{4, 52, -373}),
			wantEmpty: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue := testCase.inStack.IsEmpty()
			if !reflect.DeepEqual(actualValue, testCase.wantEmpty) {
				t.Errorf("expected stack empty: %v but got %v", testCase.wantEmpty, actualValue)
			}
		})
	}
}
