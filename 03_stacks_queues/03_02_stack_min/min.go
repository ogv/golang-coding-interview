package min

import (
	"cmp"
	"errors"

	"gitlab.com/ogv/golang-coding-interview/03_stacks_queues/types"
)

// Make sure this implements the types.Stack interface for the type int.
var _ types.Stack[int] = new(SliceStack[int])

// SliceStack is a slice-based stack implementation that can return its minimum value in constant time,
// and also do Push(), Pop(), Peek(), and IsEmpty() in constant time.
type SliceStack[E cmp.Ordered] struct {
	stack types.SliceStack[E]
	mins  []E // minimum value from oldest to oldest, from oldest to next, ..., from oldest to newest
}

// NewSliceStack creates a slice-based stack whose elements store the input values,
// added in the order they are passed in.
func NewSliceStack[S ~[]E, E cmp.Ordered](values S) *SliceStack[E] {
	result := new(SliceStack[E])
	for _, value := range values {
		result.Push(value)
	}
	return result
}

// Min returns the minimum value stored in the stack.
// If the stack is empty, it returns a non-nil error.
func (m *SliceStack[E]) Min() (E, error) {
	if m.stack.IsEmpty() {
		var zeroValue E
		return zeroValue, errors.New("empty stack has no minimum")
	}
	return m.mins[len(m.mins)-1], nil
}

// Push from the types.Stack interface.
func (m *SliceStack[E]) Push(value E) {
	newMin := value
	if len(m.mins) > 0 && m.mins[len(m.mins)-1] < value {
		newMin = m.mins[len(m.mins)-1]
	}
	m.mins = append(m.mins, newMin)
	m.stack.Push(value)
}

// Pop from the types.Stack interface.
func (m *SliceStack[E]) Pop() (E, error) {
	value, err := m.stack.Pop()
	if err != nil {
		var zeroValue E
		return zeroValue, err
	}
	m.mins = m.mins[:len(m.mins)-1]
	return value, nil
}

// Peek from the types.Stack interface.
func (m *SliceStack[E]) Peek() (E, error) {
	value, err := m.stack.Peek()
	if err != nil {
		var zeroValue E
		return zeroValue, err
	}

	return value, err
}

// IsEmpty returns true if and only if the receiver has zero elements.
func (m *SliceStack[E]) IsEmpty() bool {
	return m.stack.IsEmpty()
}
