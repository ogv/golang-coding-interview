package types

import "errors"

// Stack is a last-in, first-out data structure.
type Stack[E any] interface {
	// Push adds a value as the new top of the stack.
	Push(value E)
	// Peek returns the value at the top of the stack (the last added value)
	// It does not remove it from the stack.
	// It returns an error if the stack is empty.
	Peek() (E, error)
	// Pop removes the value at the top of the stack (the last added value) and returns it.
	// It returns an error if the stack is empty.
	Pop() (E, error)
	// IsEmpty returns true if and only if the stack does not contain any value.
	IsEmpty() bool
}

// Make sure this implements the Stack interface.
var _ Stack[int] = new(SliceStack[int])

// SliceStack is a slice-based implementation of a stack. Its zero value is ready for use.
type SliceStack[E any] struct {
	data []E
}

// NewSliceStack creates a slice-based stack whose elements store the input values,
// added in the order they are passed in.
func NewSliceStack[S ~[]E, E any](values S) *SliceStack[E] {
	result := new(SliceStack[E])
	result.pushAll(values)
	return result
}

// pushAll is a utility function for adding multiple elements at once faster.
func (s *SliceStack[E]) pushAll(values []E) {
	s.data = append(s.data, values...)
}

// Push from the Stack interface.
func (s *SliceStack[E]) Push(value E) {
	s.data = append(s.data, value)
}

// Peek from the Stack interface.
func (s *SliceStack[E]) Peek() (E, error) {
	if s.IsEmpty() {
		var zeroValue E
		return zeroValue, errors.New("cannot peek at empty stack")
	}
	return s.data[len(s.data)-1], nil
}

// Pop from the Stack interface.
func (s *SliceStack[E]) Pop() (E, error) {
	var zeroValue E
	if s.IsEmpty() {
		return zeroValue, errors.New("cannot pop from empty stack")
	}
	last := len(s.data) - 1
	result := s.data[last]
	s.data[last] = zeroValue
	s.data = s.data[:last]
	return result, nil
}

// IsEmpty from the Stack interface.
func (s *SliceStack[E]) IsEmpty() bool {
	return len(s.data) == 0
}
