package types

import "errors"

// Queue is a first-in, first-out data structure.
type Queue[E any] interface {
	// Add inserts the value argument at the end of the queue.
	Add(E)
	// Remove returns the value at the beginning of the queue, and removes it from the queue
	// It returns a non-nil error if the queue is empty.
	Remove() (E, error)
	// Peek returns the value at the beginning of the queue, but does not remove it from the queue
	// It returns a non-nil error if the queue is empty.
	Peek() (E, error)
	// IsEmpty returns true if and only if the queue contains no values.
	IsEmpty() bool
}

// Make sure this implements the Queue interface.
var _ Queue[int] = new(SliceQueue[int])

// SliceQueue is a slice-based implementation of the Queue interface.
type SliceQueue[E any] struct {
	data            []E
	addHereIndex    int
	removeHereIndex int
}

// NewSliceQueue creates a slice-based queue whose elements store the input values,
// added in the order they are passed in.
func NewSliceQueue[S ~[]E, E any](values S) *SliceQueue[E] {
	result := new(SliceQueue[E])
	result.addAll(values)
	return result
}

// addAll is a utility function for adding multiple elements at once faster.
func (s *SliceQueue[E]) addAll(values []E) {
	s.data = append(s.data, values...)
	s.addHereIndex += len(values)
}

// Add from the Queue interface.
func (s *SliceQueue[E]) Add(value E) {
	s.data = append(s.data, value)
	s.addHereIndex++
}

// Remove from the Queue interface.
func (s *SliceQueue[E]) Remove() (E, error) {
	var zeroValue E
	if s.IsEmpty() {
		return zeroValue, errors.New("cannot remove from empty queue")
	}
	result := s.data[s.removeHereIndex]
	s.data[s.removeHereIndex] = zeroValue
	s.removeHereIndex++
	if s.IsEmpty() {
		s.addHereIndex, s.removeHereIndex = 0, 0
		s.data = []E{}
	}
	return result, nil
}

// Peek from the Queue interface.
func (s *SliceQueue[E]) Peek() (E, error) {
	if s.IsEmpty() {
		var zeroValue E
		return zeroValue, errors.New("cannot peek at empty queue")
	}
	return s.data[s.removeHereIndex], nil
}

// IsEmpty from the Queue interface.
func (s *SliceQueue[E]) IsEmpty() bool {
	return s.addHereIndex == s.removeHereIndex
}
