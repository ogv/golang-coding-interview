package types

import (
	"reflect"
	"testing"
)

func TestNewSliceQueue(t *testing.T) {

	testCases := []struct {
		name      string
		inValues  []any
		wantQueue Queue[any]
	}{
		{
			name:      "success, create queue with no values",
			inValues:  []any{},
			wantQueue: &SliceQueue[any]{data: nil, addHereIndex: 0, removeHereIndex: 0},
		},
		{
			name:      "success, create queue with one nil value",
			inValues:  []any{nil},
			wantQueue: &SliceQueue[any]{data: []any{nil}, addHereIndex: 1, removeHereIndex: 0},
		},
		{
			name:      "success, create queue with one value",
			inValues:  []any{-6},
			wantQueue: &SliceQueue[any]{data: []any{-6}, addHereIndex: 1, removeHereIndex: 0},
		},
		{
			name:      "success, create queue with non-nil values",
			inValues:  []any{4, 52, -3.73, "the top"},
			wantQueue: &SliceQueue[any]{data: []any{4, 52, -3.73, "the top"}, addHereIndex: 4, removeHereIndex: 0},
		},
		{
			name:      "success, create queue with several values, including nil",
			inValues:  []any{"03", nil, -5 + 3i, 34.2, 4, 52, -3.73, "the top"},
			wantQueue: &SliceQueue[any]{data: []any{"03", nil, -5 + 3i, 34.2, 4, 52, -3.73, "the top"}, addHereIndex: 8, removeHereIndex: 0},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualQueue := NewSliceQueue(testCase.inValues)
			if !reflect.DeepEqual(actualQueue, testCase.wantQueue) {
				t.Errorf("expected new queue to be %#v but got %#v", testCase.wantQueue, actualQueue)
			}
		})
	}
}

func TestSliceQueueAdd(t *testing.T) {

	testCases := []struct {
		name      string
		inQueue   Queue[any]
		inValues  []any
		wantQueue Queue[any]
	}{
		{
			name:     "success, add one value to empty queue",
			inQueue:  &SliceQueue[any]{},
			inValues: []any{-6},
			wantQueue: &SliceQueue[any]{
				data:            []any{-6},
				addHereIndex:    1,
				removeHereIndex: 0,
			},
		},
		{
			name:     "success, add one value",
			inQueue:  NewSliceQueue([]any{4}),
			inValues: []any{-6},
			wantQueue: &SliceQueue[any]{
				data:            []any{4, -6},
				addHereIndex:    2,
				removeHereIndex: 0,
			},
		},
		{
			name:     "success, add several values to empty queue",
			inQueue:  &SliceQueue[any]{},
			inValues: []any{4, 52, -3.73, "the top"},
			wantQueue: &SliceQueue[any]{
				data:            []any{4, 52, -3.73, "the top"},
				addHereIndex:    4,
				removeHereIndex: 0,
			},
		},
		{
			name:     "success, add several values to queue",
			inQueue:  NewSliceQueue([]any{"03", -5 + 3i, 34.2}),
			inValues: []any{4, 52, -3.73, "the top"},
			wantQueue: &SliceQueue[any]{
				data:            []any{"03", -5 + 3i, 34.2, 4, 52, -3.73, "the top"},
				addHereIndex:    7,
				removeHereIndex: 0,
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			for _, value := range testCase.inValues {
				testCase.inQueue.Add(value)
			}
			if !reflect.DeepEqual(testCase.inQueue, testCase.wantQueue) {
				t.Errorf("expected queue to be %+v after Add but got %+v", testCase.wantQueue, testCase.inQueue)
			}
		})
	}
}

func TestSliceQueueRemove(t *testing.T) {
	testCases := []struct {
		name      string
		inQueue   Queue[any]
		wantValue any
		wantQueue Queue[any]
		wantAnErr bool
	}{
		{
			name:      "success, remove the only element in the queue",
			inQueue:   NewSliceQueue([]any{4}),
			wantValue: 4,
			wantQueue: &SliceQueue[any]{
				data:            []any{},
				addHereIndex:    0,
				removeHereIndex: 0,
			},
			wantAnErr: false,
		},
		{
			name:      "success, remove the oldest element in the queue",
			inQueue:   NewSliceQueue([]any{4, 52, -3.73, "the top"}),
			wantValue: 4,
			wantQueue: &SliceQueue[any]{
				data:            []any{nil, 52, -3.73, "the top"},
				addHereIndex:    4,
				removeHereIndex: 1,
			},
			wantAnErr: false,
		},
		{
			name:      "success, remove the oldest nil element in the queue",
			inQueue:   NewSliceQueue([]any{nil, 4, 52, -3.73, "the top"}),
			wantValue: nil,
			wantQueue: &SliceQueue[any]{
				data:            []any{nil, 4, 52, -3.73, "the top"},
				addHereIndex:    5,
				removeHereIndex: 1,
			},
			wantAnErr: false,
		},
		{
			name:      "failure, remove from empty queue",
			inQueue:   &SliceQueue[any]{},
			wantValue: nil,
			wantQueue: &SliceQueue[any]{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inQueue.Remove()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to remove value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inQueue, testCase.wantQueue) {
				t.Errorf("expected queue to be %+v after Remove but got %+v", testCase.wantQueue, testCase.inQueue)
			}
		})
	}
}

func TestSliceQueuePeek(t *testing.T) {
	testCases := []struct {
		name      string
		inQueue   Queue[any]
		wantValue any
		wantQueue Queue[any]
		wantAnErr bool
	}{
		{
			name:      "success, peek at the only element in the queue",
			inQueue:   NewSliceQueue([]any{4}),
			wantValue: 4,
			wantQueue: NewSliceQueue([]any{4}),
			wantAnErr: false,
		},
		{
			name:      "success, peek at the oldest element in the queue",
			inQueue:   NewSliceQueue([]any{4, 52, -3.73, "the top"}),
			wantValue: 4,
			wantQueue: NewSliceQueue([]any{4, 52, -3.73, "the top"}),
			wantAnErr: false,
		},
		{
			name:      "success, peek at the oldest nil element in the queue",
			inQueue:   NewSliceQueue([]any{nil, 4, 52, -3.73, "the top"}),
			wantValue: nil,
			wantQueue: NewSliceQueue([]any{nil, 4, 52, -3.73, "the top"}),
			wantAnErr: false,
		},
		{
			name:      "failure, peek at empty queue",
			inQueue:   &SliceQueue[any]{},
			wantValue: nil,
			wantQueue: &SliceQueue[any]{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inQueue.Peek()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to peek at value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inQueue, testCase.wantQueue) {
				t.Errorf("expected queue to be %+v after Peek but got %+v", testCase.wantQueue, testCase.inQueue)
			}
		})
	}
}

func TestSliceQueueIsEmpty(t *testing.T) {
	testCases := []struct {
		name      string
		inQueue   Queue[any]
		wantEmpty bool
	}{
		{
			name:      "zero value SliceQueue is empty",
			inQueue:   &SliceQueue[any]{},
			wantEmpty: true,
		},
		{
			name:      "empty SliceQueue",
			inQueue:   NewSliceQueue([]any{}),
			wantEmpty: true,
		},
		{
			name:      "one element SliceQueue isn't empty",
			inQueue:   NewSliceQueue([]any{52}),
			wantEmpty: false,
		},
		{
			name:      "multi element SliceQueue isn't empty",
			inQueue:   NewSliceQueue([]any{4, 52, -3.73, "the top"}),
			wantEmpty: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue := testCase.inQueue.IsEmpty()
			if !reflect.DeepEqual(actualValue, testCase.wantEmpty) {
				t.Errorf("expected queue empty: %v but got %v", testCase.wantEmpty, actualValue)
			}
		})
	}
}
