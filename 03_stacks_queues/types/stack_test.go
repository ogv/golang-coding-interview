package types

import (
	"reflect"
	"testing"
)

func TestNewSliceStack(t *testing.T) {

	testCases := []struct {
		name      string
		inValues  []any
		wantStack Stack[any]
	}{
		{
			name:      "success, create stack with no values",
			inValues:  []any{},
			wantStack: &SliceStack[any]{data: nil},
		},
		{
			name:      "success, create stack with one nil value",
			inValues:  []any{nil},
			wantStack: &SliceStack[any]{data: []any{nil}},
		},
		{
			name:      "success, create stack with one value",
			inValues:  []any{-6},
			wantStack: &SliceStack[any]{data: []any{-6}},
		},
		{
			name:      "success, create stack with non-nil values",
			inValues:  []any{4, 52, -3.73, "the top"},
			wantStack: &SliceStack[any]{data: []any{4, 52, -3.73, "the top"}},
		},
		{
			name:      "success, create stack with several values, including nil",
			inValues:  []any{"03", nil, -5 + 3i, 34.2, 4, 52, -3.73, "the top"},
			wantStack: &SliceStack[any]{data: []any{"03", nil, -5 + 3i, 34.2, 4, 52, -3.73, "the top"}},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualStack := NewSliceStack(testCase.inValues)
			if !reflect.DeepEqual(actualStack, testCase.wantStack) {
				t.Errorf("expected new stack to be %#v but got %#v", testCase.wantStack, actualStack)
			}
		})
	}
}

func TestPush(t *testing.T) {
	testCases := []struct {
		name      string
		inStack   Stack[any]
		inValues  []any
		wantStack Stack[any]
	}{
		{
			name:      "success, push one value on empty stack",
			inStack:   &SliceStack[any]{},
			inValues:  []any{-6},
			wantStack: &SliceStack[any]{data: []any{-6}},
		},
		{
			name:      "success, push one value",
			inStack:   NewSliceStack([]any{4}),
			inValues:  []any{-6},
			wantStack: &SliceStack[any]{data: []any{4, -6}},
		},
		{
			name:      "success, push several values on empty stack",
			inStack:   &SliceStack[any]{},
			inValues:  []any{4, 52, -3.73, "the top"},
			wantStack: &SliceStack[any]{data: []any{4, 52, -3.73, "the top"}},
		},
		{
			name:      "success, push several values on stack",
			inStack:   NewSliceStack([]any{"03", -5 + 3i, 34.2}),
			inValues:  []any{4, 52, -3.73, "the top"},
			wantStack: &SliceStack[any]{data: []any{"03", -5 + 3i, 34.2, 4, 52, -3.73, "the top"}},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			for _, value := range testCase.inValues {
				testCase.inStack.Push(value)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Push but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestPeek(t *testing.T) {

	testCases := []struct {
		name      string
		inStack   Stack[any]
		wantValue any
		wantStack Stack[any]
		wantAnErr bool
	}{
		{
			name:      "success, peek at the only element in the stack",
			inStack:   NewSliceStack([]any{4}),
			wantValue: 4,
			wantStack: NewSliceStack([]any{4}),
			wantAnErr: false,
		},
		{
			name:      "success, peek at the stack",
			inStack:   NewSliceStack([]any{4, 52, -3.73, "the top"}),
			wantValue: "the top",
			wantStack: NewSliceStack([]any{4, 52, -3.73, "the top"}),
			wantAnErr: false,
		},
		{
			name:      "failure, peek at empty stack",
			inStack:   &SliceStack[any]{},
			wantValue: nil,
			wantStack: &SliceStack[any]{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inStack.Peek()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to peek at value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Peek but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestPop(t *testing.T) {

	testCases := []struct {
		name      string
		inStack   Stack[any]
		wantValue any
		wantStack Stack[any]
		wantAnErr bool
	}{
		{
			name:      "success, pop the only element in the stack",
			inStack:   NewSliceStack([]any{4}),
			wantValue: 4,
			wantStack: &SliceStack[any]{data: []any{}},
			wantAnErr: false,
		},
		{
			name:      "success, pop an element from the stack",
			inStack:   NewSliceStack([]any{4, 52, -3.73, "the top"}),
			wantValue: "the top",
			wantStack: &SliceStack[any]{data: []any{4, 52, -3.73}},
			wantAnErr: false,
		},
		{
			name:      "failure, pop from empty stack",
			inStack:   &SliceStack[any]{},
			wantValue: nil,
			wantStack: &SliceStack[any]{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inStack.Pop()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to pop value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Pop but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestIsEmpty(t *testing.T) {

	testCases := []struct {
		name      string
		inStack   Stack[any]
		wantEmpty bool
	}{
		{
			name:      "zero value SliceStack is empty",
			inStack:   &SliceStack[any]{},
			wantEmpty: true,
		},
		{
			name:      "empty stack",
			inStack:   NewSliceStack([]any{}),
			wantEmpty: true,
		},
		{
			name:      "one element stack isn't empty",
			inStack:   NewSliceStack([]any{52}),
			wantEmpty: false,
		},
		{
			name:      "multi element stack isn't empty",
			inStack:   NewSliceStack([]any{4, 52, -3.73, "the top"}),
			wantEmpty: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue := testCase.inStack.IsEmpty()
			if !reflect.DeepEqual(actualValue, testCase.wantEmpty) {
				t.Errorf("expected stack empty: %v but got %v", testCase.wantEmpty, actualValue)
			}
		})
	}
}
