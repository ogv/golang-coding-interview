package sort

import (
	min "gitlab.com/ogv/golang-coding-interview/03_stacks_queues/03_02_stack_min"
	"gitlab.com/ogv/golang-coding-interview/03_stacks_queues/types"
)

// SmallestIntOnTop sorts the stack so that its elements
// go from the largest to the smallest (so they are popped in an increasing order)
// It is contractually restricted to only using stacks for auxiliary storage, not arrays.
func SmallestIntOnTop(s *min.SliceStack[int]) {
	/*
		Worst-case complexity: (when the stack is largest on top)
			- space O(length(s))
			- time O(length(s)^2)
	*/
	sortWithStack(s, func(a, b int) bool { return b <= a })
}

// sortWithStack sorts the stack so that each 2 consecutive elements
// (oldest to newest) satisfy the order given by the func
// Note that the arguments of the ordering function are values, not indices.
func sortWithStack(s *min.SliceStack[int], areInOrder func(a, b int) bool) {
	if s == nil {
		return
	}

	aux := types.SliceStack[int]{}
	value := 0
	for !s.IsEmpty() {
		value, _ = s.Pop()
		if aux.IsEmpty() {
			aux.Push(value)
			continue
		}

		auxTop, _ := aux.Peek()
		if areInOrder(value, auxTop) {
			aux.Push(value)
		} else {
			for !aux.IsEmpty() {
				auxTop, _ = aux.Pop()
				if areInOrder(value, auxTop) {
					aux.Push(auxTop)
					break
				} else {
					s.Push(auxTop)
				}
			}
			aux.Push(value)
		}
	}

	for !aux.IsEmpty() {
		auxTop, _ := aux.Pop()
		s.Push(auxTop)
	}
}

// LargestIntOnTop sorts the stack so that its elements
// go from the smallest to the largest (so they are popped in a decreasing order)
// It is contractually restricted to only using stacks for auxiliary storage, not arrays.
func LargestIntOnTop(s *min.SliceStack[int]) {
	/*
		Worst-case complexity: (when the stack is largest on top)
			- space O(length(s))
			- time O(length(s)^2)
	*/
	sortWithStack(s, func(a, b int) bool { return b >= a })
}
