package sort

import (
	"reflect"
	"testing"

	min "gitlab.com/ogv/golang-coding-interview/03_stacks_queues/03_02_stack_min"
)

func TestSmallestIntOnTop(t *testing.T) {
	testCases := []struct {
		name      string
		inStack   *min.SliceStack[int]
		wantStack *min.SliceStack[int]
	}{
		{
			name:      "success, nil input",
			inStack:   nil,
			wantStack: nil,
		},
		{
			name:      "success, empty stack",
			inStack:   min.NewSliceStack([]int{}),
			wantStack: min.NewSliceStack([]int{}),
		},
		{
			name:      "success, one element stack",
			inStack:   min.NewSliceStack([]int{374}),
			wantStack: min.NewSliceStack([]int{374}),
		},
		{
			name:      "success, smallest on top stack",
			inStack:   min.NewSliceStack([]int{74, 74, 63, 4, 4, 0, -2, -11, -25, -25}),
			wantStack: min.NewSliceStack([]int{74, 74, 63, 4, 4, 0, -2, -11, -25, -25}),
		},
		{
			name:      "success, largest on top stack",
			inStack:   min.NewSliceStack([]int{-25, -25, -11, -2, 0, 4, 4, 63, 74, 74}),
			wantStack: min.NewSliceStack([]int{74, 74, 63, 4, 4, 0, -2, -11, -25, -25}),
		},
		{
			name:      "success, all elements equal order stack",
			inStack:   min.NewSliceStack([]int{92, 92, 92, 92, 92, 92, 92, 92, 92, 92}),
			wantStack: min.NewSliceStack([]int{92, 92, 92, 92, 92, 92, 92, 92, 92, 92}),
		},
		{
			name:      "success, random elements order stack",
			inStack:   min.NewSliceStack([]int{-25, 74, 74, 4, -25, -11, -2, 4, 0, 63}),
			wantStack: min.NewSliceStack([]int{74, 74, 63, 4, 4, 0, -2, -11, -25, -25}),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			SmallestIntOnTop(testCase.inStack)
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected sorted stack to be %#v but got %#v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestLargestIntOnTop(t *testing.T) {
	testCases := []struct {
		name      string
		inStack   *min.SliceStack[int]
		wantStack *min.SliceStack[int]
	}{
		{
			name:      "success, nil input",
			inStack:   nil,
			wantStack: nil,
		},
		{
			name:      "success, empty stack",
			inStack:   min.NewSliceStack([]int{}),
			wantStack: min.NewSliceStack([]int{}),
		},
		{
			name:      "success, one element stack",
			inStack:   min.NewSliceStack([]int{374}),
			wantStack: min.NewSliceStack([]int{374}),
		},
		{
			name:      "success, smallest on top stack",
			inStack:   min.NewSliceStack([]int{74, 74, 63, 4, 4, 0, -2, -11, -25, -25}),
			wantStack: min.NewSliceStack([]int{-25, -25, -11, -2, 0, 4, 4, 63, 74, 74}),
		},
		{
			name:      "success, largest on top stack",
			inStack:   min.NewSliceStack([]int{-25, -25, -11, -2, 0, 4, 4, 63, 74, 74}),
			wantStack: min.NewSliceStack([]int{-25, -25, -11, -2, 0, 4, 4, 63, 74, 74}),
		},
		{
			name:      "success, all elements equal order stack",
			inStack:   min.NewSliceStack([]int{92, 92, 92, 92, 92, 92, 92, 92, 92, 92}),
			wantStack: min.NewSliceStack([]int{92, 92, 92, 92, 92, 92, 92, 92, 92, 92}),
		},
		{
			name:      "success, random elements order stack",
			inStack:   min.NewSliceStack([]int{-25, 74, 74, 4, -25, -11, -2, 4, 0, 63}),
			wantStack: min.NewSliceStack([]int{-25, -25, -11, -2, 0, 4, 4, 63, 74, 74}),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			LargestIntOnTop(testCase.inStack)
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected sorted stack to be %#v but got %#v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}
