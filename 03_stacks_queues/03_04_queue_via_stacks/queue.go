package viastacks

import (
	"errors"

	"gitlab.com/ogv/golang-coding-interview/03_stacks_queues/types"
)

// Make sure this implements the types.Queue interface.
var _ types.Queue[any] = &SliceQueue[any]{}

// SliceQueue implements the types.SliceQueue interface using stacks. Its zero value is ready to use.
type SliceQueue[E any] struct {
	addHere    types.SliceStack[E] // elements added to the queue are always put on this stack
	removeHere types.SliceStack[E] // elements removed from the queue are always taken from this stack
}

// NewSliceQueue creates a slice-based queue whose elements store the input values,
// added in the order they are passed in.
func NewSliceQueue[S ~[]E, E any](values S) *SliceQueue[E] {
	result := new(SliceQueue[E])
	for _, value := range values {
		result.Add(value)
	}
	return result
}

// Add from the Queue interface.
func (s *SliceQueue[E]) Add(value E) {
	/*
		Worst-case complexity:
			- time O(1)
			- space O(1)
	*/
	(&s.addHere).Push(value)
}

// Remove from the Queue interface.
func (s *SliceQueue[E]) Remove() (E, error) {
	/*
		Worst-case complexity:
			- time O(length(s)) (when there's nothing in s.removeHere)
			- space O(1)
	*/
	if s.IsEmpty() {
		return *new(E), errors.New("cannot remove from empty queue")
	}

	if s.removeHere.IsEmpty() {
		for !s.addHere.IsEmpty() {
			value, _ := s.addHere.Pop()
			s.removeHere.Push(value)
		}
	}
	return s.removeHere.Pop()
}

// Peek from the Queue interface.
func (s *SliceQueue[E]) Peek() (E, error) {
	/*
		Worst-case complexity:
			- time O(length(s)) (when there's nothing in s.removeHere)
			- space O(1)
	*/
	if s.IsEmpty() {
		return *new(E), errors.New("cannot peek at empty queue")
	}

	if s.removeHere.IsEmpty() {
		for !s.addHere.IsEmpty() {
			value, _ := s.addHere.Pop()
			s.removeHere.Push(value)
		}
	}
	return s.removeHere.Peek()
}

// IsEmpty from the Queue interface.
func (s *SliceQueue[E]) IsEmpty() bool {
	/*
		Worst-case complexity:
			- time O(1)
			- space O(1)
	*/
	return s.removeHere.IsEmpty() && s.addHere.IsEmpty()
}
