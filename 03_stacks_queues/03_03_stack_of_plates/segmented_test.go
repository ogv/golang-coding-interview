package segmented

import (
	"reflect"
	"testing"
)

func TestNewStack(t *testing.T) {
	testCases := []struct {
		name              string
		inSegmentCapacity int
		wantStack         *SliceStack[any]
	}{
		{
			name:              "success, create stack with 0 segment capacity",
			inSegmentCapacity: 0,
			wantStack: &SliceStack[any]{
				segments:           nil,
				capacityPerSegment: 0,
			},
		},
		{
			name:              "success, create stack positive segment capacity",
			inSegmentCapacity: 7,
			wantStack: &SliceStack[any]{
				segments:           nil,
				capacityPerSegment: 7,
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualStack := NewSliceStack[any](testCase.inSegmentCapacity)
			if !reflect.DeepEqual(actualStack, testCase.wantStack) {
				t.Errorf("expected new segmented stack to be %#v but got %#v", testCase.wantStack, actualStack)
			}
		})
	}
}

func TestPush(t *testing.T) {
	testCases := []struct {
		name      string
		inStack   *SliceStack[any]
		inValues  []any
		wantStack *SliceStack[any]
	}{
		{
			name:     "success, push one value on 1 capacity empty stack",
			inStack:  NewSliceStack[any](1),
			inValues: []any{-6},
			wantStack: &SliceStack[any]{
				segments: [][]any{
					{-6},
				},
				capacityPerSegment: 1,
			},
		},
		{
			name:     "success, push values that fit into one segment",
			inStack:  NewSliceStack[any](3),
			inValues: []any{"first", 4, -6},
			wantStack: &SliceStack[any]{
				segments: [][]any{
					{"first", 4, -6},
				},
				capacityPerSegment: 3,
			},
		},
		{
			name:     "success, push values that trigger 2 segments",
			inStack:  NewSliceStack[any](2),
			inValues: []any{"first", 4, -6},
			wantStack: &SliceStack[any]{
				segments: [][]any{
					{"first", 4},
					{-6},
				},
				capacityPerSegment: 2,
			},
		},
		{
			name:     "success, push values that need 3 segments",
			inStack:  NewSliceStack[any](2),
			inValues: []any{"first", 4, -6, .043, -0x34, +0b10110110},
			wantStack: &SliceStack[any]{
				segments: [][]any{
					{"first", 4},
					{-6, .043},
					{-0x34, +0b10110110},
				},
				capacityPerSegment: 2,
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			for _, value := range testCase.inValues {
				testCase.inStack.Push(value)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Push but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestPeek(t *testing.T) {
	oneSegmentOneElement := NewSliceStack[any](2)
	oneSegmentOneElement.Push(4)

	oneSegmentElements := NewSliceStack[any](2)
	oneSegmentElements.Push(-4)
	oneSegmentElements.Push(-3)

	twoSegments := NewSliceStack[any](2)
	twoSegments.Push(5)
	twoSegments.Push(6)
	twoSegments.Push(7)

	testCases := []struct {
		name      string
		inStack   SliceStack[any]
		wantValue any
		wantStack SliceStack[any]
		wantAnErr bool
	}{
		{
			name:      "success, peek at the only element in the stack",
			inStack:   *oneSegmentOneElement,
			wantValue: 4,
			wantStack: *oneSegmentOneElement,
			wantAnErr: false,
		},
		{
			name:      "success, peek at the stack",
			inStack:   *oneSegmentElements,
			wantValue: -3,
			wantStack: *oneSegmentElements,
			wantAnErr: false,
		},
		{
			name:      "success, peek at the segmented stack",
			inStack:   *twoSegments,
			wantValue: 7,
			wantStack: *twoSegments,
			wantAnErr: false,
		},
		{
			name:      "failure, peek at empty stack",
			inStack:   *NewSliceStack[any](4),
			wantValue: nil,
			wantStack: *NewSliceStack[any](4),
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inStack.Peek()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to peek at value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Peek but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestPop(t *testing.T) {
	oneSegmentOneElement := NewSliceStack[any](2)
	oneSegmentOneElement.Push(4)

	oneSegmentElements := NewSliceStack[any](2)
	oneSegmentElements.Push(-4)
	oneSegmentElements.Push(-3)

	twoSegments := NewSliceStack[any](2)
	twoSegments.Push(5)
	twoSegments.Push(6)
	twoSegments.Push(7)

	moreFullSegments := NewSliceStack[any](2)
	moreFullSegments.Push(13)
	moreFullSegments.Push(69)
	moreFullSegments.Push(-69)
	moreFullSegments.Push(-13)
	moreFullSegments.Push(74)
	moreFullSegments.Push(31)

	testCases := []struct {
		name      string
		inStack   SliceStack[any]
		wantValue any
		wantStack SliceStack[any]
		wantAnErr bool
	}{
		{
			name:      "success, pop the only element in the stack",
			inStack:   *oneSegmentOneElement,
			wantValue: 4,
			wantStack: SliceStack[any]{
				// after popping the last element, the segment is removed
				segments:           [][]any{},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop an element in one segment stack",
			inStack:   *oneSegmentElements,
			wantValue: -3,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{-4},
				},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop an element and remove a segment",
			inStack:   *twoSegments,
			wantValue: 7,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{5, 6},
				},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop an element and keep all segments",
			inStack:   *moreFullSegments,
			wantValue: 31,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{13, 69},
					{-69, -13},
					{74},
				},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "failure, pop from empty stack",
			inStack:   SliceStack[any]{},
			wantValue: nil,
			wantStack: SliceStack[any]{},
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inStack.Pop()
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to pop value %v but got %v", testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %+v after Pop but got %+v", testCase.wantStack, testCase.inStack)
			}
		})
	}
}

func TestIsEmpty(t *testing.T) {
	oneSegmentOneElement := NewSliceStack[any](2)
	oneSegmentOneElement.Push(4)

	oneSegmentElements := NewSliceStack[any](2)
	oneSegmentElements.Push(-4)
	oneSegmentElements.Push(-3)

	twoSegments := NewSliceStack[any](2)
	twoSegments.Push(5)
	twoSegments.Push(6)
	twoSegments.Push(7)

	testCases := []struct {
		name      string
		inStack   SliceStack[any]
		wantEmpty bool
	}{
		{
			name:      "zero value SliceStack is empty",
			inStack:   SliceStack[any]{},
			wantEmpty: true,
		},
		{
			name:      "empty stack",
			inStack:   *NewSliceStack[any](3),
			wantEmpty: true,
		},
		{
			name:      "one element stack isn't empty",
			inStack:   *oneSegmentOneElement,
			wantEmpty: false,
		},
		{
			name:      "one element multi segment stack isn't empty",
			inStack:   *oneSegmentElements,
			wantEmpty: false,
		},
		{
			name:      "multi elements multi segment stack isn't empty",
			inStack:   *twoSegments,
			wantEmpty: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue := testCase.inStack.IsEmpty()
			if !reflect.DeepEqual(actualValue, testCase.wantEmpty) {
				t.Errorf("expected stack empty: %v but got %v", testCase.wantEmpty, actualValue)
			}
		})
	}
}

func TestPopAt(t *testing.T) {
	oneSegmentOneElement := NewSliceStack[any](2)
	oneSegmentOneElement.Push(4)

	oneSegmentElements := NewSliceStack[any](2)
	oneSegmentElements.Push(-4)
	oneSegmentElements.Push(-3)

	twoSegments := NewSliceStack[any](2)
	twoSegments.Push(5)
	twoSegments.Push(6)
	twoSegments.Push(7)

	testCases := []struct {
		name      string
		inStack   SliceStack[any]
		inIndex   int
		wantValue any
		wantStack SliceStack[any]
		wantAnErr bool
	}{
		{
			name:      "success, pop the only element in the stack",
			inStack:   *oneSegmentOneElement,
			inIndex:   0,
			wantValue: 4,
			wantStack: SliceStack[any]{
				// after popping the last element, the segment is removed
				segments:           [][]any{},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop an element in one segment stack",
			inStack:   *oneSegmentElements,
			inIndex:   0,
			wantValue: -3,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{-4},
				},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop from the first segment and remove a segment",
			inStack:   *twoSegments,
			inIndex:   0,
			wantValue: 6,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{5, 7},
				},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},

		{
			name:      "success, pop an element from last segment",
			inStack:   *newFullSegmentsStack(),
			inIndex:   2,
			wantValue: 31,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{13, 69},
					{-69, -13},
					{74},
				},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop an element from inner segment",
			inStack:   *newFullSegmentsStack(),
			inIndex:   1,
			wantValue: -13,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{13, 69},
					{-69, 74},
					{31},
				},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop an element from the first segment",
			inStack:   *newFullSegmentsStack(),
			inIndex:   0,
			wantValue: 69,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{13, -69},
					{-13, 74},
					{31},
				},
				capacityPerSegment: 2,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop from last segment, capacity 1",
			inStack:   *newOneElementSegmentsStack(),
			inIndex:   3,
			wantValue: -16,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{23},
					{49},
					{-39},
				},
				capacityPerSegment: 1,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop from next to last segment, capacity 1",
			inStack:   *newOneElementSegmentsStack(),
			inIndex:   2,
			wantValue: -39,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{23},
					{49},
					{-16},
				},
				capacityPerSegment: 1,
			},
			wantAnErr: false,
		},

		{
			name:      "success, pop from inner segment, capacity 1",
			inStack:   *newOneElementSegmentsStack(),
			inIndex:   1,
			wantValue: 49,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{23},
					{-39},
					{-16},
				},
				capacityPerSegment: 1,
			},
			wantAnErr: false,
		},
		{
			name:      "success, pop from the first segment, capacity 1",
			inStack:   *newOneElementSegmentsStack(),
			inIndex:   0,
			wantValue: 23,
			wantStack: SliceStack[any]{
				segments: [][]any{
					{49},
					{-39},
					{-16},
				},
				capacityPerSegment: 1,
			},
			wantAnErr: false,
		},

		{
			name:      "failure, pop from empty stack",
			inStack:   SliceStack[any]{},
			inIndex:   0,
			wantValue: nil,
			wantStack: SliceStack[any]{},
			wantAnErr: true,
		},
		{
			name:      "failure, negative index",
			inStack:   *twoSegments,
			inIndex:   -3,
			wantValue: nil,
			wantStack: *twoSegments,
			wantAnErr: true,
		},
		{
			name:      "failure, large index",
			inStack:   *twoSegments,
			inIndex:   2,
			wantValue: nil,
			wantStack: *twoSegments,
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualValue, actualErr := testCase.inStack.PopAt(testCase.inIndex)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualValue, testCase.wantValue) {
				t.Errorf("expected to pop at %d value %v but got %v", testCase.inIndex, testCase.wantValue, actualValue)
			}
			if !reflect.DeepEqual(testCase.inStack, testCase.wantStack) {
				t.Errorf("expected stack to be %#v after Pop at %d but got %#v", testCase.wantStack, testCase.inIndex, testCase.inStack)
			}
		})
	}
}

func newFullSegmentsStack() *SliceStack[any] {
	result := NewSliceStack[any](2)
	result.Push(13)
	result.Push(69)
	result.Push(-69)
	result.Push(-13)
	result.Push(74)
	result.Push(31)
	return result
}

func newOneElementSegmentsStack() *SliceStack[any] {
	result := NewSliceStack[any](1)
	result.Push(23)
	result.Push(49)
	result.Push(-39)
	result.Push(-16)
	return result
}
