package segmented

import (
	"errors"
	"fmt"

	"gitlab.com/ogv/golang-coding-interview/03_stacks_queues/types"
)

// Make sure this implements the types.Stack interface.
var _ types.Stack[int] = &SliceStack[int]{}

// SliceStack is a stack implementation keeping its elements in one or more stacks,
// each stack having at most a given number of elements.
type SliceStack[E any] struct {
	segments           [][]E
	capacityPerSegment int
}

// NewSliceStack returns a segmented stack, where each segment has at most capacity elements.
func NewSliceStack[E any](capacityPerSegment int) *SliceStack[E] {
	/*
		Worst-case complexity:
			- time O(1)
			- space O(1)
	*/
	return &SliceStack[E]{capacityPerSegment: capacityPerSegment}
}

// Push from the types.Stack interface.
func (s *SliceStack[E]) Push(value E) {
	/*
		Worst-case complexity:
			- time O(capacityPerSegment) when the array backing a slice needs to be copied over. Amortized time : O(1)
			- space O(1)
	*/
	if len(s.segments) == 0 {
		s.segments = append(s.segments, []E{})
	} else if len(s.segments[len(s.segments)-1]) == s.capacityPerSegment {
		s.segments = append(s.segments, []E{})
	}
	s.segments[len(s.segments)-1] = append(s.segments[len(s.segments)-1], value)
}

// Peek from the types.Stack interface.
func (s *SliceStack[E]) Peek() (E, error) {
	/*
		Worst-case complexity:
			- time O(1)
			- space O(1)
	*/
	var zeroValue E
	if s.IsEmpty() {
		return zeroValue, errors.New("cannot peek at empty stack")
	}

	lastSegment := s.segments[len(s.segments)-1]
	return lastSegment[len(lastSegment)-1], nil
}

// Pop from the types.Stack interface.
func (s *SliceStack[E]) Pop() (E, error) {
	/*
		Worst-case complexity:
			- time O(1)
			- space O(1)

		Implementation note:
		For good time performance, space performance is reduced.
		Popping DOES remove the reference to the popped element from the stack,
		but it does NOT reduce the arrays backing segment stacks.
		Each such array still has the maximum capacity it once had, even if its size is much smaller.
		The space can be reduced by reallocating and copying elements
		(each time a segment shrinks, or only when it shrinks "enough", like the segment being half empty).
		Example:
		adding 25 elements in a segment, then popping 20 of them leads to a slice of length 5, but capacity >= 25.
	*/
	var zeroValue E
	if s.IsEmpty() {
		return zeroValue, errors.New("cannot pop from empty stack")
	}

	segment := s.segments[len(s.segments)-1]
	value := segment[len(segment)-1]
	// Here, s.segments[len(s.segments)-1] CAN'T be replaced by "segment",
	// because we need to change the element in s.segments
	s.segments[len(s.segments)-1] = segment[:len(segment)-1]
	if len(s.segments[len(s.segments)-1]) == 0 {
		s.segments[len(s.segments)-1] = nil
		s.segments = s.segments[:len(s.segments)-1]
	}
	return value, nil
}

// IsEmpty from the types.Stack interface.
func (s *SliceStack[E]) IsEmpty() bool {
	/*
		Worst-case complexity:
			- time O(1)
			- space O(1)
	*/
	return len(s.segments) == 0 || len(s.segments[0]) == 0
}

// PopAt returns the element at the top of the receiver's segmentIndex stack
// It returns an error if the segment with that index does not exist, including if segmentIndex is negative.
func (s *SliceStack[E]) PopAt(segmentIndex int) (E, error) {
	/*
		Worst-case complexity:
			- time O(len(s.segments))
			- space O(1)
	*/
	var zeroValue E
	if segmentIndex >= len(s.segments) || segmentIndex < 0 {
		return zeroValue, fmt.Errorf("invalid segment index %d", segmentIndex)
	}

	value := s.segments[segmentIndex][len(s.segments[segmentIndex])-1]
	s.segments[segmentIndex] = s.segments[segmentIndex][:len(s.segments[segmentIndex])-1]
	// Shift an element from the bottom of the next segment
	// to the beginning of the current stack, starting with segmentIndex
	for segmentIndex <= len(s.segments)-2 {
		s.segments[segmentIndex] = append(s.segments[segmentIndex], s.segments[segmentIndex+1][0])
		s.segments[segmentIndex+1] = s.segments[segmentIndex+1][1:]
		segmentIndex++
	}

	if len(s.segments[segmentIndex]) == 0 {
		s.segments = s.segments[:segmentIndex]
	}

	return value, nil
}
