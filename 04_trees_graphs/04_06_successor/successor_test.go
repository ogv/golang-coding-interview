package order

import (
	"testing"

	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestPreviousNode(t *testing.T) {
	/*
			 __3__
			|	  |
		  __2	  4__
		 |		     |
		 1		     5
	*/
	t0RootNode := &tree.BinaryTreeNodeWithParent[int]{Value: 3}
	t0RootNode.SetLeft(2)
	t0RootNode.LeftChild.SetLeft(1)
	t0RootNode.SetRight(4)
	t0RootNode.RightChild.SetRight(5)

	/*
			 _______5_______
			|               |
		    1__           __9
			   |         |
			 __3__	   __7__
		    |     |   |     |
		    2     4   6     8
	*/
	t1RootNode := &tree.BinaryTreeNodeWithParent[int]{Value: 5}
	t1RootNode.SetLeft(1)
	t1RootNode.LeftChild.SetRight(3)
	t1RootNode.LeftChild.RightChild.SetLeft(2)
	t1RootNode.LeftChild.RightChild.SetRight(4)
	t1RootNode.SetRight(9)
	t1RootNode.RightChild.SetLeft(7)
	t1RootNode.RightChild.LeftChild.SetLeft(6)
	t1RootNode.RightChild.LeftChild.SetRight(8)

	/*
			 ___3___
			|	    |
		    1__	  __5
		       | |
		       2 4
	*/
	t2RootNode := &tree.BinaryTreeNodeWithParent[int]{Value: 3}
	t2RootNode.SetLeft(1)
	t2RootNode.LeftChild.SetRight(2)
	t2RootNode.SetRight(5)
	t2RootNode.RightChild.SetLeft(4)

	testCases := []struct {
		name         string
		inNode       *tree.BinaryTreeNodeWithParent[int]
		wantPrevious *tree.BinaryTreeNodeWithParent[int]
	}{
		{
			name:         "nil node has no predecessor",
			inNode:       nil,
			wantPrevious: nil,
		},
		{
			name:         "single node has no predecessor",
			inNode:       &tree.BinaryTreeNodeWithParent[int]{Value: 235},
			wantPrevious: nil,
		},

		{
			name:         "root with left child but no right descendants under it",
			inNode:       t0RootNode,
			wantPrevious: t0RootNode.LeftChild,
		},
		{
			name:         "inner node with left child but no right descendants under it",
			inNode:       t0RootNode.LeftChild,
			wantPrevious: t0RootNode.LeftChild.LeftChild,
		},
		{
			name:         "first node as leftmost leaf has no predecessor",
			inNode:       t0RootNode.LeftChild.LeftChild,
			wantPrevious: nil,
		},

		{
			name:         "root with left child having several right descendants",
			inNode:       t1RootNode,
			wantPrevious: t1RootNode.LeftChild.RightChild.RightChild,
		},
		{
			name:         "inner node with left subtree",
			inNode:       t1RootNode.RightChild,
			wantPrevious: t1RootNode.RightChild.LeftChild.RightChild,
		},
		{
			name:         "node has left child as previous node",
			inNode:       t1RootNode.RightChild.LeftChild,
			wantPrevious: t1RootNode.RightChild.LeftChild.LeftChild,
		},
		{
			name:         "left child has parent as previous node",
			inNode:       t1RootNode.RightChild.LeftChild.RightChild,
			wantPrevious: t1RootNode.RightChild.LeftChild,
		},
		{
			name:         "leaf node with no right subtree is preceded by root",
			inNode:       t1RootNode.RightChild.LeftChild.LeftChild,
			wantPrevious: t1RootNode,
		},

		{
			name:         "node with all other nodes to the left has no predecessor",
			inNode:       t1RootNode.LeftChild,
			wantPrevious: nil,
		},
		{
			name:         "node is preceded by its right child as a leaf",
			inNode:       t1RootNode.LeftChild.RightChild,
			wantPrevious: t1RootNode.LeftChild.RightChild.LeftChild,
		},
		{
			name:         "left child as leaf is preceded by its parent",
			inNode:       t1RootNode.LeftChild.RightChild.RightChild,
			wantPrevious: t1RootNode.LeftChild.RightChild,
		},
		{
			name:         "right child as leaf is preceded by an ancestor",
			inNode:       t1RootNode.LeftChild.RightChild.LeftChild,
			wantPrevious: t1RootNode.LeftChild,
		},

		{
			name:         "root node with right subtree preceded by a left child leaf",
			inNode:       t2RootNode,
			wantPrevious: t2RootNode.LeftChild.RightChild,
		},
		{
			name:         "inner node is preceded by right leaf child",
			inNode:       t2RootNode.RightChild,
			wantPrevious: t2RootNode.RightChild.LeftChild,
		},
		{
			name:         "right leaf child is preceded by root",
			inNode:       t2RootNode.RightChild.LeftChild,
			wantPrevious: t2RootNode,
		},

		{
			name:         "inner node with no right subtree isn't preceded by anything",
			inNode:       t2RootNode.LeftChild,
			wantPrevious: nil,
		},
		{
			name:         "leaf node as left child is preceded by its parent",
			inNode:       t2RootNode.LeftChild.RightChild,
			wantPrevious: t2RootNode.LeftChild,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualPrevious := PreviousNode(testCase.inNode)
			if testCase.wantPrevious != actualPrevious {
				t.Errorf("expected previous node to be: %#v but was: %#v", testCase.wantPrevious, actualPrevious)
			}
		})
	}
}

func TestNextNode(t *testing.T) {
	/*
			 __3__
			|	  |
		  __2	  4__
		 |		     |
		 1		     5
	*/
	t0RootNode := &tree.BinaryTreeNodeWithParent[int]{Value: 3}
	t0RootNode.SetLeft(2)
	t0RootNode.LeftChild.SetLeft(1)
	t0RootNode.SetRight(4)
	t0RootNode.RightChild.SetRight(5)

	/*
			 _______5_______
			|               |
		    1__           __9
			   |         |
			 __3__	   __7__
		    |     |   |     |
		    2     4   6     8
	*/
	t1RootNode := &tree.BinaryTreeNodeWithParent[int]{Value: 5}
	t1RootNode.SetLeft(1)
	t1RootNode.LeftChild.SetRight(3)
	t1RootNode.LeftChild.RightChild.SetLeft(2)
	t1RootNode.LeftChild.RightChild.SetRight(4)
	t1RootNode.SetRight(9)
	t1RootNode.RightChild.SetLeft(7)
	t1RootNode.RightChild.LeftChild.SetLeft(6)
	t1RootNode.RightChild.LeftChild.SetRight(8)

	/*
			 ___3___
			|	    |
		    1__	  __5
		       | |
		       2 4
	*/
	t2RootNode := &tree.BinaryTreeNodeWithParent[int]{Value: 3}
	t2RootNode.SetLeft(1)
	t2RootNode.LeftChild.SetRight(2)
	t2RootNode.SetRight(5)
	t2RootNode.RightChild.SetLeft(4)

	testCases := []struct {
		name     string
		inNode   *tree.BinaryTreeNodeWithParent[int]
		wantNext *tree.BinaryTreeNodeWithParent[int]
	}{
		{
			name:     "nil node has no successor",
			inNode:   nil,
			wantNext: nil,
		},
		{
			name:     "single node has no successor",
			inNode:   &tree.BinaryTreeNodeWithParent[int]{Value: 235},
			wantNext: nil,
		},

		{
			name:     "root with right child but no left descendants under it",
			inNode:   t0RootNode,
			wantNext: t0RootNode.RightChild,
		},
		{
			name:     "inner node with right child but no left descendants under it",
			inNode:   t0RootNode.RightChild,
			wantNext: t0RootNode.RightChild.RightChild,
		},
		{
			name:     "last node as rightmost leaf has no successor",
			inNode:   t0RootNode.RightChild.RightChild,
			wantNext: nil,
		},

		{
			name:     "root with right child having several left descendants",
			inNode:   t1RootNode,
			wantNext: t1RootNode.RightChild.LeftChild.LeftChild,
		},
		{
			name:     "inner node with right subtree",
			inNode:   t1RootNode.LeftChild,
			wantNext: t1RootNode.LeftChild.RightChild.LeftChild,
		},
		{
			name:     "node has right child as next node",
			inNode:   t1RootNode.LeftChild.RightChild,
			wantNext: t1RootNode.LeftChild.RightChild.RightChild,
		},
		{
			name:     "left child has parent as next node",
			inNode:   t1RootNode.LeftChild.RightChild.LeftChild,
			wantNext: t1RootNode.LeftChild.RightChild,
		},
		{
			name:     "leaf node with no right subtree is followed by root",
			inNode:   t1RootNode.LeftChild.RightChild.RightChild,
			wantNext: t1RootNode,
		},

		{
			name:     "node with all other nodes to the left has no successor",
			inNode:   t1RootNode.RightChild,
			wantNext: nil,
		},
		{
			name:     "node is followed by its right child as a leaf",
			inNode:   t1RootNode.RightChild.LeftChild,
			wantNext: t1RootNode.RightChild.LeftChild.RightChild,
		},
		{
			name:     "left child as leaf is followed by its parent",
			inNode:   t1RootNode.RightChild.LeftChild.LeftChild,
			wantNext: t1RootNode.RightChild.LeftChild,
		},
		{
			name:     "right child as leaf is followed by an ancestor",
			inNode:   t1RootNode.RightChild.LeftChild.RightChild,
			wantNext: t1RootNode.RightChild,
		},

		{
			name:     "root node with right subtree followed by a left child leaf",
			inNode:   t2RootNode,
			wantNext: t2RootNode.RightChild.LeftChild,
		},
		{
			name:     "inner node is followed by right leaf child",
			inNode:   t2RootNode.LeftChild,
			wantNext: t2RootNode.LeftChild.RightChild,
		},
		{
			name:     "right leaf child is followed by root",
			inNode:   t2RootNode.LeftChild.RightChild,
			wantNext: t2RootNode,
		},

		{
			name:     "inner node with no right subtree isn't followed by anything",
			inNode:   t2RootNode.RightChild,
			wantNext: nil,
		},
		{
			name:     "leaf node as left child is followed by its parent",
			inNode:   t2RootNode.RightChild.LeftChild,
			wantNext: t2RootNode.RightChild,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualNext := NextNode(testCase.inNode)
			if testCase.wantNext != actualNext {
				t.Errorf("expected next node to be: %#v but was: %#v", testCase.wantNext, actualNext)
			}
		})
	}
}
