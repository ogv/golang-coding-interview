package order

import "gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"

// PreviousNode returns the node preceding the argument in the tree (the node's in-order predecessor), if any.
func PreviousNode[E any](node *tree.BinaryTreeNodeWithParent[E]) *tree.BinaryTreeNodeWithParent[E] {
	/*
		Worst-case complexity:
			- time O(tree node count)
			- space O(1)
		Scenarios:
			- the node is a leaf in a tree that's a single descending line to the left
			- the node is the root of a tree that goes left once, then only right
	*/
	if node == nil {
		return nil
	}

	if node.LeftChild != nil {
		result := node.LeftChild
		for result.RightChild != nil {
			result = result.RightChild
		}
		return result
	}

	child := node
	for child.Parent != nil && child.Parent.LeftChild == child {
		child = child.Parent
	}
	return child.Parent
}

// NextNode returns the node following the argument in the tree (the node's in-order successor), if any.
func NextNode[E any](node *tree.BinaryTreeNodeWithParent[E]) *tree.BinaryTreeNodeWithParent[E] {
	/*
		Worst-case complexity:
			- time O(tree node count)
			- space O(1)
		Scenarios:
			- the node is a leaf in a tree that's a single descending line to the right
			- the node is the root of a tree that goes right once, then only left
	*/
	if node == nil {
		return nil
	}

	if node.RightChild != nil {
		node = node.RightChild
		for node.LeftChild != nil {
			node = node.LeftChild
		}
		return node
	}

	for node.Parent != nil && node.Parent.RightChild == node {
		node = node.Parent
	}
	return node.Parent
}
