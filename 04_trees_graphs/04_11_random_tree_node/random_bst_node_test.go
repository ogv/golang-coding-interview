package random

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
)

type newBinarySearchTreeTestCase[E any] struct {
	name         string
	inComparator orderutils.ValueComparator[E]
	wantResult   *BinarySearchTree[E]
	wantAnErr    bool
}

func TestNewBinarySearchTree(t *testing.T) {
	testCases := []newBinarySearchTreeTestCase[int]{
		{
			name:         "success, comparator passed in",
			inComparator: orderutils.Ascending[int],
			wantResult: &BinarySearchTree[int]{
				rootNode:   nil,
				comparator: orderutils.Ascending[int],
			},
			wantAnErr: false,
		},
		{
			name:         "failure, no comparator passed in",
			inComparator: nil,
			wantResult:   nil,
			wantAnErr:    true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualTree, actualErr := NewBinarySearchTree(testCase.inComparator)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				pt.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if (actualTree == nil) != (testCase.wantResult == nil) {
				pt.Errorf("expected tree to be nil: %t but was nil: %t", testCase.wantResult == nil, actualTree == nil)
			}
			if actualTree != nil && actualTree.comparator == nil {
				pt.Errorf("expected tree to have a comparator, but it doesn't")
			}
		})
	}
}

func TestBinarySearchTree_Size(t *testing.T) {
	testCases := []struct {
		name       string
		inTree     *BinarySearchTree[int]
		wantResult int
	}{
		{
			name:       "nil tree",
			inTree:     nil,
			wantResult: 0,
		},
		{
			name:       "empty tree",
			inTree:     newBinarySearchTreeOfSize(t, 0),
			wantResult: 0,
		},
		{
			name:       "one node tree",
			inTree:     newBinarySearchTreeOfSize(t, 1),
			wantResult: 1,
		},
		{
			name:       "two nodes tree",
			inTree:     newBinarySearchTreeOfSize(t, 2),
			wantResult: 2,
		},
		{
			name:       "many nodes tree",
			inTree:     newBinarySearchTreeOfSize(t, 1_024),
			wantResult: 1_024,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			actualSize := testCase.inTree.Size()
			if actualSize != testCase.wantResult {
				pt.Errorf("expected tree size to be: %d but was: %d", testCase.wantResult, actualSize)
			}
		})
	}
}

func newBinarySearchTreeOfSize(t *testing.T, size int) *BinarySearchTree[int] {
	result, err := NewBinarySearchTree(orderutils.Ascending[int])
	if err != nil {
		t.Fatalf("create a binary search tree: %s", err)
	}

	for nodeIndex := 1; nodeIndex <= size; nodeIndex++ {
		result.Insert(size - nodeIndex + 1)
	}
	return result
}

func TestBinarySearchTree_Insert(t *testing.T) {
	testCases := []struct {
		name       string
		inTree     *BinarySearchTree[int]
		inValue    int
		wantResult *BinarySearchTree[int]
	}{
		{
			name:       "nil tree",
			inTree:     nil,
			inValue:    834,
			wantResult: nil,
		},
		{
			name: "empty tree, increasingly",
			inTree: &BinarySearchTree[int]{
				rootNode:   nil,
				comparator: orderutils.Ascending[int],
			},
			inValue: -83,
			wantResult: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: -83,
				},
				comparator: orderutils.Ascending[int],
			},
		},
		{
			name: "empty tree, decreasingly",
			inTree: &BinarySearchTree[int]{
				rootNode:   nil,
				comparator: orderutils.Descending[int],
			},
			inValue: -83,
			wantResult: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: -83,
				},
				comparator: orderutils.Descending[int],
			},
		},

		{
			name: "one node tree value less than root's",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 52,
			wantResult: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					descendantsCount: 1,
				},
				comparator: orderutils.Ascending[int],
			},
		},
		{
			name: "one node tree value greater than root's",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 72,
			wantResult: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 1,
				},
				comparator: orderutils.Ascending[int],
			},
		},

		{
			name: "full 2 levels tree value greater than right child",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 2,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 89,
			wantResult: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
						RightChild: &BinaryTreeNode[int]{
							Value: 89,
						},
						descendantsCount: 1,
					},
					descendantsCount: 3,
				},
				comparator: orderutils.Ascending[int],
			},
		},
		{
			name: "full 2 levels tree value less than left child",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 2,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 34,
			wantResult: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
						LeftChild: &BinaryTreeNode[int]{
							Value: 34,
						},
						descendantsCount: 1,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 3,
				},
				comparator: orderutils.Ascending[int],
			},
		},

		{
			name: "full 2 levels tree value less than right child",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 2,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 70,
			wantResult: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
						LeftChild: &BinaryTreeNode[int]{
							Value: 70,
						},
						descendantsCount: 1,
					},
					descendantsCount: 3,
				},
				comparator: orderutils.Ascending[int],
			},
		},
		{
			name: "full 2 levels tree value greater than left child",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 2,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 57,
			wantResult: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
						RightChild: &BinaryTreeNode[int]{
							Value: 57,
						},
						descendantsCount: 1,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 3,
				},
				comparator: orderutils.Ascending[int],
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			testCase.inTree.Insert(testCase.inValue)
			if testCase.wantResult == nil {
				if testCase.inTree != nil {
					pt.Errorf("expected tree after insert to be nil, but it isn't")
				}
			} else {
				if !reflect.DeepEqual(testCase.wantResult.rootNode, testCase.inTree.rootNode) {
					pt.Errorf("tree after insert is different from the expected one")
				}
			}
		})
	}
}

func TestBinarySearchTree_Find(t *testing.T) {
	testCases := []struct {
		name       string
		inTree     *BinarySearchTree[int]
		inValue    int
		wantResult *BinaryTreeNode[int]
	}{
		{
			name:       "nil tree",
			inTree:     nil,
			inValue:    834,
			wantResult: nil,
		},
		{
			name: "empty tree",
			inTree: &BinarySearchTree[int]{
				rootNode:   nil,
				comparator: orderutils.Ascending[int],
			},
			inValue:    834,
			wantResult: nil,
		},
		{
			name: "value not found in root",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: -83,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue:    18,
			wantResult: nil,
		},
		{
			name: "value found in root",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: -83,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: -83,
			wantResult: &BinaryTreeNode[int]{
				Value: -83,
			},
		},

		{
			name: "value not found in 2 level tree",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 2,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue:    0,
			wantResult: nil,
		},
		{
			name: "value found on left child in 2 level tree",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 2,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 52,
			wantResult: &BinaryTreeNode[int]{
				Value: 52,
			},
		},
		{
			name: "value found on right child in 2 level tree",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
					},
					descendantsCount: 2,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 72,
			wantResult: &BinaryTreeNode[int]{
				Value: 72,
			},
		},

		{
			name: "value found in an internal node to the left",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
						RightChild: &BinaryTreeNode[int]{
							Value: 57,
						},
						descendantsCount: 1,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
						LeftChild: &BinaryTreeNode[int]{
							Value: 70,
						},
						descendantsCount: 1,
					},
					descendantsCount: 4,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 52,
			wantResult: &BinaryTreeNode[int]{
				Value: 52,
				RightChild: &BinaryTreeNode[int]{
					Value: 57,
				},
				descendantsCount: 1,
			},
		},
		{
			name: "value found in an internal node to the right",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
						RightChild: &BinaryTreeNode[int]{
							Value: 57,
						},
						descendantsCount: 1,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
						LeftChild: &BinaryTreeNode[int]{
							Value: 70,
						},
						descendantsCount: 1,
					},
					descendantsCount: 4,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 72,
			wantResult: &BinaryTreeNode[int]{
				Value: 72,
				LeftChild: &BinaryTreeNode[int]{
					Value: 70,
				},
				descendantsCount: 1,
			},
		},

		{
			name: "value found in a leaf node to the left",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
						RightChild: &BinaryTreeNode[int]{
							Value: 57,
						},
						descendantsCount: 1,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
						LeftChild: &BinaryTreeNode[int]{
							Value: 70,
						},
						descendantsCount: 1,
					},
					descendantsCount: 4,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 57,
			wantResult: &BinaryTreeNode[int]{
				Value: 57,
			},
		},
		{
			name: "value found in a leaf node to the right",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 69,
					LeftChild: &BinaryTreeNode[int]{
						Value: 52,
						RightChild: &BinaryTreeNode[int]{
							Value: 57,
						},
						descendantsCount: 1,
					},
					RightChild: &BinaryTreeNode[int]{
						Value: 72,
						LeftChild: &BinaryTreeNode[int]{
							Value: 70,
						},
						descendantsCount: 1,
					},
					descendantsCount: 4,
				},
				comparator: orderutils.Ascending[int],
			},
			inValue: 70,
			wantResult: &BinaryTreeNode[int]{
				Value: 70,
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			actualNode := testCase.inTree.Find(testCase.inValue)
			if !reflect.DeepEqual(testCase.wantResult, actualNode) {
				pt.Errorf("found node is different from the expected node")
			}
		})
	}
}

func TestBinarySearchTree_RandomNode(t *testing.T) {
	testCases := []struct {
		name       string
		inTree     *BinarySearchTree[int]
		wantResult *BinaryTreeNode[int]
	}{
		{
			name:       "nil tree",
			inTree:     nil,
			wantResult: nil,
		},
		{
			name: "empty tree",
			inTree: &BinarySearchTree[int]{
				rootNode:   nil,
				comparator: orderutils.Ascending[int],
			},
			wantResult: nil,
		},
		{
			name: "one node tree",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: -83,
				},
				comparator: orderutils.Ascending[int],
			},
			wantResult: &BinaryTreeNode[int]{
				Value: -83,
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			actualNode := testCase.inTree.RandomNode()
			if !reflect.DeepEqual(testCase.wantResult, actualNode) {
				pt.Errorf("expected node to be %+v but was %+v", testCase.wantResult, actualNode)
			}
		})
	}
}

func Test_nodeForIndex(t *testing.T) {
	lastNodeChooser := func(startingNode *BinaryTreeNode[int]) int {
		return startingNode.Size() - 1
	}
	middleNodeChooser := func(startingNode *BinaryTreeNode[int]) int {
		return startingNode.Size() / 2
	}

	testCases := []struct {
		name       string
		inTree     *BinarySearchTree[int]
		inChooser  func(node *BinaryTreeNode[int]) int
		wantResult *BinaryTreeNode[int]
	}{
		{
			name: "empty tree",
			inTree: &BinarySearchTree[int]{
				rootNode:   nil,
				comparator: orderutils.Ascending[int],
			},
			inChooser:  nodeForIndexN(0),
			wantResult: nil,
		},
		{
			name: "one node tree, first node",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: -83,
				},
				comparator: orderutils.Ascending[int],
			},
			inChooser: nodeForIndexN(0),
			wantResult: &BinaryTreeNode[int]{
				Value: -83,
			},
		},
		{
			name: "one node tree, last node",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 7,
				},
				comparator: orderutils.Ascending[int],
			},
			inChooser: lastNodeChooser,
			wantResult: &BinaryTreeNode[int]{
				Value: 7,
			},
		},
		{
			name: "one node tree, middle node",
			inTree: &BinarySearchTree[int]{
				rootNode: &BinaryTreeNode[int]{
					Value: 40,
				},
				comparator: orderutils.Ascending[int],
			},
			inChooser: middleNodeChooser,
			wantResult: &BinaryTreeNode[int]{
				Value: 40,
			},
		},

		{
			name:      "multi level tree, node 0",
			inTree:    newAscendingMultiLevelTree(),
			inChooser: nodeForIndexN(0),
			wantResult: &BinaryTreeNode[int]{
				Value: 52,
				RightChild: &BinaryTreeNode[int]{
					Value: 57,
				},
				descendantsCount: 1,
			},
		},
		{
			name:      "multi level tree, node 1",
			inTree:    newAscendingMultiLevelTree(),
			inChooser: nodeForIndexN(1),
			wantResult: &BinaryTreeNode[int]{
				Value: 57,
			},
		},
		{
			name:      "multi level tree, node 2",
			inTree:    newAscendingMultiLevelTree(),
			inChooser: nodeForIndexN(2),
			wantResult: &BinaryTreeNode[int]{
				Value: 69,
				LeftChild: &BinaryTreeNode[int]{
					Value: 52,
					RightChild: &BinaryTreeNode[int]{
						Value: 57,
					},
					descendantsCount: 1,
				},
				RightChild: &BinaryTreeNode[int]{
					Value: 72,
					LeftChild: &BinaryTreeNode[int]{
						Value: 70,
					},
					descendantsCount: 1,
				},
				descendantsCount: 4,
			},
		},
		{
			name:      "multi level tree, node 3",
			inTree:    newAscendingMultiLevelTree(),
			inChooser: nodeForIndexN(3),
			wantResult: &BinaryTreeNode[int]{
				Value: 70,
			},
		},
		{
			name:      "multi level tree, node 4",
			inTree:    newAscendingMultiLevelTree(),
			inChooser: nodeForIndexN(4),
			wantResult: &BinaryTreeNode[int]{
				Value: 72,
				LeftChild: &BinaryTreeNode[int]{
					Value: 70,
				},
				descendantsCount: 1,
			},
		},
		{
			name:      "multi level tree, last node",
			inTree:    newAscendingMultiLevelTree(),
			inChooser: lastNodeChooser,
			wantResult: &BinaryTreeNode[int]{
				Value: 72,
				LeftChild: &BinaryTreeNode[int]{
					Value: 70,
				},
				descendantsCount: 1,
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			actualNode := nodeForIndex(testCase.inTree.rootNode, testCase.inChooser)
			if !reflect.DeepEqual(testCase.wantResult, actualNode) {
				pt.Errorf("expected node to be %+v but was %+v", testCase.wantResult, actualNode)
			}
		})
	}
}

func nodeForIndexN(index int) func(node *BinaryTreeNode[int]) int {
	return func(node *BinaryTreeNode[int]) int {
		return index
	}
}

func newAscendingMultiLevelTree() *BinarySearchTree[int] {
	return &BinarySearchTree[int]{
		rootNode: &BinaryTreeNode[int]{
			Value: 69,
			LeftChild: &BinaryTreeNode[int]{
				Value: 52,
				RightChild: &BinaryTreeNode[int]{
					Value: 57,
				},
				descendantsCount: 1,
			},
			RightChild: &BinaryTreeNode[int]{
				Value: 72,
				LeftChild: &BinaryTreeNode[int]{
					Value: 70,
				},
				descendantsCount: 1,
			},
			descendantsCount: 4,
		},
		comparator: orderutils.Ascending[int],
	}
}

func TestBinaryTreeNode_Size(t *testing.T) {
	testCases := []struct {
		name       string
		inNode     *BinaryTreeNode[int]
		wantResult int
	}{
		{
			name:       "nil node",
			inNode:     nil,
			wantResult: 0,
		},
		{
			name:       "empty node",
			inNode:     newBinarySearchTreeOfSize(t, 0).rootNode,
			wantResult: 0,
		},
		{
			name:       "one node",
			inNode:     newBinarySearchTreeOfSize(t, 1).rootNode,
			wantResult: 1,
		},
		{
			name:       "two nodes",
			inNode:     newBinarySearchTreeOfSize(t, 2).rootNode,
			wantResult: 2,
		},
		{
			name:       "11 nodes",
			inNode:     newBinarySearchTreeOfSize(t, 11).rootNode,
			wantResult: 11,
		},
		{
			name:       "many nodes",
			inNode:     newBinarySearchTreeOfSize(t, 1_024).rootNode,
			wantResult: 1_024,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			actualSize := testCase.inNode.Size()
			if actualSize != testCase.wantResult {
				pt.Errorf("expected node size to be: %d but was: %d", testCase.wantResult, actualSize)
			}
		})
	}
}
