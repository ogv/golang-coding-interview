package random

import (
	"errors"
	"math/rand"

	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
)

// NewBinarySearchTree returns an empty binary search tree that uses the given comparator for the node order
// If the comparator is nil, it returns an error.
func NewBinarySearchTree[E any](comparator orderutils.ValueComparator[E]) (*BinarySearchTree[E], error) {
	if comparator == nil {
		return nil, errors.New("nil comparator function")
	}
	return &BinarySearchTree[E]{comparator: comparator}, nil
}

// BinarySearchTree is similar to tree.BinarySearchTree
// adding support for efficiently returning a uniformly distributed random node from the tree.
type BinarySearchTree[E any] struct {
	rootNode   *BinaryTreeNode[E]
	comparator orderutils.ValueComparator[E]
}

// BinaryTreeNode is the building block of a binary tree that also stores how many descendants it has
// (a value wrapped in a node with optional left and right descendants).
type BinaryTreeNode[E any] struct {
	Value      E
	LeftChild  *BinaryTreeNode[E]
	RightChild *BinaryTreeNode[E]
	// By using this instead of subtreeNodesCount, the zero value of BinaryTreeNode is useful
	descendantsCount int
}

func (t *BinarySearchTree[E]) Size() int {
	if t == nil {
		return 0
	}
	return t.rootNode.Size()
}

// Size returns the number of nodes in the tree rooted at the receiver, including the receiver itself.
func (n *BinaryTreeNode[E]) Size() int {
	if n == nil {
		return 0
	}
	return 1 + n.descendantsCount
}

// Insert adds a node with the given value in the receiver tree,
// keeping the order given by the tree's comparator.
func (t *BinarySearchTree[E]) Insert(value E) {
	if t == nil {
		return
	}
	if t.rootNode == nil {
		t.rootNode = &BinaryTreeNode[E]{
			Value:            value,
			descendantsCount: 0,
		}
	} else {
		t.insertUnder(value, t.rootNode)
	}
}

func (t *BinarySearchTree[E]) insertUnder(value E, startingNode *BinaryTreeNode[E]) {
	for {
		startingNode.descendantsCount++
		if t.comparator(value, startingNode.Value) <= 0 {
			if startingNode.LeftChild == nil {
				startingNode.LeftChild = &BinaryTreeNode[E]{
					Value: value,
				}
				break
			} else {
				startingNode = startingNode.LeftChild
			}
		} else {
			if startingNode.RightChild == nil {
				startingNode.RightChild = &BinaryTreeNode[E]{
					Value: value,
				}
				break
			} else {
				startingNode = startingNode.RightChild
			}
		}
	}
}

// Find returns a node with a value equal to the given one, as defined by the tree's Comparator,
// or nil if no such node exists.
func (t *BinarySearchTree[E]) Find(value E) *BinaryTreeNode[E] {
	if t == nil || t.rootNode == nil {
		return nil
	}
	return t.findFrom(value, t.rootNode)
}

func (t *BinarySearchTree[E]) findFrom(value E, startingNode *BinaryTreeNode[E]) *BinaryTreeNode[E] {
	for {
		comparisonResult := t.comparator(value, startingNode.Value)
		switch {
		case comparisonResult == 0:
			return startingNode
		case comparisonResult < 0:
			if startingNode.LeftChild == nil {
				return nil
			}
			startingNode = startingNode.LeftChild
		default:
			if startingNode.RightChild == nil {
				return nil
			}
			startingNode = startingNode.RightChild
		}
	}
}

// RandomNode returns one of the tree's nodes with equal probability,
// or nil if the tree has no nodes.
func (t *BinarySearchTree[E]) RandomNode() *BinaryTreeNode[E] {
	if t.Size() == 0 {
		return nil
	}
	return nodeForIndex(t.rootNode, randomIndexChooser[E])
}

// randomIndexChooser returns an int between 0 inclusive and the number of nodes in the subtree rooted at startingNode, exclusive.
func randomIndexChooser[E any](startingNode *BinaryTreeNode[E]) int {
	return rand.Intn(startingNode.Size())
}

// nodeForIndex returns a node from the subtree rooted at startingNode,
// with the index in subtree's inorder traversal is the one given by indexChooser.
func nodeForIndex[E any](startingNode *BinaryTreeNode[E], indexChooser func(node *BinaryTreeNode[E]) int) *BinaryTreeNode[E] {
	nodeIndex := indexChooser(startingNode)
	for startingNode != nil {
		leftSize := startingNode.LeftChild.Size()
		if nodeIndex < leftSize {
			startingNode = startingNode.LeftChild
		} else if nodeIndex == leftSize {
			return startingNode
		} else {
			nodeIndex = nodeIndex - (leftSize + 1)
			startingNode = startingNode.RightChild
		}
	}
	return startingNode
}
