package paths

import (
	"testing"

	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestCountWithSum(t *testing.T) {
	for _, testCase := range generateCountWithSumTestCases() {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualCount := CountWithSum(testCase.inRootNode, testCase.inExtractor, testCase.inSum)
			if actualCount != testCase.wantCount {
				pt.Errorf("expected number of paths that sum up to %d to be %d, but got %d",
					testCase.inSum, testCase.wantCount, actualCount)
			}
		})
	}
}

func TestCountWithSumB(t *testing.T) {
	for _, testCase := range generateCountWithSumTestCases() {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualCount := CountWithSumB(testCase.inRootNode, testCase.inExtractor, testCase.inSum)
			if actualCount != testCase.wantCount {
				pt.Errorf("expected number of paths that sum up to %d to be %d, but got %d",
					testCase.inSum, testCase.wantCount, actualCount)
			}
		})
	}
}

func generateCountWithSumTestCases() []countWithSumTestCase[int] {
	return []countWithSumTestCase[int]{
		{
			name:        "nil node, zero sum",
			inRootNode:  nil,
			inExtractor: nil,
			inSum:       0,
			wantCount:   0,
		},
		{
			name:        "empty node, zero sum",
			inRootNode:  &tree.BinaryTreeNode[int]{},
			inExtractor: nil,
			inSum:       0,
			wantCount:   0,
		},
		{
			name:        "empty node, positive sum",
			inRootNode:  &tree.BinaryTreeNode[int]{},
			inExtractor: nil,
			inSum:       14,
			wantCount:   0,
		},
		{
			name:        "empty node, negative sum",
			inRootNode:  &tree.BinaryTreeNode[int]{},
			inExtractor: nil,
			inSum:       -38,
			wantCount:   0,
		},
		{
			name: "one node tree, node value doesn't match",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 14,
			},
			inExtractor: inputAsIntExtractor,
			inSum:       15,
			wantCount:   0,
		},
		{
			name: "one node tree, node value matches",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 14,
			},
			inExtractor: inputAsIntExtractor,
			inSum:       14,
			wantCount:   1,
		},
		{
			name: "small tree, no path matches",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 14,
				LeftChild: &tree.BinaryTreeNode[int]{
					Value: -2,
				},
				RightChild: &tree.BinaryTreeNode[int]{
					Value: -5,
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       26,
			wantCount:   0,
		},
		{
			name: "small tree, left path matches",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 14,
				LeftChild: &tree.BinaryTreeNode[int]{
					Value: -2,
				},
				RightChild: &tree.BinaryTreeNode[int]{
					Value: -5,
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       12,
			wantCount:   1,
		},
		{
			name: "small tree, right path matches",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 14,
				LeftChild: &tree.BinaryTreeNode[int]{
					Value: -2,
				},
				RightChild: &tree.BinaryTreeNode[int]{
					Value: -5,
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       9,
			wantCount:   1,
		},
		{
			name: "small tree, all paths match",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 14,
				LeftChild: &tree.BinaryTreeNode[int]{
					Value: -2,
				},
				RightChild: &tree.BinaryTreeNode[int]{
					Value: -2,
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       12,
			wantCount:   2,
		},
		{
			name: "negative sum, all kinds of nodes and paths match",
			inRootNode: &tree.BinaryTreeNode[int]{
				// #1. Root node -9
				Value: -9,
				LeftChild: &tree.BinaryTreeNode[int]{
					// #2. Root to inner node path with zero: -9+0
					Value: 0,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: -6,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #3. Inner node to inner node paths: -6-3
							// #4. Inner node to inner node path with zero: 0-6-3
							Value: -3,
							LeftChild: &tree.BinaryTreeNode[int]{
								// #5. Inner node: -9
								Value: -9,
								RightChild: &tree.BinaryTreeNode[int]{
									// #6. Leaf node: -9
									Value: -9,
								},
							},
						},
					},
				},
				RightChild: &tree.BinaryTreeNode[int]{
					Value: -5,
					LeftChild: &tree.BinaryTreeNode[int]{
						Value: 2,
					},
					RightChild: &tree.BinaryTreeNode[int]{
						Value: -7,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #7. Inner node to leaf path: -5-7+3
							Value: 3,
						},
						RightChild: &tree.BinaryTreeNode[int]{
							// #8.Root to leaf path: -9-5-7+12
							Value: 12,
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       -9,
			wantCount:   8,
		},
		{
			name: "zero sum, all kinds of nodes and paths match",
			inRootNode: &tree.BinaryTreeNode[int]{
				// #1. Root node: 0
				Value: 0,
				LeftChild: &tree.BinaryTreeNode[int]{
					// #2. Root to inner node path with zeroes only: 0+0
					Value: 0,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: -6,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #3. Inner node to inner node paths: -6+6
							// #4. Inner node to inner node path with zero: 0-6+6
							// #9. Root to inner node path with zeroes: 0+0-6+6
							Value: 6,
							LeftChild: &tree.BinaryTreeNode[int]{
								// #5. Inner node: 0
								// #10. Inner node to inner node path with zero: -6+6+0
								// #11. Root node to inner node path with zeroes: 0+0-6+6+0
								// #12. Inner node to inner node path with zeroes: 0-6+6+0
								// #13. Root node inner node path with zeroes: 0+0-6+6+0
								Value: 0,
								RightChild: &tree.BinaryTreeNode[int]{
									// #6. Leaf node: 0
									// #14. Inner node to inner node path with zeroes only: 0+0
									// #15. Inner node to inner node path with zeroes: -6+6+0+0
									// #16. Inner node to inner node path with zeroes: 0-6+6+0+0
									// #17. Root node to leaf node path with zeroes: 0+0-6+6+0+0
									Value: 0,
								},
							},
						},
					},
				},
				RightChild: &tree.BinaryTreeNode[int]{
					Value: -5,
					LeftChild: &tree.BinaryTreeNode[int]{
						Value: 2,
					},
					RightChild: &tree.BinaryTreeNode[int]{
						Value: -7,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #7. Inner node to leaf path: -5-7+12
							// #8. Root to leaf path with zero: 0-5-7+12
							Value: 12,
						},
						RightChild: &tree.BinaryTreeNode[int]{
							Value: -1,
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       0,
			wantCount:   17,
		},
		{
			name: "positive sum, all kinds of nodes and paths match",
			inRootNode: &tree.BinaryTreeNode[int]{
				// #1. Root node 9
				Value: 9,
				LeftChild: &tree.BinaryTreeNode[int]{
					// #2. Root to inner node path with zero: 9+0
					Value: 0,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: 6,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #3. Inner node to inner node paths: 6+3
							// #4. Inner node to inner node path with zero: 0+6+3
							Value: 3,
							LeftChild: &tree.BinaryTreeNode[int]{
								// #5. Inner node: 9
								Value: 9,
								RightChild: &tree.BinaryTreeNode[int]{
									// #6. Leaf node: 9
									Value: 9,
								},
							},
						},
					},
				},
				RightChild: &tree.BinaryTreeNode[int]{
					Value: 5,
					LeftChild: &tree.BinaryTreeNode[int]{
						Value: -2,
					},
					RightChild: &tree.BinaryTreeNode[int]{
						Value: 7,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #7. Inner node to leaf path: 5+7-3
							Value: -3,
						},
						RightChild: &tree.BinaryTreeNode[int]{
							// #8.Root to leaf path: 9+5+7-12
							Value: -12,
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       9,
			wantCount:   8,
		},

		{
			name: "positive sum, single line, nothing matches",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 9,
				LeftChild: &tree.BinaryTreeNode[int]{
					Value: 0,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: 6,
						LeftChild: &tree.BinaryTreeNode[int]{
							Value: 3,
							LeftChild: &tree.BinaryTreeNode[int]{
								Value: 9,
								RightChild: &tree.BinaryTreeNode[int]{
									Value: 9,
								},
							},
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       -538,
			wantCount:   0,
		},
		{
			name: "zero sum, single line, nothing matches",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 9,
				LeftChild: &tree.BinaryTreeNode[int]{
					Value: 1,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: 6,
						LeftChild: &tree.BinaryTreeNode[int]{
							Value: 3,
							LeftChild: &tree.BinaryTreeNode[int]{
								Value: 9,
								RightChild: &tree.BinaryTreeNode[int]{
									Value: 9,
								},
							},
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       0,
			wantCount:   0,
		},
		{
			name: "positive sum, single line, nothing matches",
			inRootNode: &tree.BinaryTreeNode[int]{
				Value: 9,
				LeftChild: &tree.BinaryTreeNode[int]{
					Value: 0,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: 6,
						LeftChild: &tree.BinaryTreeNode[int]{
							Value: 3,
							LeftChild: &tree.BinaryTreeNode[int]{
								Value: 9,
								RightChild: &tree.BinaryTreeNode[int]{
									Value: 9,
								},
							},
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       538,
			wantCount:   0,
		},

		{
			name: "negative sum, single line, all kinds of nodes and paths match",
			inRootNode: &tree.BinaryTreeNode[int]{
				// #1. Root node -9
				Value: -9,
				LeftChild: &tree.BinaryTreeNode[int]{
					// #2. Root to inner node path with zero: -9+0
					Value: 0,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: -6,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #3. Inner node to inner node paths: -6-3
							// #4. Inner node to inner node path with zero: 0-6-3
							Value: -3,
							LeftChild: &tree.BinaryTreeNode[int]{
								// #5. Inner node: -9
								Value: -9,
								RightChild: &tree.BinaryTreeNode[int]{
									// #6. Leaf node: -9
									Value: -9,
								},
							},
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       -9,
			wantCount:   6,
		},
		{
			name: "zero sum, single line, all kinds of nodes and paths match",
			inRootNode: &tree.BinaryTreeNode[int]{
				// #1. Root node: 0
				Value: 0,
				LeftChild: &tree.BinaryTreeNode[int]{
					// #2. Root to inner node path with zeroes only: 0+0
					Value: 0,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: -6,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #3. Inner node to inner node paths: -6+6
							// #4. Inner node to inner node path with zero: 0-6+6
							// #5. Root to inner node path with zeroes: 0+0-6+6
							Value: 6,
							LeftChild: &tree.BinaryTreeNode[int]{
								// #6. Inner node: 0
								// #7. Inner node to inner node path with zero: -6+6+0
								// #8. Root node to inner node path with zeroes: 0+0-6+6+0
								// #9. Inner node to inner node path with zeroes: 0-6+6+0
								// #10. Root node inner node path with zeroes: 0+0-6+6+0
								Value: 0,
								RightChild: &tree.BinaryTreeNode[int]{
									// #11. Leaf node: 0
									// #12. Inner node to inner node path with zeroes only: 0+0
									// #13. Inner node to inner node path with zeroes: -6+6+0+0
									// #14. Inner node to inner node path with zeroes: 0-6+6+0+0
									// #15. Root node to leaf node path with zeroes: 0+0-6+6+0+0
									Value: 0,
								},
							},
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       0,
			wantCount:   15,
		},
		{
			name: "positive sum, single line, all kinds of nodes and paths match",
			inRootNode: &tree.BinaryTreeNode[int]{
				// #1. Root node 9
				Value: 9,
				LeftChild: &tree.BinaryTreeNode[int]{
					// #2. Root to inner node path with zero: 9+0
					Value: 0,
					RightChild: &tree.BinaryTreeNode[int]{
						Value: 6,
						LeftChild: &tree.BinaryTreeNode[int]{
							// #3. Inner node to inner node paths: 6+3
							// #4. Inner node to inner node path with zero: 0+6+3
							Value: 3,
							LeftChild: &tree.BinaryTreeNode[int]{
								// #5. Inner node: 9
								Value: 9,
								RightChild: &tree.BinaryTreeNode[int]{
									// #6. Leaf node: 9
									Value: 9,
								},
							},
						},
					},
				},
			},
			inExtractor: inputAsIntExtractor,
			inSum:       9,
			wantCount:   6,
		},
	}
}

type countWithSumTestCase[E any] struct {
	name        string
	inRootNode  *tree.BinaryTreeNode[E]
	inExtractor IntKeyExtractor[E]
	inSum       int
	wantCount   int
}

func inputAsIntExtractor(value int) int {
	return value
}
