package paths

import "gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"

// CountWithSum returns the number of paths inside the tree with that root
// where the nodes' keys add up to the given sum
// Each path must only go downwards (from parent nodes to child nodes)
// Each path can start from internal nodes (it does not need to start or end at the root or a leaf).
func CountWithSum[E any](rootNode *tree.BinaryTreeNode[E], keyExtractor IntKeyExtractor[E], targetSum int) int {
	return countWithSum(rootNode, keyExtractor, targetSum, 0, map[int]int{})
}

// IntKeyExtractor returns the int key from a node's value, that gives a binary search tree's ordering.
type IntKeyExtractor[E any] func(value E) int

func countWithSum[E any](node *tree.BinaryTreeNode[E], keyExtractor IntKeyExtractor[E], targetSum int, runningSum int, sumsToCounts map[int]int) int {
	if node == nil {
		return 0
	}
	if keyExtractor == nil {
		return 0
	}

	// Count paths with sum ending at the current node
	runningSum += keyExtractor(node.Value)
	missingSum := runningSum - targetSum
	pathsCount := sumsToCounts[missingSum]
	// One more path ends at the current node.
	if runningSum == targetSum {
		pathsCount++
	}

	adjustCountForSum(sumsToCounts, runningSum, 1)
	pathsCount += countWithSum(node.LeftChild, keyExtractor, targetSum, runningSum, sumsToCounts)
	pathsCount += countWithSum(node.RightChild, keyExtractor, targetSum, runningSum, sumsToCounts)
	adjustCountForSum(sumsToCounts, runningSum, -1)

	return pathsCount
}

func adjustCountForSum(sumsToCounts map[int]int, runningSum int, delta int) {
	newCount := sumsToCounts[runningSum] + delta
	if newCount == 0 {
		delete(sumsToCounts, runningSum)
	} else {
		sumsToCounts[runningSum] = newCount
	}
}

// CountWithSumB is another implementation that returns the number of paths inside the given tree
// where the nodes' keys add up to the given sum
// See the documentation of CountWithSum for specification details.
func CountWithSumB[E any](rootNode *tree.BinaryTreeNode[E], keyExtractor IntKeyExtractor[E], targetSum int) int {
	return countWithSumB(rootNode, keyExtractor, targetSum)
}

func countWithSumB[E any](rootNode *tree.BinaryTreeNode[E], keyExtractor IntKeyExtractor[E], targetSum int) int {
	if rootNode == nil {
		return 0
	}
	if keyExtractor == nil {
		return 0
	}

	result := countWithSumFromNodeB(rootNode, keyExtractor, targetSum)
	result += countWithSumB(rootNode.LeftChild, keyExtractor, targetSum)
	result += countWithSumB(rootNode.RightChild, keyExtractor, targetSum)

	return result
}

func countWithSumFromNodeB[E any](rootNode *tree.BinaryTreeNode[E], keyExtractor IntKeyExtractor[E], targetSum int) int {
	if rootNode == nil {
		return 0
	}

	totalPaths := 0
	targetSum -= keyExtractor(rootNode.Value)
	if targetSum == 0 {
		totalPaths++
	}

	totalPaths += countWithSumFromNodeB(rootNode.LeftChild, keyExtractor, targetSum)
	totalPaths += countWithSumFromNodeB(rootNode.RightChild, keyExtractor, targetSum)
	return totalPaths
}
