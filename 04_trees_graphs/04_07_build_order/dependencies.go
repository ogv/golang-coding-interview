package order

import (
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/directed"
)

// FromDependencies starts from a graph of dependencies and returns a node ordering (their indices)
// such that each node appears before all the nodes it depends on, if possible.
// A relation from node A to node B in the input graph means A depends on B,
// so A must appear after B in the output.
// If no such ordering exists, it returns a zero-length slice.
func FromDependencies[E any](graph directed.Graph[E]) []int {
	/*
		Worst-case complexity:
			- time O(nodes count + dependencies count)
			- space O(nodes count + dependencies count)
		Relative complexity: (compared to FromDependenciesB())
			- time is less than 1/4,
			- space is less than 1/8, allocations are 1/3
	*/
	result := make([]int, 0)

	// Which nodes depend on each node?
	nodesToDependents := make(map[int][]int)
	// How many nodes does each node depend on?
	dependenciesCounts := make([]int, len(graph.Nodes))
	for nodeIndex, node := range graph.Nodes {
		for _, neighbourIndex := range node.Neighbours {
			dependenciesCounts[nodeIndex]++
			nodesToDependents[neighbourIndex] = append(nodesToDependents[neighbourIndex], nodeIndex)
		}
	}

	// Add nodes that don't depend on any other node
	for nodeIndex, dependenciesCount := range dependenciesCounts {
		if dependenciesCount == 0 {
			result = append(result, nodeIndex)
		}
	}

	// Remove existing independent nodes as dependencies, add newly independent nodes, repeat
	for orderedNodeIndex := 0; orderedNodeIndex < len(graph.Nodes); orderedNodeIndex++ {
		if orderedNodeIndex >= len(result) {
			return nil
		}

		for _, dependentNode := range nodesToDependents[result[orderedNodeIndex]] {
			dependenciesCounts[dependentNode]--
			if dependenciesCounts[dependentNode] == 0 {
				result = append(result, dependentNode)
			}
		}
	}

	return result
}

// FromDependenciesB starts from a graph of dependencies and returns a node ordering (their indices)
// such that each node appears before all the nodes it depends on, if possible.
// A relation from node A to node B in the input graph means A depends on B,
// so A must appear after B in the output.
// If no such ordering exists, it returns a zero-length slice.
func FromDependenciesB[E any](graph directed.Graph[E]) []int {
	/*
		Worst-case complexity:
			- time O(nodes count + dependencies count)
			- space O(nodes count + dependencies count)
	*/
	result := make([]int, 0)

	nodesToDependencies := make(map[int]map[int]struct{})
	nodesToDependants := make(map[int]map[int]struct{})
	freeNodes := make([]int, 0)

	for index, node := range graph.Nodes {
		for _, neighbour := range node.Neighbours {
			if nodesToDependencies[index] == nil {
				nodesToDependencies[index] = make(map[int]struct{})
			}
			nodesToDependencies[index][neighbour] = struct{}{}

			if nodesToDependants[neighbour] == nil {
				nodesToDependants[neighbour] = make(map[int]struct{})
			}
			nodesToDependants[neighbour][index] = struct{}{}
		}
		if len(node.Neighbours) == 0 {
			freeNodes = append(freeNodes, index)
		}
	}

	newFreeNodes := make([]int, 0)
	for len(freeNodes) >= 1 {
		for _, freeNode := range freeNodes {
			result = append(result, freeNode)

			for dependentNode := range nodesToDependants[freeNode] {
				delete(nodesToDependencies[dependentNode], freeNode)
				if len(nodesToDependencies[dependentNode]) == 0 {
					delete(nodesToDependencies, dependentNode)
					newFreeNodes = append(newFreeNodes, dependentNode)
				}
			}

			delete(nodesToDependants, freeNode)
		}
		freeNodes = newFreeNodes
		newFreeNodes = nil
	}

	if len(result) < len(graph.Nodes) {
		return nil
	}

	return result
}
