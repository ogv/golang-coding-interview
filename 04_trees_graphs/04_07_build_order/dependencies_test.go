package order

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/directed"
)

type fromDependenciesTestCase[E any] struct {
	name      string
	inGraph   directed.Graph[E]
	wantOrder []int
}

// result is here just to make sure the compiler doesn't remove the function calls that we want to benchmark
var result []int

func BenchmarkFromDependencies_MultiLevel(b *testing.B) {
	inGraph := newMultiLevelConnectedGraph()
	for i := 0; i < b.N; i++ {
		result = FromDependencies(inGraph)
	}
}

func BenchmarkFromDependenciesB_MultiLevel(b *testing.B) {
	inGraph := newMultiLevelConnectedGraph()
	for i := 0; i < b.N; i++ {
		result = FromDependenciesB(inGraph)
	}
}

func BenchmarkFromDependencies_FullCycle(b *testing.B) {
	inGraph := newFullCycleGraph()
	for i := 0; i < b.N; i++ {
		result = FromDependencies(inGraph)
	}
}

func BenchmarkFromDependenciesB_FullCycle(b *testing.B) {
	inGraph := newFullCycleGraph()
	for i := 0; i < b.N; i++ {
		result = FromDependenciesB(inGraph)
	}
}

func BenchmarkFromDependencies_MultiLevelCycle(b *testing.B) {
	inGraph := newMultiLevelConnectedCycleGraph()
	for i := 0; i < b.N; i++ {
		result = FromDependencies(inGraph)
	}
}

func BenchmarkFromDependenciesB_MultiLevelCycle(b *testing.B) {
	inGraph := newMultiLevelConnectedCycleGraph()
	for i := 0; i < b.N; i++ {
		result = FromDependenciesB(inGraph)
	}
}

func TestFromDependencies(t *testing.T) {
	for _, testCase := range fromDependenciesTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			actualOrder := FromDependencies(testCase.inGraph)
			if !reflect.DeepEqual(testCase.wantOrder, actualOrder) {
				t.Fatalf("expected order to be: %+v but was: %+v", testCase.wantOrder, actualOrder)
			}
		})
	}
}

func fromDependenciesTestCases() []fromDependenciesTestCase[string] {
	oneNodeNilDepsGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "singleton", Neighbours: nil},
		},
	}
	oneNodeEmptyDepsGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "singleton", Neighbours: []int{}},
		},
	}
	oneNodeSelfDepGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "singleton", Neighbours: []int{0}},
		},
	}
	twoNodesEmptyDepsGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0a", Neighbours: []int{}},
			{Value: "1b", Neighbours: []int{}},
		},
	}
	secondDependsOnFirstGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0A", Neighbours: []int{}},
			{Value: "1D", Neighbours: []int{0}},
		},
	}
	firstDependsOnSecondGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0B", Neighbours: []int{1}},
			{Value: "1C", Neighbours: []int{}},
		},
	}
	multiLevelConnectedDepsGraph := newMultiLevelConnectedGraph()
	multiLevelCycleConnectedGraph := newMultiLevelConnectedCycleGraph()
	fullCycleGraph := newFullCycleGraph()
	twoNodeCycleConnectedGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0A", Neighbours: []int{1, 6, 2}},
			{Value: "1B", Neighbours: []int{3, 2}},
			{Value: "2C", Neighbours: []int{5, 4}},
			{Value: "3D", Neighbours: []int{1}}, // 1 -> 3 -> 1 cycle
			{Value: "4E", Neighbours: []int{5}},
			{Value: "5F", Neighbours: []int{}},
			{Value: "6G", Neighbours: []int{}},
		},
	}
	twoIslandsGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0A", Neighbours: []int{1, 6, 2}},
			{Value: "1B", Neighbours: []int{3, 2}},
			{Value: "2C", Neighbours: []int{5, 4}},
			{Value: "3D", Neighbours: []int{}},
			{Value: "4E", Neighbours: []int{5}},
			{Value: "5F", Neighbours: []int{}},
			{Value: "6G", Neighbours: []int{}},
			{Value: "7H", Neighbours: []int{}},
			{Value: "8I", Neighbours: []int{7}},
		},
	}
	cycleMultipleIslandsGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0A", Neighbours: []int{1, 6, 2}},
			{Value: "1B", Neighbours: []int{3, 2}},
			{Value: "2C", Neighbours: []int{5, 4}},
			{Value: "3D", Neighbours: []int{}},
			{Value: "4E", Neighbours: []int{5}},
			{Value: "5F", Neighbours: []int{}},
			{Value: "6G", Neighbours: []int{}},
			{Value: "7H", Neighbours: []int{8}},
			{Value: "8I", Neighbours: []int{7}},
		},
	}

	testCases := []fromDependenciesTestCase[string]{
		{
			name:      "empty graph",
			inGraph:   directed.Graph[string]{},
			wantOrder: []int{},
		},
		{
			name:    "one node graph, nil dependencies",
			inGraph: oneNodeNilDepsGraph,
			wantOrder: []int{
				0,
			},
		},
		{
			name:    "one node graph, empty dependencies",
			inGraph: oneNodeEmptyDepsGraph,
			wantOrder: []int{
				0,
			},
		},
		{
			name:      "one self-dependent graph, ordering impossible",
			inGraph:   oneNodeSelfDepGraph,
			wantOrder: nil,
		},
		{
			name:    "two nodes graph, empty dependencies",
			inGraph: twoNodesEmptyDepsGraph,
			wantOrder: []int{
				0,
				1,
			},
		},
		{
			name:    "two nodes graph, second depends on first",
			inGraph: secondDependsOnFirstGraph,
			wantOrder: []int{
				0,
				1,
			},
		},
		{
			name:    "two nodes graph, first depends on second",
			inGraph: firstDependsOnSecondGraph,
			wantOrder: []int{
				1,
				0,
			},
		},
		{
			name:    "multi level dependencies",
			inGraph: multiLevelConnectedDepsGraph,
			wantOrder: []int{
				3,
				5,
				6,
				4,
				2,
				1,
				0,
			},
		},
		{
			name:      "multi level connected graph with cycle, ordering impossible",
			inGraph:   multiLevelCycleConnectedGraph,
			wantOrder: nil,
		},
		{
			name:      "a dependency cycle includes all the nodes, ordering impossible",
			inGraph:   fullCycleGraph,
			wantOrder: nil,
		},
		{
			name:      "connected graph with two node cycle, ordering impossible",
			inGraph:   twoNodeCycleConnectedGraph,
			wantOrder: nil,
		},
		{
			name:    "two islands of dependencies",
			inGraph: twoIslandsGraph,
			wantOrder: []int{
				3,
				5,
				6,
				7,
				4,
				8,
				2,
				1,
				0,
			},
		},
		{
			name:      "multiple islands graph with two node cycle, ordering impossible",
			inGraph:   cycleMultipleIslandsGraph,
			wantOrder: nil,
		},
	}

	return testCases
}

func newMultiLevelConnectedCycleGraph() directed.Graph[string] {
	multiLevelCycleConnectedGraph := directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0A", Neighbours: []int{1, 6, 2}},
			{Value: "1B", Neighbours: []int{3, 2}},
			{Value: "2C", Neighbours: []int{5, 4}},
			{Value: "3D", Neighbours: []int{}},
			{Value: "4E", Neighbours: []int{5}},
			{Value: "5F", Neighbours: []int{0}}, //  0 -> 2 -> 4 -> 5 -> 0 cycle
			{Value: "6G", Neighbours: []int{}},
		},
	}
	return multiLevelCycleConnectedGraph
}

func newMultiLevelConnectedGraph() directed.Graph[string] {
	return directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0A", Neighbours: []int{1, 6, 2}},
			{Value: "1B", Neighbours: []int{3, 2}},
			{Value: "2C", Neighbours: []int{5, 4}},
			{Value: "3D", Neighbours: []int{}},
			{Value: "4E", Neighbours: []int{5}},
			{Value: "5F", Neighbours: []int{}},
			{Value: "6G", Neighbours: []int{}},
		},
	}
}

func newFullCycleGraph() directed.Graph[string] {
	return directed.Graph[string]{
		Nodes: []directed.GraphNode[string]{
			{Value: "0A", Neighbours: []int{1}},
			{Value: "1B", Neighbours: []int{2}},
			{Value: "2C", Neighbours: []int{3}},
			{Value: "3D", Neighbours: []int{4}},
			{Value: "4E", Neighbours: []int{5}},
			{Value: "5F", Neighbours: []int{6}},
			{Value: "6G", Neighbours: []int{0}}, //  0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 0 cycle
		},
	}
}

func TestFromDependenciesB(t *testing.T) {
	for _, testCase := range fromDependenciesTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			actualOrder := FromDependenciesB(testCase.inGraph)
			if !reflect.DeepEqual(testCase.wantOrder, actualOrder) {
				t.Fatalf("expected order to be: %+v but was: %+v", testCase.wantOrder, actualOrder)
			}
		})
	}
}
