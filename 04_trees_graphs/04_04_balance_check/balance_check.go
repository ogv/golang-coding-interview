package check

import (
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

// IsBalanced returns true if and only if the binary tree with that root is balanced,
// which means each node's left and right subtree differ in size by at most 1 node.
func IsBalanced[E any](root *tree.BinaryTreeNode[E]) bool {
	/*
		Worst-case complexity:
			- time O(tree node count)
			- space O(tree node count)
		Scenario: the tree is balanced
	*/
	return subtreeSize(root) >= 0
}

func subtreeSize[E any](root *tree.BinaryTreeNode[E]) int {
	if root == nil {
		return 0
	}

	minSubtreeSize := subtreeSize(root.LeftChild)
	if minSubtreeSize < 0 {
		return -1
	}
	maxSubtreeSize := subtreeSize(root.RightChild)
	if maxSubtreeSize < 0 {
		return -1
	}
	if maxSubtreeSize < minSubtreeSize {
		minSubtreeSize, maxSubtreeSize = maxSubtreeSize, minSubtreeSize
	}
	if maxSubtreeSize-minSubtreeSize >= 2 {
		return -1
	}

	return 1 + minSubtreeSize + maxSubtreeSize
}
