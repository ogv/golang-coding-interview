package check

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestIsBalanced(t *testing.T) {

	testCases := []struct {
		name         string
		inRoot       *tree.BinaryTreeNode[int]
		wantBalanced bool
	}{
		{
			name:         "nil root is balanced",
			inRoot:       nil,
			wantBalanced: true,
		},
		{
			name:         "one node tree is balanced",
			inRoot:       &tree.BinaryTreeNode[int]{Value: 9},
			wantBalanced: true,
		},
		{
			name: "one node with left child is balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 9,
				LeftChild:  &tree.BinaryTreeNode[int]{Value: 7},
				RightChild: nil,
			},
			wantBalanced: true,
		},
		{
			name: "one node with right child is balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 9,
				LeftChild:  nil,
				RightChild: &tree.BinaryTreeNode[int]{Value: 8},
			},
			wantBalanced: true,
		},
		{
			name: "one node with two children is balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 9,
				LeftChild:  &tree.BinaryTreeNode[int]{Value: 7},
				RightChild: &tree.BinaryTreeNode[int]{Value: 8},
			},
			wantBalanced: true,
		},

		{
			name: "one node with two left and one right is balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 9,
				LeftChild: &tree.BinaryTreeNode[int]{Value: 7,
					LeftChild: &tree.BinaryTreeNode[int]{Value: -5},
				},
				RightChild: &tree.BinaryTreeNode[int]{Value: 8},
			},
			wantBalanced: true,
		},
		{
			name: "one node with one left and two right is balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 9,
				LeftChild: &tree.BinaryTreeNode[int]{Value: 8},
				RightChild: &tree.BinaryTreeNode[int]{Value: 7,
					RightChild: &tree.BinaryTreeNode[int]{Value: -5},
				},
			},
			wantBalanced: true,
		},

		{
			name: "one node with two left descendants isn't balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 9,
				LeftChild: &tree.BinaryTreeNode[int]{Value: 7,
					LeftChild:  &tree.BinaryTreeNode[int]{Value: -3},
					RightChild: nil,
				},
				RightChild: nil,
			},
			wantBalanced: false,
		},
		{
			name: "one node with two right descendants isn't balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 9,
				LeftChild: nil,
				RightChild: &tree.BinaryTreeNode[int]{Value: 8,
					LeftChild:  nil,
					RightChild: &tree.BinaryTreeNode[int]{Value: -4},
				},
			},
			wantBalanced: false,
		},

		/*
								  ___________827___________
								 |                         |
							 ____27_____			  ____832____
							|			|            |		     |
						 ___3		  __243__	   _827_	 __853__
						|	         |       |	  |     |	|       |
			          __1            81     729  827   832 818     901
					 |
					-9
		*/
		{
			name: "one node with unbalanced subtree on the left isn't balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 827,
				LeftChild: &tree.BinaryTreeNode[int]{Value: 27,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 3,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 1,
							LeftChild: &tree.BinaryTreeNode[int]{Value: -9},
						},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 243,
						LeftChild:  &tree.BinaryTreeNode[int]{Value: 81},
						RightChild: &tree.BinaryTreeNode[int]{Value: 729},
					},
				},
				RightChild: &tree.BinaryTreeNode[int]{Value: 832,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 827,
						LeftChild:  &tree.BinaryTreeNode[int]{Value: 827},
						RightChild: &tree.BinaryTreeNode[int]{Value: 832},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 853,
						LeftChild:  &tree.BinaryTreeNode[int]{Value: 818},
						RightChild: &tree.BinaryTreeNode[int]{Value: 901},
					},
				},
			},
			wantBalanced: false,
		},
		/*
					  ___________827___________
					 |                         |
				 ____27_____			  ____832____
				|			|            |		     |
			 __3__		  __243__	   _827_	    853__
			|	  |      |       |	  |     |	         |
			1     9      81     729  827   832          901__
			 	   		 	       	   		 	             |
			                      	                       -818
		*/
		{
			name: "one node with unbalanced subtree on the right isn't balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 827,
				LeftChild: &tree.BinaryTreeNode[int]{Value: 27,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 3,
						LeftChild:  &tree.BinaryTreeNode[int]{Value: 1},
						RightChild: &tree.BinaryTreeNode[int]{Value: 9},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 243,
						LeftChild:  &tree.BinaryTreeNode[int]{Value: 81},
						RightChild: &tree.BinaryTreeNode[int]{Value: 729},
					},
				},
				RightChild: &tree.BinaryTreeNode[int]{Value: 832,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 827,
						LeftChild:  &tree.BinaryTreeNode[int]{Value: 827},
						RightChild: &tree.BinaryTreeNode[int]{Value: 832},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 853,
						RightChild: &tree.BinaryTreeNode[int]{Value: 901,
							RightChild: &tree.BinaryTreeNode[int]{Value: -818},
						},
					},
				},
			},
			wantBalanced: false,
		},

		/*
				 ____27_____
				|			|
			 __3__		 __243__
			|	  |		|		|
			1     9     81		729
		*/
		{
			name: "perfect tree is balanced",
			inRoot: &tree.BinaryTreeNode[int]{Value: 27,
				LeftChild: &tree.BinaryTreeNode[int]{Value: 3,
					LeftChild:  &tree.BinaryTreeNode[int]{Value: 1},
					RightChild: &tree.BinaryTreeNode[int]{Value: 9},
				},
				RightChild: &tree.BinaryTreeNode[int]{Value: 243,
					LeftChild: &tree.BinaryTreeNode[int]{
						LeftChild: &tree.BinaryTreeNode[int]{Value: 81},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 729},
				},
			},
			wantBalanced: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualBalanced := IsBalanced(testCase.inRoot)
			if !reflect.DeepEqual(testCase.wantBalanced, actualBalanced) {
				t.Errorf("expected tree to be balanced %t but was %t", testCase.wantBalanced, actualBalanced)
			}
		})
	}
}
