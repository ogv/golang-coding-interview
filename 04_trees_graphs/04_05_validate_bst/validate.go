package validatebst

import (
	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

// IsBinarySearchTree returns true if the binary tree argument is a binary search tree
// That is, each node's key is greater than or equal to all the nodes in its left subtree,
// and less than or equal to all the nodes in its right subtree, according to the tree's comparator.
// Note that it also returns false for an invalid binary tree (one missing the comparator).
func IsBinarySearchTree[E any](binaryTree *tree.BinarySearchTree[E]) bool {
	/*
		Worst-case complexity:
			- time O(tree node count)
			- space O(tree node count)
		Scenario : the tree is a binary search tree,
	*/
	if binaryTree == nil {
		return true
	}
	if binaryTree.Comparator == nil {
		return false
	}

	return isBinarySearchTree(binaryTree.RootNode, binaryTree.Comparator, nil, nil)
}

func isBinarySearchTree[E any](node *tree.BinaryTreeNode[E], comparator orderutils.ValueComparator[E], minNode, maxNode *tree.BinaryTreeNode[E]) bool {
	if node == nil {
		return true
	}

	if (minNode != nil && comparator(node.Value, minNode.Value) < 0) ||
		(maxNode != nil && comparator(node.Value, maxNode.Value) > 0) {
		return false
	}

	if !isBinarySearchTree(node.LeftChild, comparator, minNode, node) {
		return false
	}
	if !isBinarySearchTree(node.RightChild, comparator, node, maxNode) {
		return false
	}

	return true
}
