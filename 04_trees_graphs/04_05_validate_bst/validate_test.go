package validatebst

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestIsBinarySearchTree(t *testing.T) {
	testCases := []struct {
		name    string
		inTree  *tree.BinarySearchTree[int]
		wantBst bool
	}{
		{
			name:    "nil tree is a binary search tree",
			inTree:  nil,
			wantBst: true,
		},
		{
			name:    "tree with no comparator isn't a binary search tree",
			inTree:  &tree.BinarySearchTree[int]{Comparator: nil},
			wantBst: false,
		},
		{
			name: "tree with a single node is a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode:   &tree.BinaryTreeNode[int]{Value: 24},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: true,
		},

		{
			name: "tree with a node and equal left child is a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 24,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 24},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: true,
		},
		{
			name: "tree with a node and smaller left child is a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 24,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 23},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: true,
		},
		{
			name: "tree with a node and greater left child isn't a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 24,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 25},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: false,
		},

		{
			name: "tree with a node and equal right child is a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 24,
					RightChild: &tree.BinaryTreeNode[int]{Value: 24},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: true,
		},
		{
			name: "tree with a node and greater right child is a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 24,
					RightChild: &tree.BinaryTreeNode[int]{Value: 25},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: true,
		},
		{
			name: "tree with a node and smaller right child isn't a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 24,
					RightChild: &tree.BinaryTreeNode[int]{Value: 23},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: false,
		},

		/*
				 __51__
				|	   |
			 __48	   210__
			|		        |
			32		       372
		*/
		{
			name: "valid tree with only left and right descendants is a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 51,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 48,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 32},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 210,
						RightChild: &tree.BinaryTreeNode[int]{Value: 372},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: true,
		},
		/*
				 __51__
				|	   |
			 __48	   210__
			|		        |
			50		       372
		*/
		{
			name: "left child with invalid left child isn't a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 51,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 48,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 50},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 210,
						RightChild: &tree.BinaryTreeNode[int]{Value: 372},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: false,
		},
		/*
				 __51__
				|	   |
			 __48	   210__
			|		        |
			32		       153
		*/
		{
			name: "right child with invalid right child isn't a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 51,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 48,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 32},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 210,
						RightChild: &tree.BinaryTreeNode[int]{Value: 153},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: false,
		},

		/*
				 ______210______
				|               |
			   50__           __1430
				   |         |
				__54__	   __210__
			   |      |   |       |
			   51     211 210     344
		*/
		{
			name: "leaf in left subtree greater than root so it isn't a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 210,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 50,
						RightChild: &tree.BinaryTreeNode[int]{Value: 54,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 51},
							RightChild: &tree.BinaryTreeNode[int]{Value: 211},
						},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 1430,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 210,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 210},
							RightChild: &tree.BinaryTreeNode[int]{Value: 344},
						},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: false,
		},
		/*
				 ______210______
				|               |
			   50__           __1430
				   |         |
				__54__	   __450__
			   |      |   |       |
			   51     62  209     386
		*/
		{
			name: "leaf in right subtree smaller than root so it isn't a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 210,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 50,
						RightChild: &tree.BinaryTreeNode[int]{Value: 54,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 51},
							RightChild: &tree.BinaryTreeNode[int]{Value: 62},
						},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 1430,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 450,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 209},
							RightChild: &tree.BinaryTreeNode[int]{Value: 386},
						},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: false,
		},

		/*
				 ______210______
				|               |
			   50__           __1430
				   |         |
				__54__	   __450__
			   |      |   |       |
			   51     62  211     1430
		*/
		{
			name: "valid partial binary tree is a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 210,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 50,
						RightChild: &tree.BinaryTreeNode[int]{Value: 54,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 51},
							RightChild: &tree.BinaryTreeNode[int]{Value: 62},
						},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 1430,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 450,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 211},
							RightChild: &tree.BinaryTreeNode[int]{Value: 1430},
						},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: true,
		},

		/*
					  ___________827___________
					 |                         |
				 ____27_____			  ____832____
				|			|            |		     |
			 __3__		 __243__	   _827_	 __853__
			|	  |		|		|     |	    |	|		|
			1     9     81     729   827   832 849     901
		*/
		{
			name: "valid perfect binary tree is a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 827,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 27,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 3,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 1},
							RightChild: &tree.BinaryTreeNode[int]{Value: 9},
						},
						RightChild: &tree.BinaryTreeNode[int]{Value: 243,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 81},
							RightChild: &tree.BinaryTreeNode[int]{Value: 729},
						},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 832,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 827,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 827},
							RightChild: &tree.BinaryTreeNode[int]{Value: 832},
						},
						RightChild: &tree.BinaryTreeNode[int]{Value: 853,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 849},
							RightChild: &tree.BinaryTreeNode[int]{Value: 901},
						},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: true,
		},

		/*
					  ___________827___________
					 |                         |
				 ____27_____			  ____832____
				|			|            |		     |
			 __3__		 __243__	   _827_	 __853__
			|	  |		|		|     |	    |	|		|
			1     9     6      729   827   832 849     901
		*/
		{
			name: "perfect binary tree with invalid left subtree isn't a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 827,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 27,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 3,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 1},
							RightChild: &tree.BinaryTreeNode[int]{Value: 9},
						},
						RightChild: &tree.BinaryTreeNode[int]{Value: 243,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 6},
							RightChild: &tree.BinaryTreeNode[int]{Value: 729},
						},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 832,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 827,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 827},
							RightChild: &tree.BinaryTreeNode[int]{Value: 832},
						},
						RightChild: &tree.BinaryTreeNode[int]{Value: 853,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 849},
							RightChild: &tree.BinaryTreeNode[int]{Value: 901},
						},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: false,
		},

		/*
					  ___________827___________
					 |                         |
				 ____27_____			  ____832____
				|			|            |		     |
			 __3__		 __243__	   _827_	 __853__
			|	  |		|		|     |	    |	|		|
			1     9     81     729   827   832 818     901
		*/
		{
			name: "perfect binary tree with invalid right subtree isn't a binary search tree",
			inTree: &tree.BinarySearchTree[int]{
				RootNode: &tree.BinaryTreeNode[int]{Value: 827,
					LeftChild: &tree.BinaryTreeNode[int]{Value: 27,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 3,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 1},
							RightChild: &tree.BinaryTreeNode[int]{Value: 9},
						},
						RightChild: &tree.BinaryTreeNode[int]{Value: 243,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 81},
							RightChild: &tree.BinaryTreeNode[int]{Value: 729},
						},
					},
					RightChild: &tree.BinaryTreeNode[int]{Value: 832,
						LeftChild: &tree.BinaryTreeNode[int]{Value: 827,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 827},
							RightChild: &tree.BinaryTreeNode[int]{Value: 832},
						},
						RightChild: &tree.BinaryTreeNode[int]{Value: 853,
							LeftChild:  &tree.BinaryTreeNode[int]{Value: 818},
							RightChild: &tree.BinaryTreeNode[int]{Value: 901},
						},
					},
				},
				Comparator: orderutils.Ascending[int],
			},
			wantBst: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualIsBst := IsBinarySearchTree(testCase.inTree)
			if !reflect.DeepEqual(testCase.wantBst, actualIsBst) {
				t.Errorf("expected a binary search tree: %t but was: %t", testCase.wantBst, actualIsBst)
			}
		})
	}
}
