package depths

import (
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

// BinaryTreeNodesByLevel returns pointers to nodes in the tree with that root,
// grouped by level, each slice holding pointers to all the nodes from the same level.
// The root's level is 0 (so it's in the first slice),
// its children are at level 1 (so they're in the second slice),
// its children's children are at level 2 (in the third slice), and so on.
func BinaryTreeNodesByLevel[E any](root *tree.BinaryTreeNode[E]) [][]*tree.BinaryTreeNode[E] {
	/*
		Worst-case complexity:
			- time O(tree node count)
			- space O(tree node count)
		Scenario: the node is the root of a tree that's a single descending line
	*/
	result := make([][]*tree.BinaryTreeNode[E], 0)
	if root == nil {
		return result
	}

	levelList := []*tree.BinaryTreeNode[E]{root}
	for len(levelList) >= 1 {
		result = append(result, levelList)
		nextLevelList := make([]*tree.BinaryTreeNode[E], 0)
		for _, element := range levelList {
			if element.LeftChild != nil {
				nextLevelList = append(nextLevelList, element.LeftChild)
			}
			if element.RightChild != nil {
				nextLevelList = append(nextLevelList, element.RightChild)
			}
		}
		levelList = nextLevelList
	}
	return result
}
