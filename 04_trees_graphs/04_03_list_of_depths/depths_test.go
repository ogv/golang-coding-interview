package depths

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestBinaryTreeNodesByLevel(t *testing.T) {

	oneElementList := []*tree.BinaryTreeNode[int]{
		{Value: 12},
	}
	oneLevelList := [][]*tree.BinaryTreeNode[int]{
		oneElementList,
	}

	/*
			 __51__
			|	   |
		 __50	   372__
		|		        |
		32		       210
	*/
	balancedLevel2Right0 := tree.BinaryTreeNode[int]{Value: 210}
	balancedLevel2Left0 := tree.BinaryTreeNode[int]{Value: 32}
	balancedLevel1Left0 := tree.BinaryTreeNode[int]{Value: 50, LeftChild: &balancedLevel2Left0}
	balancedLevel1Right0 := tree.BinaryTreeNode[int]{Value: 372, RightChild: &balancedLevel2Right0}
	balancedRootNode := tree.BinaryTreeNode[int]{Value: 51, LeftChild: &balancedLevel1Left0, RightChild: &balancedLevel1Right0}

	balancedThreeLevelList := [][]*tree.BinaryTreeNode[int]{
		{&balancedRootNode},
		{&balancedLevel1Left0, &balancedLevel1Right0},
		{&balancedLevel2Left0, &balancedLevel2Right0},
	}

	leftOnlyTwoLevelList := [][]*tree.BinaryTreeNode[int]{
		{&balancedLevel1Left0},
		{&balancedLevel2Left0},
	}

	rightOnlyTwoLevelList := [][]*tree.BinaryTreeNode[int]{
		{&balancedLevel1Right0},
		{&balancedLevel2Right0},
	}

	/*
			 ____27_____
			|			|
		 __3__		 __243__
		|	  |		|		|
		1     9     81		729
	*/
	fullLevel2Left0 := tree.BinaryTreeNode[int]{Value: 1}
	fullLevel2Right0 := tree.BinaryTreeNode[int]{Value: 9}
	fullLevel1Left0 := tree.BinaryTreeNode[int]{Value: 3, LeftChild: &fullLevel2Left0, RightChild: &fullLevel2Right0}
	fullLevel2Left1 := tree.BinaryTreeNode[int]{Value: 81}
	fullLevel2Right1 := tree.BinaryTreeNode[int]{Value: 729}
	fullLevel1Right0 := tree.BinaryTreeNode[int]{Value: 243, LeftChild: &fullLevel2Left1, RightChild: &fullLevel2Right1}
	fullRootNode := tree.BinaryTreeNode[int]{Value: 27, LeftChild: &fullLevel1Left0, RightChild: &fullLevel1Right0}

	fullThreeLevelList := [][]*tree.BinaryTreeNode[int]{
		{&fullRootNode},
		{&fullLevel1Left0, &fullLevel1Right0},
		{&fullLevel2Left0, &fullLevel2Right0, &fullLevel2Left1, &fullLevel2Right1},
	}

	testCases := []struct {
		name       string
		inRoot     *tree.BinaryTreeNode[int]
		wantResult [][]*tree.BinaryTreeNode[int]
	}{
		{
			name:       "success, nil root",
			inRoot:     nil,
			wantResult: [][]*tree.BinaryTreeNode[int]{},
		},
		{
			name:       "success, root only",
			inRoot:     &tree.BinaryTreeNode[int]{Value: 12},
			wantResult: oneLevelList,
		},
		{
			name:       "success, balanced tree",
			inRoot:     &balancedRootNode,
			wantResult: balancedThreeLevelList,
		},
		{
			name:       "success, left children only",
			inRoot:     &balancedLevel1Left0,
			wantResult: leftOnlyTwoLevelList,
		},
		{
			name:       "success, right children only",
			inRoot:     &balancedLevel1Right0,
			wantResult: rightOnlyTwoLevelList,
		},
		{
			name:       "success, full binary tree",
			inRoot:     &fullRootNode,
			wantResult: fullThreeLevelList,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualList := BinaryTreeNodesByLevel(testCase.inRoot)
			if !reflect.DeepEqual(actualList, testCase.wantResult) {
				t.Errorf("expected list to be %#v but was %#v", testCase.wantResult, actualList)
			}
		})
	}
}
