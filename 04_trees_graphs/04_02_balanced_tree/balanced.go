package balanced

import (
	"fmt"

	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

// NewBinarySearchTreeWithValues returns a balanced binary search tree
// that stores all the received values, and uses the comparator for the order
// If the comparator is nil, or the values are not strictly increasing as defined by the comparator, it returns an error.
func NewBinarySearchTreeWithValues[E any](comparator orderutils.ValueComparator[E], values []E) (*tree.BinarySearchTree[E], error) {
	result, err := tree.NewBinarySearchTree(comparator)
	if err != nil {
		return nil, err
	}

	for index := 0; index <= len(values)-2; index++ {
		if comparator(values[index], values[index+1]) >= 0 {
			return nil, fmt.Errorf("duplicate or smaller value at index %d", index+1)
		}
	}

	result.RootNode = buildBalancedSearchTree(values)
	return result, nil
}

func buildBalancedSearchTree[S ~[]E, E any](values S) *tree.BinaryTreeNode[E] {
	if len(values) == 0 {
		return nil
	}

	result := tree.BinaryTreeNode[E]{
		Value: values[len(values)/2],
	}
	result.LeftChild = buildBalancedSearchTree(values[:len(values)/2])
	result.RightChild = buildBalancedSearchTree(values[len(values)/2+1:])
	return &result
}
