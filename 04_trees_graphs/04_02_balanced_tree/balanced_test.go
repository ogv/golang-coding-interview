package balanced

import (
	"image"
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestNewBinarySearchTreeWithValues(t *testing.T) {
	/*
		 {32, 50, 51, 210, 372}
			 __51__
			|	   |
		 __50	 __372
		|		|
		32		210
	*/
	oddLevel2Left1 := tree.BinaryTreeNode[int]{Value: 210}
	oddLevel2Left0 := tree.BinaryTreeNode[int]{Value: 32}
	oddLevel1Left0 := tree.BinaryTreeNode[int]{Value: 50, LeftChild: &oddLevel2Left0}
	oddLevel1Right0 := tree.BinaryTreeNode[int]{Value: 372, LeftChild: &oddLevel2Left1}
	oddRootNode := tree.BinaryTreeNode[int]{Value: 51, LeftChild: &oddLevel1Left0, RightChild: &oddLevel1Right0}

	/*
		{32, 50, 51, 210, 372, 1430}
			 ___210___
			|         |
		 __50__		 _1430
		|	   |	|
		32    51	372
	*/
	evenLevel2Left0 := tree.BinaryTreeNode[int]{Value: 32}
	evenLevel2Right0 := tree.BinaryTreeNode[int]{Value: 51}
	evenLevel1Left0 := tree.BinaryTreeNode[int]{Value: 50, LeftChild: &evenLevel2Left0, RightChild: &evenLevel2Right0}
	evenLevel2Left1 := tree.BinaryTreeNode[int]{Value: 372}
	evenLevel1Right0 := tree.BinaryTreeNode[int]{Value: 1430, LeftChild: &evenLevel2Left1}
	evenRootNode := tree.BinaryTreeNode[int]{Value: 210, LeftChild: &evenLevel1Left0, RightChild: &evenLevel1Right0}

	/*
		{1, 3, 9, 27, 81, 243, 729}
				 ____27_____
				|			|
			 __3__		 __243__
			|	  |		|		|
			1     9     81		729
	*/
	fullLevel2Left0 := tree.BinaryTreeNode[int]{Value: 1}
	fullLevel2Right0 := tree.BinaryTreeNode[int]{Value: 9}
	fullLevel1Left0 := tree.BinaryTreeNode[int]{Value: 3, LeftChild: &fullLevel2Left0, RightChild: &fullLevel2Right0}
	fullLevel2Left1 := tree.BinaryTreeNode[int]{Value: 81}
	fullLevel2Right1 := tree.BinaryTreeNode[int]{Value: 729}
	fullLevel1Right0 := tree.BinaryTreeNode[int]{Value: 243, LeftChild: &fullLevel2Left1, RightChild: &fullLevel2Right1}
	fullRootNode := tree.BinaryTreeNode[int]{Value: 27, LeftChild: &fullLevel1Left0, RightChild: &fullLevel1Right0}

	testCases := []struct {
		name           string
		inValues       []int
		inComparator   orderutils.ValueComparator[int]
		wantResultRoot *tree.BinaryTreeNode[int]
		wantAnErr      bool
	}{
		{
			name:           "success, nil values",
			inValues:       nil,
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: nil,
			wantAnErr:      false,
		},
		{
			name:           "success, empty values",
			inValues:       []int{},
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: nil,
			wantAnErr:      false,
		},
		{
			name:           "success, one value",
			inValues:       []int{32},
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: &tree.BinaryTreeNode[int]{Value: 32},
			wantAnErr:      false,
		},
		{
			name:           "success, left only",
			inValues:       []int{32, 50},
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: &oddLevel1Left0,
			wantAnErr:      false,
		},
		{
			name:           "success, left and right",
			inValues:       []int{32, 50, 51},
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: &evenLevel1Left0,
			wantAnErr:      false,
		},

		{
			name:           "success, odd values length",
			inValues:       []int{32, 50, 51, 210, 372},
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: &oddRootNode,
			wantAnErr:      false,
		},
		{
			name:           "success, even values length",
			inValues:       []int{32, 50, 51, 210, 372, 1430},
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: &evenRootNode,
			wantAnErr:      false,
		},
		{
			name:           "success, full binary tree",
			inValues:       []int{1, 3, 9, 27, 81, 243, 729},
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: &fullRootNode,
			wantAnErr:      false,
		},
		{
			name:           "failure, nil comparator",
			inValues:       []int{1, 3, 9, 27, 81, 243, 729},
			inComparator:   nil,
			wantResultRoot: nil,
			wantAnErr:      true,
		},
		{
			name:           "failure, duplicate values",
			inValues:       []int{32, 51, 51, 63},
			wantResultRoot: nil,
			inComparator:   orderutils.Ascending[int],
			wantAnErr:      true,
		},
		{
			name:           "failure, non-increasing values",
			inValues:       []int{32, 63, 51},
			inComparator:   orderutils.Ascending[int],
			wantResultRoot: nil,
			wantAnErr:      true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualTree, actualErr := NewBinarySearchTreeWithValues(testCase.inComparator, testCase.inValues)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !testCase.wantAnErr {
				if !reflect.DeepEqual(testCase.wantResultRoot, actualTree.RootNode) {
					t.Errorf("expected tree root to be %#v but was %#v", testCase.wantResultRoot, actualTree.RootNode)
				}
				if actualTree.Comparator == nil {
					t.Errorf("expected tree to have a comparator, but was nil")
				}
			}
		})
	}
}

func TestVariousComparatorsForBinarySearchTree(t *testing.T) {
	/*
		2D points ordered by X :
		{(1,8), (3,2), (9,1), (27,16), (81,64), (243,32), (729,4)}
				 _____27,16_____
				|               |
			 __3,2__         __243,32__
			|       |		|          |
			1,8     9,1     81,64     729,4
	*/
	byXLevel2Left0 := tree.BinaryTreeNode[image.Point]{Value: image.Point{X: 1, Y: 8}}
	byXLevel2Right0 := tree.BinaryTreeNode[image.Point]{Value: image.Point{X: 9, Y: 1}}
	byXLevel1Left0 := tree.BinaryTreeNode[image.Point]{
		Value:      image.Point{X: 3, Y: 2},
		LeftChild:  &byXLevel2Left0,
		RightChild: &byXLevel2Right0,
	}
	byXLevel2Left1 := tree.BinaryTreeNode[image.Point]{Value: image.Point{X: 81, Y: 64}}
	byXLevel2Right1 := tree.BinaryTreeNode[image.Point]{Value: image.Point{X: 729, Y: 4}}
	byXLevel1Right0 := tree.BinaryTreeNode[image.Point]{
		Value:      image.Point{X: 243, Y: 32},
		LeftChild:  &byXLevel2Left1,
		RightChild: &byXLevel2Right1,
	}
	byXRootNode := tree.BinaryTreeNode[image.Point]{
		Value:      image.Point{X: 27, Y: 16},
		LeftChild:  &byXLevel1Left0,
		RightChild: &byXLevel1Right0,
	}

	/*
		2D points ordered by Y :
		{(9,1), (3,2), (729,4), (1,8), (27,16), (243,32), (81,64)}
				 _____1,8_____
				|             |
			 __3,2__        __243,32__
			|       |      |          |
			9,1    729,4   27,16     81,64
	*/
	byYLevel2Left0 := tree.BinaryTreeNode[image.Point]{Value: image.Point{X: 9, Y: 1}}
	byYLevel2Right0 := tree.BinaryTreeNode[image.Point]{Value: image.Point{X: 729, Y: 4}}
	byYLevel1Left0 := tree.BinaryTreeNode[image.Point]{
		Value:      image.Point{X: 3, Y: 2},
		LeftChild:  &byYLevel2Left0,
		RightChild: &byYLevel2Right0,
	}
	byYLevel2Left1 := tree.BinaryTreeNode[image.Point]{Value: image.Point{X: 27, Y: 16}}
	byYLevel2Right1 := tree.BinaryTreeNode[image.Point]{Value: image.Point{X: 81, Y: 64}}
	byYLevel1Right0 := tree.BinaryTreeNode[image.Point]{
		Value:      image.Point{X: 243, Y: 32},
		LeftChild:  &byYLevel2Left1,
		RightChild: &byYLevel2Right1,
	}
	byYRootNode := tree.BinaryTreeNode[image.Point]{
		Value:      image.Point{X: 1, Y: 8},
		LeftChild:  &byYLevel1Left0,
		RightChild: &byYLevel1Right0,
	}

	testCases := []struct {
		name           string
		inValues       []image.Point
		inComparator   orderutils.ValueComparator[image.Point]
		wantResultRoot *tree.BinaryTreeNode[image.Point]
		wantAnErr      bool
	}{
		{
			name: "success, full binary tree ordered by X",
			inValues: []image.Point{
				{X: 1, Y: 8},
				{X: 3, Y: 2},
				{X: 9, Y: 1},
				{X: 27, Y: 16},
				{X: 81, Y: 64},
				{X: 243, Y: 32},
				{X: 729, Y: 4},
			},
			inComparator:   pointsByX,
			wantResultRoot: &byXRootNode,
			wantAnErr:      false,
		},
		{
			name: "success, full binary tree ordered by X",
			inValues: []image.Point{
				{X: 9, Y: 1},
				{X: 3, Y: 2},
				{X: 729, Y: 4},
				{X: 1, Y: 8},
				{X: 27, Y: 16},
				{X: 243, Y: 32},
				{X: 81, Y: 64},
			},
			inComparator:   pointsByY,
			wantResultRoot: &byYRootNode,
			wantAnErr:      false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualTree, actualErr := NewBinarySearchTreeWithValues(testCase.inComparator, testCase.inValues)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !testCase.wantAnErr {
				if !reflect.DeepEqual(testCase.wantResultRoot, actualTree.RootNode) {
					t.Errorf("expected tree root to be %#v but was %#v", testCase.wantResultRoot, actualTree.RootNode)
				}
				if actualTree.Comparator == nil {
					t.Errorf("expected tree to have a comparator, but was nil")
				}
			}
		})
	}
}

func pointsByX(a image.Point, b image.Point) int {
	return smallerFirst(a.X, b.X)
}

func pointsByY(a image.Point, b image.Point) int {
	return smallerFirst(a.Y, b.Y)
}

func smallerFirst(a int, b int) int {
	if a < b {
		return -1
	} else if a > b {
		return 1
	} else {
		return 0
	}
}
