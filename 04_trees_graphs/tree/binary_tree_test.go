package tree

import (
	"reflect"
	"slices"
	"testing"
)

func TestBinaryTreeNode_Preorder(t *testing.T) {
	singleNode := &BinaryTreeNode[int]{
		Value: 953,
	}
	leftOnlyNode := &BinaryTreeNode[int]{
		Value: 954,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
	}
	rightOnlyNode := &BinaryTreeNode[int]{
		Value: 953,
		RightChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
	}
	rootChildrenOnly := &BinaryTreeNode[int]{
		Value: 953,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
		RightChild: &BinaryTreeNode[int]{
			Value: 7437,
		},
	}
	leftSideChain := &BinaryTreeNode[int]{
		Value: 953,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
			LeftChild: &BinaryTreeNode[int]{
				Value: 40,
				LeftChild: &BinaryTreeNode[int]{
					Value: 8,
				},
			},
		},
	}
	rightSideChain := &BinaryTreeNode[int]{
		Value: 953,
		RightChild: &BinaryTreeNode[int]{
			Value: 3876,
			RightChild: &BinaryTreeNode[int]{
				Value: 40,
				RightChild: &BinaryTreeNode[int]{
					Value: 8,
				},
			},
		},
	}
	diverseTree := newDiverseTree()

	testCases := []seqTestCase[int, *BinaryTreeNode[int]]{
		{
			name:       "nil",
			inNode:     nil,
			wantValues: nil,
		},
		{
			name:       "single node",
			inNode:     singleNode,
			wantValues: []*BinaryTreeNode[int]{singleNode},
		},
		{
			name:       "root and left",
			inNode:     leftOnlyNode,
			wantValues: []*BinaryTreeNode[int]{leftOnlyNode, leftOnlyNode.LeftChild},
		},
		{
			name:       "root and right",
			inNode:     rightOnlyNode,
			wantValues: []*BinaryTreeNode[int]{rightOnlyNode, rightOnlyNode.RightChild},
		},
		{
			name:       "root and both children",
			inNode:     rootChildrenOnly,
			wantValues: []*BinaryTreeNode[int]{rootChildrenOnly, rootChildrenOnly.LeftChild, rootChildrenOnly.RightChild},
		},
		{
			name:       "left side chain",
			inNode:     leftSideChain,
			wantValues: []*BinaryTreeNode[int]{leftSideChain, leftSideChain.LeftChild, leftSideChain.LeftChild.LeftChild, leftSideChain.LeftChild.LeftChild.LeftChild},
		},
		{
			name:       "right side chain",
			inNode:     rightSideChain,
			wantValues: []*BinaryTreeNode[int]{rightSideChain, rightSideChain.RightChild, rightSideChain.RightChild.RightChild, rightSideChain.RightChild.RightChild.RightChild},
		},
		{
			name:   "diverse tree",
			inNode: diverseTree,
			wantValues: []*BinaryTreeNode[int]{
				diverseTree, diverseTree.LeftChild, diverseTree.LeftChild.LeftChild, diverseTree.LeftChild.LeftChild.LeftChild, diverseTree.LeftChild.LeftChild.LeftChild.LeftChild,
				diverseTree.LeftChild.RightChild, diverseTree.LeftChild.RightChild.RightChild,
				diverseTree.RightChild, diverseTree.RightChild.LeftChild, diverseTree.RightChild.LeftChild.LeftChild,
				diverseTree.RightChild.RightChild, diverseTree.RightChild.RightChild.LeftChild, diverseTree.RightChild.RightChild.RightChild,
			},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotNodes []*BinaryTreeNode[int]
			for node := range testCase.inNode.Preorder() {
				gotNodes = append(gotNodes, node)
			}
			if !slices.Equal(testCase.wantValues, gotNodes) {
				pt.Errorf("expected nodes to be %v but got %v", testCase.wantValues, gotNodes)
			}
		})
	}
}

type seqTestCase[E, V any] struct {
	name       string
	inNode     *BinaryTreeNode[E]
	wantValues []V
}

func newDiverseTree() *BinaryTreeNode[int] {
	/*
					  ___________825___________
					 |                         |
				 ____27_____			  ____832____
				|			|            |		     |
			 ___3		    243__	   _827 	 __853__
			|	                 |	  |      	|       |
		  __1                   729  827       818     901
		 |
		-9
	*/
	return &BinaryTreeNode[int]{Value: 825,
		LeftChild: &BinaryTreeNode[int]{Value: 27,
			LeftChild: &BinaryTreeNode[int]{Value: 3,
				LeftChild: &BinaryTreeNode[int]{Value: 1,
					LeftChild: &BinaryTreeNode[int]{Value: -9},
				},
			},
			RightChild: &BinaryTreeNode[int]{Value: 243,
				RightChild: &BinaryTreeNode[int]{Value: 729},
			},
		},
		RightChild: &BinaryTreeNode[int]{Value: 832,
			LeftChild: &BinaryTreeNode[int]{Value: 827,
				LeftChild: &BinaryTreeNode[int]{Value: 827},
			},
			RightChild: &BinaryTreeNode[int]{Value: 853,
				LeftChild:  &BinaryTreeNode[int]{Value: 818},
				RightChild: &BinaryTreeNode[int]{Value: 901},
			},
		},
	}
}

func TestBinaryTreeNode_PreorderValues(t *testing.T) {
	testCases := []seqTestCase[int, int]{
		{
			name:       "nil",
			inNode:     nil,
			wantValues: nil,
		},
		{
			name: "single node",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			wantValues: []int{953},
		},
		{
			name: "root and left",
			inNode: &BinaryTreeNode[int]{
				Value: 954,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
				},
			},
			wantValues: []int{954, 3876},
		},
		{
			name: "root and right",
			inNode: &BinaryTreeNode[int]{
				Value: 955,
				RightChild: &BinaryTreeNode[int]{
					Value: 7437,
				},
			},
			wantValues: []int{955, 7437},
		},
		{
			name: "root and both children",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
				},
				RightChild: &BinaryTreeNode[int]{
					Value: 7437,
				},
			},
			wantValues: []int{953, 3876, 7437},
		},
		{
			name: "left side chain",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
					LeftChild: &BinaryTreeNode[int]{
						Value: 40,
						LeftChild: &BinaryTreeNode[int]{
							Value: 8,
						},
					},
				},
			},
			wantValues: []int{953, 3876, 40, 8},
		},
		{
			name: "right side chain",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				RightChild: &BinaryTreeNode[int]{
					Value: 3876,
					RightChild: &BinaryTreeNode[int]{
						Value: 40,
						RightChild: &BinaryTreeNode[int]{
							Value: 8,
						},
					},
				},
			},
			wantValues: []int{953, 3876, 40, 8},
		},
		{
			name:       "diverse tree",
			inNode:     newDiverseTree(),
			wantValues: []int{825, 27, 3, 1, -9, 243, 729, 832, 827, 827, 853, 818, 901},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotValues []int
			for nodeValue := range testCase.inNode.PreorderValues() {
				gotValues = append(gotValues, nodeValue)
			}
			if !slices.Equal(testCase.wantValues, gotValues) {
				pt.Errorf("expected node values to be %v but got %v", testCase.wantValues, gotValues)
			}
		})
	}
}

func TestBinaryTreeNode_PreorderValues_Searching(t *testing.T) {
	testCases := []searchSeqTestCase[int, int]{
		{
			name:       "nil, not found",
			inNode:     nil,
			inTarget:   4,
			wantValues: nil,
		},
		{
			name: "single node, not found",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			inTarget:   5,
			wantValues: []int{953},
		},
		{
			name: "single node, found",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			inTarget:   953,
			wantValues: []int{953},
		},
		{
			name:       "diverse tree, not found",
			inNode:     newDiverseTree(),
			inTarget:   4,
			wantValues: []int{825, 27, 3, 1, -9, 243, 729, 832, 827, 827, 853, 818, 901},
		},
		{
			name:       "diverse tree, first value found",
			inNode:     newDiverseTree(),
			inTarget:   825,
			wantValues: []int{825},
		},
		{
			name:       "diverse tree, inner value found",
			inNode:     newDiverseTree(),
			inTarget:   853,
			wantValues: []int{825, 27, 3, 1, -9, 243, 729, 832, 827, 827, 853},
		},
		{
			name:       "diverse tree, last value found",
			inNode:     newDiverseTree(),
			inTarget:   901,
			wantValues: []int{825, 27, 3, 1, -9, 243, 729, 832, 827, 827, 853, 818, 901},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotSearchedValues []int

			valueSearcher := func(value int) bool {
				gotSearchedValues = append(gotSearchedValues, value)
				return value != testCase.inTarget
			}
			valuesSeq := testCase.inNode.PreorderValues()
			valuesSeq(valueSearcher)

			if !slices.Equal(testCase.wantValues, gotSearchedValues) {
				pt.Errorf("expected searched node values to be %v but got %v", testCase.wantValues, gotSearchedValues)
			}
		})
	}
}

type searchSeqTestCase[E, V any] struct {
	name       string
	inNode     *BinaryTreeNode[E]
	inTarget   E
	wantValues []V
}

func TestBinaryTreeNode_Inorder(t *testing.T) {
	singleNode := &BinaryTreeNode[int]{
		Value: 953,
	}
	leftOnlyNode := &BinaryTreeNode[int]{
		Value: 954,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
	}
	rightOnlyNode := &BinaryTreeNode[int]{
		Value: 953,
		RightChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
	}
	rootChildrenOnly := &BinaryTreeNode[int]{
		Value: 953,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
		RightChild: &BinaryTreeNode[int]{
			Value: 7437,
		},
	}
	leftSideChain := &BinaryTreeNode[int]{
		Value: 953,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
			LeftChild: &BinaryTreeNode[int]{
				Value: 40,
				LeftChild: &BinaryTreeNode[int]{
					Value: 8,
				},
			},
		},
	}
	rightSideChain := &BinaryTreeNode[int]{
		Value: 953,
		RightChild: &BinaryTreeNode[int]{
			Value: 3876,
			RightChild: &BinaryTreeNode[int]{
				Value: 40,
				RightChild: &BinaryTreeNode[int]{
					Value: 8,
				},
			},
		},
	}
	diverseTree := newDiverseTree()

	testCases := []seqTestCase[int, *BinaryTreeNode[int]]{
		{
			name:       "nil",
			inNode:     nil,
			wantValues: nil,
		},
		{
			name:       "single node",
			inNode:     singleNode,
			wantValues: []*BinaryTreeNode[int]{singleNode},
		},
		{
			name:       "root and left",
			inNode:     leftOnlyNode,
			wantValues: []*BinaryTreeNode[int]{leftOnlyNode.LeftChild, leftOnlyNode},
		},
		{
			name:       "root and right",
			inNode:     rightOnlyNode,
			wantValues: []*BinaryTreeNode[int]{rightOnlyNode, rightOnlyNode.RightChild},
		},
		{
			name:       "root and both children",
			inNode:     rootChildrenOnly,
			wantValues: []*BinaryTreeNode[int]{rootChildrenOnly.LeftChild, rootChildrenOnly, rootChildrenOnly.RightChild},
		},
		{
			name:       "left side chain",
			inNode:     leftSideChain,
			wantValues: []*BinaryTreeNode[int]{leftSideChain.LeftChild.LeftChild.LeftChild, leftSideChain.LeftChild.LeftChild, leftSideChain.LeftChild, leftSideChain},
		},
		{
			name:       "right side chain",
			inNode:     rightSideChain,
			wantValues: []*BinaryTreeNode[int]{rightSideChain, rightSideChain.RightChild, rightSideChain.RightChild.RightChild, rightSideChain.RightChild.RightChild.RightChild},
		},
		{
			name:   "diverse tree",
			inNode: diverseTree,
			wantValues: []*BinaryTreeNode[int]{
				diverseTree.LeftChild.LeftChild.LeftChild.LeftChild, diverseTree.LeftChild.LeftChild.LeftChild,
				diverseTree.LeftChild.LeftChild, diverseTree.LeftChild,
				diverseTree.LeftChild.RightChild, diverseTree.LeftChild.RightChild.RightChild,
				diverseTree,
				diverseTree.RightChild.LeftChild.LeftChild, diverseTree.RightChild.LeftChild,
				diverseTree.RightChild,
				diverseTree.RightChild.RightChild.LeftChild,
				diverseTree.RightChild.RightChild,
				diverseTree.RightChild.RightChild.RightChild,
			},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotNodes []*BinaryTreeNode[int]
			for node := range testCase.inNode.Inorder() {
				gotNodes = append(gotNodes, node)
			}
			if !slices.Equal(testCase.wantValues, gotNodes) {
				pt.Errorf("expected nodes to be %v but got %v", testCase.wantValues, gotNodes)
			}
		})
	}
}

func TestBinaryTreeNode_InorderValues(t *testing.T) {
	testCases := []seqTestCase[int, int]{
		{
			name:       "nil",
			inNode:     nil,
			wantValues: nil,
		},
		{
			name: "single node",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			wantValues: []int{953},
		},
		{
			name: "root and left",
			inNode: &BinaryTreeNode[int]{
				Value: 954,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
				},
			},
			wantValues: []int{3876, 954},
		},
		{
			name: "root and right",
			inNode: &BinaryTreeNode[int]{
				Value: 955,
				RightChild: &BinaryTreeNode[int]{
					Value: 7437,
				},
			},
			wantValues: []int{955, 7437},
		},
		{
			name: "root and both children",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
				},
				RightChild: &BinaryTreeNode[int]{
					Value: 7437,
				},
			},
			wantValues: []int{3876, 953, 7437},
		},
		{
			name: "left side chain",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
					LeftChild: &BinaryTreeNode[int]{
						Value: 40,
						LeftChild: &BinaryTreeNode[int]{
							Value: 8,
						},
					},
				},
			},
			wantValues: []int{8, 40, 3876, 953},
		},
		{
			name: "right side chain",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				RightChild: &BinaryTreeNode[int]{
					Value: 3876,
					RightChild: &BinaryTreeNode[int]{
						Value: 40,
						RightChild: &BinaryTreeNode[int]{
							Value: 8,
						},
					},
				},
			},
			wantValues: []int{953, 3876, 40, 8},
		},
		{
			name:       "diverse tree",
			inNode:     newDiverseTree(),
			wantValues: []int{-9, 1, 3, 27, 243, 729, 825, 827, 827, 832, 818, 853, 901},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotValues []int
			for nodeValue := range testCase.inNode.InorderValues() {
				gotValues = append(gotValues, nodeValue)
			}
			if !slices.Equal(testCase.wantValues, gotValues) {
				pt.Errorf("expected node values to be %v but got %v", testCase.wantValues, gotValues)
			}
		})
	}
}

func TestBinaryTreeNode_InorderValues_Searching(t *testing.T) {
	testCases := []searchSeqTestCase[int, int]{
		{
			name:       "nil, not found",
			inNode:     nil,
			inTarget:   4,
			wantValues: nil,
		},
		{
			name: "single node, not found",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			inTarget:   5,
			wantValues: []int{953},
		},
		{
			name: "single node, found",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			inTarget:   953,
			wantValues: []int{953},
		},
		{
			name:       "diverse tree, not found",
			inNode:     newDiverseTree(),
			inTarget:   4,
			wantValues: []int{-9, 1, 3, 27, 243, 729, 825, 827, 827, 832, 818, 853, 901},
		},
		{
			name:       "diverse tree, first value found",
			inNode:     newDiverseTree(),
			inTarget:   -9,
			wantValues: []int{-9},
		},
		{
			name:       "diverse tree, inner value found",
			inNode:     newDiverseTree(),
			inTarget:   853,
			wantValues: []int{-9, 1, 3, 27, 243, 729, 825, 827, 827, 832, 818, 853},
		},
		{
			name:       "diverse tree, last value found",
			inNode:     newDiverseTree(),
			inTarget:   901,
			wantValues: []int{-9, 1, 3, 27, 243, 729, 825, 827, 827, 832, 818, 853, 901},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotSearchedValues []int

			valueSearcher := func(value int) bool {
				gotSearchedValues = append(gotSearchedValues, value)
				return value != testCase.inTarget
			}
			valuesSeq := testCase.inNode.InorderValues()
			valuesSeq(valueSearcher)

			if !slices.Equal(testCase.wantValues, gotSearchedValues) {
				pt.Errorf("expected searched node values to be %v but got %v", testCase.wantValues, gotSearchedValues)
			}
		})
	}
}

func TestBinaryTreeNode_Postorder(t *testing.T) {
	singleNode := &BinaryTreeNode[int]{
		Value: 953,
	}
	leftOnlyNode := &BinaryTreeNode[int]{
		Value: 954,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
	}
	rightOnlyNode := &BinaryTreeNode[int]{
		Value: 953,
		RightChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
	}
	rootChildrenOnly := &BinaryTreeNode[int]{
		Value: 953,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
		RightChild: &BinaryTreeNode[int]{
			Value: 7437,
		},
	}
	leftSideChain := &BinaryTreeNode[int]{
		Value: 953,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
			LeftChild: &BinaryTreeNode[int]{
				Value: 40,
				LeftChild: &BinaryTreeNode[int]{
					Value: 8,
				},
			},
		},
	}
	rightSideChain := &BinaryTreeNode[int]{
		Value: 953,
		RightChild: &BinaryTreeNode[int]{
			Value: 3876,
			RightChild: &BinaryTreeNode[int]{
				Value: 40,
				RightChild: &BinaryTreeNode[int]{
					Value: 8,
				},
			},
		},
	}
	diverseTree := newDiverseTree()

	testCases := []seqTestCase[int, *BinaryTreeNode[int]]{
		{
			name:       "nil",
			inNode:     nil,
			wantValues: nil,
		},
		{
			name:       "single node",
			inNode:     singleNode,
			wantValues: []*BinaryTreeNode[int]{singleNode},
		},
		{
			name:       "root and left",
			inNode:     leftOnlyNode,
			wantValues: []*BinaryTreeNode[int]{leftOnlyNode.LeftChild, leftOnlyNode},
		},
		{
			name:       "root and right",
			inNode:     rightOnlyNode,
			wantValues: []*BinaryTreeNode[int]{rightOnlyNode.RightChild, rightOnlyNode},
		},
		{
			name:       "root and both children",
			inNode:     rootChildrenOnly,
			wantValues: []*BinaryTreeNode[int]{rootChildrenOnly.LeftChild, rootChildrenOnly.RightChild, rootChildrenOnly},
		},
		{
			name:       "left side chain",
			inNode:     leftSideChain,
			wantValues: []*BinaryTreeNode[int]{leftSideChain.LeftChild.LeftChild.LeftChild, leftSideChain.LeftChild.LeftChild, leftSideChain.LeftChild, leftSideChain},
		},
		{
			name:       "right side chain",
			inNode:     rightSideChain,
			wantValues: []*BinaryTreeNode[int]{rightSideChain.RightChild.RightChild.RightChild, rightSideChain.RightChild.RightChild, rightSideChain.RightChild, rightSideChain},
		},
		{
			name:   "diverse tree",
			inNode: diverseTree,
			wantValues: []*BinaryTreeNode[int]{
				diverseTree.LeftChild.LeftChild.LeftChild.LeftChild,
				diverseTree.LeftChild.LeftChild.LeftChild, diverseTree.LeftChild.LeftChild,
				diverseTree.LeftChild.RightChild.RightChild, diverseTree.LeftChild.RightChild,
				diverseTree.LeftChild,
				diverseTree.RightChild.LeftChild.LeftChild, diverseTree.RightChild.LeftChild,
				diverseTree.RightChild.RightChild.LeftChild, diverseTree.RightChild.RightChild.RightChild,
				diverseTree.RightChild.RightChild,
				diverseTree.RightChild,
				diverseTree,
			},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotNodes []*BinaryTreeNode[int]
			for node := range testCase.inNode.Postorder() {
				gotNodes = append(gotNodes, node)
			}
			if !slices.Equal(testCase.wantValues, gotNodes) {
				pt.Errorf("expected nodes to be %v but got %v", testCase.wantValues, gotNodes)
			}
		})
	}
}

func TestBinaryTreeNode_PostorderValues(t *testing.T) {
	testCases := []seqTestCase[int, int]{
		{
			name:       "nil",
			inNode:     nil,
			wantValues: nil,
		},
		{
			name: "single node",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			wantValues: []int{953},
		},
		{
			name: "root and left",
			inNode: &BinaryTreeNode[int]{
				Value: 954,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
				},
			},
			wantValues: []int{3876, 954},
		},
		{
			name: "root and right",
			inNode: &BinaryTreeNode[int]{
				Value: 955,
				RightChild: &BinaryTreeNode[int]{
					Value: 7437,
				},
			},
			wantValues: []int{7437, 955},
		},
		{
			name: "root and both children",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
				},
				RightChild: &BinaryTreeNode[int]{
					Value: 7437,
				},
			},
			wantValues: []int{3876, 7437, 953},
		},
		{
			name: "left side chain",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
					LeftChild: &BinaryTreeNode[int]{
						Value: 40,
						LeftChild: &BinaryTreeNode[int]{
							Value: 8,
						},
					},
				},
			},
			wantValues: []int{8, 40, 3876, 953},
		},
		{
			name: "right side chain",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				RightChild: &BinaryTreeNode[int]{
					Value: 3876,
					RightChild: &BinaryTreeNode[int]{
						Value: 40,
						RightChild: &BinaryTreeNode[int]{
							Value: 8,
						},
					},
				},
			},
			wantValues: []int{8, 40, 3876, 953},
		},
		{
			name:       "diverse tree",
			inNode:     newDiverseTree(),
			wantValues: []int{-9, 1, 3, 729, 243, 27, 827, 827, 818, 901, 853, 832, 825},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotValues []int
			for nodeValue := range testCase.inNode.PostorderValues() {
				gotValues = append(gotValues, nodeValue)
			}
			if !slices.Equal(testCase.wantValues, gotValues) {
				pt.Errorf("expected node values to be %v but got %v", testCase.wantValues, gotValues)
			}
		})
	}
}

func TestBinaryTreeNode_PostorderValues_Searching(t *testing.T) {
	testCases := []searchSeqTestCase[int, int]{
		{
			name:       "nil, not found",
			inNode:     nil,
			inTarget:   4,
			wantValues: nil,
		},
		{
			name: "single node, not found",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			inTarget:   5,
			wantValues: []int{953},
		},
		{
			name: "single node, found",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			inTarget:   953,
			wantValues: []int{953},
		},
		{
			name:       "diverse tree, not found",
			inNode:     newDiverseTree(),
			inTarget:   4,
			wantValues: []int{-9, 1, 3, 729, 243, 27, 827, 827, 818, 901, 853, 832, 825},
		},
		{
			name:       "diverse tree, first value found",
			inNode:     newDiverseTree(),
			inTarget:   -9,
			wantValues: []int{-9},
		},
		{
			name:       "diverse tree, inner value found",
			inNode:     newDiverseTree(),
			inTarget:   853,
			wantValues: []int{-9, 1, 3, 729, 243, 27, 827, 827, 818, 901, 853},
		},
		{
			name:       "diverse tree, last value found",
			inNode:     newDiverseTree(),
			inTarget:   825,
			wantValues: []int{-9, 1, 3, 729, 243, 27, 827, 827, 818, 901, 853, 832, 825},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotSearchedValues []int

			valueSearcher := func(value int) bool {
				gotSearchedValues = append(gotSearchedValues, value)
				return value != testCase.inTarget
			}
			valuesSeq := testCase.inNode.PostorderValues()
			valuesSeq(valueSearcher)

			if !slices.Equal(testCase.wantValues, gotSearchedValues) {
				pt.Errorf("expected searched node values to be %v but got %v", testCase.wantValues, gotSearchedValues)
			}
		})
	}
}

func TestBinaryTreeNode_Levels(t *testing.T) {
	singleNode := &BinaryTreeNode[int]{
		Value: 953,
	}
	leftOnlyNode := &BinaryTreeNode[int]{
		Value: 954,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
	}
	rightOnlyNode := &BinaryTreeNode[int]{
		Value: 953,
		RightChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
	}
	rootChildrenOnly := &BinaryTreeNode[int]{
		Value: 953,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
		},
		RightChild: &BinaryTreeNode[int]{
			Value: 7437,
		},
	}
	leftSideChain := &BinaryTreeNode[int]{
		Value: 953,
		LeftChild: &BinaryTreeNode[int]{
			Value: 3876,
			LeftChild: &BinaryTreeNode[int]{
				Value: 40,
				LeftChild: &BinaryTreeNode[int]{
					Value: 8,
				},
			},
		},
	}
	rightSideChain := &BinaryTreeNode[int]{
		Value: 953,
		RightChild: &BinaryTreeNode[int]{
			Value: 3876,
			RightChild: &BinaryTreeNode[int]{
				Value: 40,
				RightChild: &BinaryTreeNode[int]{
					Value: 8,
				},
			},
		},
	}
	diverseTree := newDiverseTree()

	testCases := []seq2TestCase[int, int, []*BinaryTreeNode[int]]{
		{
			name:       "nil",
			inNode:     nil,
			wantValues: nil,
		},
		{
			name:   "single node",
			inNode: singleNode,
			wantValues: map[int][]*BinaryTreeNode[int]{
				0: {singleNode},
			},
		},
		{
			name:   "root and left",
			inNode: leftOnlyNode,
			wantValues: map[int][]*BinaryTreeNode[int]{
				0: {leftOnlyNode},
				1: {leftOnlyNode.LeftChild},
			},
		},
		{
			name:   "root and right",
			inNode: rightOnlyNode,
			wantValues: map[int][]*BinaryTreeNode[int]{
				0: {rightOnlyNode},
				1: {rightOnlyNode.RightChild},
			},
		},
		{
			name:   "root and both children",
			inNode: rootChildrenOnly,
			wantValues: map[int][]*BinaryTreeNode[int]{
				0: {rootChildrenOnly},
				1: {rootChildrenOnly.LeftChild, rootChildrenOnly.RightChild},
			},
		},
		{
			name:   "left side chain",
			inNode: leftSideChain,
			wantValues: map[int][]*BinaryTreeNode[int]{
				0: {leftSideChain},
				1: {leftSideChain.LeftChild},
				2: {leftSideChain.LeftChild.LeftChild},
				3: {leftSideChain.LeftChild.LeftChild.LeftChild},
			},
		},
		{
			name:   "right side chain",
			inNode: rightSideChain,
			wantValues: map[int][]*BinaryTreeNode[int]{
				0: {rightSideChain},
				1: {rightSideChain.RightChild},
				2: {rightSideChain.RightChild.RightChild},
				3: {rightSideChain.RightChild.RightChild.RightChild},
			},
		},
		{
			name:   "diverse tree",
			inNode: diverseTree,
			wantValues: map[int][]*BinaryTreeNode[int]{
				0: {diverseTree},
				1: {diverseTree.LeftChild, diverseTree.RightChild},
				2: {diverseTree.LeftChild.LeftChild, diverseTree.LeftChild.RightChild,
					diverseTree.RightChild.LeftChild, diverseTree.RightChild.RightChild},
				3: {diverseTree.LeftChild.LeftChild.LeftChild, diverseTree.LeftChild.RightChild.RightChild,
					diverseTree.RightChild.LeftChild.LeftChild, diverseTree.RightChild.RightChild.LeftChild, diverseTree.RightChild.RightChild.RightChild},
				4: {diverseTree.LeftChild.LeftChild.LeftChild.LeftChild},
			},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			currentI := 0
			for i, nodes := range testCase.inNode.Levels() {
				if i != currentI {
					pt.Errorf("expected index to be: %v but was: %v", currentI+1, i)
				}
				if !slices.Equal(nodes, testCase.wantValues[i]) {
					pt.Errorf("expected level %d to be: %#v but was: %#v", i, testCase.wantValues[i], nodes)
				}
				currentI++
			}
		})
	}
}

func TestBinaryTreeNode_LevelValues(t *testing.T) {
	testCases := []seq2TestCase[int, int, []int]{
		{
			name:       "nil",
			inNode:     nil,
			wantValues: nil,
		},
		{
			name: "single node",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			wantValues: map[int][]int{
				0: {953},
			},
		},
		{
			name: "root and left",
			inNode: &BinaryTreeNode[int]{
				Value: 954,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
				},
			},
			wantValues: map[int][]int{
				0: {954},
				1: {3876},
			},
		},
		{
			name: "root and right",
			inNode: &BinaryTreeNode[int]{
				Value: 955,
				RightChild: &BinaryTreeNode[int]{
					Value: 7437,
				},
			},
			wantValues: map[int][]int{
				0: {955},
				1: {7437},
			},
		},
		{
			name: "root and both children",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
				},
				RightChild: &BinaryTreeNode[int]{
					Value: 7437,
				},
			},
			wantValues: map[int][]int{
				0: {953},
				1: {3876, 7437},
			},
		},
		{
			name: "left side chain",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				LeftChild: &BinaryTreeNode[int]{
					Value: 3876,
					LeftChild: &BinaryTreeNode[int]{
						Value: 40,
						LeftChild: &BinaryTreeNode[int]{
							Value: 8,
						},
					},
				},
			},
			wantValues: map[int][]int{
				0: {953},
				1: {3876},
				2: {40},
				3: {8},
			},
		},
		{
			name: "right side chain",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
				RightChild: &BinaryTreeNode[int]{
					Value: 3876,
					RightChild: &BinaryTreeNode[int]{
						Value: 40,
						RightChild: &BinaryTreeNode[int]{
							Value: 8,
						},
					},
				},
			},
			wantValues: map[int][]int{
				0: {953},
				1: {3876},
				2: {40},
				3: {8},
			},
		},
		{
			name:   "diverse tree",
			inNode: newDiverseTree(),
			wantValues: map[int][]int{
				0: {825},
				1: {27, 832},
				2: {3, 243, 827, 853},
				3: {1, 729, 827, 818, 901},
				4: {-9},
			},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			currentI := 0
			for i, values := range testCase.inNode.LevelValues() {
				if i != currentI {
					pt.Errorf("expected index to be: %v but was: %v", currentI, i)
				}
				if !slices.Equal(values, testCase.wantValues[i]) {
					pt.Errorf("expected level %d to be: %#v but was: %#v", i, testCase.wantValues[i], values)
				}
				currentI++
			}
		})
	}
}

func TestBinaryTreeNode_LevelValues_Searching(t *testing.T) {
	testCases := []searchSeqTestCase[int, []int]{
		{
			name:       "nil, not found",
			inNode:     nil,
			inTarget:   4,
			wantValues: nil,
		},
		{
			name: "single node, not found",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			inTarget: 5,
			wantValues: [][]int{
				{953},
			},
		},
		{
			name: "single node, found",
			inNode: &BinaryTreeNode[int]{
				Value: 953,
			},
			inTarget: 953,
			wantValues: [][]int{
				{953},
			},
		},
		{
			name:     "diverse tree, last value found",
			inNode:   newDiverseTree(),
			inTarget: -9,
			wantValues: [][]int{
				{825},
				{27, 832},
				{3, 243, 827, 853},
				{1, 729, 827, 818, 901},
				{-9},
			},
		},
		{
			name:     "diverse tree, not found",
			inNode:   newDiverseTree(),
			inTarget: 4,
			wantValues: [][]int{
				{825},
				{27, 832},
				{3, 243, 827, 853},
				{1, 729, 827, 818, 901},
				{-9},
			},
		},
		{
			name:     "diverse tree, first value found",
			inNode:   newDiverseTree(),
			inTarget: 825,
			wantValues: [][]int{
				{825},
			},
		},
		{
			name:     "diverse tree, inner value found",
			inNode:   newDiverseTree(),
			inTarget: 853,
			wantValues: [][]int{
				{825},
				{27, 832},
				{3, 243, 827, 853},
			},
		},
		{
			name:     "diverse tree, last value found",
			inNode:   newDiverseTree(),
			inTarget: -9,
			wantValues: [][]int{
				{825},
				{27, 832},
				{3, 243, 827, 853},
				{1, 729, 827, 818, 901},
				{-9},
			},
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotSearchedValues [][]int
			valueSearcher := func(level int, values []int) bool {
				gotSearchedValues = append(gotSearchedValues, values)
				return !slices.Contains(values, testCase.inTarget)
			}
			levelsSeq := testCase.inNode.LevelValues()
			levelsSeq(valueSearcher)

			if !slices.EqualFunc(testCase.wantValues, gotSearchedValues, slices.Equal) {
				pt.Errorf("expected searched node values to be %v but got %v", testCase.wantValues, gotSearchedValues)
			}
		})
	}
}

type seq2TestCase[E, K comparable, V any] struct {
	name       string
	inNode     *BinaryTreeNode[E]
	wantValues map[K]V
}

func TestBinaryTreeNodeWithParent_SetLeft(t *testing.T) {
	testCases := []setValueTestCase[map[int]string]{
		{
			name:      "nil child, set nil value",
			inNode:    &BinaryTreeNodeWithParent[map[int]string]{},
			inValue:   nil,
			wantValue: nil,
		},
		{
			name:   "nil child, set map value",
			inNode: &BinaryTreeNodeWithParent[map[int]string]{},
			inValue: map[int]string{
				0:  "zero",
				1:  "one",
				11: "eleven",
			},
			wantValue: map[int]string{
				0:  "zero",
				1:  "one",
				11: "eleven",
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			testCase.inNode.SetLeft(testCase.inValue)
			if testCase.inNode.LeftChild == nil {
				pt.Fatalf("child is nil after setting a value for it")
			}
			gotValue := testCase.inNode.LeftChild.Value

			wantReflected := reflect.ValueOf(testCase.wantValue)
			gotReflected := reflect.ValueOf(gotValue)

			wantKind := wantReflected.Kind()
			gotKind := gotReflected.Kind()
			if gotKind != wantKind {
				pt.Errorf("wanted kind %v but got %v", wantKind, gotKind)
			}

			if testCase.wantValue == nil {
				if gotValue != nil {
					pt.Errorf("wanted nil but got %v", gotValue)
				}
			} else {
				wantType := wantReflected.Type()
				gotType := gotReflected.Type()
				if gotType != wantType {
					pt.Errorf("wanted type %v but got %v", wantType, gotType)
				}
				if !reflect.DeepEqual(testCase.wantValue, gotValue) {
					pt.Errorf("%v not equal to %v", gotValue, testCase.wantValue)
				}
			}
		})
	}
}

type setValueTestCase[E any] struct {
	name      string
	inNode    *BinaryTreeNodeWithParent[E]
	inValue   E
	wantValue E
}

func TestBinaryTreeNodeWithParent_SetRight(t *testing.T) {
	testCases := []setValueTestCase[map[int]string]{
		{
			name:      "nil child, set nil value",
			inNode:    &BinaryTreeNodeWithParent[map[int]string]{},
			inValue:   nil,
			wantValue: nil,
		},
		{
			name:   "nil child, set map value",
			inNode: &BinaryTreeNodeWithParent[map[int]string]{},
			inValue: map[int]string{
				0:  "zero",
				1:  "one",
				11: "eleven",
			},
			wantValue: map[int]string{
				0:  "zero",
				1:  "one",
				11: "eleven",
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			testCase.inNode.SetRight(testCase.inValue)
			if testCase.inNode.RightChild == nil {
				pt.Fatalf("child is nil after setting a value for it")
			}
			gotValue := testCase.inNode.RightChild.Value

			wantReflected := reflect.ValueOf(testCase.wantValue)
			gotReflected := reflect.ValueOf(gotValue)

			wantKind := wantReflected.Kind()
			gotKind := gotReflected.Kind()
			if gotKind != wantKind {
				pt.Errorf("wanted kind %v but got %v", wantKind, gotKind)
			}

			if testCase.wantValue == nil {
				if gotValue != nil {
					pt.Errorf("wanted nil but got %v", gotValue)
				}
			} else {
				wantType := wantReflected.Type()
				gotType := gotReflected.Type()
				if gotType != wantType {
					pt.Errorf("wanted type %v but got %v", wantType, gotType)
				}
				if !reflect.DeepEqual(testCase.wantValue, gotValue) {
					pt.Errorf("%v not equal to %v", gotValue, testCase.wantValue)
				}
			}
		})
	}
}
