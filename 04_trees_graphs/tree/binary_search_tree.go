package tree

import (
	"errors"

	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
)

// NewBinarySearchTree returns an empty binary search tree, that uses the given comparator for ordering the nodes
// If the comparator is nil, it returns an error
// This approach has 2 main benefits:
// - minimality, offering the same comparator TYPE, no matter what kind of values are stored in the tree
// - flexibility, allowing different orderings of the same values.
func NewBinarySearchTree[E any](comparator orderutils.ValueComparator[E]) (*BinarySearchTree[E], error) {
	if comparator == nil {
		return nil, errors.New("nil comparator function")
	}
	return &BinarySearchTree[E]{Comparator: comparator}, nil
}

// BinarySearchTree is a binary tree where each node's key
// is greater than or equal to all the keys from its left subtree
// and less than or equal to all the keys from its right subtree.
type BinarySearchTree[E any] struct {
	RootNode   *BinaryTreeNode[E]
	Comparator orderutils.ValueComparator[E] // defines the ordering of the tree nodes
}
