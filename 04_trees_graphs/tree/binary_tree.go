package tree

import (
	"iter"
)

// BinaryTreeNode is the building block of a binary tree
// (a value wrapped in a node with optional left and right descendants).
type BinaryTreeNode[E any] struct {
	Value      E
	LeftChild  *BinaryTreeNode[E]
	RightChild *BinaryTreeNode[E]
}

// Preorder returns an iterator over all nodes of the tree
// rooted at the receiver, going depth-first in preorder.
func (n *BinaryTreeNode[E]) Preorder() iter.Seq[*BinaryTreeNode[E]] {
	return preorderIterator(n, nodeIdentity)
}

func nodeIdentity[E any](n *BinaryTreeNode[E]) *BinaryTreeNode[E] {
	return n
}

func preorderIterator[E, T any](root *BinaryTreeNode[E], nodeMapper func(*BinaryTreeNode[E]) T) iter.Seq[T] {
	if root == nil {
		return func(yield func(T) bool) {}
	}

	return func(yield func(T) bool) {
		nodesStack := []*BinaryTreeNode[E]{root}
		for len(nodesStack) >= 1 {
			last := len(nodesStack) - 1
			topNode := nodesStack[last]
			nodesStack[last] = nil
			nodesStack = nodesStack[:last]

			if !yield(nodeMapper(topNode)) {
				return
			}
			if topNode.RightChild != nil {
				nodesStack = append(nodesStack, topNode.RightChild)
			}
			if topNode.LeftChild != nil {
				nodesStack = append(nodesStack, topNode.LeftChild)
			}
		}
	}
}

// PreorderValues returns an iterator over all values of the tree
// rooted at the receiver, going depth-first using preorder.
func (n *BinaryTreeNode[E]) PreorderValues() iter.Seq[E] {
	return preorderIterator(n, nodeValue)
}

func nodeValue[E any](n *BinaryTreeNode[E]) E {
	return n.Value
}

// Inorder returns an iterator over all nodes of the tree
// rooted at the receiver, going depth-first using inorder.
func (n *BinaryTreeNode[E]) Inorder() iter.Seq[*BinaryTreeNode[E]] {
	return inorderIterator(n, nodeIdentity)
}

func inorderIterator[E, T any](root *BinaryTreeNode[E], nodeMapper func(*BinaryTreeNode[E]) T) iter.Seq[T] {
	if root == nil {
		return func(yield func(T) bool) {}
	}

	return func(yield func(T) bool) {
		nodesStack := []*BinaryTreeNode[E]{}
		current := root
		for current != nil || len(nodesStack) >= 1 {
			if current != nil {
				nodesStack = append(nodesStack, current)
				current = current.LeftChild
			} else {
				last := len(nodesStack) - 1
				current = nodesStack[last]
				nodesStack[last] = nil
				nodesStack = nodesStack[:last]

				if !yield(nodeMapper(current)) {
					return
				}
				current = current.RightChild
			}
		}
	}
}

// InorderValues returns an iterator over all values of the tree
// rooted at the receiver, going depth-first using inorder.
func (n *BinaryTreeNode[E]) InorderValues() iter.Seq[E] {
	return inorderIterator(n, nodeValue)
}

// Postorder returns an iterator over all nodes of the tree
// rooted at the receiver, going bottom up to the receiver, using postorder.
func (n *BinaryTreeNode[E]) Postorder() iter.Seq[*BinaryTreeNode[E]] {
	return postorderIterator(n, nodeIdentity)
}

func postorderIterator[E, T any](root *BinaryTreeNode[E], nodeMapper func(*BinaryTreeNode[E]) T) iter.Seq[T] {
	if root == nil {
		return func(yield func(T) bool) {}
	}

	return func(yield func(T) bool) {
		nodesStack := []*BinaryTreeNode[E]{}
		current := root
		previous := (*BinaryTreeNode[E])(nil)

		for current != nil || len(nodesStack) >= 1 {
			if current != nil {
				nodesStack = append(nodesStack, current)
				current = current.LeftChild
			} else {
				last := len(nodesStack) - 1
				topNode := nodesStack[last]

				if topNode.RightChild != nil && topNode.RightChild != previous {
					current = topNode.RightChild
				} else {
					if !yield(nodeMapper(topNode)) {
						return
					}
					previous = topNode
					nodesStack[last] = nil
					nodesStack = nodesStack[:last]
				}
			}
		}
	}
}

// PostorderValues returns an iterator over all values of the tree
// rooted at the receiver, going bottom up to the receiver, using postorder.
func (n *BinaryTreeNode[E]) PostorderValues() iter.Seq[E] {
	return postorderIterator(n, nodeValue)
}

// Levels returns an iterator over all nodes of the tree rooted at the receiver,
// grouped by level starting from the receiver, going breadth-first.
// Index levels start at 0 and increase by 1 every consecutive level.
func (n *BinaryTreeNode[E]) Levels() iter.Seq2[int, []*BinaryTreeNode[E]] {
	return levelIterator(n, nodeIdentity)
}

func levelIterator[E, T any](root *BinaryTreeNode[E], nodeMapper func(*BinaryTreeNode[E]) T) iter.Seq2[int, []T] {
	if root == nil {
		return func(yield func(int, []T) bool) {}
	}

	return func(yield func(int, []T) bool) {
		level := 0
		levelNodes := []*BinaryTreeNode[E]{root}
		for len(levelNodes) >= 1 {
			levelData := make([]T, len(levelNodes))
			for i, node := range levelNodes {
				levelData[i] = nodeMapper(node)
			}
			if !yield(level, levelData) {
				return
			}

			nextLevel := []*BinaryTreeNode[E]{}
			for _, node := range levelNodes {
				if node.LeftChild != nil {
					nextLevel = append(nextLevel, node.LeftChild)
				}
				if node.RightChild != nil {
					nextLevel = append(nextLevel, node.RightChild)
				}
			}
			levelNodes = nextLevel
			level++
		}
	}
}

// LevelValues returns an iterator over all values of the tree rooted at the receiver,
// grouped by level starting from the receiver, going breadth-first.
// Index levels start at 0 and increase by 1 every consecutive level.
func (n *BinaryTreeNode[E]) LevelValues() iter.Seq2[int, []E] {
	return levelIterator(n, nodeValue)
}

// BinaryTreeNodeWithParent is a binary tree node with a link to its parent
type BinaryTreeNodeWithParent[E any] struct {
	Value      E
	LeftChild  *BinaryTreeNodeWithParent[E]
	RightChild *BinaryTreeNodeWithParent[E]
	Parent     *BinaryTreeNodeWithParent[E]
}

// SetLeft replaces the receiver's left child with a new node containing the value from the argument
func (n *BinaryTreeNodeWithParent[E]) SetLeft(value E) {
	n.LeftChild = &BinaryTreeNodeWithParent[E]{
		Value:  value,
		Parent: n,
	}
}

// SetRight replaces the receiver's right child with a new node containing the value from the argument
func (n *BinaryTreeNodeWithParent[E]) SetRight(value E) {
	n.RightChild = &BinaryTreeNodeWithParent[E]{
		Value:  value,
		Parent: n,
	}
}
