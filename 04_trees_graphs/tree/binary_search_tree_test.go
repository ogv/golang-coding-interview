package tree

import (
	"testing"

	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
)

func TestNewBinarySearchTree(t *testing.T) {
	testCases := []struct {
		name         string
		inComparator orderutils.ValueComparator[int]
		wantResult   *BinarySearchTree[int]
		wantAnErr    bool
	}{
		{
			name:         "success, comparator passed in",
			inComparator: orderutils.Ascending[int],
			wantResult: &BinarySearchTree[int]{
				RootNode:   nil,
				Comparator: orderutils.Ascending[int],
			},
			wantAnErr: false,
		},
		{
			name:         "failure, no comparator passed in",
			inComparator: nil,
			wantResult:   nil,
			wantAnErr:    true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualTree, actualErr := NewBinarySearchTree(testCase.inComparator)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if (actualTree == nil) != (testCase.wantResult == nil) {
				t.Errorf("expected tree to be nil: %t but was nil: %t", testCase.wantResult == nil, actualTree == nil)
			}
			if actualTree != nil && actualTree.Comparator == nil {
				t.Errorf("expected tree to have a comparator, but it doesn't")
			}
		})
	}
}
