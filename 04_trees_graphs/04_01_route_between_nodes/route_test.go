package route

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/directed"
)

func TestDirectedRouteExists(t *testing.T) {
	oneNodeNoRelationsGraph, err := directed.NewGraph(
		[]any{1},
		[]directed.IntPair{},
	)
	if err != nil {
		t.FailNow()
	}

	oneNodeSelfRelationGraph, err := directed.NewGraph(
		[]any{1},
		[]directed.IntPair{
			{From: 0, To: 0},
		},
	)
	if err != nil {
		t.FailNow()
	}

	unconnectedGraph, err := directed.NewGraph(
		[]any{21, "A string value", 21, +32 - 1i},
		[]directed.IntPair{
			{From: 0, To: 0},
			{From: 0, To: 1},
			{From: 2, To: 3},
			{From: 3, To: 2},
		},
	)
	if err != nil {
		t.FailNow()
	}

	// 2 > 0 > 1
	//     0 > 3
	connectedAcyclicGraph, err := directed.NewGraph(
		[]any{21, "A string value", 21, +32 - 1i},
		[]directed.IntPair{
			{From: 2, To: 0},
			{From: 0, To: 1},
			{From: 0, To: 3},
		},
	)
	if err != nil {
		t.FailNow()
	}

	// 2 > 0 > 1 > 3 > 2
	connectedCyclicGraph, err := directed.NewGraph(
		[]any{21, "A string value", 21, +32 - 1i},
		[]directed.IntPair{
			{From: 2, To: 0},
			{From: 3, To: 2},
			{From: 0, To: 1},
			{From: 1, To: 3},
		},
	)
	if err != nil {
		t.FailNow()
	}

	maximalGraph, err := directed.NewGraph(
		[]any{21, "A string value", 21, +32 - 1i},
		[]directed.IntPair{
			{From: 0, To: 0},
			{From: 0, To: 1},
			{From: 0, To: 2},
			{From: 0, To: 3},
			{From: 2, To: 3},
			{From: 2, To: 2},
			{From: 2, To: 1},
			{From: 2, To: 0},
			{From: 1, To: 2},
			{From: 1, To: 3},
			{From: 1, To: 1},
			{From: 1, To: 0},
			{From: 3, To: 1},
			{From: 3, To: 3},
			{From: 3, To: 0},
			{From: 3, To: 2},
		},
	)
	if err != nil {
		t.FailNow()
	}

	testCases := []struct {
		name              string
		inGraph           directed.Graph[any]
		inFrom            int
		inTo              int
		wantPathExistence bool
		wantAnErr         bool
	}{
		{
			name:              "failure, empty graph",
			inGraph:           directed.Graph[any]{},
			inFrom:            0,
			inTo:              0,
			wantPathExistence: false,
			wantAnErr:         true,
		},
		{
			name:              "failure, negative from",
			inGraph:           *oneNodeNoRelationsGraph,
			inFrom:            -1,
			inTo:              0,
			wantPathExistence: false,
			wantAnErr:         true,
		},
		{
			name:              "failure, large from",
			inGraph:           *oneNodeNoRelationsGraph,
			inFrom:            3,
			inTo:              0,
			wantPathExistence: false,
			wantAnErr:         true,
		},
		{
			name:              "failure, negative to",
			inGraph:           *oneNodeNoRelationsGraph,
			inFrom:            0,
			inTo:              -1,
			wantPathExistence: false,
			wantAnErr:         true,
		},
		{
			name:              "failure, large to",
			inGraph:           *oneNodeNoRelationsGraph,
			inFrom:            0,
			inTo:              3,
			wantPathExistence: false,
			wantAnErr:         true,
		},
		{
			name:              "success, one node no relations graph",
			inGraph:           *oneNodeNoRelationsGraph,
			inFrom:            0,
			inTo:              0,
			wantPathExistence: false,
			wantAnErr:         false,
		},
		{
			name:              "success, one node one self relation graph",
			inGraph:           *oneNodeSelfRelationGraph,
			inFrom:            0,
			inTo:              0,
			wantPathExistence: true,
			wantAnErr:         false,
		},
		{
			name:              "success, one step path in unconnected graph",
			inGraph:           *unconnectedGraph,
			inFrom:            0,
			inTo:              1,
			wantPathExistence: true,
			wantAnErr:         false,
		},
		{
			name:              "success, two step path to same node in unconnected graph",
			inGraph:           *unconnectedGraph,
			inFrom:            2,
			inTo:              2,
			wantPathExistence: true,
			wantAnErr:         false,
		},
		{
			name:              "success, no path to other node with the same value",
			inGraph:           *unconnectedGraph,
			inFrom:            0,
			inTo:              2,
			wantPathExistence: false,
			wantAnErr:         false,
		},

		{
			name:              "success, multi step path exists in a connected acyclic graph",
			inGraph:           *connectedAcyclicGraph,
			inFrom:            2,
			inTo:              3,
			wantPathExistence: true,
			wantAnErr:         false,
		},
		{
			name:              "success, no path exists in the opposite direction in a connected acyclic graph",
			inGraph:           *connectedAcyclicGraph,
			inFrom:            3,
			inTo:              2,
			wantPathExistence: false,
			wantAnErr:         false,
		},

		{
			name:              "success, multi step path exists in a connected cyclic graph",
			inGraph:           *connectedCyclicGraph,
			inFrom:            2,
			inTo:              1,
			wantPathExistence: true,
			wantAnErr:         false,
		},
		{
			name:              "success, path exists in a complete graph",
			inGraph:           *maximalGraph,
			inFrom:            3,
			inTo:              1,
			wantPathExistence: true,
			wantAnErr:         false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualExistence, actualErr := DirectedRouteExists(testCase.inGraph, testCase.inFrom, testCase.inTo)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualExistence, testCase.wantPathExistence) {
				t.Errorf("expected path to exist: %#v, got: %#v", testCase.wantPathExistence, actualExistence)
			}
		})
	}
}
