package route

/*
	Route between nodes:
	Given a directed graph, design an algorithm to find out whether there is a route between two nodes
*/
import (
	"fmt"

	"gitlab.com/ogv/golang-coding-interview/03_stacks_queues/types"
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/directed"
)

// DirectedRouteExists returns true if there is a path in the directed graph
// between the nodes given by the two indices
// It returns an error if one of the indices is outside the range [0, graph nodes count -1].
func DirectedRouteExists[E any](graph directed.Graph[E], from int, to int) (bool, error) {
	/*
		Worst-case complexity:
			- space O(graph nodes aka vertices)
			- time O(graph nodes aka vertices)
	*/
	if from < 0 || from >= len(graph.Nodes) {
		return false, fmt.Errorf("from node index %d out of range", from)
	}
	if to < 0 || to >= len(graph.Nodes) {
		return false, fmt.Errorf("to node index %d out of range", to)
	}

	nodesReached := map[int]struct{}{}
	nodesToReach := types.NewSliceQueue([]int{})
	for _, neighbour := range graph.Nodes[from].Neighbours {
		nodesToReach.Add(neighbour)
	}

	for !nodesToReach.IsEmpty() {
		entry, _ := nodesToReach.Remove()
		asNodeIndex := entry
		if asNodeIndex == to {
			return true, nil
		}

		nodesReached[asNodeIndex] = struct{}{}
		for _, neighbour := range graph.Nodes[asNodeIndex].Neighbours {
			if _, ok := nodesReached[neighbour]; !ok {
				nodesToReach.Add(neighbour)
			}
		}
	}

	return false, nil
}
