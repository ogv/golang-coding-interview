package ancestor

import (
	"testing"

	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestLowest(t *testing.T) {
	/*
			 _______5_______
			|               |
		    1__           __9
			   |         |
			 __3__	   __7__
		    |     |   |     |
		    2     4   6     8
	*/
	t1RootNode := &tree.BinaryTreeNodeWithParent[int]{Value: 5}
	t1RootNode.SetLeft(1)
	t1RootNode.LeftChild.SetRight(3)
	t1RootNode.LeftChild.RightChild.SetLeft(2)
	t1RootNode.LeftChild.RightChild.SetRight(4)
	t1RootNode.SetRight(9)
	t1RootNode.RightChild.SetLeft(7)
	t1RootNode.RightChild.LeftChild.SetLeft(6)
	t1RootNode.RightChild.LeftChild.SetRight(8)

	/*
			 _______5
			|
		    1__
			   |
			   3
	*/
	t2RootNode := &tree.BinaryTreeNodeWithParent[int]{Value: 5}
	t2RootNode.SetLeft(1)
	t2RootNode.LeftChild.SetRight(3)

	var testCases = []struct {
		name         string
		inNodeA      *tree.BinaryTreeNodeWithParent[int]
		inNodeB      *tree.BinaryTreeNodeWithParent[int]
		wantAncestor *tree.BinaryTreeNodeWithParent[int]
	}{
		{
			name:         "nil nodes",
			inNodeA:      nil,
			inNodeB:      nil,
			wantAncestor: nil,
		},
		{
			name:         "node A nil",
			inNodeA:      nil,
			inNodeB:      t1RootNode.LeftChild,
			wantAncestor: nil,
		},
		{
			name:         "node B nil",
			inNodeA:      t1RootNode.LeftChild,
			inNodeB:      nil,
			wantAncestor: nil,
		},
		{
			name:         "root node and itself",
			inNodeA:      t1RootNode,
			inNodeB:      t1RootNode,
			wantAncestor: t1RootNode,
		},
		{
			name:         "same inner node",
			inNodeA:      t1RootNode.LeftChild,
			inNodeB:      t1RootNode.LeftChild,
			wantAncestor: t1RootNode.LeftChild,
		},
		{
			name:         "same leaf node",
			inNodeA:      t1RootNode.LeftChild.RightChild.LeftChild,
			inNodeB:      t1RootNode.LeftChild.RightChild.LeftChild,
			wantAncestor: t1RootNode.LeftChild.RightChild.LeftChild,
		},
		{
			name:         "lowest ancestor of far away leaves is root",
			inNodeA:      t1RootNode.LeftChild.RightChild.LeftChild,
			inNodeB:      t1RootNode.RightChild.LeftChild.RightChild,
			wantAncestor: t1RootNode,
		},
		{
			name:         "lowest ancestor of far away inner nodes is root",
			inNodeA:      t1RootNode.LeftChild.RightChild,
			inNodeB:      t1RootNode.RightChild.LeftChild,
			wantAncestor: t1RootNode,
		},
		{
			name:         "siblings have parent as lowest common ancestor",
			inNodeA:      t1RootNode.LeftChild.RightChild.LeftChild,
			inNodeB:      t1RootNode.LeftChild.RightChild.RightChild,
			wantAncestor: t1RootNode.LeftChild.RightChild,
		},
		{
			name:         "nodes on different levels with root ancestor, first deeper",
			inNodeA:      t1RootNode.RightChild.LeftChild.RightChild,
			inNodeB:      t1RootNode.LeftChild,
			wantAncestor: t1RootNode,
		},
		{
			name:         "nodes on different levels with root ancestor, second deeper",
			inNodeA:      t1RootNode.LeftChild,
			inNodeB:      t1RootNode.RightChild.LeftChild.RightChild,
			wantAncestor: t1RootNode,
		},
		{
			name:         "different trees",
			inNodeA:      t1RootNode.LeftChild,
			inNodeB:      t2RootNode.LeftChild,
			wantAncestor: nil,
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualAncestor := LowestWithParent(testCase.inNodeA, testCase.inNodeB)
			if testCase.wantAncestor != actualAncestor {
				t.Errorf("expected lowest common ancestor: %#v but was: %#v", testCase.wantAncestor, actualAncestor)
			}
		})
	}
}
