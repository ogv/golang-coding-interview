package ancestor

import (
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

// LowestWithParent returns the lowest common ancestors of two nodes from a binary tree
// If any of the nodes is nil or the nodes are from different trees, it returns nil
// Note that if one node is an ancestor of the other, the first node is their common ancestor, NOT the first node's parent
// In particular, the lowest common ancestor of a node and itself is that node.
func LowestWithParent[E any](a, b *tree.BinaryTreeNodeWithParent[E]) *tree.BinaryTreeNodeWithParent[E] {
	/*
		Worst-case complexity:
			- time O(depth of a + depth of b in tree), at most O(a's tree node count) + O(b's tree node count)
			- space O(1)
		Scenario: the nodes are in linear tree(s) (the nodes form a list, either all of them left children or right children)
	*/
	if a == nil || b == nil {
		return nil
	}
	if a == b {
		return a
	}

	aDepth := nodeWithParentDepth(a)
	bDepth := nodeWithParentDepth(b)
	if aDepth < bDepth {
		aDepth, bDepth = bDepth, aDepth
		a, b = b, a
	}
	for aDepth > bDepth {
		a = a.Parent
		aDepth--
	}

	for a != b {
		a = a.Parent
		b = b.Parent
	}

	return a
}

func nodeWithParentDepth[E any](node *tree.BinaryTreeNodeWithParent[E]) int {
	depth := 0
	for ancestor := node.Parent; ancestor != nil; ancestor = ancestor.Parent {
		depth++
	}
	return depth
}
