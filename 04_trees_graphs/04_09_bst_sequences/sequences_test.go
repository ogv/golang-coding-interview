package sequences

import (
	"reflect"
	"sort"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/00_utils/orderutils"
	balanced "gitlab.com/ogv/golang-coding-interview/04_trees_graphs/04_02_balanced_tree"
	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestBstSequences(t *testing.T) {
	emptyTree, err := balanced.NewBinarySearchTreeWithValues(
		orderutils.Ascending[int],
		[]int{})
	if err != nil {
		t.Fatalf("could not create search tree: %s", err)
	}

	valueA := 160
	oneNodeTree, err := balanced.NewBinarySearchTreeWithValues(
		orderutils.Ascending[int],
		[]int{valueA})
	if err != nil {
		t.Fatalf("could not create search tree: %s", err)
	}

	valueB := 4
	leftChildTree, err := balanced.NewBinarySearchTreeWithValues(
		orderutils.Ascending[int],
		[]int{valueB, valueA})
	if err != nil {
		t.Fatalf("could not create search tree: %s", err)
	}

	valueC := 201
	rightChildTree, err := balanced.NewBinarySearchTreeWithValues(
		orderutils.Ascending[int],
		[]int{valueA, valueC})
	if err != nil {
		t.Fatalf("could not create search tree: %s", err)
	}

	rootTwoChildrenTree, err := balanced.NewBinarySearchTreeWithValues(
		orderutils.Ascending[int],
		[]int{valueB, valueA, valueC})
	if err != nil {
		t.Fatalf("could not create search tree: %s", err)
	}

	valueD := 302
	fourNodesTree, err := balanced.NewBinarySearchTreeWithValues(
		orderutils.Ascending[int],
		[]int{valueB, valueA, valueC, valueD})
	if err != nil {
		t.Fatalf("could not create search tree: %s", err)
	}

	valueE := 450
	fiveNodesTree, err := balanced.NewBinarySearchTreeWithValues(
		orderutils.Ascending[int],
		[]int{valueB, valueA, valueC, valueD, valueE})
	if err != nil {
		t.Fatalf("could not create search tree: %s", err)
	}

	testCases := []struct {
		name          string
		inBst         tree.BinarySearchTree[int]
		wantSequences [][]int
	}{
		{
			name:  "empty",
			inBst: *emptyTree,
			wantSequences: [][]int{
				{},
			},
		},
		{
			name:  "root only",
			inBst: *oneNodeTree,
			wantSequences: [][]int{
				{valueA},
			},
		},
		{
			name:  "root and left child",
			inBst: *leftChildTree,
			wantSequences: [][]int{
				{valueA, valueB},
			},
		},
		{
			name:  "root and right child",
			inBst: *rightChildTree,
			wantSequences: [][]int{
				{valueC, valueA},
			},
		},
		{
			name:  "root and two children",
			inBst: *rootTwoChildrenTree,
			wantSequences: [][]int{
				{valueA, valueC, valueB},
				{valueA, valueB, valueC},
			},
		},
		{
			name:  "four nodes",
			inBst: *fourNodesTree,
			/*
					 ____C____
					|		  |
				 ___A		  D
				|
				B
			*/
			wantSequences: [][]int{
				{valueC, valueA, valueB, valueD},
				{valueC, valueD, valueA, valueB},
				{valueC, valueA, valueD, valueB},
			},
		},
		{
			name:  "five nodes",
			inBst: *fiveNodesTree,
			/*
					 ____C____
					|		  |
				 ___A	   ___E
				|		  |
				B		  D
			*/
			wantSequences: [][]int{
				{valueC, valueA, valueB, valueE, valueD},
				{valueC, valueE, valueD, valueA, valueB},
				{valueC, valueA, valueE, valueB, valueD},
				{valueC, valueA, valueE, valueD, valueB},
				{valueC, valueE, valueA, valueB, valueD},
				{valueC, valueE, valueA, valueD, valueB},
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(ft *testing.T) {
			ft.Parallel()
			actualSequences := BstSequences(testCase.inBst)
			if len(actualSequences) != len(testCase.wantSequences) {
				ft.Errorf("expected %d results but got %d", len(testCase.wantSequences), len(actualSequences))
			}
			sort.Slice(testCase.wantSequences,
				sequenceLexicographicallyLess(testCase.wantSequences, orderutils.Ascending[int]),
			)
			sort.Slice(actualSequences,
				sequenceLexicographicallyLess(actualSequences, orderutils.Ascending[int]),
			)
			if !reflect.DeepEqual(actualSequences, testCase.wantSequences) {
				ft.Errorf("expected sequence: %#v but was: %#v", testCase.wantSequences, actualSequences)
			}
		})
	}
}

func sequenceLexicographicallyLess(in [][]int, comparator orderutils.ValueComparator[int]) func(int, int) bool {
	return func(i int, j int) bool {
		for elementsIndex := 0; elementsIndex < len(in[i]) && elementsIndex < len(in[j]); elementsIndex++ {
			comparisonResult := comparator(in[i][elementsIndex], in[j][elementsIndex])
			if comparisonResult != 0 {
				return comparisonResult < 0
			}
		}

		return len(in[i]) < len(in[j])
	}
}
