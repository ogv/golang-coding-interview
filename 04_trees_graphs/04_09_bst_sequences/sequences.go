package sequences

import "gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"

// BstSequences receives a binary search tree with distinct elements,
// and it returns all possible arrays of values that could have led to this tree
// by traversing through an array from left to right and inserting each element.
func BstSequences[E any](searchTree tree.BinarySearchTree[E]) [][]E {
	return generateBstSequences(searchTree.RootNode)
}

func generateBstSequences[E any](rootNode *tree.BinaryTreeNode[E]) [][]E {
	if rootNode == nil {
		return [][]E{
			{},
		}
	}

	result := make([][]E, 0)
	prefix := []E{rootNode.Value}
	leftSequences := generateBstSequences(rootNode.LeftChild)
	rightSequences := generateBstSequences(rootNode.RightChild)

	for _, leftSequence := range leftSequences {
		for _, rightSequence := range rightSequences {
			weavedSequences := new([][]E)
			weaveLists(leftSequence, rightSequence, prefix, weavedSequences)
			result = append(result, *weavedSequences...)
		}
	}
	return result
}

func weaveLists[S ~[]E, E any](first, second, prefix S, result *[]S) {
	// One list is empty. Add the other list to a copy of the prefix
	if len(first) == 0 || len(second) == 0 {
		singleWeave := make([]E, len(prefix))
		copy(singleWeave, prefix)
		singleWeave = append(singleWeave, first...)
		singleWeave = append(singleWeave, second...)
		*result = append(*result, singleWeave)
		return
	}

	// Recurse with head of first moved to the prefix.
	// Removing the head changes first, so we'll add the head back where it was afterwards
	firstCopy := first[:]
	firstHead := first[0]
	first = first[1:]
	prefix = append(prefix, firstHead)
	weaveLists(first, second, prefix, result)
	prefix[len(prefix)-1] = *new(E)
	prefix = prefix[:len(prefix)-1]
	first = firstCopy

	// Recurse with head of second moved to the prefix.
	// Removing the head changes second, so we'll add the head back where it was afterwards
	secondHead := second[0]
	second = second[1:]
	prefix = append(prefix, secondHead)
	weaveLists(first, second, prefix, result)
	prefix[len(prefix)-1] = *new(E)
}
