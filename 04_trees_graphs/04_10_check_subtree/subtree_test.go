package subtree

import (
	"testing"

	"gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"
)

func TestContains(t *testing.T) {
	emptyNode := tree.BinaryTreeNode[int]{}

	multiLevelTreeA := newMultiLevelTree()
	leafA := multiLevelTreeA.RightChild.LeftChild.RightChild
	innerATwoChildren := multiLevelTreeA.RightChild.LeftChild
	innerASingleChild := multiLevelTreeA.RightChild.RightChild

	multiLevelTreeB := newMultiLevelTree()
	leafB := multiLevelTreeB.RightChild.LeftChild.RightChild
	innerBTwoChildren := multiLevelTreeB.RightChild.LeftChild
	innerBSingleChild := multiLevelTreeB.RightChild.RightChild

	testCases := []struct {
		name         string
		inA          *tree.BinaryTreeNode[int]
		inB          *tree.BinaryTreeNode[int]
		wantContains bool
	}{
		{
			name:         "nil input",
			inA:          nil,
			inB:          nil,
			wantContains: true,
		},
		{
			name:         "empty node and nil",
			inA:          &emptyNode,
			inB:          nil,
			wantContains: true,
		},
		{
			name:         "root node and nil",
			inA:          multiLevelTreeA,
			inB:          nil,
			wantContains: true,
		},
		{
			name:         "both trees with the same empty node",
			inA:          &emptyNode,
			inB:          &emptyNode,
			wantContains: true,
		},
		{
			name:         "trees with one empty node",
			inA:          &tree.BinaryTreeNode[int]{},
			inB:          &tree.BinaryTreeNode[int]{},
			wantContains: true,
		},
		{
			name:         "trees with one identical value",
			inA:          &tree.BinaryTreeNode[int]{Value: -14},
			inB:          &tree.BinaryTreeNode[int]{Value: -14},
			wantContains: true,
		},
		{
			name:         "trees with different values",
			inA:          &tree.BinaryTreeNode[int]{Value: -14},
			inB:          &tree.BinaryTreeNode[int]{Value: -15},
			wantContains: false,
		},
		{
			name:         "root of the same tree",
			inA:          multiLevelTreeA,
			inB:          multiLevelTreeA,
			wantContains: true,
		},
		{
			name:         "inner node with children of the same tree",
			inA:          innerATwoChildren,
			inB:          innerATwoChildren,
			wantContains: true,
		},
		{
			name:         "inner nodes single child of the same tree",
			inA:          innerASingleChild,
			inB:          innerASingleChild,
			wantContains: true,
		},
		{
			name:         "same leaves of the same tree",
			inA:          leafA,
			inB:          leafA,
			wantContains: true,
		},

		{
			name:         "roots of identical trees",
			inA:          multiLevelTreeA,
			inB:          multiLevelTreeB,
			wantContains: true,
		},
		{
			name:         "identical inner nodes with children of identical trees",
			inA:          innerATwoChildren,
			inB:          innerBTwoChildren,
			wantContains: true,
		},
		{
			name:         "identical inner nodes with single child of identical tree",
			inA:          innerASingleChild,
			inB:          innerBSingleChild,
			wantContains: true,
		},
		{
			name:         "identical leaves of identical trees",
			inA:          leafA,
			inB:          leafB,
			wantContains: true,
		},

		{
			name:         "identical inner 2 children node of identical tree",
			inA:          multiLevelTreeA,
			inB:          innerBTwoChildren,
			wantContains: true,
		},
		{
			name:         "swapped identical inner 2 children node of identical tree",
			inA:          innerBTwoChildren,
			inB:          multiLevelTreeA,
			wantContains: false,
		},
		{
			name:         "identical inner 1 child subtree of identical tree",
			inA:          multiLevelTreeA,
			inB:          innerBSingleChild,
			wantContains: true,
		},
		{
			name:         "swapped identical inner 1 child subtree of identical tree",
			inA:          innerBSingleChild,
			inB:          multiLevelTreeA,
			wantContains: false,
		},
		{
			name:         "identical leaf subtree of identical tree",
			inA:          multiLevelTreeA,
			inB:          leafB,
			wantContains: true,
		},
		{
			name:         "swapped identical leaf subtree of identical tree",
			inA:          leafB,
			inB:          multiLevelTreeA,
			wantContains: false,
		},
		{
			name:         "identical perfect subtrees",
			inA:          multiLevelTreeA,
			inB:          multiLevelTreeB,
			wantContains: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			gotContains := Contains(testCase.inA, testCase.inB)
			if gotContains != testCase.wantContains {
				pt.Errorf("want Contains(%+v, %+v): %t got: %t", testCase.inA, testCase.inB, testCase.wantContains, gotContains)
			}
		})
	}
}

/*
 *			  ___________827___________
 *			 |                         |
 *		 ____27_____			  ____832____
 *		|			|            |		     |
 *	 __3__		  __243__	   _827_	    853__
 *	|	  |      |       |	  |     |	         |
 *	1     9      81     729  827   832          901__
 *				 	       	   		 	             |
 *			             	                       -818
 */
func newMultiLevelTree() *tree.BinaryTreeNode[int] {
	return &tree.BinaryTreeNode[int]{Value: 827,
		LeftChild: &tree.BinaryTreeNode[int]{Value: 27,
			LeftChild: &tree.BinaryTreeNode[int]{Value: 3,
				LeftChild:  &tree.BinaryTreeNode[int]{Value: 1},
				RightChild: &tree.BinaryTreeNode[int]{Value: 9},
			},
			RightChild: &tree.BinaryTreeNode[int]{Value: 243,
				LeftChild:  &tree.BinaryTreeNode[int]{Value: 81},
				RightChild: &tree.BinaryTreeNode[int]{Value: 729},
			},
		},
		RightChild: &tree.BinaryTreeNode[int]{Value: 832,
			LeftChild: &tree.BinaryTreeNode[int]{Value: 827,
				LeftChild:  &tree.BinaryTreeNode[int]{Value: 827},
				RightChild: &tree.BinaryTreeNode[int]{Value: 832},
			},
			RightChild: &tree.BinaryTreeNode[int]{Value: 853,
				RightChild: &tree.BinaryTreeNode[int]{Value: 901,
					RightChild: &tree.BinaryTreeNode[int]{Value: -818},
				},
			},
		},
	}
}
