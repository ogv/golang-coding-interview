package subtree

import "gitlab.com/ogv/golang-coding-interview/04_trees_graphs/tree"

// Contains returns true if and only if the tree rooted in the first argument
// includes the entire tree rooted at the second argument (it has a subtree with the same structure and values).
// This means either the second node appears in the first node's tree (even as the root),
// or a tree identical in structure and values to the second one's appears in the first node's tree.
func Contains[E comparable](a, b *tree.BinaryTreeNode[E]) bool {
	if treeIsEmpty(b) {
		return true
	}
	return firstContainsSecond(a, b)
}

func treeIsEmpty[E any](n *tree.BinaryTreeNode[E]) bool {
	return n == nil
}

func firstContainsSecond[E comparable](a, b *tree.BinaryTreeNode[E]) bool {
	switch {
	case sameNode(a, b):
		return true
	case treeIsEmpty(a):
		return treeIsEmpty(b)
	case structureAndValuesMatch(a, b):
		return true
	default:
		return firstContainsSecond(a.LeftChild, b) || firstContainsSecond(a.RightChild, b)
	}
}

func sameNode[E any](a, b *tree.BinaryTreeNode[E]) bool {
	return a == b
}

func structureAndValuesMatch[E comparable](a, b *tree.BinaryTreeNode[E]) bool {
	switch {
	case atLeastOneTreeEmpty(a, b):
		return false
	case valuesAreDifferent(a, b):
		return false
	default:
		return firstContainsSecond(a.LeftChild, b.LeftChild) && firstContainsSecond(a.RightChild, b.RightChild)
	}
}

func atLeastOneTreeEmpty[E any](a, b *tree.BinaryTreeNode[E]) bool {
	return treeIsEmpty(a) || treeIsEmpty(b)
}

func valuesAreDifferent[E comparable](a, b *tree.BinaryTreeNode[E]) bool {
	return a.Value != b.Value
}
