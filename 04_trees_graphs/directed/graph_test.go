package directed

import (
	"reflect"
	"testing"
)

func TestNewGraph(t *testing.T) {
	node0 := GraphNode[any]{Value: 21, Neighbours: nil}
	node1 := GraphNode[any]{Value: "A string value", Neighbours: nil}
	node2 := GraphNode[any]{Value: 21, Neighbours: nil}
	node3 := GraphNode[any]{Value: +32 - 1i, Neighbours: nil}

	node0.Neighbours = []int{2, 3}
	node1.Neighbours = []int{1}
	node3.Neighbours = []int{2}

	isleNode0 := GraphNode[any]{Value: 21, Neighbours: nil}
	isleNode1 := GraphNode[any]{Value: "A string value", Neighbours: nil}
	isleNode2 := GraphNode[any]{Value: 21, Neighbours: nil}
	isleNode3 := GraphNode[any]{Value: +32 - 1i, Neighbours: nil}

	isleNode0.Neighbours = []int{0}
	isleNode1.Neighbours = []int{1}
	isleNode2.Neighbours = []int{2}
	isleNode3.Neighbours = []int{3}

	productNode0 := GraphNode[any]{Value: 21, Neighbours: nil}
	productNode1 := GraphNode[any]{Value: "A string value", Neighbours: nil}
	productNode2 := GraphNode[any]{Value: 21, Neighbours: nil}
	productNode3 := GraphNode[any]{Value: +32 - 1i, Neighbours: nil}

	productNode0.Neighbours = []int{0, 1, 2, 3}
	productNode1.Neighbours = []int{0, 1, 2, 3}
	productNode2.Neighbours = []int{0, 1, 2, 3}
	productNode3.Neighbours = []int{0, 1, 2, 3}

	testCases := []struct {
		name        string
		inValues    []any
		inRelations []IntPair
		wantGraph   *Graph[any]
		wantAnErr   bool
	}{
		{
			name:        "success, nil inputs creates the empty graph",
			inValues:    nil,
			inRelations: nil,
			wantGraph:   &Graph[any]{},
			wantAnErr:   false,
		},
		{
			name:        "success, empty inputs creates the empty graph",
			inValues:    []any{},
			inRelations: []IntPair{},
			wantGraph:   &Graph[any]{},
			wantAnErr:   false,
		},
		{
			name:        "success, values with nil relations creates a nodes-only graph",
			inValues:    []any{21, "A string value", +32 - 1i},
			inRelations: []IntPair{},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 21, Neighbours: nil},
					{Value: "A string value", Neighbours: nil},
					{Value: +32 - 1i, Neighbours: nil},
				},
			},
			wantAnErr: false,
		},
		{
			name:        "success, values with duplicate and nil relations creates a nodes-only graph",
			inValues:    []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 21, Neighbours: nil},
					{Value: "A string value", Neighbours: nil},
					{Value: 21, Neighbours: nil},
					{Value: +32 - 1i, Neighbours: nil},
				},
			},
			wantAnErr: false,
		},

		{
			name:     "success, values with duplicated values and some relations creates a directed graph",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 0, To: 2},
				{From: 0, To: 3},
				{From: 1, To: 1},
				{From: 3, To: 2},
			},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					node0,
					node1,
					node2,
					node3,
				},
			},
			wantAnErr: false,
		},
		{
			name:     "success, every node in relation to itself only creates an <<archipelago>> directed graph",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 0, To: 0},
				{From: 1, To: 1},
				{From: 2, To: 2},
				{From: 3, To: 3},
			},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					isleNode0,
					isleNode1,
					isleNode2,
					isleNode3,
				},
			},
			wantAnErr: false,
		},
		{
			name:     "success, every node in relation to every other node creates a <<cartesian product>> directed graph",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 0, To: 0},
				{From: 0, To: 1},
				{From: 0, To: 2},
				{From: 0, To: 3},
				{From: 1, To: 0},
				{From: 1, To: 1},
				{From: 1, To: 2},
				{From: 1, To: 3},
				{From: 2, To: 0},
				{From: 2, To: 1},
				{From: 2, To: 2},
				{From: 2, To: 3},
				{From: 3, To: 0},
				{From: 3, To: 1},
				{From: 3, To: 2},
				{From: 3, To: 3},
			},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					productNode0,
					productNode1,
					productNode2,
					productNode3,
				},
			},
			wantAnErr: false,
		},

		{
			name:     "failure, nil values and one relation",
			inValues: nil,
			inRelations: []IntPair{
				{From: 0, To: 0},
			},
			wantGraph: nil,
			wantAnErr: true,
		},
		{
			name:     "failure, empty values and one relation",
			inValues: []any{},
			inRelations: []IntPair{
				{From: 0, To: 0},
			},
			wantGraph: nil,
			wantAnErr: true,
		},
		{
			name:     "failure, relation with negative From",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 2, To: 3},
				{From: -1, To: 0},
			},
			wantGraph: nil,
			wantAnErr: true,
		},
		{
			name:     "failure, relation with negative To",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 2, To: 3},
				{From: 0, To: -1},
			},
			wantGraph: nil,
			wantAnErr: true,
		},
		{
			name:     "failure, relation with From = length of values",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 2, To: 3},
				{From: 4, To: 0},
			},
			wantGraph: nil,
			wantAnErr: true,
		},
		{
			name:     "failure, relation with To = length of values",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 2, To: 3},
				{From: 0, To: 4},
			},
			wantGraph: nil,
			wantAnErr: true,
		},
		{
			name:     "failure, relation with From > length of values",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 2, To: 3},
				{From: 7, To: 0},
			},
			wantGraph: nil,
			wantAnErr: true,
		},
		{
			name:     "failure, relation with To > length of values",
			inValues: []any{21, "A string value", 21, +32 - 1i},
			inRelations: []IntPair{
				{From: 2, To: 3},
				{From: 0, To: 8},
			},
			wantGraph: nil,
			wantAnErr: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualGraph, actualErr := NewGraph(testCase.inValues, testCase.inRelations)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Fatalf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualGraph, testCase.wantGraph) {
				t.Fatalf("expected to build graph %#v but got %#v", testCase.wantGraph, actualGraph)
			}
		})
	}
}

func TestGraphCopy(t *testing.T) {
	testCases := []struct {
		name      string
		inGraph   *Graph[any]
		wantGraph *Graph[any]
	}{
		{
			name:      "nil graph",
			inGraph:   nil,
			wantGraph: nil,
		},
		{
			name:      "empty graph",
			inGraph:   &Graph[any]{},
			wantGraph: &Graph[any]{},
		},
		{
			name: "graph with empty nodes",
			inGraph: &Graph[any]{
				Nodes: []GraphNode[any]{},
			},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{},
			},
		},
		{
			name: "one node with nil relations",
			inGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 14, Neighbours: nil},
				},
			},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 14, Neighbours: nil},
				},
			},
		},
		{
			name: "one node with empty relations",
			inGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 15, Neighbours: []int{}},
				},
			},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 15, Neighbours: []int{}},
				},
			},
		},
		{
			name: "graph with nil relations",
			inGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 21, Neighbours: nil},
					{Value: "A string value", Neighbours: nil},
					{Value: +32 - 1i, Neighbours: nil},
				},
			},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 21, Neighbours: nil},
					{Value: "A string value", Neighbours: nil},
					{Value: +32 - 1i, Neighbours: nil},
				},
			},
		},
		{
			name: "graph with duplicate values and nil relations",
			inGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 21, Neighbours: nil},
					{Value: "A string value", Neighbours: nil},
					{Value: 21, Neighbours: nil},
					{Value: +32 - 1i, Neighbours: nil},
				},
			},
			wantGraph: &Graph[any]{
				Nodes: []GraphNode[any]{
					{Value: 21, Neighbours: nil},
					{Value: "A string value", Neighbours: nil},
					{Value: 21, Neighbours: nil},
					{Value: +32 - 1i, Neighbours: nil},
				},
			},
		},

		{
			name:      "graph with duplicated values and some relations",
			inGraph:   newGraphWithNeighbours(),
			wantGraph: newGraphWithNeighbours(),
		},
		{
			name:      "graph with isle nodes",
			inGraph:   newIsleGraph(),
			wantGraph: newIsleGraph(),
		},
		{
			name:      "graph with all the edges",
			inGraph:   newFullGraph(),
			wantGraph: newFullGraph(),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actualGraph := GraphCopy(testCase.inGraph)
			if !reflect.DeepEqual(actualGraph, testCase.wantGraph) {
				t.Fatalf("expected copy to be graph %#v but got %#v", testCase.wantGraph, actualGraph)
			}

			if actualGraph == nil {
				return
			}

			// Make sure the copy doesn't share memory with the original
			if len(actualGraph.Nodes) >= 1 && (&actualGraph.Nodes[0] == &testCase.inGraph.Nodes[0]) {
				t.Fatalf("expected copy graph NOT to share the slice of nodes, but it does")
			}
			for index, node := range actualGraph.Nodes {
				originalNode := testCase.inGraph.Nodes[index]
				if len(node.Neighbours) >= 1 && (&node.Neighbours[0] == &originalNode.Neighbours[0]) {
					t.Fatalf("expected copy graph NOT to share the slice of neighbours, but node at %d does", index)
				}
			}
		})
	}
}

func newGraphWithNeighbours() *Graph[any] {
	node0 := GraphNode[any]{Value: 21, Neighbours: nil}
	node1 := GraphNode[any]{Value: "A string value", Neighbours: nil}
	node2 := GraphNode[any]{Value: 21, Neighbours: nil}
	node3 := GraphNode[any]{Value: +32 - 1i, Neighbours: nil}

	node0.Neighbours = []int{2, 3}
	node1.Neighbours = []int{1}
	node3.Neighbours = []int{2}

	return &Graph[any]{
		Nodes: []GraphNode[any]{
			node0,
			node1,
			node2,
			node3,
		},
	}
}

func newIsleGraph() *Graph[any] {
	isleNode0 := GraphNode[any]{Value: 21, Neighbours: nil}
	isleNode1 := GraphNode[any]{Value: "A string value", Neighbours: nil}
	isleNode2 := GraphNode[any]{Value: 21, Neighbours: nil}
	isleNode3 := GraphNode[any]{Value: +32 - 1i, Neighbours: nil}

	isleNode0.Neighbours = []int{0}
	isleNode1.Neighbours = []int{1}
	isleNode2.Neighbours = []int{2}
	isleNode3.Neighbours = []int{3}

	return &Graph[any]{
		Nodes: []GraphNode[any]{
			isleNode0,
			isleNode1,
			isleNode2,
			isleNode3,
		},
	}
}

func newFullGraph() *Graph[any] {
	productNode0 := GraphNode[any]{Value: 21, Neighbours: nil}
	productNode1 := GraphNode[any]{Value: "A string value", Neighbours: nil}
	productNode2 := GraphNode[any]{Value: 21, Neighbours: nil}
	productNode3 := GraphNode[any]{Value: +32 - 1i, Neighbours: nil}

	productNode0.Neighbours = []int{0, 1, 2, 3}
	productNode1.Neighbours = []int{0, 1, 2, 3}
	productNode2.Neighbours = []int{0, 1, 2, 3}
	productNode3.Neighbours = []int{0, 1, 2, 3}

	return &Graph[any]{
		Nodes: []GraphNode[any]{
			productNode0,
			productNode1,
			productNode2,
			productNode3,
		},
	}
}
