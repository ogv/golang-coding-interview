package directed

import (
	"fmt"
	"slices"
)

// Graph is the representation of a binary relation that's not necessarily symmetric.
type Graph[E any] struct {
	Nodes []GraphNode[E]
}

// GraphNode is a node in a directed graph that can store any value.
type GraphNode[E any] struct {
	Value      E
	Neighbours []int // the neighbour's index inside the graph's slice of nodes
}

// NewGraph returns a directed graph containing one node per input value,
// whose relations are between nodes with values given by each ordered pair (From, To)
// It returns an error if any relation refers to an index outside the range [0...len(values)-1].
func NewGraph[S ~[]E, E any](values S, relations []IntPair) (*Graph[E], error) {
	result := new(Graph[E])

	for _, value := range values {
		result.Nodes = append(result.Nodes, GraphNode[E]{Value: value})
	}

	for _, relation := range relations {
		if relation.From < 0 || relation.From >= len(result.Nodes) {
			return nil, fmt.Errorf("from index out of range %d", relation.From)
		}
		if relation.To < 0 || relation.To >= len(result.Nodes) {
			return nil, fmt.Errorf("to index out of range %d", relation.To)
		}

		result.Nodes[relation.From].Neighbours = append(result.Nodes[relation.From].Neighbours, relation.To)
	}

	return result, nil
}

// IntPair is simply a pair of integers.
type IntPair struct {
	From int
	To   int
}

// GraphCopy makes a deep copy of the graph argument that shares no memory with the original.
// Note that even if they share no "structural" memory,
// changes to a node value in a graph might be visible to its copy and vice versa.
// Example: if the value stored in a graph node is or has a pointer (like a slice)
// and the value pointed by it changes, it will be visible in both the copy and the original.
func GraphCopy[E any](graph *Graph[E]) *Graph[E] {
	if graph == nil {
		return nil
	}

	result := new(Graph[E])
	if graph.Nodes != nil {
		result.Nodes = make([]GraphNode[E], len(graph.Nodes))
	}

	for index, node := range graph.Nodes {
		result.Nodes[index].Value = node.Value
		if node.Neighbours != nil {
			result.Nodes[index].Neighbours = slices.Clone(node.Neighbours)
		}
	}

	return result
}
