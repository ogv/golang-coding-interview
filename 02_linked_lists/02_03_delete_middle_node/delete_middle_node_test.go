package delmid

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

func TestDeleteMiddleNode(t *testing.T) {
	testCases := []struct {
		name      string
		inList    *single.List[any]
		wantList  *single.List[any]
		wantAnErr bool
	}{
		{
			name:      "error, empty list",
			inList:    single.NewList[any](nil),
			wantList:  single.NewList[any](nil),
			wantAnErr: true,
		},
		{
			name:      "error, one element int list",
			inList:    single.NewList([]any{12}),
			wantList:  single.NewList([]any{12}),
			wantAnErr: true,
		},
		{
			name:      "error, two elements int list",
			inList:    single.NewList([]any{12, -5}),
			wantList:  single.NewList([]any{12, -5}),
			wantAnErr: true,
		},
		{
			name:      "success, odd length element int list",
			inList:    single.NewList([]any{4, 53, 12}),
			wantList:  single.NewList([]any{4, 12}),
			wantAnErr: false,
		},
		{
			name:      "success, even length element int list",
			inList:    single.NewList([]any{92, 4, 53, 12}),
			wantList:  single.NewList([]any{92, 53, 12}),
			wantAnErr: false,
		},
		{
			name:      "success, longer odd length element int list",
			inList:    single.NewList([]any{4, 53, 12, 843, 649, 23, 64}),
			wantList:  single.NewList([]any{4, 53, 12, 649, 23, 64}),
			wantAnErr: false,
		},
		{
			name:      "success,longer even length element int list",
			inList:    single.NewList([]any{92, 4, 53, 12, 22, 65, 93, 43}),
			wantList:  single.NewList([]any{92, 4, 53, 22, 65, 93, 43}),
			wantAnErr: false,
		},

		{
			name:      "error, one element string list",
			inList:    single.NewList([]any{"12"}),
			wantList:  single.NewList([]any{"12"}),
			wantAnErr: true,
		},
		{
			name:      "error, two elements string list",
			inList:    single.NewList([]any{"12", "-5"}),
			wantList:  single.NewList([]any{"12", "-5"}),
			wantAnErr: true,
		},
		{
			name:      "success, odd length element string list",
			inList:    single.NewList([]any{"4", "53", "12"}),
			wantList:  single.NewList([]any{"4", "12"}),
			wantAnErr: false,
		},
		{
			name:      "success, even length element string list",
			inList:    single.NewList([]any{"92", "4", "53", "12"}),
			wantList:  single.NewList([]any{"92", "53", "12"}),
			wantAnErr: false,
		},
		{
			name:      "success, longer odd length element string list",
			inList:    single.NewList([]any{"4", "53", "12", "843", "649", "23", "64"}),
			wantList:  single.NewList([]any{"4", "53", "12", "649", "23", "64"}),
			wantAnErr: false,
		},
		{
			name:      "success,longer even length element string list",
			inList:    single.NewList([]any{"92", "4", "53", "12", "22", "65", "93", "43"}),
			wantList:  single.NewList([]any{"92", "4", "53", "22", "65", "93", "43"}),
			wantAnErr: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualErr := DeleteMiddleNode(testCase.inList)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(testCase.inList, testCase.wantList) {
				t.Errorf("for %v, after removal of the middle element expected %v but got %v", testCase.inList, testCase.wantList, testCase.inList)
			}
		})
	}
}
