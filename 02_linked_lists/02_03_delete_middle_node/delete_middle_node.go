package delmid

import (
	"fmt"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

// DeleteMiddleNode removes the middle element from the singly linked list
// If the list has an even number of elements n = 2*k, it removes element k
// If it has an odd number of elements n = 2*k + 1, it removes element k+1
// It returns an error if the list has less than 3 elements (it doesn't have a middle, only 0 to 2 ends).
func DeleteMiddleNode[E any](a *single.List[E]) error {
	length := a.Len()
	if length <= 2 {
		return fmt.Errorf("list too short: %d", length)
	}

	previous, current := (*single.Node[E])(nil), a.Head
	for currentIndex := 1; currentIndex <= (length-1)/2; currentIndex++ {
		previous, current = current, current.Next
	}

	previous.Next = current.Next
	current.Next = nil
	a.Length--
	return nil
}
