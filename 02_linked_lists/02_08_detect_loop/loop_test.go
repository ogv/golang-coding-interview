package loop

import (
	"testing"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

func TestIsInList(t *testing.T) {
	oneElement := single.NewList([]any{323})

	oneElementLoop := single.Copy(oneElement)
	oneElementLoop.Tail.Next = oneElementLoop.Head

	twoElementsSlice := []any{192, 345}
	twoElementLoop := single.NewList(twoElementsSlice)
	twoElementLoop.Tail.Next = twoElementLoop.Head

	twoElementTailLoop := single.NewList(twoElementsSlice)
	twoElementTailLoop.Tail.Next = twoElementTailLoop.Tail

	nonEmptySlice := []any{2, 4, -21, "a string", -5.35, "another string"}
	fromSecondNodeLoop := single.NewList(nonEmptySlice)
	fromSecondNodeLoop.Tail.Next = fromSecondNodeLoop.Head.Next

	fromThirdNodeLoop := single.NewList(nonEmptySlice)
	fromThirdNodeLoop.Tail.Next = fromThirdNodeLoop.Head.Next.Next

	fromFourthNodeLoop := single.NewList(nonEmptySlice)
	fromFourthNodeLoop.Tail.Next = fromFourthNodeLoop.Head.Next.Next.Next

	fromFifthNodeLoop := single.NewList(nonEmptySlice)
	fromFifthNodeLoop.Tail.Next = fromFifthNodeLoop.Head.Next.Next.Next.Next

	fromSixthNodeLoop := single.NewList(nonEmptySlice)
	fromSixthNodeLoop.Tail.Next = fromSixthNodeLoop.Head.Next.Next.Next.Next.Next

	testCases := []struct {
		name    string
		inListA *single.List[any]
		wantOut bool
	}{
		{
			name:    "nil list has no loop",
			inListA: nil,
			wantOut: false,
		},

		{
			name:    "empty list has no loop",
			inListA: single.NewList[any](nil),
			wantOut: false,
		},
		{
			name:    "no loop in one element list",
			inListA: oneElement,
			wantOut: false,
		},
		{
			name:    "loop in one element list",
			inListA: oneElementLoop,
			wantOut: true,
		},
		{
			name:    "loop in two element list to head",
			inListA: twoElementLoop,
			wantOut: true,
		},
		{
			name:    "loop in two element list to tail",
			inListA: twoElementTailLoop,
			wantOut: true,
		},

		{
			name:    "loop from second list element",
			inListA: fromSecondNodeLoop,
			wantOut: true,
		},
		{
			name:    "loop from third list element",
			inListA: fromThirdNodeLoop,
			wantOut: true,
		},
		{
			name:    "loop from fourth list element",
			inListA: fromFourthNodeLoop,
			wantOut: true,
		},
		{
			name:    "loop from fifth list element",
			inListA: fromFifthNodeLoop,
			wantOut: true,
		},
		{
			name:    "loop from sixth list element",
			inListA: fromSixthNodeLoop,
			wantOut: true,
		},
		{
			name:    "no loop in longer odd length list",
			inListA: single.NewList([]any{11, 54, 3, 2, -4}),
			wantOut: false,
		},
		{
			name:    "no loop in longer even length list",
			inListA: single.NewList([]any{11, 54, 2, -4}),
			wantOut: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := InLinkedList(testCase.inListA)
			if actualOut != testCase.wantOut {
				t.Errorf("expected %v but got %v", testCase.wantOut, actualOut)
			}
		})
	}
}

func TestFirstNodeInLoop(t *testing.T) {
	oneElement := single.NewList([]any{323})

	oneElementLoop := single.Copy(oneElement)
	oneElementLoop.Tail.Next = oneElementLoop.Head

	twoElementsSlice := []any{192, 345}
	twoElementLoop := single.NewList(twoElementsSlice)
	twoElementLoop.Tail.Next = twoElementLoop.Head

	twoElementTailLoop := single.NewList(twoElementsSlice)
	twoElementTailLoop.Tail.Next = twoElementTailLoop.Tail

	nonEmptySlice := []any{2, 4, -21, "a string", -5.35, "another string"}
	fromSecondNodeLoop := single.NewList(nonEmptySlice)
	fromSecondNodeLoop.Tail.Next = fromSecondNodeLoop.Head.Next

	fromThirdNodeLoop := single.NewList(nonEmptySlice)
	fromThirdNodeLoop.Tail.Next = fromThirdNodeLoop.Head.Next.Next

	fromFourthNodeLoop := single.NewList(nonEmptySlice)
	fromFourthNodeLoop.Tail.Next = fromFourthNodeLoop.Head.Next.Next.Next

	fromFifthNodeLoop := single.NewList(nonEmptySlice)
	fromFifthNodeLoop.Tail.Next = fromFifthNodeLoop.Head.Next.Next.Next.Next

	fromSixthNodeLoop := single.NewList(nonEmptySlice)
	fromSixthNodeLoop.Tail.Next = fromSixthNodeLoop.Head.Next.Next.Next.Next.Next

	testCases := []struct {
		name    string
		inListA *single.List[any]
		wantOut *single.Node[any]
	}{
		{
			name:    "nil list has no loop",
			inListA: nil,
			wantOut: nil,
		},

		{
			name:    "empty list has no loop",
			inListA: single.NewList[any](nil),
			wantOut: nil,
		},
		{
			name:    "no loop in one element list",
			inListA: oneElement,
			wantOut: nil,
		},
		{
			name:    "loop in one element list",
			inListA: oneElementLoop,
			wantOut: oneElementLoop.Head,
		},
		{
			name:    "loop in two element list to head",
			inListA: twoElementLoop,
			wantOut: twoElementLoop.Head,
		},
		{
			name:    "loop in two element list to tail",
			inListA: twoElementTailLoop,
			wantOut: twoElementTailLoop.Tail,
		},

		{
			name:    "loop from second list element",
			inListA: fromSecondNodeLoop,
			wantOut: fromSecondNodeLoop.Head.Next,
		},
		{
			name:    "loop from third list element",
			inListA: fromThirdNodeLoop,
			wantOut: fromThirdNodeLoop.Head.Next.Next,
		},
		{
			name:    "loop from fourth list element",
			inListA: fromFourthNodeLoop,
			wantOut: fromFourthNodeLoop.Head.Next.Next.Next,
		},
		{
			name:    "loop from fifth list element",
			inListA: fromFifthNodeLoop,
			wantOut: fromFifthNodeLoop.Head.Next.Next.Next.Next,
		},
		{
			name:    "loop from sixth list element",
			inListA: fromSixthNodeLoop,
			wantOut: fromSixthNodeLoop.Head.Next.Next.Next.Next.Next,
		},

		{
			name:    "no loop in longer odd length list",
			inListA: single.NewList([]any{11, 54, 3, 2, -4}),
			wantOut: nil,
		},
		{
			name:    "no loop in longer even length list",
			inListA: single.NewList([]any{11, 54, 2, -4}),
			wantOut: nil,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := FirstNodeInLoop(testCase.inListA)
			if actualOut != testCase.wantOut {
				t.Errorf("expected %v but got %v", testCase.wantOut, actualOut)
			}
		})
	}
}
