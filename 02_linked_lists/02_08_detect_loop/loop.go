package loop

import (
	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

// InLinkedList returns true if and only if the singly linked list includes a loop.
func InLinkedList[E any](a *single.List[E]) bool {
	/*
		The list has a loop if and only if there exists a node in the loop
		Worst-case complexity:
			- time O(length(a))
			- space O(1)
	*/
	return FirstNodeInLoop(a) != nil
}

// FirstNodeInLoop returns the first node that is part of a loop inside the linked list
// (the list's tail points to it), or nil.
func FirstNodeInLoop[E any](a *single.List[E]) *single.Node[E] {
	/*
		Go over the list with a slow and a fast pointer.
		For each step the slow pointer takes, the fast one takes two steps.
		If the fast pointer runs into the slow one, the list has a loop.
		Otherwise (the fast pointer gets to the end of the list), it doesn't.
		Worst-case complexity:
			- time O(length(a))
			- space O(1)
	*/
	if a == nil {
		return nil
	}

	slowNode, fastNode := a.Head, a.Head
	for fastNode != nil {
		slowNode = slowNode.Next
		fastNode = fastNode.Next
		if fastNode != nil {
			fastNode = fastNode.Next
			if fastNode == slowNode {
				break
			}
		}
	}

	if fastNode != nil {
		slowNode = a.Head
		for slowNode != fastNode {
			slowNode = slowNode.Next
			fastNode = fastNode.Next
		}
		return slowNode
	}
	return nil
}
