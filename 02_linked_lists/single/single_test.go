package single

import (
	"maps"
	"reflect"
	"slices"
	"testing"
)

func TestList_Len(t *testing.T) {
	testCases := []struct {
		name    string
		inList  *List[any]
		wantLen int
	}{
		{
			name:    "nil list",
			inList:  nil,
			wantLen: 0,
		},
		{
			name:    "empty list",
			inList:  NewList([]any{}),
			wantLen: 0,
		},
		{
			name:    "list of nil",
			inList:  NewList([]any{nil}),
			wantLen: 1,
		},
		{
			name:    "list of nils",
			inList:  NewList([]any{nil, nil}),
			wantLen: 2,
		},
		{
			name:    "list of values",
			inList:  NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
			wantLen: 4,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualLen := testCase.inList.Len()
			if actualLen != testCase.wantLen {
				pt.Errorf("expected len to be: %#v but was: %#v", testCase.wantLen, actualLen)
			}
		})
	}
}

func TestCopy(t *testing.T) {
	testCases := []struct {
		name     string
		inList   *List[any]
		wantCopy *List[any]
	}{
		{
			name:     "nil list",
			inList:   nil,
			wantCopy: nil,
		},
		{
			name:     "empty list",
			inList:   NewList([]any{}),
			wantCopy: NewList([]any{}),
		},
		{
			name:     "list of nil",
			inList:   NewList([]any{nil}),
			wantCopy: NewList([]any{nil}),
		},
		{
			name:     "list of nils",
			inList:   NewList([]any{nil, nil}),
			wantCopy: NewList([]any{nil, nil}),
		},
		{
			name:     "list of values",
			inList:   NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
			wantCopy: NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualCopy := Copy(testCase.inList)
			if !reflect.DeepEqual(actualCopy, testCase.wantCopy) {
				pt.Errorf("expected copy to be: %#v but was: %#v", testCase.wantCopy, actualCopy)
			}
		})
	}
}

func TestList_AddFront(t *testing.T) {
	testCases := []struct {
		name       string
		inList     *List[any]
		inElement  any
		wantResult *List[any]
	}{
		{
			name:       "empty list, nil element",
			inList:     NewList([]any{}),
			inElement:  nil,
			wantResult: NewList([]any{nil}),
		},
		{
			name:       "empty list, non-nil element",
			inList:     NewList([]any{}),
			inElement:  26,
			wantResult: NewList([]any{26}),
		},
		{
			name:       "one element list, nil element",
			inList:     NewList([]any{31}),
			inElement:  nil,
			wantResult: NewList([]any{nil, 31}),
		},
		{
			name:       "one element list, non-nil element",
			inList:     NewList([]any{31}),
			inElement:  53,
			wantResult: NewList([]any{53, 31}),
		},
		{
			name:       "multi element list, nil element",
			inList:     NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
			inElement:  nil,
			wantResult: NewList([]any{nil, 73, 2.e+3, 15 - 1i, "the end"}),
		},
		{
			name:       "multi element list, non-nil element",
			inList:     NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
			inElement:  92,
			wantResult: NewList([]any{92, 73, 2.e+3, 15 - 1i, "the end"}),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualResult := testCase.inList.AddFront(testCase.inElement)
			if !reflect.DeepEqual(actualResult, testCase.wantResult) {
				pt.Errorf("expected result to be: %#v but was: %#v", testCase.wantResult, actualResult)
			}
		})
	}
}

func TestList_AddBack(t *testing.T) {
	testCases := []struct {
		name       string
		inList     *List[any]
		inElement  any
		wantResult *List[any]
	}{
		{
			name:       "empty list, nil element",
			inList:     NewList([]any{}),
			inElement:  nil,
			wantResult: NewList([]any{nil}),
		},
		{
			name:       "empty list, non-nil element",
			inList:     NewList([]any{}),
			inElement:  26,
			wantResult: NewList([]any{26}),
		},
		{
			name:       "one element list, nil element",
			inList:     NewList([]any{31}),
			inElement:  nil,
			wantResult: NewList([]any{31, nil}),
		},
		{
			name:       "one element list, non-nil element",
			inList:     NewList([]any{31}),
			inElement:  53,
			wantResult: NewList([]any{31, 53}),
		},
		{
			name:       "multi element list, nil element",
			inList:     NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
			inElement:  nil,
			wantResult: NewList([]any{73, 2.e+3, 15 - 1i, "the end", nil}),
		},
		{
			name:       "multi element list, non-nil element",
			inList:     NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
			inElement:  92,
			wantResult: NewList([]any{73, 2.e+3, 15 - 1i, "the end", 92}),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualResult := testCase.inList.AddBack(testCase.inElement)
			if !reflect.DeepEqual(actualResult, testCase.wantResult) {
				pt.Errorf("expected result to be: %#v but was: %#v", testCase.wantResult, actualResult)
			}
		})
	}
}

func TestList_String(t *testing.T) {
	testCases := []struct {
		name       string
		inList     List[any]
		wantString string
	}{
		{
			name:       "empty list literal",
			inList:     List[any]{},
			wantString: "[]",
		},
		{
			name:       "empty list",
			inList:     *NewList([]any{}),
			wantString: "[]",
		},
		{
			name:       "list of nil",
			inList:     *NewList([]any{nil}),
			wantString: "[<nil>]",
		},
		{
			name:       "list of nils",
			inList:     *NewList([]any{nil, nil}),
			wantString: "[<nil>, <nil>]",
		},
		{
			name:       "list of values",
			inList:     *NewList([]any{73, nil, 2.5767e+3, 15 - 1i, "the end"}),
			wantString: "[73, <nil>, 2576.7, (15-1i), the end]",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualString := testCase.inList.String()
			if actualString != testCase.wantString {
				pt.Errorf("expected list representation to be: %#v but was: %#v", testCase.wantString, actualString)
			}
		})
	}
}

func TestNode_String(t *testing.T) {
	testCases := []struct {
		name       string
		inNode     Node[any]
		wantString string
	}{
		{
			name:       "empty node",
			inNode:     Node[any]{},
			wantString: "<nil>",
		},
		{
			name: "element with integer",
			inNode: Node[any]{
				Value: -723,
			},
			wantString: "-723",
		},
		{
			name: "element with string",
			inNode: Node[any]{
				Value: "A 5+ß little here.",
			},
			wantString: "A 5+ß little here.",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actualLen := testCase.inNode.String()
			if actualLen != testCase.wantString {
				pt.Errorf("expected node representation to be: %#v but was: %#v", testCase.wantString, actualLen)
			}
		})
	}
}

func TestList_All(t *testing.T) {
	testCases := []struct {
		name        string
		inList      *List[any]
		wantEntries map[int]any
	}{
		{
			name:        "nil list",
			inList:      nil,
			wantEntries: map[int]any{},
		},
		{
			name:        "empty list",
			inList:      NewList([]any{}),
			wantEntries: map[int]any{},
		},
		{
			name:   "list of nil",
			inList: NewList([]any{nil}),
			wantEntries: map[int]any{
				0: nil,
			},
		},
		{
			name:   "list of nils",
			inList: NewList([]any{nil, nil}),
			wantEntries: map[int]any{
				0: nil,
				1: nil,
			},
		},
		{
			name:   "list of single value",
			inList: NewList([]any{26}),
			wantEntries: map[int]any{
				0: 26,
			},
		},
		{
			name:   "list of values",
			inList: NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
			wantEntries: map[int]any{
				0: 73,
				1: 2.e+3,
				2: 15 - 1i,
				3: "the end",
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			gotEntries := make(map[int]any)
			previousI := -1
			for i, value := range testCase.inList.All() {
				if i != previousI+1 {
					pt.Errorf("expected index to be: %v but was: %v", previousI+1, i)
				}
				gotEntries[i] = value
				previousI++
			}
			if !maps.Equal(gotEntries, testCase.wantEntries) {
				pt.Errorf("expected entries to be: %#v but was: %#v", testCase.wantEntries, gotEntries)
			}
		})
	}
}

func TestList_All_Searching(t *testing.T) {
	testCases := []searchSeq2TestCase[any, int]{
		{
			name:       "nil, not found",
			inList:     nil,
			inTarget:   4,
			wantValues: nil,
		},
		{
			name:       "empty list, not found",
			inList:     NewList([]any{}),
			inTarget:   7,
			wantValues: map[int]any{},
		},
		{
			name:     "list of nil, not found",
			inList:   NewList([]any{nil}),
			inTarget: 8,
			wantValues: map[int]any{
				0: nil,
			},
		},
		{
			name:     "list of nil, value found",
			inList:   NewList([]any{nil}),
			inTarget: nil,
			wantValues: map[int]any{
				0: nil,
			},
		},
		{
			name:     "list of nils, not found",
			inList:   NewList([]any{nil, nil}),
			inTarget: 9,
			wantValues: map[int]any{
				0: nil,
				1: nil,
			},
		},
		{
			name:     "list of nils, value found",
			inList:   NewList([]any{nil, nil}),
			inTarget: nil,
			wantValues: map[int]any{
				0: nil,
			},
		},
		{
			name:     "list of single value, not found",
			inList:   NewList([]any{26}),
			inTarget: -26,
			wantValues: map[int]any{
				0: 26,
			},
		},
		{
			name:     "list of single value, value found",
			inList:   NewList([]any{26}),
			inTarget: 26,
			wantValues: map[int]any{
				0: 26,
			},
		},
		{
			name:     "list of values, not found",
			inList:   NewList([]any{73, 2.e+3, 15 - 2i, "the end"}),
			inTarget: nil,
			wantValues: map[int]any{
				0: 73,
				1: 2.e+3,
				2: 15 - 2i,
				3: "the end",
			},
		},
		{
			name:     "list of values, first value found",
			inList:   NewList([]any{73, 2.e+3, 15 - 2i, "the end"}),
			inTarget: 73,
			wantValues: map[int]any{
				0: 73,
			},
		},
		{
			name:     "list of values, inner value found",
			inList:   NewList([]any{73, 2.e+3, 15 - 2i, "the end"}),
			inTarget: 15 - 2i,
			wantValues: map[int]any{
				0: 73,
				1: 2.e+3,
				2: 15 - 2i,
			},
		},
		{
			name:     "list of values, last value found",
			inList:   NewList([]any{73, 2.e+3, 15 - 2i, "the end"}),
			inTarget: "the end",
			wantValues: map[int]any{
				0: 73,
				1: 2.e+3,
				2: 15 - 2i,
				3: "the end",
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			gotSearchedValues := make(map[int]any)

			currentI := 0
			valueSearcher := func(i int, value any) bool {
				if i != currentI {
					pt.Errorf("expected index to be: %v but was: %v", currentI+1, i)
				}
				gotSearchedValues[i] = value
				currentI++
				return value != testCase.inTarget
			}
			allSeq := testCase.inList.All()
			allSeq(valueSearcher)

			if !maps.Equal(testCase.wantValues, gotSearchedValues) {
				pt.Errorf("expected searched values to be %v but got %v", testCase.wantValues, gotSearchedValues)
			}
		})
	}
}

type searchSeq2TestCase[E, K comparable] struct {
	name       string
	inList     *List[E]
	inTarget   E
	wantValues map[K]E
}

func TestList_Values(t *testing.T) {
	testCases := []struct {
		name       string
		inList     *List[any]
		wantValues []any
	}{
		{
			name:       "nil list",
			inList:     nil,
			wantValues: []any{},
		},
		{
			name:       "empty list",
			inList:     NewList([]any{}),
			wantValues: []any{},
		},
		{
			name:       "list of nil",
			inList:     NewList([]any{nil}),
			wantValues: []any{nil},
		},
		{
			name:       "list of nils",
			inList:     NewList([]any{nil, nil}),
			wantValues: []any{nil, nil},
		},
		{
			name:       "list of single value",
			inList:     NewList([]any{26}),
			wantValues: []any{26},
		},
		{
			name:       "list of values",
			inList:     NewList([]any{73, 2.e+3, 15 - 1i, "the end"}),
			wantValues: []any{73, 2.e+3, 15 - 1i, "the end"},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotValues []any
			for value := range testCase.inList.Values() {
				gotValues = append(gotValues, value)
			}
			if !slices.Equal(gotValues, testCase.wantValues) {
				pt.Errorf("expected values to be: %#v but was: %#v", testCase.wantValues, gotValues)
			}
		})
	}
}

func TestList_Values_Searching(t *testing.T) {
	testCases := []searchSeqTestCase[any]{
		{
			name:       "nil, not found",
			inList:     nil,
			inTarget:   4,
			wantValues: nil,
		},
		{
			name:       "empty list, not found",
			inList:     NewList([]any{}),
			inTarget:   7,
			wantValues: []any{},
		},
		{
			name:       "list of nil, not found",
			inList:     NewList([]any{nil}),
			inTarget:   8,
			wantValues: []any{nil},
		},
		{
			name:       "list of nil, value found",
			inList:     NewList([]any{nil}),
			inTarget:   nil,
			wantValues: []any{nil},
		},
		{
			name:       "list of nils, not found",
			inList:     NewList([]any{nil, nil}),
			inTarget:   9,
			wantValues: []any{nil, nil},
		},
		{
			name:       "list of nils, value found",
			inList:     NewList([]any{nil, nil}),
			inTarget:   nil,
			wantValues: []any{nil},
		},
		{
			name:       "list of single value, not found",
			inList:     NewList([]any{26}),
			inTarget:   -26,
			wantValues: []any{26},
		},
		{
			name:       "list of single value, value found",
			inList:     NewList([]any{26}),
			inTarget:   26,
			wantValues: []any{26},
		},
		{
			name:       "list of values, not found",
			inList:     NewList([]any{73, 2.e+3, 15 - 2i, "the end"}),
			inTarget:   nil,
			wantValues: []any{73, 2.e+3, 15 - 2i, "the end"},
		},
		{
			name:       "list of values, first value found",
			inList:     NewList([]any{73, 2.e+3, 15 - 2i, "the end"}),
			inTarget:   73,
			wantValues: []any{73},
		},
		{
			name:       "list of values, inner value found",
			inList:     NewList([]any{73, 2.e+3, 15 - 2i, "the end"}),
			inTarget:   15 - 2i,
			wantValues: []any{73, 2.e+3, 15 - 2i},
		},
		{
			name:       "list of values, last value found",
			inList:     NewList([]any{73, 2.e+3, 15 - 2i, "the end"}),
			inTarget:   "the end",
			wantValues: []any{73, 2.e+3, 15 - 2i, "the end"},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var gotSearchedValues []any
			valueSearcher := func(value any) bool {
				gotSearchedValues = append(gotSearchedValues, value)
				return value != testCase.inTarget
			}
			valuesSeq := testCase.inList.Values()
			valuesSeq(valueSearcher)

			if !slices.Equal(testCase.wantValues, gotSearchedValues) {
				pt.Errorf("expected searched values to be %v but got %v", testCase.wantValues, gotSearchedValues)
			}
		})
	}
}

type searchSeqTestCase[E any] struct {
	name       string
	inList     *List[E]
	inTarget   E
	wantValues []E
}
