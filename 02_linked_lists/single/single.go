package single

import (
	"fmt"
	"iter"
	"strings"
)

// Node is a node in a singly linked list.
type Node[E any] struct {
	Value E
	Next  *Node[E]
}

// String satisfies the fmt.Stringer interface.
func (n Node[E]) String() string {
	return fmt.Sprintf("%v", n.Value)
}

// List is a singly linked list.
type List[E any] struct {
	Head, Tail *Node[E]
	Length     int
}

// String satisfies the fmt.Stringer interface.
func (sl List[E]) String() string {
	builder := strings.Builder{}
	builder.WriteRune('[')
	for node := sl.Head; node != nil; node = node.Next {
		if builder.Len() >= 2 {
			builder.WriteString(", ")
		}
		builder.WriteString(node.String())
	}
	builder.WriteRune(']')

	return builder.String()
}

// NewList creates a singly linked list
// whose nodes store the input values in the order they are given.
func NewList[E any](values []E) *List[E] {
	result := new(List[E])
	for _, value := range values {
		result = result.AddBack(value)
	}

	return result
}

// Copy creates a singly linked list
// that has the same values as the list parameter
// It creates a "shallow" copy of the values of the list parameter
// For a nil list, it returns a nil result.
func Copy[E any](s *List[E]) *List[E] {
	if s == nil {
		return nil
	}

	result := new(List[E])
	for node := s.Head; node != nil; node = node.Next {
		result = result.AddBack(node.Value)
	}

	return result
}

// AddFront adds a node with the given value at the front of the singly linked list.
func (sl *List[E]) AddFront(value E) *List[E] {
	node := &Node[E]{Value: value}
	if sl.Tail == nil {
		sl.Head, sl.Tail = node, node
	} else {
		node.Next = sl.Head
		sl.Head = node
	}
	sl.Length++

	return sl
}

// AddBack adds a node with the given value at the end of the singly linked list.
func (sl *List[E]) AddBack(value E) *List[E] {
	node := &Node[E]{Value: value}
	if sl.Tail == nil {
		sl.Head, sl.Tail = node, node
	} else {
		sl.Tail.Next = node
		sl.Tail = node
	}
	sl.Length++

	return sl
}

// Len returns the number of elements in the list in constant time.
func (sl *List[E]) Len() int {
	if sl == nil {
		return 0
	}
	return sl.Length
}

// All returns an iterator over the index-value pairs in the list in the usual order.
func (sl *List[E]) All() iter.Seq2[int, E] {
	return func(yield func(int, E) bool) {
		if sl == nil {
			return
		}
		for i, current := 0, sl.Head; current != nil; i, current = i+1, current.Next {
			if !yield(i, current.Value) {
				return
			}
		}
	}
}

// Values returns an iterator over the elements in the list in order.
func (sl *List[E]) Values() iter.Seq[E] {
	return func(yield func(E) bool) {
		if sl == nil {
			return
		}
		for current := sl.Head; current != nil; current = current.Next {
			if !yield(current.Value) {
				return
			}
		}
	}
}
