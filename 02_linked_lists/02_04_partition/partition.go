package partition

import (
	"cmp"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

// ByValue orders the elements of the list around a pivot value,
// so that all the nodes less than the pivot
// come before all the nodes greater than or equal to it.
// If the pivot is contained within the list,
// it only needs to be after the elements less than it.
// It can appear anywhere in the "greater than or equal" part;
// it does not need to appear between the parts.
func ByValue[E cmp.Ordered](sl *single.List[E], pivot E) {
	// Link the elements less than the pivot, and the elements greater than the pivot,
	// then link the two parts together, and update the list's head and tail
	if sl == nil {
		return
	}

	firstLtPivot, lastLtPivot := (*single.Node[E])(nil), (*single.Node[E])(nil)
	firstGtePivot, lastGtePivot := (*single.Node[E])(nil), (*single.Node[E])(nil)

	for currentNode := sl.Head; currentNode != nil; currentNode = currentNode.Next {
		if currentNode.Value >= pivot {
			if firstGtePivot == nil {
				firstGtePivot = currentNode
				lastGtePivot = currentNode
			} else {
				lastGtePivot.Next = currentNode
				lastGtePivot = currentNode
			}
		} else {
			if firstLtPivot == nil {
				firstLtPivot = currentNode
				lastLtPivot = currentNode
			} else {
				lastLtPivot.Next = currentNode
				lastLtPivot = currentNode
			}
		}
	}

	if firstLtPivot == nil {
		sl.Head = firstGtePivot
	} else {
		sl.Head = firstLtPivot
		lastLtPivot.Next = firstGtePivot
	}
	if firstGtePivot == nil {
		sl.Tail = lastLtPivot
	} else {
		sl.Tail = lastGtePivot
		sl.Tail.Next = nil
	}
}

// ByIntB is another implementation of partitioning elements by a pivot
// See the documentation of ByInt for specification details.
func ByIntB[E cmp.Ordered](sl *single.List[E], pivot E) {
	// Keep the elements less than the pivot in sl,
	// the ones greater than or equal to it in a different list,
	// then add the second list at the end of the first (if the first has an element),
	// or set the list's head and tail to the second list's head and tail
	if sl == nil {
		return
	}

	gteList := single.List[E]{}

	for previous, current := (*single.Node[E])(nil), sl.Head; current != nil; {
		if current.Value >= pivot {
			gteList.AddBack(current.Value)

			if previous != nil {
				previous.Next = current.Next
			}
			if sl.Head == current {
				sl.Head = current.Next
			}
			if sl.Tail == current {
				sl.Tail = previous
			}
			sl.Length--

			current = current.Next
		} else {
			previous = current
			current = current.Next
		}
	}

	if sl.Len() == 0 {
		*sl = gteList
	} else if gteList.Len() > 0 {
		sl.Tail.Next = gteList.Head
		sl.Tail = gteList.Tail
		sl.Length += gteList.Length
	}
}
