package partition

import (
	"cmp"
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

func BenchmarkByValue_1000_Ordered(b *testing.B) {
	inList := newOrderedIntList(1_000)
	benchmarkIntPartitioner(b, ByValue, inList)
}

func newOrderedIntList(len int) *single.List[int] {
	values := make([]int, len)
	for i := 0; i < len; i++ {
		values[i] = i
	}
	return single.NewList(values)
}

func benchmarkIntPartitioner(b *testing.B, partitioner func(*single.List[int], int), sl *single.List[int]) {
	for i := 0; i < b.N; i++ {
		partitioner(sl, sl.Len()/2)
	}
}

func BenchmarkByValueB_1000_Ordered(b *testing.B) {
	inList := newOrderedIntList(1_000)
	benchmarkIntPartitioner(b, ByIntB, inList)
}

func BenchmarkByValue_1000_ReverseOrdered(b *testing.B) {
	inList := newReverseOrderedIntList(1_000)
	benchmarkIntPartitioner(b, ByValue, inList)
}

func newReverseOrderedIntList(len int) *single.List[int] {
	values := make([]int, len)
	for i := 0; i < len; i++ {
		values[i] = len - 1 - i
	}
	return single.NewList(values)
}

func BenchmarkByValueB_1000_ReverseOrdered(b *testing.B) {
	inList := newReverseOrderedIntList(1_000)
	benchmarkIntPartitioner(b, ByIntB, inList)
}

func TestByValue(t *testing.T) {
	for _, testCase := range generateByValueIntTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			ByValue(testCase.inList, testCase.inPivot)
			if !reflect.DeepEqual(testCase.inList, testCase.wantList) {
				t.Errorf("for %v and pivot %d, after partitioning expected %v but got %v", testCase.inList, testCase.inPivot, testCase.wantList, testCase.inList)
			}
		})
	}
}

type byValueTestCase[E cmp.Ordered] struct {
	name     string
	inList   *single.List[E]
	inPivot  int
	wantList *single.List[E]
}

func generateByValueIntTestCases() []byValueTestCase[int] {
	return []byValueTestCase[int]{
		{
			name:     "success, nil list",
			inList:   nil,
			inPivot:  11,
			wantList: nil,
		},
		{
			name:     "success, empty list",
			inList:   single.NewList[int](nil),
			inPivot:  11,
			wantList: single.NewList[int](nil),
		},
		{
			name:     "success, pivot less than one element int list",
			inList:   single.NewList([]int{12}),
			inPivot:  11,
			wantList: single.NewList([]int{12}),
		},
		{
			name:     "success, pivot equal to one element int list",
			inList:   single.NewList([]int{12}),
			inPivot:  12,
			wantList: single.NewList([]int{12}),
		},
		{
			name:     "success, pivot greater than one element int list",
			inList:   single.NewList([]int{12}),
			inPivot:  13,
			wantList: single.NewList([]int{12}),
		},

		{
			name:     "success, pivot less than all in int list",
			inList:   single.NewList([]int{21, 12, 14}),
			inPivot:  11,
			wantList: single.NewList([]int{21, 12, 14}),
		},
		{
			name:     "success, pivot equal to all in int list",
			inList:   single.NewList([]int{12, 12, 12}),
			inPivot:  12,
			wantList: single.NewList([]int{12, 12, 12}),
		},

		{
			name:     "success, pivot inside list sorted increasingly",
			inList:   single.NewList([]int{-25, 3, 9, 12}),
			inPivot:  9,
			wantList: single.NewList([]int{-25, 3, 9, 12}),
		},

		{
			name:     "success, pivot inside list sorted decreasingly",
			inList:   single.NewList([]int{12, 9, 3, -25}),
			inPivot:  9,
			wantList: single.NewList([]int{3, -25, 12, 9}),
		},

		{
			name:     "success, pivot greater than all in int list",
			inList:   single.NewList([]int{-25, 3, 9, 12}),
			inPivot:  13,
			wantList: single.NewList([]int{-25, 3, 9, 12}),
		},
		{
			name:     "success, pivot inside, less than some in int list",
			inList:   single.NewList([]int{21, 3, 11}),
			inPivot:  11,
			wantList: single.NewList([]int{3, 21, 11}),
		},
		{
			name:     "success, pivot not inside, less than some in int list",
			inList:   single.NewList([]int{25, 3, 96, 12}),
			inPivot:  13,
			wantList: single.NewList([]int{3, 12, 25, 96}),
		},
	}
}

func TestByValueB(t *testing.T) {
	for _, testCase := range generateByValueIntTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			ByIntB(testCase.inList, testCase.inPivot)
			if !reflect.DeepEqual(testCase.inList, testCase.wantList) {
				t.Errorf("for %v and pivot %d, after partitioning expected %v but got %v", testCase.inList, testCase.inPivot, testCase.wantList, testCase.inList)
			}
		})
	}
}
