package removedups

import "gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"

// RemoveDuplicates returns a singly linked list
// that only contains the first appearance of each value from the input list
func RemoveDuplicates[E comparable](a single.List[E]) single.List[E] {
	result := new(single.List[E])
	existingNodes := map[E]struct{}{}

	for node := a.Head; node != nil; node = node.Next {
		if _, ok := existingNodes[node.Value]; !ok {
			existingNodes[node.Value] = struct{}{}
			result = result.AddBack(node.Value)
		}
	}

	return *result
}
