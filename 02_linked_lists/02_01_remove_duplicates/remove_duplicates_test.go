package removedups

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

func TestRemoveDuplicates_int(t *testing.T) {
	testCases := []struct {
		name    string
		inList  *single.List[int]
		wantOut *single.List[int]
	}{
		{
			name:    "nil list",
			inList:  single.NewList[int](nil),
			wantOut: single.NewList[int](nil),
		},
		{
			name:    "one element int list",
			inList:  single.NewList([]int{12}),
			wantOut: single.NewList([]int{12}),
		},
		{
			name:    "two identical elements int list",
			inList:  single.NewList([]int{12, 12}),
			wantOut: single.NewList([]int{12}),
		},
		{
			name:    "two distinct elements int list",
			inList:  single.NewList([]int{12, -5}),
			wantOut: single.NewList([]int{12, -5}),
		},
		{
			name:    "duplicated and unique elements int list",
			inList:  single.NewList([]int{12, -5, -5, 12, 11}),
			wantOut: single.NewList([]int{12, -5, 11}),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := RemoveDuplicates(*testCase.inList)
			if !reflect.DeepEqual(actualOut, *testCase.wantOut) {
				t.Errorf("for %v expected %v but got %v", testCase.inList, testCase.wantOut, actualOut)
			}
		})
	}
}

func TestRemoveDuplicates_string(t *testing.T) {
	testCases := []struct {
		name    string
		inList  *single.List[string]
		wantOut *single.List[string]
	}{
		{
			name:    "nil list",
			inList:  single.NewList[string](nil),
			wantOut: single.NewList[string](nil),
		},
		{
			name:    "one element string list",
			inList:  single.NewList([]string{"12"}),
			wantOut: single.NewList([]string{"12"}),
		},
		{
			name:    "two identical elements string list",
			inList:  single.NewList([]string{"12", "12"}),
			wantOut: single.NewList([]string{"12"}),
		},
		{
			name:    "two distinct elements string list",
			inList:  single.NewList([]string{"12", "-5"}),
			wantOut: single.NewList([]string{"12", "-5"}),
		},
		{
			name:    "duplicated and unique elements string list",
			inList:  single.NewList([]string{"12", "-5", "-5", "12", "11"}),
			wantOut: single.NewList([]string{"12", "-5", "11"}),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := RemoveDuplicates(*testCase.inList)
			if !reflect.DeepEqual(actualOut, *testCase.wantOut) {
				t.Errorf("for %v expected %v but got %v", testCase.inList, testCase.wantOut, actualOut)
			}
		})
	}
}
