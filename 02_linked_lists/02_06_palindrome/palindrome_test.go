package palindrome

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

func TestIsPalindrome(t *testing.T) {
	checkIsPalindrome(t, generateIsPalindromeIntTestCases())
	checkIsPalindrome(t, generateIsPalindromeStructTestCases())
}

func checkIsPalindrome[C comparable](t *testing.T, testCases []isPalindromeTestCase[C]) {
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := IsPalindrome(*testCase.inList)
			if !reflect.DeepEqual(actualOut, testCase.wantOut) {
				t.Errorf("for %v, expected %v but got %v", testCase.inList, testCase.wantOut, actualOut)
			}
		})
	}
}

type isPalindromeTestCase[T comparable] struct {
	name    string
	inList  *single.List[T]
	wantOut bool
}

func generateIsPalindromeIntTestCases() []isPalindromeTestCase[int] {
	return []isPalindromeTestCase[int]{
		{
			name:    "empty list is int palindrome",
			inList:  single.NewList([]int(nil)),
			wantOut: true,
		},
		{
			name:    "one element int list is int palindrome",
			inList:  single.NewList([]int{12}),
			wantOut: true,
		},
		{
			name:    "two element int list, not palindrome",
			inList:  single.NewList([]int{12, 20}),
			wantOut: false,
		},
		{
			name:    "two element int list, palindrome",
			inList:  single.NewList([]int{12, 12}),
			wantOut: true,
		},
		{
			name:    "longer even length int list, palindrome",
			inList:  single.NewList([]int{-15, 4, 4, -15}),
			wantOut: true,
		},
		{
			name:    "longer even length int list, not palindrome",
			inList:  single.NewList([]int{-15, 4, 5, -15}),
			wantOut: false,
		},
		{
			name:    "longer odd length int list, palindrome",
			inList:  single.NewList([]int{-15, 4, 24, 4, -15}),
			wantOut: true,
		},
		{
			name:    "longer odd length int list, inner not palindrome",
			inList:  single.NewList([]int{-15, 4, 24, 5, -15}),
			wantOut: false,
		},
		{
			name:    "longer odd length int list, outer not palindrome",
			inList:  single.NewList([]int{-15, 4, 24, 4, -22}),
			wantOut: false,
		},
	}
}

func generateIsPalindromeStructTestCases() []isPalindromeTestCase[palindromeSample] {
	return []isPalindromeTestCase[palindromeSample]{
		{
			name:    "empty structs is palindrome",
			inList:  single.NewList([]palindromeSample(nil)),
			wantOut: true,
		},
		{
			name:    "one element structs is palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: 12}}),
			wantOut: true,
		},
		{
			name:    "two element structs with different names, not palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: 12}, {name: "First", value: 12}}),
			wantOut: false,
		},
		{
			name:    "two element structs with different values, not palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: 12}, {name: "first", value: 20}}),
			wantOut: false,
		},
		{
			name:    "two element structs, palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: 12}, {name: "first", value: 12}}),
			wantOut: true,
		},
		{
			name:    "longer even length structs, palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: -15}, {name: "IN", value: 4}, {name: "IN", value: 4}, {name: "first", value: -15}}),
			wantOut: true,
		},
		{
			name:    "longer even length structs with different names, not palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: -15}, {name: "IN", value: 4}, {name: "ON", value: 5}, {name: "first", value: -15}}),
			wantOut: false,
		},
		{
			name:    "longer even length structs with different values, not palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: -15}, {name: "IN", value: 4}, {name: "IN", value: 5}, {name: "first", value: -15}}),
			wantOut: false,
		},
		{
			name:    "longer odd length structs, palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: -15}, {name: "IN", value: 4}, {name: "mid", value: 24}, {name: "IN", value: 4}, {name: "first", value: -15}}),
			wantOut: true,
		},
		{
			name:    "longer odd length structs with different names, inner not palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: -15}, {name: "IN", value: 4}, {name: "mid", value: 24}, {name: "IT", value: 4}, {name: "first", value: -15}}),
			wantOut: false,
		},
		{
			name:    "longer odd length structs with different values, inner not palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: -15}, {name: "IN", value: 4}, {name: "mid", value: 24}, {name: "IN", value: 5}, {name: "first", value: -15}}),
			wantOut: false,
		},
		{
			name:    "longer odd length structs with different names, outer not palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: -15}, {name: "IN", value: 4}, {name: "mid", value: 24}, {name: "IN", value: 4}, {name: "last", value: -15}}),
			wantOut: false,
		},
		{
			name:    "longer odd length structs with different values, outer not palindrome",
			inList:  single.NewList([]palindromeSample{{name: "first", value: -15}, {name: "IN", value: 4}, {name: "mid", value: 24}, {name: "IN", value: 4}, {name: "first", value: -22}}),
			wantOut: false,
		},
	}
}

type palindromeSample struct {
	name  string
	value int
}
