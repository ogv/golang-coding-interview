package palindrome

import (
	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

// IsPalindrome returns true if and only if the singly linked list is a palindrome
// (the values it stores are the same from head to tail and from tail to head).
func IsPalindrome[T comparable](a single.List[T]) bool {
	/*
		Worst-case complexity:
			- time O(n)
			- space O(n)
	*/
	firstHalf := make([]T, 0, a.Len()/2)
	node := a.Head
	for index := 0; index < a.Len()/2; index, node = index+1, node.Next {
		firstHalf = append(firstHalf, node.Value)
	}

	if a.Len()%2 != 0 {
		node = node.Next
	}

	for index := len(firstHalf) - 1; index >= 0; index-- {
		if node.Value != firstHalf[index] {
			return false
		}
		node = node.Next
	}

	return true
}
