package intersection

import (
	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

// ListsIntersect returns true if and only if the singly linked lists have at least one node in common
// If lists do not share nodes, they do not intersect, even if they represent the same sequence of values.
func ListsIntersect[E any](a, b *single.List[E]) bool {
	/*
		The lists intersect if and only if their last node exists and is the same one
		Worst-case complexity:
			- time O(1)
			- space O(1)
	*/
	if a == nil || a.Tail == nil || b == nil || b.Tail == nil {
		return false
	}
	return a.Tail == b.Tail
}

// ListsIntersectB is another implementation of list intersection detection
// See the documentation of ListsIntersect for details.
func ListsIntersectB[E any](a, b *single.List[E]) bool {
	/*
		The lists intersect if and only if their first common node exists
		Worst-case complexity:
			- time O(Max(length(a), length(b)))
			- space O(1)
	*/
	return FirstCommonNode(a, b) != nil
}

// FirstCommonNode returns the first node that appears in both lists, or nil if no such node exists
// If the lists do not share nodes, they do not intersect, even if they store the same sequence of values.
func FirstCommonNode[E any](a, b *single.List[E]) *single.Node[E] {
	/*
		Go over the first (length of longer - length of shorter) elements in the longer list
		Then go step by step and return the first element
		where the node in list A pointer and the list B pointer are the same
		Worst-case complexity:
			- time O(Max(length(a), length(b)))
			- space O(1)
	*/
	if a == nil || b == nil {
		return nil
	}
	if a == b {
		return a.Head
	}

	shorter, longer := a, b
	if shorter.Len() > longer.Len() {
		shorter, longer = longer, shorter
	}

	longerNode := longer.Head
	for index := 0; index < longer.Len()-shorter.Len(); index++ {
		longerNode = longerNode.Next
	}

	for shorterNode := shorter.Head; shorterNode != nil; shorterNode, longerNode = shorterNode.Next, longerNode.Next {
		if shorterNode == longerNode {
			return shorterNode
		}
	}

	return nil
}
