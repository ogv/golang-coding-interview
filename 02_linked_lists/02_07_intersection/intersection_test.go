package intersection

import (
	"testing"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

func TestListsIntersect(t *testing.T) {
	for _, testCase := range generateIntersectTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := ListsIntersect(testCase.inListA, testCase.inListB)
			if actualOut != testCase.wantOut {
				t.Errorf("for %v and %v, expected %v but got %v", testCase.inListA, testCase.inListB, testCase.wantOut, actualOut)
			}
		})
	}
}

type listsIntersectTestCase struct {
	name    string
	inListA *single.List[any]
	inListB *single.List[any]
	wantOut bool
}

func generateIntersectTestCases() []listsIntersectTestCase {
	empty := single.NewList([]any{})

	nonEmptySlice := []any{2, 4, -21, "a string", -5.35, "another string"}
	nonEmpty := single.NewList(nonEmptySlice)

	oneElement := single.NewList([]any{323})

	nonEmptyCopy := &single.List[any]{}
	*nonEmptyCopy = *nonEmpty

	longerNonEmpty := &single.List[any]{}
	*longerNonEmpty = *nonEmpty
	longerNonEmpty.AddFront("b")
	longerNonEmpty.AddFront("a")

	midIntersectionA := &single.List[any]{}
	*midIntersectionA = *nonEmpty
	midIntersectionA.AddFront("x")
	midIntersectionA.AddFront("y")

	midIntersectionB := &single.List[any]{}
	*midIntersectionB = *nonEmpty
	midIntersectionB.AddFront("z")
	midIntersectionB.AddFront("t")

	lastIntersectionA := &single.List[any]{}
	*lastIntersectionA = *oneElement
	lastIntersectionA.AddFront(83)
	lastIntersectionA.AddFront(98)

	lastIntersectionB := &single.List[any]{}
	*lastIntersectionB = *oneElement
	lastIntersectionB.AddFront(21)
	lastIntersectionB.AddFront(30)

	return []listsIntersectTestCase{
		{
			name:    "nil lists do not intersect",
			inListA: nil,
			inListB: nil,
			wantOut: false,
		},
		{
			name:    "nil list does not intersect with non-nil list",
			inListA: nil,
			inListB: oneElement,
			wantOut: false,
		},
		{
			name:    "non-nil list does not intersect with nil list",
			inListB: nil,
			inListA: oneElement,
			wantOut: false,
		},
		{
			name:    "empty lists do not intersect",
			inListA: single.NewList[any](nil),
			inListB: single.NewList[any](nil),
			wantOut: false,
		},
		{
			name:    "empty list has no node in common with non-empty list",
			inListA: single.NewList[any](nil),
			inListB: oneElement,
			wantOut: false,
		},
		{
			name:    "non-empty list has no node in common with empty list",
			inListA: oneElement,
			inListB: single.NewList[any](nil),
			wantOut: false,
		},
		{
			name:    "identical lists with separate nodes have no node in common",
			inListA: single.NewList([]any{3, 2, -4}),
			inListB: single.NewList([]any{3, 2, -4}),
			wantOut: false,
		},
		{
			name:    "identical lists with same nodes do have nodes in common",
			inListA: nonEmpty,
			inListB: nonEmptyCopy,
			wantOut: true,
		},
		{
			name:    "list and longer list with separate nodes have no node in common",
			inListA: single.NewList([]any{3, 2, -4}),
			inListB: single.NewList([]any{11, 54, 3, 2, -4}),
			wantOut: false,
		},
		{
			name:    "list and shorter list with separate nodes have no node in common",
			inListA: single.NewList([]any{11, 54, 3, 2, -4}),
			inListB: single.NewList([]any{3, 2, -4}),
			wantOut: false,
		},
		{
			name:    "list and longer list with same nodes and additional nodes in front do have nodes in common",
			inListA: nonEmpty,
			inListB: longerNonEmpty,
			wantOut: true,
		},
		{
			name:    "longer list with same nodes and additional nodes in front and list do have nodes in common",
			inListA: longerNonEmpty,
			inListB: nonEmpty,
			wantOut: true,
		},
		{
			name:    "lists with intersection inside the list do have nodes in common",
			inListA: midIntersectionA,
			inListB: midIntersectionB,
			wantOut: true,
		},
		{
			name:    "lists with intersection at the end of the list do have nodes in common",
			inListA: lastIntersectionA,
			inListB: lastIntersectionB,
			wantOut: true,
		},
		{
			name:    "same empty list does not intersect itself",
			inListA: empty,
			inListB: empty,
			wantOut: false,
		},
		{
			name:    "same non-empty list intersects itself",
			inListA: nonEmpty,
			inListB: nonEmpty,
			wantOut: true,
		},
	}
}

func TestListsIntersectB(t *testing.T) {
	for _, testCase := range generateIntersectTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := ListsIntersectB(testCase.inListA, testCase.inListB)
			if actualOut != testCase.wantOut {
				t.Errorf("for %v and %v, expected %v but got %v", testCase.inListA, testCase.inListB, testCase.wantOut, actualOut)
			}
		})
	}
}

func TestFirstCommonNode(t *testing.T) {
	empty := single.NewList([]any{})
	oneElement := single.NewList([]any{323})

	nonEmpty := single.NewList([]any{2, 4, -21, "a string", -5.35, "another string"})
	nonEmptyCopy := &single.List[any]{}
	*nonEmptyCopy = *nonEmpty

	longerNonEmpty := &single.List[any]{}
	*longerNonEmpty = *nonEmpty
	longerNonEmpty.AddFront("b")
	longerNonEmpty.AddFront("a")

	midIntersectionA := &single.List[any]{}
	*midIntersectionA = *nonEmpty
	midIntersectionNode := midIntersectionA.Head
	midIntersectionA.AddFront("x")
	midIntersectionA.AddFront("y")

	midIntersectionB := &single.List[any]{}
	*midIntersectionB = *nonEmpty
	midIntersectionB.AddFront("z")
	midIntersectionB.AddFront("t")

	lastIntersectionA := &single.List[any]{}
	*lastIntersectionA = *oneElement
	lastIntersectionA.AddFront(83)
	lastIntersectionA.AddFront(98)

	lastIntersectionB := &single.List[any]{}
	*lastIntersectionB = *oneElement
	lastIntersectionB.AddFront(21)
	lastIntersectionB.AddFront(30)

	testCases := []struct {
		name    string
		inListA *single.List[any]
		inListB *single.List[any]
		wantOut *single.Node[any]
	}{
		{
			name:    "nil lists have no common element",
			inListA: nil,
			inListB: nil,
			wantOut: nil,
		},
		{
			name:    "nil list does not intersect with non-nil list",
			inListA: nil,
			inListB: oneElement,
			wantOut: nil,
		},
		{
			name:    "nil list does not intersect with non-nil list",
			inListA: nil,
			inListB: oneElement,
			wantOut: nil,
		},

		{
			name:    "empty lists do not intersect",
			inListA: single.NewList[any](nil),
			inListB: single.NewList[any](nil),
			wantOut: nil,
		},
		{
			name:    "empty list has no node in common with non-empty list",
			inListA: single.NewList[any](nil),
			inListB: oneElement,
			wantOut: nil,
		},
		{
			name:    "non-empty list has no node in common with empty list",
			inListA: oneElement,
			inListB: single.NewList[any](nil),
			wantOut: nil,
		},
		{
			name:    "identical lists with separate nodes have no node in common",
			inListA: single.NewList([]any{3, 2, -4}),
			inListB: single.NewList([]any{3, 2, -4}),
			wantOut: nil,
		},
		{
			name:    "identical lists with same nodes do have nodes in common",
			inListA: nonEmpty,
			inListB: nonEmptyCopy,
			wantOut: nonEmptyCopy.Head,
		},
		{
			name:    "list and longer list with separate nodes have no node in common",
			inListA: single.NewList([]any{3, 2, -4}),
			inListB: single.NewList([]any{11, 54, 3, 2, -4}),
			wantOut: nil,
		},
		{
			name:    "list and shorter list with separate nodes have no node in common",
			inListA: single.NewList([]any{11, 54, 3, 2, -4}),
			inListB: single.NewList([]any{3, 2, -4}),
			wantOut: nil,
		},
		{
			name:    "list and longer list with same nodes and additional nodes in front do have nodes in common",
			inListA: nonEmpty,
			inListB: longerNonEmpty,
			wantOut: nonEmpty.Head,
		},
		{
			name:    "longer list with same nodes and additional nodes in front and list do have nodes in common",
			inListA: longerNonEmpty,
			inListB: nonEmpty,
			wantOut: nonEmpty.Head,
		},
		{
			name:    "lists with intersection inside the list do have nodes in common",
			inListA: midIntersectionA,
			inListB: midIntersectionB,
			wantOut: midIntersectionNode,
		},
		{
			name:    "lists with intersection at the end of the list do have nodes in common",
			inListA: lastIntersectionA,
			inListB: lastIntersectionB,
			wantOut: lastIntersectionA.Tail,
		},
		{
			name:    "same empty list does not intersect itself",
			inListA: empty,
			inListB: empty,
			wantOut: nil,
		},
		{
			name:    "same non-empty list intersects itself",
			inListA: nonEmpty,
			inListB: nonEmpty,
			wantOut: nonEmpty.Head,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := FirstCommonNode(testCase.inListA, testCase.inListB)
			if actualOut != testCase.wantOut {
				t.Errorf("for %v and %v, expected %v but got %v", testCase.inListA, testCase.inListB, testCase.wantOut, actualOut)
			}
		})
	}
}
