package sum

import (
	"fmt"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

// LeastSignificantFirst receives two base-10 numbers as singly lists of digits,
// and returns their sum as a singly linked list.
// Each list starts with the least significant digit of the number, and ends with the most significant one.
// And so does the returned list.
// It treats an empty list as representing the number 0
// If a value in any of the lists isn't between 0 and 9 inclusively, it returns a non-nil error.
func LeastSignificantFirst(a, b *single.List[int]) (*single.List[int], error) {
	if !onlyHasDecimalDigits(a) {
		return nil, fmt.Errorf("the first list includes a non-decimal integer value")
	}
	if !onlyHasDecimalDigits(b) {
		return nil, fmt.Errorf("the second list includes a non-decimal integer value")
	}

	if a == nil {
		return single.Copy(b), nil
	}
	if b == nil {
		return single.Copy(a), nil
	}

	result := new(single.List[int])
	carry, sum := 0, 0

	sNode, tNode := a.Head, b.Head
	for sNode != nil && tNode != nil {
		sum = carry + sNode.Value + tNode.Value
		if sum >= 10 {
			carry = 1
			sum -= 10
		} else {
			carry = 0
		}
		result.AddBack(sum)

		sNode, tNode = sNode.Next, tNode.Next
	}

	remainingNode := sNode
	if sNode == nil {
		remainingNode = tNode
	}

	for remainingNode != nil {
		sum = carry + remainingNode.Value
		if sum >= 10 {
			carry = 1
			sum -= 10
		} else {
			carry = 0
		}
		result.AddBack(sum)

		remainingNode = remainingNode.Next
	}

	if carry > 0 {
		result.AddBack(carry)
	}

	return result, nil
}

func onlyHasDecimalDigits(s *single.List[int]) bool {
	if s == nil {
		return true
	}

	for node := s.Head; node != nil; node = node.Next {
		if node.Value < 0 || node.Value > 9 {
			return false
		}
	}

	return true
}

// MostSignificantFirst receives two base-10 numbers as singly lists of digits,
// and returns their sum as a singly linked list
// Each list starts with the most significant digit of the number, and ends with the least significant one.
// And so does the returned list.
// For details, see LeastSignificantFirst.
func MostSignificantFirst(s, t *single.List[int]) (*single.List[int], error) {
	if !onlyHasDecimalDigits(s) {
		return nil, fmt.Errorf("the first list includes a non-decimal integer value")
	}
	if !onlyHasDecimalDigits(t) {
		return nil, fmt.Errorf("the second list includes a non-decimal integer value")
	}

	if s == nil {
		return single.Copy(t), nil
	}
	if t == nil {
		return single.Copy(s), nil
	}

	if t.Len() > s.Len() {
		s, t = t, s
	}
	lengthDifference := s.Len() - t.Len()

	sums := make([]int, s.Len())
	carries := make([]int, s.Len())

	sNode, tNode := s.Head, t.Head
	for index := 0; index < lengthDifference; index++ {
		sums[index] = sNode.Value
		sNode = sNode.Next
	}

	for index := lengthDifference; index < s.Len(); index, sNode, tNode = index+1, sNode.Next, tNode.Next {
		sums[index] = sNode.Value + tNode.Value
		if sums[index] >= 10 {
			sums[index] -= 10
			carries[index] = 1
		} else {
			carries[index] = 0
		}
	}

	result := new(single.List[int])
	sum, carry := 0, 0
	for index := len(sums) - 1; index >= 0; index-- {
		sum = sums[index] + carry
		if sum >= 10 {
			sum -= 10
			carry = 1
		} else {
			carry = 0
		}
		carry += carries[index]
		result.AddFront(sum)
	}

	if carry >= 1 {
		result.AddFront(carry)
	}

	return result, nil
}
