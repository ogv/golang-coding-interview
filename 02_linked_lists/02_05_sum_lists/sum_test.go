package sum

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

func TestLeastSignificantFirst(t *testing.T) {
	testCases := []struct {
		name      string
		inListA   *single.List[int]
		inListB   *single.List[int]
		wantList  *single.List[int]
		wantAnErr bool
	}{
		{
			name:      "success, both lists nil",
			inListA:   nil,
			inListB:   nil,
			wantList:  nil,
			wantAnErr: false,
		},
		{
			name:      "success, only first list nil",
			inListA:   nil,
			inListB:   single.NewList([]int{3, 0, 9}),
			wantList:  single.NewList([]int{3, 0, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, only second list nil",
			inListA:   single.NewList([]int{3, 0, 9}),
			inListB:   nil,
			wantList:  single.NewList([]int{3, 0, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, both lists empty",
			inListA:   &single.List[int]{},
			inListB:   &single.List[int]{},
			wantList:  &single.List[int]{},
			wantAnErr: false,
		},
		{
			name:      "success, only first list empty",
			inListA:   &single.List[int]{},
			inListB:   single.NewList([]int{3, 0, 9}),
			wantList:  single.NewList([]int{3, 0, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, only second list empty",
			inListA:   single.NewList([]int{3, 0, 9}),
			inListB:   &single.List[int]{},
			wantList:  single.NewList([]int{3, 0, 9}),
			wantAnErr: false,
		},
		{
			name:      "failure, small value in the first list",
			inListA:   single.NewList([]int{3, 0, -1}),
			inListB:   nil,
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, small value in the first list, second list ok",
			inListA:   single.NewList([]int{3, 0, -1}),
			inListB:   single.NewList([]int{4, 0, 1}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, small value in the second list",
			inListA:   nil,
			inListB:   single.NewList([]int{3, 0, -1}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, small value in the second list, first list ok",
			inListA:   single.NewList([]int{4, 0, 1}),
			inListB:   single.NewList([]int{3, 0, -1}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, large value in the first list",
			inListA:   single.NewList([]int{3, 10, 9}),
			inListB:   nil,
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, large value in the first list, second list ok",
			inListA:   single.NewList([]int{3, 10, 9}),
			inListB:   single.NewList([]int{3, 2, 9}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, large value in the second list",
			inListA:   nil,
			inListB:   single.NewList([]int{3, 10, 9}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, large value in the second list, first list ok",
			inListA:   single.NewList([]int{3, 2, 9}),
			inListB:   single.NewList([]int{3, 10, 9}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "success, single digit, no carry",
			inListA:   single.NewList([]int{1}),
			inListB:   single.NewList([]int{0}),
			wantList:  single.NewList([]int{1}),
			wantAnErr: false,
		},
		{
			name:      "success, single digit, carry",
			inListA:   single.NewList([]int{9}),
			inListB:   single.NewList([]int{9}),
			wantList:  single.NewList([]int{8, 1}),
			wantAnErr: false,
		},
		{
			name:      "success, same length, no carry",
			inListA:   single.NewList([]int{6, 3, 1}),
			inListB:   single.NewList([]int{3, 0, 7}),
			wantList:  single.NewList([]int{9, 3, 8}),
			wantAnErr: false,
		},
		{
			name:      "success, same length, partial carry",
			inListA:   single.NewList([]int{6, 3, 1}),
			inListB:   single.NewList([]int{9, 0, 7}),
			wantList:  single.NewList([]int{5, 4, 8}),
			wantAnErr: false,
		},
		{
			name:      "success, same length, longer result",
			inListA:   single.NewList([]int{6, 9, 2}),
			inListB:   single.NewList([]int{9, 0, 7}),
			wantList:  single.NewList([]int{5, 0, 0, 1}),
			wantAnErr: false,
		},
		{
			name:      "success, first list longer, no carry",
			inListA:   single.NewList([]int{6, 3, 1}),
			inListB:   single.NewList([]int{3}),
			wantList:  single.NewList([]int{9, 3, 1}),
			wantAnErr: false,
		},
		{
			name:      "success, first list longer, partial carry",
			inListA:   single.NewList([]int{6, 3, 1}),
			inListB:   single.NewList([]int{9}),
			wantList:  single.NewList([]int{5, 4, 1}),
			wantAnErr: false,
		},
		{
			name:      "success, first list longer, longer result",
			inListA:   single.NewList([]int{6, 9, 9}),
			inListB:   single.NewList([]int{9, 0}),
			wantList:  single.NewList([]int{5, 0, 0, 1}),
			wantAnErr: false,
		},
		{
			name:      "success, second list longer, no carry",
			inListA:   single.NewList([]int{3}),
			inListB:   single.NewList([]int{6, 3, 1}),
			wantList:  single.NewList([]int{9, 3, 1}),
			wantAnErr: false,
		},
		{
			name:      "success, second list longer, partial carry",
			inListA:   single.NewList([]int{9}),
			inListB:   single.NewList([]int{6, 3, 1}),
			wantList:  single.NewList([]int{5, 4, 1}),
			wantAnErr: false,
		},
		{
			name:      "success, second list longer, longer result",
			inListA:   single.NewList([]int{9, 0}),
			inListB:   single.NewList([]int{6, 9, 9}),
			wantList:  single.NewList([]int{5, 0, 0, 1}),
			wantAnErr: false,
		},

		{
			name:      "success, long lists, with carry",
			inListA:   single.NewList([]int{3, 1, 2, 5, 6, 5, 8, 9, 2, 5, 6, 2, 0, 1, 8, 9}),
			inListB:   single.NewList([]int{2, 7, 8, 6, 2, 3, 8, 5, 2, 9, 7, 9, 8, 8, 7, 2}),
			wantList:  single.NewList([]int{5, 8, 0, 2, 9, 8, 6, 5, 5, 4, 4, 2, 9, 9, 5, 2, 1}),
			wantAnErr: false,
		},
		{
			name:      "success, first list longer, longer result",
			inListA:   single.NewList([]int{6, 9, 9}),
			inListB:   single.NewList([]int{9, 0}),
			wantList:  single.NewList([]int{5, 0, 0, 1}),
			wantAnErr: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut, actualErr := LeastSignificantFirst(testCase.inListA, testCase.inListB)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualOut, testCase.wantList) {
				t.Errorf("for %v and %v, expected Sum %v but got %v",
					testCase.inListA, testCase.inListB, testCase.wantList, actualOut)
			}
		})
	}
}

func TestMostSignificantFirst(t *testing.T) {
	testCases := []struct {
		name      string
		inListA   *single.List[int]
		inListB   *single.List[int]
		wantList  *single.List[int]
		wantAnErr bool
	}{
		{
			name:      "success, both lists nil",
			inListA:   nil,
			inListB:   nil,
			wantList:  nil,
			wantAnErr: false,
		},
		{
			name:      "success, only first list nil",
			inListA:   nil,
			inListB:   single.NewList([]int{3, 0, 9}),
			wantList:  single.NewList([]int{3, 0, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, only second list nil",
			inListA:   single.NewList([]int{3, 0, 9}),
			inListB:   nil,
			wantList:  single.NewList([]int{3, 0, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, both lists empty",
			inListA:   &single.List[int]{},
			inListB:   &single.List[int]{},
			wantList:  &single.List[int]{},
			wantAnErr: false,
		},
		{
			name:      "success, only first list empty",
			inListA:   &single.List[int]{},
			inListB:   single.NewList([]int{3, 0, 9}),
			wantList:  single.NewList([]int{3, 0, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, only second list empty",
			inListA:   single.NewList([]int{3, 0, 9}),
			inListB:   &single.List[int]{},
			wantList:  single.NewList([]int{3, 0, 9}),
			wantAnErr: false,
		},
		{
			name:      "failure, small value in the first list",
			inListA:   single.NewList([]int{3, 0, -1}),
			inListB:   nil,
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, small value in the first list, second list ok",
			inListA:   single.NewList([]int{3, 0, -1}),
			inListB:   single.NewList([]int{4, 0, 1}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, small value in the second list",
			inListA:   nil,
			inListB:   single.NewList([]int{3, 0, -1}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, small value in the second list, first list ok",
			inListA:   single.NewList([]int{4, 0, 1}),
			inListB:   single.NewList([]int{3, 0, -1}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, large value in the first list",
			inListA:   single.NewList([]int{3, 10, 9}),
			inListB:   nil,
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, large value in the first list, second list ok",
			inListA:   single.NewList([]int{3, 10, 9}),
			inListB:   single.NewList([]int{3, 2, 9}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, large value in the second list",
			inListA:   nil,
			inListB:   single.NewList([]int{3, 10, 9}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "failure, large value in the second list, first list ok",
			inListA:   single.NewList([]int{3, 2, 9}),
			inListB:   single.NewList([]int{3, 10, 9}),
			wantList:  nil,
			wantAnErr: true,
		},
		{
			name:      "success, single digit, no carry",
			inListA:   single.NewList([]int{1}),
			inListB:   single.NewList([]int{0}),
			wantList:  single.NewList([]int{1}),
			wantAnErr: false,
		},
		{
			name:      "success, single digit, carry",
			inListA:   single.NewList([]int{9}),
			inListB:   single.NewList([]int{9}),
			wantList:  single.NewList([]int{1, 8}),
			wantAnErr: false,
		},
		{
			name:      "success, same length, no carry",
			inListA:   single.NewList([]int{1, 3, 6}),
			inListB:   single.NewList([]int{7, 0, 3}),
			wantList:  single.NewList([]int{8, 3, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, same length, partial carry",
			inListA:   single.NewList([]int{1, 3, 6}),
			inListB:   single.NewList([]int{7, 0, 9}),
			wantList:  single.NewList([]int{8, 4, 5}),
			wantAnErr: false,
		},
		{
			name:      "success, same length, longer result",
			inListA:   single.NewList([]int{2, 9, 6}),
			inListB:   single.NewList([]int{7, 0, 9}),
			wantList:  single.NewList([]int{1, 0, 0, 5}),
			wantAnErr: false,
		},
		{
			name:      "success, first list longer, no carry",
			inListA:   single.NewList([]int{1, 3, 6}),
			inListB:   single.NewList([]int{3}),
			wantList:  single.NewList([]int{1, 3, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, first list longer, partial carry",
			inListA:   single.NewList([]int{1, 3, 6}),
			inListB:   single.NewList([]int{9}),
			wantList:  single.NewList([]int{1, 4, 5}),
			wantAnErr: false,
		},
		{
			name:      "success, first list longer, longer result",
			inListA:   single.NewList([]int{9, 9, 6}),
			inListB:   single.NewList([]int{0, 9}),
			wantList:  single.NewList([]int{1, 0, 0, 5}),
			wantAnErr: false,
		},
		{
			name:      "success, second list longer, no carry",
			inListA:   single.NewList([]int{3}),
			inListB:   single.NewList([]int{1, 3, 6}),
			wantList:  single.NewList([]int{1, 3, 9}),
			wantAnErr: false,
		},
		{
			name:      "success, second list longer, partial carry",
			inListA:   single.NewList([]int{9}),
			inListB:   single.NewList([]int{1, 3, 6}),
			wantList:  single.NewList([]int{1, 4, 5}),
			wantAnErr: false,
		},
		{
			name:      "success, second list longer, longer result",
			inListA:   single.NewList([]int{0, 9}),
			inListB:   single.NewList([]int{9, 9, 6}),
			wantList:  single.NewList([]int{1, 0, 0, 5}),
			wantAnErr: false,
		},
		{
			name:      "success, long lists, with carry",
			inListA:   single.NewList([]int{9, 8, 1, 0, 2, 6, 5, 2, 9, 8, 5, 6, 5, 2, 1, 3}),
			inListB:   single.NewList([]int{2, 7, 8, 8, 9, 7, 9, 2, 5, 8, 3, 2, 6, 8, 7, 2}),
			wantList:  single.NewList([]int{1, 2, 5, 9, 9, 2, 4, 4, 5, 5, 6, 8, 9, 2, 0, 8, 5}),
			wantAnErr: false,
		},
		{
			name:      "success, first list longer, longer result",
			inListA:   single.NewList([]int{9, 9, 6}),
			inListB:   single.NewList([]int{0, 9}),
			wantList:  single.NewList([]int{1, 0, 0, 5}),
			wantAnErr: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut, actualErr := MostSignificantFirst(testCase.inListA, testCase.inListB)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualOut, testCase.wantList) {
				t.Errorf("for %v and %v, expected Sum %v but got %v",
					testCase.inListA, testCase.inListB, testCase.wantList, actualOut)
			}
		})
	}
}
