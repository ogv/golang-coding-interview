package kthtolast

import (
	"fmt"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

// Get returns the k-th to last element of a singly linked list
// It returns an error if the list has less than k elements.
func Get[E any](a single.List[E], k int) (E, error) {
	/*
		Worst-case complexity:
			- time O(list length-k)
			- space O(1)
	*/

	/*
		k-th to last (k-th from the back) means at position len(a)-1-k.
		So just find the list's length,
		then move a pointer from its head to that position.
	*/
	listLength := a.Len()
	if k >= listLength {
		return *new(E), fmt.Errorf("less than %d elements in list", k)
	}

	neededPosition := listLength - 1 - k
	currentNode := a.Head
	for currentPosition := 0; currentPosition < neededPosition; currentPosition++ {
		currentNode = currentNode.Next
	}

	return currentNode.Value, nil
}

// GetB returns the k-th to last element of a singly linked list
// It returns an error if the list has less than k elements
func GetB[E any](a single.List[E], k int) (E, error) {
	/*
		Worst-case complexity:
			- time O(list length)
			- space O(1)
	*/

	// This implementation is too slow for a long list, because it always traverses the entire list.
	// I am leaving it here as a cautionary tale.
	toEnd := a.Head
	currentPosition := 0
	for toEnd != nil && currentPosition < k {
		toEnd = toEnd.Next
		currentPosition++
	}

	if toEnd == nil {
		return *new(E), fmt.Errorf("less than %d elements in list", k)
	}

	kThToLast := a.Head
	for toEnd.Next != nil {
		toEnd = toEnd.Next
		kThToLast = kThToLast.Next
	}

	return kThToLast.Value, nil
}
