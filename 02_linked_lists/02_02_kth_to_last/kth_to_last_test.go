package kthtolast

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/02_linked_lists/single"
)

var (
	getResult any
	getErr    error
)

func BenchmarkGet_First(b *testing.B) {
	list := newListOfLength(1024 * 1024)
	k := list.Len() - 1
	benchmarkGet(b, *list, k)
}

func benchmarkGet[E any](b *testing.B, list single.List[E], k int) {
	for i := 0; i < b.N; i++ {
		getResult, getErr = Get(list, k)
	}
}

func newListOfLength(length int) *single.List[int] {
	result := new(single.List[int])
	for index := 0; index < length; index++ {
		result = result.AddBack(index)
	}
	return result
}

func BenchmarkGetB_First(b *testing.B) {
	list := newListOfLength(1024 * 1024)
	k := list.Len() - 1
	benchmarkGetB(b, *list, k)
}

func benchmarkGetB[E any](b *testing.B, list single.List[E], k int) {
	for i := 0; i < b.N; i++ {
		getResult, getErr = GetB(list, k)
	}
}

func BenchmarkGet_Last(b *testing.B) {
	benchmarkGet(b, *newListOfLength(1024 * 1024), 0)
}

func BenchmarkGetB_Last(b *testing.B) {
	benchmarkGetB(b, *newListOfLength(1024 * 1024), 0)
}

func BenchmarkGet_KTooLarge(b *testing.B) {
	list := newListOfLength(1024 * 1024)
	k := list.Len()
	benchmarkGet(b, *list, k)
}

func BenchmarkGetB_KTooLarge(b *testing.B) {
	list := newListOfLength(1024 * 1024)
	k := list.Len()
	benchmarkGetB(b, *list, k)
}

func TestGet(t *testing.T) {
	for _, testCase := range generateKthToLastTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut, actualErr := Get(*testCase.inList, testCase.inK)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualOut, testCase.wantOut) {
				t.Errorf("for %v, %d from the back expected element %v but got %v", testCase.inList, testCase.inK, testCase.wantOut, actualOut)
			}
		})
	}
}

type kthToLastTestCase struct {
	name      string
	inList    *single.List[any]
	inK       int
	wantOut   any
	wantAnErr bool
}

func generateKthToLastTestCases() []kthToLastTestCase {
	return []kthToLastTestCase{
		{
			name:      "error, empty list, last element",
			inList:    single.NewList[any](nil),
			inK:       0,
			wantOut:   nil,
			wantAnErr: true,
		},
		{
			name:      "error, empty list, kth from last element",
			inList:    single.NewList[any](nil),
			inK:       3,
			wantOut:   nil,
			wantAnErr: true,
		},
		{
			name:      "success, one element int list, last element",
			inList:    single.NewList([]any{12}),
			inK:       0,
			wantOut:   12,
			wantAnErr: false,
		},
		{
			name:      "failure, one element int list, next to last element",
			inList:    single.NewList([]any{12}),
			inK:       1,
			wantOut:   nil,
			wantAnErr: true,
		},
		{
			name:      "success, multi element int list, last element",
			inList:    single.NewList([]any{4, 53, 12}),
			inK:       0,
			wantOut:   12,
			wantAnErr: false,
		},
		{
			name:      "failure, multi element int list, large index element",
			inList:    single.NewList([]any{4, 53, 12}),
			inK:       3,
			wantOut:   nil,
			wantAnErr: true,
		},
		{
			name:      "success, multi element int list, first element",
			inList:    single.NewList([]any{-15, 4, 53, 12}),
			inK:       3,
			wantOut:   -15,
			wantAnErr: false,
		},
		{
			name:      "success, multi element int list, intermediate element",
			inList:    single.NewList([]any{-15, 4, 53, 12}),
			inK:       2,
			wantOut:   4,
			wantAnErr: false,
		},
		{
			name:      "success, one element string list, last element",
			inList:    single.NewList([]any{"12"}),
			inK:       0,
			wantOut:   "12",
			wantAnErr: false,
		},
		{
			name:      "failure, one element string list, next to last element",
			inList:    single.NewList([]any{"12"}),
			inK:       1,
			wantOut:   nil,
			wantAnErr: true,
		},
		{
			name:      "success, multi element string list, last element",
			inList:    single.NewList([]any{"4", "53", "12"}),
			inK:       0,
			wantOut:   "12",
			wantAnErr: false,
		},
		{
			name:      "failure, multi element string list, large index element",
			inList:    single.NewList([]any{"4", "53", "12"}),
			inK:       3,
			wantOut:   nil,
			wantAnErr: true,
		},
		{
			name:      "success, multi element string list, first element",
			inList:    single.NewList([]any{"-15", "4", "53", "12"}),
			inK:       3,
			wantOut:   "-15",
			wantAnErr: false,
		},
		{
			name:      "success, multi element string list, intermediate element",
			inList:    single.NewList([]any{"-15", "4", "53", "12"}),
			inK:       2,
			wantOut:   "4",
			wantAnErr: false,
		},
	}
}
func TestGetB(t *testing.T) {
	for _, testCase := range generateKthToLastTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut, actualErr := GetB(*testCase.inList, testCase.inK)
			if gotAnErr := actualErr != nil; gotAnErr != testCase.wantAnErr {
				t.Errorf("wanted an error: %t, got an error: %t", testCase.wantAnErr, gotAnErr)
			}
			if !reflect.DeepEqual(actualOut, testCase.wantOut) {
				t.Errorf("for %v, %d from the back expected element %v but got %v", testCase.inList, testCase.inK, testCase.wantOut, actualOut)
			}
		})
	}
}
