package orderutils

import (
	"image"
	"math"
	"reflect"
	"sort"
	"testing"
)

func TestAscending(t *testing.T) {
	for _, testCase := range generateAscendingTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			actualResult := Ascending[int](testCase.inA, testCase.inB)
			if actualResult != testCase.wantResult {
				t.Errorf("expected comparison between %v and %v to be %d but was %d", testCase.inA, testCase.inB, testCase.wantResult, actualResult)
			}
		})
	}
}

func generateAscendingTestCases() []comparatorTestCase {
	maxInt := int(^uint(0) >> 1)
	minInt := -maxInt - 1

	return []comparatorTestCase{
		{
			name:       "min and min",
			inA:        minInt,
			inB:        minInt,
			wantResult: 0,
		},
		{
			name:       "min and negative",
			inA:        minInt,
			inB:        -6_103,
			wantResult: -1,
		},
		{
			name:       "min and zero",
			inA:        minInt,
			inB:        0,
			wantResult: -1,
		},
		{
			name:       "min and positive",
			inA:        minInt,
			inB:        5_092,
			wantResult: -1,
		},
		{
			name:       "min and max",
			inA:        minInt,
			inB:        maxInt,
			wantResult: -1,
		},

		{
			name:       "negative and min",
			inA:        -6_103,
			inB:        minInt,
			wantResult: 1,
		},
		{
			name:       "negative and larger negative",
			inA:        -6_103,
			inB:        -128,
			wantResult: -1,
		},
		{
			name:       "negative and equal negative",
			inA:        -6_103,
			inB:        -6_103,
			wantResult: 0,
		},
		{
			name:       "negative and smaller negative",
			inA:        -6_103,
			inB:        -7_416,
			wantResult: 1,
		},
		{
			name:       "negative and zero",
			inA:        -6_103,
			inB:        0,
			wantResult: -1,
		},
		{
			name:       "negative and positive",
			inA:        -6_103,
			inB:        5_092,
			wantResult: -1,
		},
		{
			name:       "negative and max",
			inA:        -6_103,
			inB:        maxInt,
			wantResult: -1,
		},

		{
			name:       "zero and min",
			inA:        0,
			inB:        minInt,
			wantResult: 1,
		},
		{
			name:       "zero and negative",
			inA:        0,
			inB:        -6_103,
			wantResult: 1,
		},
		{
			name:       "zero and zero",
			inA:        0,
			inB:        0,
			wantResult: 0,
		},
		{
			name:       "zero and positive",
			inA:        0,
			inB:        5_092,
			wantResult: -1,
		},
		{
			name:       "zero and max",
			inA:        0,
			inB:        maxInt,
			wantResult: -1,
		},

		{
			name:       "positive and min",
			inA:        5_092,
			inB:        minInt,
			wantResult: 1,
		},
		{
			name:       "positive and negative",
			inA:        5_092,
			inB:        -6_103,
			wantResult: 1,
		},
		{
			name:       "positive and zero",
			inA:        5_092,
			inB:        0,
			wantResult: 1,
		},
		{
			name:       "positive and smaller positive",
			inA:        5_092,
			inB:        217,
			wantResult: 1,
		},
		{
			name:       "positive and equal positive",
			inA:        5_092,
			inB:        5_092,
			wantResult: 0,
		},
		{
			name:       "positive and larger positive",
			inA:        5_092,
			inB:        8_294,
			wantResult: -1,
		},
		{
			name:       "positive and max int",
			inA:        5_092,
			inB:        maxInt,
			wantResult: -1,
		},

		{
			name:       "max and min",
			inA:        maxInt,
			inB:        minInt,
			wantResult: 1,
		},
		{
			name:       "max and negative",
			inA:        maxInt,
			inB:        -6_103,
			wantResult: 1,
		},
		{
			name:       "max and zero",
			inA:        maxInt,
			inB:        0,
			wantResult: 1,
		},
		{
			name:       "max and positive",
			inA:        maxInt,
			inB:        5_092,
			wantResult: 1,
		},
		{
			name:       "max and max",
			inA:        maxInt,
			inB:        maxInt,
			wantResult: 0,
		},
	}
}

type comparatorTestCase struct {
	name       string
	inA        int
	inB        int
	wantResult int
}

func TestDescending(t *testing.T) {
	for _, testCase := range generateDescendingTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			actualResult := Descending(testCase.inA, testCase.inB)
			if actualResult != testCase.wantResult {
				t.Errorf("expected comparison between %v and %v to be %d but was %d", testCase.inA, testCase.inB, testCase.wantResult, actualResult)
			}
		})
	}
}

func generateDescendingTestCases() []comparatorTestCase {
	testCases := generateAscendingTestCases()
	for i := range testCases {
		testCases[i].wantResult = -testCases[i].wantResult
	}
	return testCases
}

func TestQuickSort(t *testing.T) {
	testSortFunc(t, QuickSort)
}

func testSortFunc(t *testing.T, sorter func(values sort.Interface)) {
	for _, testCase := range generateSortTestCases() {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			sorter(testCase.inValues)
			if !reflect.DeepEqual(testCase.inValues, testCase.wantValues) {
				pt.Errorf("after sort expected data to be %v but got %v", testCase.wantValues, testCase.inValues)
			}
		})
	}
}

func generateSortTestCases() []sortTestCase {
	return []sortTestCase{
		{
			name:       "empty int slice",
			inValues:   sort.IntSlice{},
			wantValues: sort.IntSlice{},
		},
		{
			name:       "one min int slice",
			inValues:   sort.IntSlice{math.MinInt64},
			wantValues: sort.IntSlice{math.MinInt64},
		},
		{
			name:       "one max int slice",
			inValues:   sort.IntSlice{math.MaxInt64},
			wantValues: sort.IntSlice{math.MaxInt64},
		},
		{
			name:       "ordered min max int slice",
			inValues:   sort.IntSlice{math.MinInt64, 0, math.MaxInt64},
			wantValues: sort.IntSlice{math.MinInt64, 0, math.MaxInt64},
		},
		{
			name:       "reverse ordered min max int slice",
			inValues:   sort.IntSlice{math.MaxInt64, 0, math.MinInt64},
			wantValues: sort.IntSlice{math.MinInt64, 0, math.MaxInt64},
		},
		{
			name:       "random order unique positive values",
			inValues:   sort.IntSlice{91, 28, 73, 46, 55, 64, 37, 82, 19},
			wantValues: sort.IntSlice{19, 28, 37, 46, 55, 64, 73, 82, 91},
		},
		{
			name:       "random order unique values int slice",
			inValues:   sort.IntSlice{-235, 1_352, -40_235, -40_241, 0, 4_325, 31, 932, -51_987},
			wantValues: sort.IntSlice{-51_987, -40_241, -40_235, -235, 0, 31, 932, 1_352, 4_325},
		},
		{
			name:       "random order duplicate values int slice",
			inValues:   sort.IntSlice{-235, 1_352, -40_235, -40_241, 1_352, 0, 4_325, 4_325, 31, 932, -51_987},
			wantValues: sort.IntSlice{-51_987, -40_241, -40_235, -235, 0, 31, 932, 1_352, 1_352, 4_325, 4_325},
		},
		{
			name:       "all identical negative values",
			inValues:   sort.IntSlice{-235, -235, -235, -235, -235, -235, -235, -235},
			wantValues: sort.IntSlice{-235, -235, -235, -235, -235, -235, -235, -235},
		},
		{
			name:       "all identical positive values",
			inValues:   sort.IntSlice{932, 932, 932, 932, 932, 932, 932, 932},
			wantValues: sort.IntSlice{932, 932, 932, 932, 932, 932, 932, 932},
		},
	}
}

type sortTestCase struct {
	name       string
	inValues   sort.Interface
	wantValues sort.Interface
}

func TestSelectionSort(t *testing.T) {
	testSortFunc(t, SelectionSort)
}

func TestMaxSelectionSort(t *testing.T) {
	testSortFunc(t, MaxSelectionSort)
}

func TestInsertionSort(t *testing.T) {
	testSortFunc(t, InsertionSort)
}

func TestHeapSort(t *testing.T) {
	testSortFunc(t, HeapSort)
}

func TestBubbleSort(t *testing.T) {
	testSortFunc(t, BubbleSort)
}

func TestBubbleSortB(t *testing.T) {
	testSortFunc(t, BubbleSortB)
}

func TestMergeSort(t *testing.T) {
	testSortWithComparatorFunc(t, generateComparatorSortTestCases_Point, MergeSortBy[[]image.Point, image.Point])
}

func testSortWithComparatorFunc[S ~[]E, E any](
	t *testing.T,
	caseGenerator func() []sortComparatorTestCase[S, E],
	sorter func(S, ValueComparator[E])) {

	for _, testCase := range caseGenerator() {
		t.Run(testCase.name, func(pt *testing.T) {
			sorter(testCase.inValues, testCase.inComparator)
			if !reflect.DeepEqual(testCase.inValues, testCase.wantValues) {
				pt.Errorf("after comparator sort expected data to be %v but got %v", testCase.wantValues, testCase.inValues)
			}
		})
	}
}

type sortComparatorTestCase[S ~[]E, E any] struct {
	name         string
	inValues     S
	inComparator ValueComparator[E]
	wantValues   S
}

func generateComparatorSortTestCases_Point() []sortComparatorTestCase[[]image.Point, image.Point] {
	return []sortComparatorTestCase[[]image.Point, image.Point]{
		{
			name:         "nil slice, x first",
			inValues:     nil,
			inComparator: byXThenY,
			wantValues:   nil,
		},
		{
			name:         "empty slice, x first",
			inValues:     []image.Point{},
			inComparator: byXThenY,
			wantValues:   []image.Point{},
		},
		{
			name:         "empty slice, y first",
			inValues:     []image.Point{},
			inComparator: byYThenX,
			wantValues:   []image.Point{},
		},
		{
			name: "one min-min slice, x first",
			inValues: []image.Point{
				{X: math.MinInt64, Y: math.MinInt64},
			},
			inComparator: byXThenY,
			wantValues: []image.Point{
				{X: math.MinInt64, Y: math.MinInt64},
			},
		},
		{
			name: "one min-min slice, y first",
			inValues: []image.Point{
				{X: math.MinInt64, Y: math.MinInt64},
			},
			inComparator: byYThenX,
			wantValues: []image.Point{
				{X: math.MinInt64, Y: math.MinInt64},
			},
		},
		{
			name: "one max-max slice, x first",
			inValues: []image.Point{
				{X: math.MaxInt64, Y: math.MaxInt64},
			},
			inComparator: byXThenY,
			wantValues: []image.Point{
				{X: math.MaxInt64, Y: math.MaxInt64},
			},
		},
		{
			name: "one max-max slice, y first",
			inValues: []image.Point{
				{X: math.MinInt64, Y: math.MinInt64},
			},
			inComparator: byYThenX,
			wantValues: []image.Point{
				{X: math.MinInt64, Y: math.MinInt64},
			},
		},
		{
			name: "ordered min-max slice, x first",
			inValues: []image.Point{
				{X: math.MinInt64, Y: math.MaxInt64},
				{X: 0, Y: 0},
				{X: math.MaxInt64, Y: math.MinInt64},
			},
			inComparator: byXThenY,
			wantValues: []image.Point{
				{X: math.MinInt64, Y: math.MaxInt64},
				{X: 0, Y: 0},
				{X: math.MaxInt64, Y: math.MinInt64},
			},
		},
		{
			name: "ordered min-max slice, y first",
			inValues: []image.Point{
				{X: math.MaxInt64, Y: math.MinInt64},
				{X: 0, Y: 0},
				{X: math.MinInt64, Y: math.MaxInt64},
			},
			inComparator: byYThenX,
			wantValues: []image.Point{
				{X: math.MaxInt64, Y: math.MinInt64},
				{X: 0, Y: 0},
				{X: math.MinInt64, Y: math.MaxInt64},
			},
		},
		{
			name: "reverse ordered min-max slice, x first",
			inValues: []image.Point{
				{X: math.MaxInt64, Y: math.MinInt64},
				{X: 0, Y: 0},
				{X: math.MinInt64, Y: math.MaxInt64},
			},
			inComparator: byXThenY,
			wantValues: []image.Point{
				{X: math.MinInt64, Y: math.MaxInt64},
				{X: 0, Y: 0},
				{X: math.MaxInt64, Y: math.MinInt64},
			},
		},
		{
			name: "reverse ordered min-max slice, y first",
			inValues: []image.Point{
				{X: math.MinInt64, Y: math.MaxInt64},
				{X: 0, Y: 0},
				{X: math.MaxInt64, Y: math.MinInt64},
			},
			inComparator: byYThenX,
			wantValues: []image.Point{
				{X: math.MaxInt64, Y: math.MinInt64},
				{X: 0, Y: 0},
				{X: math.MinInt64, Y: math.MaxInt64},
			},
		},
		{
			name: "random order unique positive values, x first",
			inValues: []image.Point{
				{X: 729, Y: 4},
				{X: 3, Y: 2},
				{X: 27, Y: 16},
				{X: 81, Y: 64},
				{X: 243, Y: 32},
				{X: 9, Y: 1},
				{X: 1, Y: 8},
			},
			inComparator: byXThenY,
			wantValues: []image.Point{
				{1, 8},
				{3, 2},
				{9, 1},
				{27, 16},
				{81, 64},
				{243, 32},
				{729, 4},
			},
		},
		{
			name: "random order unique positive values, y first",
			inValues: []image.Point{
				{X: 729, Y: 4},
				{X: 3, Y: 2},
				{X: 27, Y: 16},
				{X: 81, Y: 64},
				{X: 243, Y: 32},
				{X: 9, Y: 1},
				{X: 1, Y: 8},
			},
			inComparator: byYThenX,
			wantValues: []image.Point{
				{9, 1},
				{3, 2},
				{729, 4},
				{1, 8},
				{27, 16},
				{243, 32},
				{81, 64},
			},
		},
		{
			name: "random order unique values, x first",
			inValues: []image.Point{
				{X: -235, Y: -51_987},
				{X: 1_352, Y: 932},
				{X: -40_235, Y: 31},
				{X: -40_241, Y: 4_325},
				{X: 0, Y: 0},
				{X: 4_325, Y: -40_241},
				{X: 31, Y: -40_235},
				{X: 932, Y: 1_352},
				{X: -51_987, Y: -235},
			},
			inComparator: byXThenY,
			wantValues: []image.Point{
				{X: -51_987, Y: -235},
				{X: -40_241, Y: 4_325},
				{X: -40_235, Y: 31},
				{X: -235, Y: -51_987},
				{X: 0, Y: 0},
				{X: 31, Y: -40_235},
				{X: 932, Y: 1_352},
				{X: 1_352, Y: 932},
				{X: 4_325, Y: -40_241},
			},
		},
		{
			name: "random order unique values, y first",
			inValues: []image.Point{
				{X: -235, Y: -51_987},
				{X: 1_352, Y: 932},
				{X: -40_235, Y: 31},
				{X: -40_241, Y: 4_325},
				{X: 0, Y: 0},
				{X: 4_325, Y: -40_241},
				{X: 31, Y: -40_235},
				{X: 932, Y: 1_352},
				{X: -51_987, Y: -235},
			},
			inComparator: byYThenX,
			wantValues: []image.Point{
				{X: -235, Y: -51_987},
				{X: 4_325, Y: -40_241},
				{X: 31, Y: -40_235},
				{X: -51_987, Y: -235},
				{X: 0, Y: 0},
				{X: -40_235, Y: 31},
				{X: 1_352, Y: 932},
				{X: 932, Y: 1_352},
				{X: -40_241, Y: 4_325},
			},
		},
		{
			name: "random order duplicate values, x first",
			inValues: []image.Point{
				{X: -235, Y: -51_987},
				{X: 1_352, Y: 932},
				{X: -40_235, Y: 31},
				{X: -40_241, Y: 4_325},
				{X: 1_352, Y: 932},
				{X: 0, Y: 0},
				{X: 4_325, Y: -40_241},
				{X: 4_325, Y: -40_241},
				{X: 31, Y: -40_235},
				{X: 932, Y: 1_352},
				{X: -51_987, Y: -235},
			},
			inComparator: byXThenY,
			wantValues: []image.Point{
				{X: -51_987, Y: -235},
				{X: -40_241, Y: 4_325},
				{X: -40_235, Y: 31},
				{X: -235, Y: -51_987},
				{X: 0, Y: 0},
				{X: 31, Y: -40_235},
				{X: 932, Y: 1_352},
				{X: 1_352, Y: 932},
				{X: 1_352, Y: 932},
				{X: 4_325, Y: -40_241},
				{X: 4_325, Y: -40_241},
			},
		},
		{
			name: "random order duplicate values, y first",
			inValues: []image.Point{
				{X: -235, Y: -51_987},
				{X: 1_352, Y: 932},
				{X: -40_235, Y: 31},
				{X: -40_241, Y: 4_325},
				{X: 1_352, Y: 932},
				{X: 0, Y: 0},
				{X: 4_325, Y: -40_241},
				{X: 4_325, Y: -40_241},
				{X: 31, Y: -40_235},
				{X: 932, Y: 1_352},
				{X: -51_987, Y: -235},
			},
			inComparator: byYThenX,
			wantValues: []image.Point{
				{X: -235, Y: -51_987},
				{X: 4_325, Y: -40_241},
				{X: 4_325, Y: -40_241},
				{X: 31, Y: -40_235},
				{X: -51_987, Y: -235},
				{X: 0, Y: 0},
				{X: -40_235, Y: 31},
				{X: 1_352, Y: 932},
				{X: 1_352, Y: 932},
				{X: 932, Y: 1_352},
				{X: -40_241, Y: 4_325},
			},
		},
		{
			name: "all identical negative values, x first",
			inValues: []image.Point{{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
			},

			inComparator: byXThenY,
			wantValues: []image.Point{{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
			},
		},
		{
			name: "all identical negative values, y first",
			inValues: []image.Point{{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
			},

			inComparator: byYThenX,
			wantValues: []image.Point{{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
				{X: -235, Y: -738},
			},
		},
		{
			name: "all identical positive values, x first",
			inValues: []image.Point{{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
			},
			inComparator: byXThenY,
			wantValues: []image.Point{{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
			},
		},
		{
			name: "all identical positive values, y first",
			inValues: []image.Point{{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
			},
			inComparator: byYThenX,
			wantValues: []image.Point{{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
				{X: 932, Y: 305},
			},
		},
	}
}

func byXThenY(a image.Point, b image.Point) int {
	for _, comparator := range []ValueComparator[image.Point]{byX, byY} {
		result := comparator(a, b)
		if result != 0 {
			return result
		}
	}
	return 0
}

func byX(a image.Point, b image.Point) int {
	if a.X < b.X {
		return -1
	}
	if a.X > b.X {
		return 1
	}
	return 0
}

func byY(a image.Point, b image.Point) int {
	if a.Y < b.Y {
		return -1
	}
	if a.Y > b.Y {
		return 1
	}
	return 0
}

func byYThenX(a image.Point, b image.Point) int {
	for _, comparator := range []ValueComparator[image.Point]{byY, byX} {
		result := comparator(a, b)
		if result != 0 {
			return result
		}
	}
	return 0
}
