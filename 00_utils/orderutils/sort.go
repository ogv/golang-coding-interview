package orderutils

import (
	"cmp"
	"math/rand"
	"sort"
)

// ValueComparator defines an ordering.
// It should return a negative value if the first value comes before the second one,
// a positive value if the first value comes after the second one,
// or zero if they are considered equal by that ordering.
type ValueComparator[E any] func(a E, b E) int

// Ascending is a ValueComparator that orders values increasingly.
func Ascending[E cmp.Ordered](a, b E) int {
	if a < b {
		return -1
	}
	if a > b {
		return 1
	}
	return 0
}

// Descending is a ValueComparator that orders values decreasingly.
func Descending[E cmp.Ordered](a, b E) int {
	return -Ascending(a, b)
}

// QuickSort sorts the received values by choosing a pivot value among them,
// placing it in its final position, between the smaller ones and the larger or equal ones,
// and repeating the process with the two sections that have just been produced,
// until it deals with intervals of 1 value.
func QuickSort(values sort.Interface) {
	quickSortHelper(values, 0, values.Len())
}

// quickSortHelper sorts the interval of values between left inclusive and right exclusive.
func quickSortHelper(values sort.Interface, left int, right int) {
	if left < right {
		pivotIndex := partition(values, left, right)
		quickSortHelper(values, left, pivotIndex)
		quickSortHelper(values, pivotIndex+1, right)
	}
}

// partition chooses a pivot value between left inclusive and right exclusive,
// and places it between the smaller ones and the larger or equal ones in the given interval
// It returns the index inside values where the pivot value ends up
func partition(values sort.Interface, left int, right int) int {
	// Choose a random pivot and put it at the interval's end, for good performance on almost sorted values
	values.Swap(rand.Intn(right-left)+left, right-1)
	pivotIndex := right - 1
	storeIndex := left - 1
	for i := left; i < right; i++ {
		if values.Less(i, pivotIndex) {
			storeIndex++
			values.Swap(i, storeIndex)
		}
	}
	values.Swap(pivotIndex, storeIndex+1)
	return storeIndex + 1
}

func MergeSortBy[S ~[]E, E any](values S, comparator ValueComparator[E]) {
	mergeSortHelper(values, make(S, len(values)), 0, len(values), comparator)
}

func mergeSortHelper[S ~[]E, E any](values S, memo S, low, high int, comparator ValueComparator[E]) {
	if high-low <= 1 {
		return
	}
	middle := low + (high-low)/2
	mergeSortHelper(values, memo, low, middle, comparator)
	mergeSortHelper(values, memo, middle, high, comparator)
	merge(values, memo, low, high, comparator)
}

func merge[S ~[]E, E any](values S, memo S, low, high int, comparator ValueComparator[E]) {
	copy(memo[low:high], values[low:high])

	middle := low + (high-low)/2
	memoLeft := low
	memoRight := middle
	current := low
	for memoLeft < middle && memoRight < high {
		if comparator(memo[memoLeft], memo[memoRight]) <= 0 {
			values[current] = memo[memoLeft]
			memoLeft++
		} else {
			values[current] = memo[memoRight]
			memoRight++
		}
		current++
	}

	remaining := middle - memoLeft
	copy(values[current:current+remaining], memo[memoLeft:memoLeft+remaining])
}

// SelectionSort sorts the received values by repeatedly putting the minimum value
// from the last N values at the beginning of those values,
// starting with all the values and ending with the last 2 values
func SelectionSort(values sort.Interface) {
	for suffixStart := 0; suffixStart < values.Len(); suffixStart++ {
		minIndex := suffixStart
		for afterSortedIndex := suffixStart + 1; afterSortedIndex < values.Len(); afterSortedIndex++ {
			if values.Less(afterSortedIndex, minIndex) {
				minIndex = afterSortedIndex
			}
		}

		values.Swap(suffixStart, minIndex)
	}
}

// MaxSelectionSort sorts the received values by repeatedly putting the maximum value
// from the first N values at the end of those values,
// starting with all the values and ending with the first 2 values
func MaxSelectionSort(values sort.Interface) {
	for suffixEnd := values.Len() - 1; suffixEnd >= 0; suffixEnd-- {
		maxIndex := suffixEnd
		for beforeSortedIndex := suffixEnd - 1; beforeSortedIndex >= 0; beforeSortedIndex-- {
			if values.Less(maxIndex, beforeSortedIndex) {
				maxIndex = beforeSortedIndex
			}
		}

		values.Swap(suffixEnd, maxIndex)
	}
}

// InsertionSort sorts the received values by repeatedly inserting the value
// that follows the first N values already sorted
// among those values, starting with just the first value
func InsertionSort(values sort.Interface) {
	for afterSortedIndex := 1; afterSortedIndex < values.Len(); afterSortedIndex++ {
		valueToInsertIndex := afterSortedIndex
		for sortedIndex := afterSortedIndex - 1; sortedIndex >= 0; sortedIndex-- {
			if values.Less(valueToInsertIndex, sortedIndex) {
				values.Swap(valueToInsertIndex, sortedIndex)
				valueToInsertIndex = sortedIndex
			}
		}
	}
}

// HeapSort sorts the received values in place by repeatedly building a max heap,
// swapping the largest value (from the root) with the last leaf of the heap,
// and restoring a heap with one less element : the previous root
// It starts with a heap of all the values and no ordered values, and finishes with no elements in the heap and all ordered values.
func HeapSort(values sort.Interface) {
	const rootIndex = 0
	makeMaxHeap(values)

	endIndex := values.Len() - 1
	for endIndex > 0 {
		values.Swap(rootIndex, endIndex)
		endIndex--
		siftDown(values, rootIndex, endIndex)
	}
}

// makeMaxHeap puts the values in max heap order (with the maximum value in the root), in place
func makeMaxHeap(values sort.Interface) {
	startIndex := parentIndex(values.Len() - 1)
	for startIndex >= 0 {
		siftDown(values, startIndex, values.Len()-1)
		startIndex--
	}
}

// parentIndex returns the node's parent in a binary heap.
// It assumes the received index is positive, without checking this.
func parentIndex(nodeIndex int) int {
	return (nodeIndex - 1) / 2
}

// siftDown repairs the heap whose root element is at startIndex and whose last element is at endIndex
// It assumes the heaps rooted at the children of startIndex are valid
func siftDown(values sort.Interface, startIndex int, endIndex int) {
	parentIndex := startIndex
	childIndex := firstChildIndex(parentIndex)
	for childIndex <= endIndex {
		swapIndex := parentIndex
		if values.Less(parentIndex, childIndex) {
			swapIndex = childIndex
		}
		secondChildIndex := secondChildIndex(parentIndex)
		if secondChildIndex <= endIndex && values.Less(swapIndex, secondChildIndex) {
			swapIndex = secondChildIndex
		}

		if swapIndex == parentIndex {
			return
		}
		values.Swap(parentIndex, swapIndex)
		parentIndex = swapIndex
		childIndex = firstChildIndex(parentIndex)
	}
}

// firstChildIndex returns the node's left (first) child in a binary heap.
func firstChildIndex(nodeIndex int) int {
	return 2*nodeIndex + 1
}

// firstChildIndex returns the node's right (second) child in a binary heap.
func secondChildIndex(nodeIndex int) int {
	return 2*nodeIndex + 2
}

// BubbleSort sorts the received values in place, by starting with all the values,
// repeatedly moving the largest value at the end, and starting over with the elements before that value,
// until no move is necessary
// It avoids looking at the last N-1 values when passing the Nth time (those are already in their final positions)
func BubbleSort(values sort.Interface) {
	unsortedLength := values.Len()
	dataIsSorted := false
	for !dataIsSorted {
		dataIsSorted = true
		for bubbleIndex := 1; bubbleIndex <= unsortedLength-1; bubbleIndex++ {
			if values.Less(bubbleIndex, bubbleIndex-1) {
				values.Swap(bubbleIndex, bubbleIndex-1)
				dataIsSorted = false
			}
		}

		unsortedLength--
	}
}

// BubbleSortB sorts the received values in place, by starting with all the values,
// repeatedly moving the largest value at the end, and starting over with the elements before that value,
// until no move is necessary
func BubbleSortB(values sort.Interface) {
	dataIsSorted := false
	for !dataIsSorted {
		dataIsSorted = true
		for bubbleIndex := 1; bubbleIndex <= values.Len()-1; bubbleIndex++ {
			if values.Less(bubbleIndex, bubbleIndex-1) {
				values.Swap(bubbleIndex, bubbleIndex-1)
				dataIsSorted = false
			}
		}
	}
}
