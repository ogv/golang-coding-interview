package sliceutils

import (
	"reflect"
	"testing"
)

func TestCopyIntMatrix(t *testing.T) {
	testCases := []struct {
		name    string
		in      [][]int
		wantOut [][]int
	}{
		{
			name:    "nil matrix",
			in:      nil,
			wantOut: nil,
		},
		{
			name:    "empty matrix",
			in:      [][]int{},
			wantOut: [][]int{},
		},
		{
			name:    "one element matrix",
			in:      [][]int{{21}},
			wantOut: [][]int{{21}},
		},
		{
			name:    "1 row",
			in:      [][]int{{21, 22, 23}},
			wantOut: [][]int{{21, 22, 23}},
		},
		{
			name:    "3 rows, 1 column",
			in:      [][]int{{21}, {22}, {23}},
			wantOut: [][]int{{21}, {22}, {23}},
		},
		{
			name: "nil row in different row lengths matrix",
			in: [][]int{
				{21, 22, 23},
				nil,
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 22, 23},
				nil,
				{27, 28, 29},
			},
		},
		{
			name: "different row lengths matrix",
			in: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 29},
			},
		},
		{
			name: "longer row in the middle",
			in: [][]int{
				{21, 22, 23, 24},
				{25, 26, 27, 28, 29, 30},
				{31, 32, 33},
			},
			wantOut: [][]int{
				{21, 22, 23, 24},
				{25, 26, 27, 28, 29, 30},
				{31, 32, 33},
			},
		},

		{
			name: "square matrix",
			in: [][]int{
				{21, 22, 23},
				{24, 25, 26},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 22, 23},
				{24, 25, 26},
				{27, 28, 29},
			},
		},
		{
			name: "larger square matrix",
			in: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
				{41, 42, 43, 44, 45},
			},
			wantOut: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
				{41, 42, 43, 44, 45},
			},
		},

		{
			name: "less rows than columns matrix",
			in: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
			},
			wantOut: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
			},
		},
		{
			name: "more rows than columns matrix",
			in: [][]int{
				{21, 22, 23, 24},
				{27, 28, 29, 30},
				{33, 34, 35, 36},
				{39, 40, 41, 42},
				{45, 46, 47, 48},
				{51, 52, 53, 54},
			},
			wantOut: [][]int{
				{21, 22, 23, 24},
				{27, 28, 29, 30},
				{33, 34, 35, 36},
				{39, 40, 41, 42},
				{45, 46, 47, 48},
				{51, 52, 53, 54},
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := CopyIntMatrix(testCase.in)
			if !reflect.DeepEqual(actualOut, testCase.wantOut) {
				t.Fatalf("for %v expected %v but got %v", testCase.in, testCase.wantOut, actualOut)
			}

			if actualOut == nil {
				return
			}

			// Make sure the copy doesn't share memory with the original
			if len(actualOut) >= 1 && (&actualOut[0] == &testCase.in[0]) {
				t.Fatalf("expected copy matrix NOT to share the slice of rows, but it does")
			}
			for index := range actualOut {
				if len(actualOut[index]) >= 1 && (&actualOut[index][0] == &testCase.in[index][0]) {
					t.Fatalf("expected copy matrix NOT to share any slice, but row %d does", index)
				}
			}
		})
	}
}
