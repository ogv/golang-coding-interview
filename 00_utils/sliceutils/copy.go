package sliceutils

// CopyIntMatrix returns a copy of the matrix argument.
// The result uses its own memory (it does not share any of the argument's elements)
func CopyIntMatrix(m [][]int) [][]int {
	if m == nil {
		return nil
	}

	result := make([][]int, len(m))
	for row := 0; row < len(m); row++ {
		if m[row] != nil {
			result[row] = make([]int, len(m[row]))
			copy(result[row], m[row])
		}
	}

	return result
}
