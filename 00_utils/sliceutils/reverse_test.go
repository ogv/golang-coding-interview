package sliceutils

import (
	"fmt"
	"math"
	"reflect"
	"testing"
)

type reverseTestCase[T any] struct {
	name      string
	in        []T
	wantSlice []T
}

func TestReverse_Int(t *testing.T) {
	for _, testCase := range reverseIntTestCases() {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			var inSlice []int
			if testCase.in != nil {
				inSlice = make([]int, len(testCase.in))
				copy(inSlice, testCase.in)
			}
			Reverse(testCase.in)
			if !reflect.DeepEqual(testCase.wantSlice, testCase.in) {
				pt.Errorf("Reverse(%v)\nexpected:\n%+v\ngot:\n%+v", inSlice, testCase.wantSlice, testCase.in)
			}
		})
	}
}

func TestReverseCopy_Int(t *testing.T) {
	for _, testCase := range reverseIntTestCases() {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			got := ReverseCopy(testCase.in)
			if !reflect.DeepEqual(testCase.wantSlice, got) {
				pt.Errorf("ReverseCopy(%v)\nexpected:\n%+v\ngot:\n%+v", testCase.in, testCase.wantSlice, got)
			}
		})
	}
}

func reverseIntTestCases() []reverseTestCase[int] {
	return []reverseTestCase[int]{
		{
			name:      "nil",
			in:        nil,
			wantSlice: nil,
		},
		{
			name:      "zero elements",
			in:        []int{},
			wantSlice: []int{},
		},
		{
			name:      "one element",
			in:        []int{1},
			wantSlice: []int{1},
		},
		{
			name:      "two elements",
			in:        []int{1, 2},
			wantSlice: []int{2, 1},
		},
		{
			name:      "three elements",
			in:        []int{1, 2, 3},
			wantSlice: []int{3, 2, 1},
		},
		{
			name:      "four elements",
			in:        []int{1, 2, 3, 4},
			wantSlice: []int{4, 3, 2, 1},
		},
		{
			name:      "even length with extreme values",
			in:        []int{math.MinInt, math.MinInt, math.MaxInt, math.MaxInt},
			wantSlice: []int{math.MaxInt, math.MaxInt, math.MinInt, math.MinInt},
		},
		{
			name:      "odd length with extreme values",
			in:        []int{math.MinInt, math.MinInt, 0, math.MaxInt, math.MaxInt},
			wantSlice: []int{math.MaxInt, math.MaxInt, 0, math.MinInt, math.MinInt},
		},
	}
}

var (
	reverseElementCounts = []int{10, 1_000, 100_000, 1_000_000}
	reverseCopy          []int
)

func BenchmarkReverseCopy_Int(b *testing.B) {
	for _, count := range reverseElementCounts {
		elements := make([]int, count)
		b.Run(fmt.Sprintf("%d_elements", count), func(sb *testing.B) {
			for i := 0; i < sb.N; i++ {
				reverseCopy = ReverseCopy(elements)
			}
		})
	}
}
