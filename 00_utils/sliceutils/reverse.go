package sliceutils

// Reverse changes the order of elements in the slice.
func Reverse[E any](in []E) {
	for left, right := 0, len(in)-1; left < right; left, right = left+1, right-1 {
		in[left], in[right] = in[right], in[left]
	}
}

// ReverseCopy returns a copy of the input with the order of the elements reversed. It does not change the input.
func ReverseCopy[E any](in []E) []E {
	var result []E
	if in == nil {
		return result
	}
	result = make([]E, len(in))
	outI := len(result) - 1
	for i := range in {
		result[outI] = in[i]
		outI--
	}
	return result
}
