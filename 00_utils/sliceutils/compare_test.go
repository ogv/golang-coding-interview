package sliceutils

import (
	"math"
	"testing"
)

func TestEqualSlices(t *testing.T) {
	testEqualSlices(t, equalSlicesTestCases())
}

func testEqualSlices[E comparable](t *testing.T, testCases []equalSlicesTestCase[E]) {
	for _, testCase := range testCases {
		t.Run(testCase.name, func(pt *testing.T) {
			pt.Parallel()
			actual := EqualSlices(testCase.inA, testCase.inB)
			if actual != testCase.wantEqual {
				pt.Errorf("expected slices to be equal: %v but got %v", testCase.wantEqual, actual)

			}
		})
	}
}

func equalSlicesTestCases() []equalSlicesTestCase[int] {
	return []equalSlicesTestCase[int]{
		{
			name:      "nil slices are equal",
			inA:       nil,
			inB:       nil,
			wantEqual: true,
		},
		{
			name:      "nil and empty int slices, unequal",
			inA:       nil,
			inB:       []int{},
			wantEqual: false,
		},
		{
			name:      "empty and nil int slices unequal",
			inA:       []int{},
			inB:       nil,
			wantEqual: false,
		},
		{
			name:      "empty int slices equal",
			inA:       []int{},
			inB:       []int{},
			wantEqual: true,
		},
		{
			name:      "min int slices equal",
			inA:       []int{math.MinInt64},
			inB:       []int{math.MinInt64},
			wantEqual: true,
		},
		{
			name:      "max int slices equal",
			inA:       []int{math.MaxInt64},
			inB:       []int{math.MaxInt64},
			wantEqual: true,
		},
		{
			name:      "empty and non-empty int slices unequal",
			inA:       []int{},
			inB:       []int{46, 55},
			wantEqual: false,
		},
		{
			name:      "non-empty and empty int slices unequal",
			inA:       []int{46, 55},
			inB:       []int{},
			wantEqual: false,
		},
		{
			name:      "first one shorter, so unequal",
			inA:       []int{91, 28, 73, 46, 55, 64, 37, 82},
			inB:       []int{91, 28, 73, 46, 55, 64, 37, 82, 19},
			wantEqual: false,
		},
		{
			name:      "first one longer, so unequal",
			inA:       []int{91, 28, 73, 46, 55, 64, 37, 82, 19},
			inB:       []int{91, 28, 73, 46, 55, 64, 37, 82},
			wantEqual: false,
		},
		{
			name:      "same elements, different order, so unequal",
			inA:       []int{91, 28, 73, 46, 55, 64, 37, 82, 19},
			inB:       []int{91, 28, 73, 46, 55, 64, 37, 19, 82},
			wantEqual: false,
		},
		{
			name:      "same elements, so equal",
			inA:       []int{91, 28, 73, 46, 55, 64, 37, 82, 19},
			inB:       []int{91, 28, 73, 46, 55, 64, 37, 82, 19},
			wantEqual: true,
		},
		{
			name:      "same elements with duplicate values, different order, so unequal",
			inA:       []int{-235, 1_352, -40_235, -40_241, 1_352, 0, 4_325, 4_325, 31, 932, -51_987},
			inB:       []int{-235, 1_352, -40_235, -40_241, 1_352, 0, 4_325, 4_325, 31, -51_987, 932},
			wantEqual: false,
		},
		{
			name:      "same elements with duplicate values, so equal",
			inA:       []int{-235, 1_352, -40_235, -40_241, 1_352, 0, 4_325, 4_325, 31, 932, -51_987},
			inB:       []int{-235, 1_352, -40_235, -40_241, 1_352, 0, 4_325, 4_325, 31, 932, -51_987},
			wantEqual: true,
		},
	}
}

type equalSlicesTestCase[E comparable] struct {
	name      string
	inA       []E
	inB       []E
	wantEqual bool
}
