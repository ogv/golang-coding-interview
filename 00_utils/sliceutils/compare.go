package sliceutils

// EqualSlices returns true if and only if the slices have equal elements in the same order.
// A nil slice is only equal to a nil slice.
func EqualSlices[T comparable](a, b []T) bool {
	switch {
	case a == nil && b == nil:
		return true
	case (a == nil) != (b == nil):
		return false
	case len(a) != len(b):
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
