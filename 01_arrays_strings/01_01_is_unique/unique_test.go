package unique

import (
	"testing"
)

func TestUnique(t *testing.T) {
	testCases := []struct {
		name    string
		in      string
		wantOut bool
	}{
		{
			name:    "empty string",
			in:      "",
			wantOut: true,
		},
		{
			name:    "one ASCII letter",
			in:      "a",
			wantOut: true,
		},
		{
			name:    "repeated ASCII letter",
			in:      "aa",
			wantOut: false,
		},
		{
			name:    "one multi-byte character",
			in:      "木",
			wantOut: true,
		},
		{
			name:    "repeated multi-byte character",
			in:      "木木",
			wantOut: false,
		},
		{
			name:    "unique single, double and triple-byte characters",
			in:      "4木ΩX",
			wantOut: true,
		},
		{
			name:    "repeating single, double and triple-byte characters",
			in:      "4木Ω木4Ω",
			wantOut: false,
		},
		{
			name:    "repeating and non-repeating single, double and triple-byte characters",
			in:      "X4木Ω木4Ω",
			wantOut: false,
		},
		{
			name:    "unique spaces",
			in:      " \t\n",
			wantOut: true,
		},
		{
			name:    "repeated spaces",
			in:      " \t \n \n \t",
			wantOut: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := IsUnique(testCase.in)
			if actualOut != testCase.wantOut {
				t.Errorf("for %q expected %t but got %t", testCase.in, testCase.wantOut, actualOut)
			}
		})
	}
}
