package unique

// IsUnique returns true if and only if its string argument has unique characters
// (none of its characters repeat)
func IsUnique(str string) bool {
	strChars := map[rune]struct{}{}

	for _, r := range str {
		if _, ok := strChars[r]; ok {
			return false
		}
		strChars[r] = struct{}{}
	}

	return true
}
