package rotmat

import "fmt"

var (
	// errMatrixNotSquare says that a matrix that should have been square isn't
	errMatrixNotSquare = fmt.Errorf("the matrix is not square")
)

// RotateMatrixClockwise rotates the elements inside the square matrix
// It returns an error if the given matrix isn't square,
// meaning it has at least one row with a length different from the number of rows
func RotateMatrixClockwise(m [][]int) error {
	for row := 0; row < len(m); row++ {
		if len(m[row]) != len(m) {
			return errMatrixNotSquare
		}
	}

	for frame := 0; frame < len(m)/2; frame++ {
		for column := frame; column < len(m)-1-frame; column++ {
			value := m[frame][column]
			m[frame][column] = m[len(m)-1-column][frame]
			m[len(m)-1-column][frame] = m[len(m)-1-frame][len(m)-1-column]
			m[len(m)-1-frame][len(m)-1-column] = m[column][len(m)-1-frame]
			m[column][len(m)-1-frame] = value
		}
	}

	return nil
}

// RotateMatrixClockwiseS rotates the elements inside the square matrix
// It returns an error if the given matrix isn't square,
// meaning it has at least one row with a length different from the number of rows
func RotateMatrixClockwiseS(m [][]int) error {
	for row := 0; row < len(m); row++ {
		if len(m[row]) != len(m) {
			return errMatrixNotSquare
		}
	}

	// Transpose the matrix (the rows become columns and vice versa)
	for row := 0; row < len(m); row++ {
		for column := 0; column <= row; column++ {
			m[row][column], m[column][row] = m[column][row], m[row][column]
		}
	}

	// Reverse the order of the columns : put them last to first
	for column := 0; column < len(m)/2; column++ {
		for row := 0; row < len(m); row++ {
			m[row][column], m[row][len(m)-1-column] = m[row][len(m)-1-column], m[row][column]
		}
	}

	return nil
}

// RotateMatrixCounterClockwise rotates the elements inside the square matrix counter-clockwise once
// It returns an error if the given matrix isn't square,
// meaning it has at least one row with a length different from the number of rows
func RotateMatrixCounterClockwise(m [][]int) error {
	for row := 0; row < len(m); row++ {
		if len(m[row]) != len(m) {
			return errMatrixNotSquare
		}
	}

	for frame := 0; frame < len(m)/2; frame++ {
		for column := frame; column < len(m)-1-frame; column++ {
			value := m[frame][column]
			m[frame][column] = m[column][len(m)-1-frame]
			m[column][len(m)-1-frame] = m[len(m)-1-frame][len(m)-1-column]
			m[len(m)-1-frame][len(m)-1-column] = m[len(m)-1-column][frame]
			m[len(m)-1-column][frame] = value
		}
	}

	return nil
}

// RotateMatrixCounterClockwiseS rotates the elements inside the square matrix counter-clockwise once
// It returns an error if the given matrix isn't square,
// meaning it has at least one row with a length different from the number of rows
func RotateMatrixCounterClockwiseS(m [][]int) error {
	for row := 0; row < len(m); row++ {
		if len(m[row]) != len(m) {
			return errMatrixNotSquare
		}
	}

	// Transpose the matrix (the rows become columns and vice versa)
	for row := 0; row < len(m); row++ {
		for column := 0; column <= row; column++ {
			m[row][column], m[column][row] = m[column][row], m[row][column]
		}
	}

	// Reverse the order of the rows : put them last to first
	for row := 0; row < len(m)/2; row++ {
		m[row], m[len(m)-1-row] = m[len(m)-1-row], m[row]
	}

	return nil
}
