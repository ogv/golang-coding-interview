package rotmat

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/00_utils/sliceutils"
)

type rotateTestCase struct {
	name    string
	in      [][]int
	wantOut [][]int
	wantErr error
}

func TestRotateMatrixClockwise(t *testing.T) {
	for _, testCase := range generateRotateClockwiseTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			originalInput := sliceutils.CopyIntMatrix(testCase.in)
			outErr := RotateMatrixClockwise(testCase.in)
			if outErr != testCase.wantErr {
				t.Errorf("for %q expected error %s but got %s", originalInput, testCase.wantErr, outErr)
			}
			if !reflect.DeepEqual(testCase.in, testCase.wantOut) {
				t.Errorf("for %v expected %v but got %v", originalInput, testCase.wantOut, testCase.in)
			}
		})
	}
}

func generateRotateClockwiseTestCases() []rotateTestCase {
	return []rotateTestCase{
		{
			name:    "nil matrix",
			in:      nil,
			wantOut: nil,
			wantErr: nil,
		},
		{
			name:    "empty matrix",
			in:      [][]int{},
			wantOut: [][]int{},
			wantErr: nil,
		},
		{
			name:    "one element matrix",
			in:      [][]int{{21}},
			wantOut: [][]int{{21}},
			wantErr: nil,
		},
		{
			name:    "1 row, 2 columns matrix",
			in:      [][]int{{21, 22}},
			wantOut: [][]int{{21, 22}},
			wantErr: errMatrixNotSquare,
		},
		{
			name:    "2 rows, 1 column matrix",
			in:      [][]int{{21}, {22}},
			wantOut: [][]int{{21}, {22}},
			wantErr: errMatrixNotSquare,
		},
		{
			name: "different row lengths matrix",
			in: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 29},
			},
			wantErr: errMatrixNotSquare,
		},

		{
			name: "odd length square matrix",
			in: [][]int{
				{21, 22, 23},
				{24, 25, 26},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{27, 24, 21},
				{28, 25, 22},
				{29, 26, 23},
			},
			wantErr: nil,
		},
		{
			name: "larger odd length square matrix",
			in: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
				{41, 42, 43, 44, 45},
			},
			wantOut: [][]int{
				{41, 36, 31, 26, 21},
				{42, 37, 32, 27, 22},
				{43, 38, 33, 28, 23},
				{44, 39, 34, 29, 24},
				{45, 40, 35, 30, 25},
			},
			wantErr: nil,
		},

		{
			name: "even length square matrix",
			in: [][]int{
				{21, 22, 23, 24},
				{25, 26, 27, 28},
				{29, 30, 31, 32},
				{33, 34, 35, 36},
			},
			wantOut: [][]int{
				{33, 29, 25, 21},
				{34, 30, 26, 22},
				{35, 31, 27, 23},
				{36, 32, 28, 24},
			},
			wantErr: nil,
		},
		{
			name: "larger even length square matrix",
			in: [][]int{
				{21, 22, 23, 24, 25, 26},
				{27, 28, 29, 30, 31, 32},
				{33, 34, 35, 36, 37, 38},
				{39, 40, 41, 42, 43, 44},
				{45, 46, 47, 48, 49, 50},
				{51, 52, 53, 54, 55, 56},
			},
			wantOut: [][]int{
				{51, 45, 39, 33, 27, 21},
				{52, 46, 40, 34, 28, 22},
				{53, 47, 41, 35, 29, 23},
				{54, 48, 42, 36, 30, 24},
				{55, 49, 43, 37, 31, 25},
				{56, 50, 44, 38, 32, 26},
			},
			wantErr: nil,
		},
	}
}

func TestRotateMatrixClockwiseS(t *testing.T) {
	for _, testCase := range generateRotateClockwiseTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			originalInput := sliceutils.CopyIntMatrix(testCase.in)
			outErr := RotateMatrixClockwiseS(testCase.in)
			if outErr != testCase.wantErr {
				t.Errorf("for %q expected error %s but got %s", originalInput, testCase.wantErr, outErr)
			}
			if !reflect.DeepEqual(testCase.in, testCase.wantOut) {
				t.Errorf("for %v expected %v but got %v", originalInput, testCase.wantOut, testCase.in)
			}
		})
	}
}

func TestRotateMatrixCounterClockwise(t *testing.T) {
	for _, testCase := range generateRotateCounterClockwiseTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			originalInput := sliceutils.CopyIntMatrix(testCase.in)
			outErr := RotateMatrixCounterClockwise(testCase.in)
			if outErr != testCase.wantErr {
				t.Errorf("for %q expected error %s but got %s", originalInput, testCase.wantErr, outErr)
			}
			if !reflect.DeepEqual(testCase.in, testCase.wantOut) {
				t.Errorf("for %v expected %v but got %v", originalInput, testCase.wantOut, testCase.in)
			}
		})
	}
}

func generateRotateCounterClockwiseTestCases() []rotateTestCase {
	return []rotateTestCase{
		{
			name:    "nil matrix",
			in:      nil,
			wantOut: nil,
			wantErr: nil,
		},
		{
			name:    "empty matrix",
			in:      [][]int{},
			wantOut: [][]int{},
			wantErr: nil,
		},
		{
			name:    "one element matrix",
			in:      [][]int{{21}},
			wantOut: [][]int{{21}},
			wantErr: nil,
		},
		{
			name:    "1 row, 2 columns matrix",
			in:      [][]int{{21, 22}},
			wantOut: [][]int{{21, 22}},
			wantErr: errMatrixNotSquare,
		},
		{
			name:    "2 rows, 1 column matrix",
			in:      [][]int{{21}, {22}},
			wantOut: [][]int{{21}, {22}},
			wantErr: errMatrixNotSquare,
		},
		{
			name: "different row lengths matrix",
			in: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 29},
			},
			wantErr: errMatrixNotSquare,
		},

		{
			name: "odd length square matrix",
			in: [][]int{
				{21, 22, 23},
				{24, 25, 26},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{23, 26, 29},
				{22, 25, 28},
				{21, 24, 27},
			},
			wantErr: nil,
		},
		{
			name: "even length square matrix",
			in: [][]int{
				{21, 22, 23, 24},
				{25, 26, 27, 28},
				{29, 30, 31, 32},
				{33, 34, 35, 36},
			},
			wantOut: [][]int{
				{24, 28, 32, 36},
				{23, 27, 31, 35},
				{22, 26, 30, 34},
				{21, 25, 29, 33},
			},
			wantErr: nil,
		},
	}
}

func TestRotateMatrixCounterClockwiseS(t *testing.T) {
	for _, testCase := range generateRotateCounterClockwiseTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			originalInput := sliceutils.CopyIntMatrix(testCase.in)
			outErr := RotateMatrixCounterClockwiseS(testCase.in)
			if outErr != testCase.wantErr {
				t.Errorf("for %q expected error %s but got %s", originalInput, testCase.wantErr, outErr)
			}
			if !reflect.DeepEqual(testCase.in, testCase.wantOut) {
				t.Errorf("for %v expected %v but got %v", originalInput, testCase.wantOut, testCase.in)
			}
		})
	}
}
