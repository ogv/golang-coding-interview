package strcomp

import (
	"testing"
)

func TestCompressString(t *testing.T) {
	testCases := []struct {
		name    string
		in      string
		wantOut string
	}{
		{
			name:    "empty string",
			in:      "",
			wantOut: "",
		},
		{
			name:    "one ASCII letter",
			in:      "a",
			wantOut: "a",
		},
		{
			name:    "2 ASCII letters",
			in:      "aa",
			wantOut: "aa",
		},
		{
			name:    "3 ASCII letters",
			in:      "aaa",
			wantOut: "a3",
		},
		{
			name:    "one multi-byte character",
			in:      "木",
			wantOut: "木",
		},

		{
			name:    "2 multi-byte characters",
			in:      "木木",
			wantOut: "木2",
		},

		{
			name:    "spaces at the end",
			in:      "gul      ",
			wantOut: "g1u1l1 6",
		},
		{
			name:    "multiple compressed characters",
			in:      "aaabccccaa",
			wantOut: "a3b1c4a2",
		},
		{
			name:    "ending in a single character",
			in:      "aaabcccca",
			wantOut: "a3b1c4a1",
		},
		{
			name:    "unique spaces",
			in:      " \t\n  ",
			wantOut: " \t\n  ",
		},
		{
			name:    "repeated spaces",
			in:      "   \t        ",
			wantOut: " 3\t1 8",
		},
		{
			name:    "spaces and letters",
			in:      "Mr   Jo      ",
			wantOut: "M1r1 3J1o1 6",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := CompressString(testCase.in)
			if actualOut != testCase.wantOut {
				t.Errorf("for %q expected %q but got %q", testCase.in, testCase.wantOut, actualOut)
			}
		})
	}
}
