package strcomp

import (
	"strconv"
	"strings"
)

// CompressString does a basic compression of the input string
// It uses the counts of consecutive repeated characters
// For example, "aaabccccaa" would become "a3b1c4a2"
// If the result would not become smaller than the original string,
// it returns the original string.
func CompressString(s string) string {
	builder := strings.Builder{}
	previousRune := int32(-1)
	count := 0
	for _, r := range s {
		if r == previousRune || previousRune < 0 {
			count++
		} else {
			builder.WriteRune(previousRune)
			builder.WriteString(strconv.Itoa(count))
			count = 1
		}
		previousRune = r
	}

	// Add the last sequence of identical characters
	// For the empty string, it still writes -1 and "1", but that is discarded as longer than ""
	builder.WriteRune(previousRune)
	builder.WriteString(strconv.Itoa(count))

	if builder.Len() < len(s) {
		return builder.String()
	}
	return s
}
