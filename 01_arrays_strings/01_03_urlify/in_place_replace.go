package urlify

// URLify replaces all spaces in a string with "%20"
// It assumes that the string has sufficient space at the end to hold the additional characters
func URLify(str string) string {
	asRunes := []rune(str)
	write, read := len(asRunes)-1, len(asRunes)-1

	for read >= 0 && asRunes[read] == ' ' {
		read--
	}

	encodedSpace := []rune("%20")
	// Remember if there was any need to move a character or not.
	// So that, for "regular     ", it returns "regular     ", NOT "regulregular"
	anyReplacement := false
	for read >= 0 {
		if asRunes[read] == ' ' {
			anyReplacement = true
			write = writeReversed(asRunes, encodedSpace, write)
		} else {
			asRunes[write] = asRunes[read]
			write--
		}

		read--
	}

	if anyReplacement {
		return string(asRunes)
	}
	return str
}

// writeReversed writes t in s, from the start position in s towards its beginning
// It writes at most len(t) runes, replacing runes from s, not inserting into s
// It returns the position in s right before the last written rune of t
func writeReversed(s []rune, t []rune, start int) int {
	sIndex := start
	tIndex := len(t) - 1
	for ; sIndex >= 0 && tIndex >= 0; sIndex, tIndex = sIndex-1, tIndex-1 {
		s[sIndex] = t[tIndex]
	}

	return sIndex
}
