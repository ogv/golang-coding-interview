package urlify

import (
	"testing"
)

func TestInPlaceReplace(t *testing.T) {
	testCases := []struct {
		name    string
		in      string
		wantOut string
	}{
		{
			name:    "empty string",
			in:      "",
			wantOut: "",
		},
		{
			name:    "one ASCII letter",
			in:      "a",
			wantOut: "a",
		},
		{
			name:    "repeated ASCII letter",
			in:      "aa",
			wantOut: "aa",
		},
		{
			name:    "one multi-byte character",
			in:      "木",
			wantOut: "木",
		},
		{
			name:    "repeated multi-byte character",
			in:      "木木",
			wantOut: "木木",
		},
		{
			name:    "spaces only at the end",
			in:      "regular     ",
			wantOut: "regular     ",
		},
		{
			name:    "unique spaces",
			in:      " \t\n  ",
			wantOut: "%20\t\n",
		},
		{
			name:    "repeated spaces",
			in:      " \t \n \n \t        ",
			wantOut: "%20\t%20\n%20\n%20\t",
		},
		{
			name:    "spaces and letters",
			in:      "Mr  John Smith      ",
			wantOut: "Mr%20%20John%20Smith",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := URLify(testCase.in)
			if actualOut != testCase.wantOut {
				t.Errorf("for %q expected %q but got %q", testCase.in, testCase.wantOut, actualOut)
			}
		})
	}
}
