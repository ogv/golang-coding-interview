package permpal

import "unicode"

// IsPalindromePermutation returns true if and only if the string argument
// is a permutation of a palindrome, where a palindrome only takes into account letters,
// not spaces, digits or any other kind of character.
// It ignores case, treating letters with different cases as the same letter.
func IsPalindromePermutation(s string) bool {
	counts := map[rune]int{}
	for _, r := range s {
		if unicode.IsLetter(r) {
			counts[unicode.ToUpper(r)]++
		}
	}

	// A palindrome either has two halves with letters appearing an even number of times,
	// or the same thing, plus a letter in the middle
	oddCount := 0
	for _, count := range counts {
		if count%2 != 0 {
			if oddCount == 1 {
				return false
			}
			oddCount++
		}
	}

	return true
}
