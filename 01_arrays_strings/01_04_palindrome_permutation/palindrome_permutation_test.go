package permpal

import (
	"testing"
)

func TestPalindromePermutation(t *testing.T) {
	testCases := []struct {
		name    string
		in      string
		wantOut bool
	}{
		{
			name:    "empty string",
			in:      "",
			wantOut: true,
		},
		{
			name:    "one ASCII letter",
			in:      "a",
			wantOut: true,
		},
		{
			name:    "repeated ASCII letter",
			in:      "aa",
			wantOut: true,
		},
		{
			name:    "repeated ASCII letter, different cases",
			in:      "BAba",
			wantOut: true,
		},
		{
			name:    "one multi-byte character",
			in:      "木",
			wantOut: true,
		},
		{
			name:    "repeated multi-byte character",
			in:      "木木",
			wantOut: true,
		},
		{
			name:    "unique single, double and triple-byte characters",
			in:      "4木ΩX",
			wantOut: false,
		},
		{
			name:    "repeating single, double and triple-byte characters",
			in:      "4木Ω木4Ω",
			wantOut: true,
		},
		{
			name:    "repeating and non-repeating single, double and triple-byte characters",
			in:      "X木Ω木4Ω",
			wantOut: true,
		},
		{
			name:    "more than one character appearing an odd number of times",
			in:      "XY4木Ω木4ΩXXYY",
			wantOut: false,
		},
		{
			name:    "no letters",
			in:      " \t\n",
			wantOut: true,
		},
		{
			name:    "letters and spaces",
			in:      "Tact \t \n Coa\n \t",
			wantOut: true,
		},
		{
			name:    "ignores digits",
			in:      "Tact \t 16\n Coa\n \t",
			wantOut: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := IsPalindromePermutation(testCase.in)
			if actualOut != testCase.wantOut {
				t.Errorf("for %q expected %t but got %t", testCase.in, testCase.wantOut, actualOut)
			}
		})
	}
}
