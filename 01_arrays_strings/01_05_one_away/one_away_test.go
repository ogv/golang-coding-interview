package oneaway

import (
	"testing"
)

func TestIsOneAway(t *testing.T) {
	testCases := []struct {
		name    string
		inS     string
		inT     string
		wantOut bool
	}{
		{
			name:    "empty strings",
			inS:     "",
			inT:     "",
			wantOut: true,
		},
		{
			name:    "one char added",
			inS:     "four",
			inT:     "fours",
			wantOut: true,
		},
		{
			name:    "more than one char added",
			inS:     "four",
			inT:     "fourth",
			wantOut: false,
		},
		{
			name:    "more than one char removed",
			inS:     "fourth",
			inT:     "four",
			wantOut: false,
		},
		{
			name:    "one char removed",
			inS:     "fours",
			inT:     "four",
			wantOut: true,
		},
		{
			name:    "same single character, same order",
			inS:     "A",
			inT:     "A",
			wantOut: true,
		},
		{
			name:    "different case, same order",
			inS:     "pan",
			inT:     "pAn",
			wantOut: true,
		},
		{
			name:    "same characters, same order",
			inS:     "pan",
			inT:     "pan",
			wantOut: true,
		},
		{
			name:    "same characters, different order",
			inS:     "pan",
			inT:     "nap",
			wantOut: false,
		},
		{
			name:    "same unique characters, different order",
			inS:     "STREETS",
			inT:     "TREESTS",
			wantOut: false,
		},
		{
			name:    "same length, one-character replacement",
			inS:     "four",
			inT:     "tour",
			wantOut: true,
		},
		{
			name:    "same length, multiple character replacements",
			inS:     "four",
			inT:     "grin",
			wantOut: false,
		},
		{
			name:    "multi-byte same length, same characters",
			inS:     "4木Ω木4Ω",
			inT:     "4木Ω木4Ω",
			wantOut: true,
		},
		{
			name:    "multi-byte same length, one replacement",
			inS:     "4木Ω",
			inT:     "4あΩ",
			wantOut: true,
		},
		{
			name:    "multi-byte same length, two replacements",
			inS:     "4木Ω",
			inT:     "4あα",
			wantOut: false,
		},
		{
			name:    "multi-byte same length, same characters, different order",
			inS:     "4木Ω木4Ω",
			inT:     "Ω木4Ω4木",
			wantOut: false,
		},
		{
			name:    "different lengths, too many differences",
			inS:     "TREETS",
			inT:     "TREASTS",
			wantOut: false,
		},
		{
			name:    "same whitespace, same order",
			inS:     " \t \n \n \t",
			inT:     " \t \n \n \t",
			wantOut: true,
		},
		{
			name:    "same whitespace, different order",
			inS:     " \t \n \n \t",
			inT:     "\n\t    \t\n",
			wantOut: false,
		},
		{
			name:    "whitespace, one removal",
			inS:     " \t \n \n \t",
			inT:     " \t \n  \t",
			wantOut: true,
		},
		{
			name:    "whitespace, one insert",
			inS:     " \t \n \n \t",
			inT:     " \t \n \t\n \t",
			wantOut: true,
		},
		{
			name:    "whitespace, one replacement",
			inS:     " \t \n \n \t",
			inT:     " \t \n \t \t",
			wantOut: true,
		},
		{
			name:    "whitespace, more than one removal",
			inS:     " \t \n \n \t",
			inT:     " \t   \t",
			wantOut: false,
		},
		{
			name:    "whitespace, more than one insert",
			inS:     " \t \n \n \t",
			inT:     " \t \n \t\n\t \t",
			wantOut: false,
		},
		{
			name:    "whitespace, more than one replacement",
			inS:     " \t \n \n \t",
			inT:     " \t \t \t \t",
			wantOut: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := IsOneAway(testCase.inS, testCase.inT)
			if actualOut != testCase.wantOut {
				t.Errorf("for %q and %q expected %t but got %t", testCase.inS, testCase.inT, testCase.wantOut, actualOut)
			}
		})
	}
}
