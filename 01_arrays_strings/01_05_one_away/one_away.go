package oneaway

// IsOneAway return true if and only if the two strings are at most one edit away
// An edit is defined as inserting a char, removing a char, or replacing a char
func IsOneAway(s, t string) bool {
	longer := []rune(s)
	shorter := []rune(t)

	if len(shorter) > len(longer) {
		longer, shorter = shorter, longer
	}
	if len(longer)-len(shorter) > 1 {
		return false
	}

	foundDifference := false
	for shorterI, longerI := 0, 0; shorterI < len(shorter); {
		if shorter[shorterI] == longer[longerI] {
			shorterI++
			longerI++
		} else {
			if foundDifference {
				return false
			}
			foundDifference = true
			if len(shorter) == len(longer) {
				shorterI++
			}
			longerI++
		}
	}

	return true
}
