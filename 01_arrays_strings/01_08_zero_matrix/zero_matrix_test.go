package zeromat

import (
	"reflect"
	"testing"

	"gitlab.com/ogv/golang-coding-interview/00_utils/sliceutils"
)

type clearOnZeroIntersectionTestCase struct {
	name    string
	in      [][]int
	wantOut [][]int
}

func TestClearOnZeroIntersection(t *testing.T) {
	for _, testCase := range generateClearOnZeroIntersectionTestCases() {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			originalInput := sliceutils.CopyIntMatrix(testCase.in)
			ClearOnZeroIntersection(testCase.in)
			if !reflect.DeepEqual(testCase.in, testCase.wantOut) {
				t.Errorf("for %v expected %v but got %v", originalInput, testCase.wantOut, testCase.in)
			}
		})
	}
}

func generateClearOnZeroIntersectionTestCases() []clearOnZeroIntersectionTestCase {
	return []clearOnZeroIntersectionTestCase{
		{
			name:    "nil matrix",
			in:      nil,
			wantOut: nil,
		},
		{
			name:    "empty matrix",
			in:      [][]int{},
			wantOut: [][]int{},
		},
		{
			name:    "one element matrix",
			in:      [][]int{{21}},
			wantOut: [][]int{{21}},
		},
		{
			name:    "one element with zero",
			in:      [][]int{{00}},
			wantOut: [][]int{{00}},
		},
		{
			name:    "1 row",
			in:      [][]int{{21, 22, 23}},
			wantOut: [][]int{{21, 22, 23}},
		},
		{
			name:    "1 row with zero",
			in:      [][]int{{21, 00, 23}},
			wantOut: [][]int{{00, 00, 00}},
		},
		{
			name:    "3 rows, 1 column",
			in:      [][]int{{21}, {22}, {23}},
			wantOut: [][]int{{21}, {22}, {23}},
		},
		{
			name:    "3 rows, 1 column with zero",
			in:      [][]int{{21}, {22}, {00}},
			wantOut: [][]int{{00}, {00}, {00}},
		},
		{
			name: "nil row in different row lengths matrix",
			in: [][]int{
				{21, 22, 23},
				nil,
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 22, 23},
				nil,
				{27, 28, 29},
			},
		},
		{
			name: "different row lengths matrix",
			in: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 29},
			},
		},
		{
			name: "longer row with zero",
			in: [][]int{
				{21, 22, 23},
				{24, 25},
				{27, 28, 00},
			},
			wantOut: [][]int{
				{21, 22, 00},
				{24, 25},
				{00, 00, 00},
			},
		},
		{
			name: "nil row and longer row with zero",
			in: [][]int{
				{21, 22, 23},
				nil,
				{27, 28, 00},
			},
			wantOut: [][]int{
				{21, 22, 00},
				nil,
				{00, 00, 00},
			},
		},
		{
			name: "longer row in the middle",
			in: [][]int{
				{21, 22, 23, 24},
				{25, 26, 27, 28, 29, 30},
				{31, 32, 33},
			},
			wantOut: [][]int{
				{21, 22, 23, 24},
				{25, 26, 27, 28, 29, 30},
				{31, 32, 33},
			},
		},
		{
			name: "longer row in the middle with zero",
			in: [][]int{
				{21, 22, 23, 24},
				{25, 26, 27, 00, 29, 30},
				{31, 32, 33},
			},
			wantOut: [][]int{
				{21, 22, 23, 00},
				{00, 00, 00, 00, 00, 00},
				{31, 32, 33},
			},
		},

		{
			name: "square matrix",
			in: [][]int{
				{21, 22, 23},
				{24, 25, 26},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 22, 23},
				{24, 25, 26},
				{27, 28, 29},
			},
		},
		{
			name: "square matrix with zero in the centre",
			in: [][]int{
				{21, 22, 23},
				{24, 00, 26},
				{27, 28, 29},
			},
			wantOut: [][]int{
				{21, 00, 23},
				{00, 00, 00},
				{27, 00, 29},
			},
		},
		{
			name: "square matrix with zero in more than one place",
			in: [][]int{
				{21, 22, 23},
				{00, 25, 26},
				{27, 00, 29},
			},
			wantOut: [][]int{
				{00, 00, 23},
				{00, 00, 00},
				{00, 00, 00},
			},
		},
		{
			name: "larger square matrix",
			in: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
				{41, 42, 43, 44, 45},
			},
			wantOut: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
				{41, 42, 43, 44, 45},
			},
		},
		{
			name: "larger square matrix with zero in more than one place",
			in: [][]int{
				{21, 00, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 00, 35},
				{36, 37, 38, 39, 40},
				{41, 42, 43, 44, 45},
			},
			wantOut: [][]int{
				{00, 00, 00, 00, 00},
				{26, 00, 28, 00, 30},
				{00, 00, 00, 00, 00},
				{36, 00, 38, 00, 40},
				{41, 00, 43, 00, 45},
			},
		},

		{
			name: "less rows than columns matrix",
			in: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
			},
			wantOut: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 28, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 37, 38, 39, 40},
			},
		},
		{
			name: "less rows than columns matrix with zero in more than one place",
			in: [][]int{
				{21, 22, 23, 24, 25},
				{26, 27, 00, 29, 30},
				{31, 32, 33, 34, 35},
				{36, 00, 38, 39, 40},
			},
			wantOut: [][]int{
				{21, 00, 00, 24, 25},
				{00, 00, 00, 00, 00},
				{31, 00, 00, 34, 35},
				{00, 00, 00, 00, 00},
			},
		},
		{
			name: "more rows than columns matrix",
			in: [][]int{
				{21, 22, 23, 24},
				{27, 28, 29, 30},
				{33, 34, 35, 36},
				{39, 40, 41, 42},
				{45, 46, 47, 48},
				{51, 52, 53, 54},
			},
			wantOut: [][]int{
				{21, 22, 23, 24},
				{27, 28, 29, 30},
				{33, 34, 35, 36},
				{39, 40, 41, 42},
				{45, 46, 47, 48},
				{51, 52, 53, 54},
			},
		},
		{
			name: "more rows than columns matrix with zero in more than one place",
			in: [][]int{
				{21, 22, 23, 24},
				{27, 00, 29, 30},
				{33, 34, 35, 00},
				{00, 40, 41, 42},
				{45, 46, 00, 48},
				{51, 52, 53, 54},
			},
			wantOut: [][]int{
				{00, 00, 00, 00},
				{00, 00, 00, 00},
				{00, 00, 00, 00},
				{00, 00, 00, 00},
				{00, 00, 00, 00},
				{00, 00, 00, 00},
			},
		},
	}
}
