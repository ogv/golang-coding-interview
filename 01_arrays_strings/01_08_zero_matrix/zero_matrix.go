package zeromat

// ClearOnZeroIntersection sets the elements from row R or from column C to zero
// if the element at row R and column C is zero
// It does not do other changes to the matrix parameter
func ClearOnZeroIntersection(m [][]int) {
	rowsToClear := map[int]struct{}{}
	columnsToClear := map[int]struct{}{}
	/*
		Worst-case complexity:
			- space O(rows+columns)
			- time O(rows*columns)
	*/
	// First see which rows and columns need to be zeroed.
	// Only then do the zeroing (if we would do it while finding them, we'd reset more than needed
	// 3 5					0 5		0 0
	// 0 9  should become	0 0	not	0 0
	for row := 0; row < len(m); row++ {
		for column := 0; column < len(m[row]); column++ {
			if m[row][column] == 0 {
				rowsToClear[row] = struct{}{}
				columnsToClear[column] = struct{}{}
			}
		}
	}

	for row := range rowsToClear {
		for column := 0; column < len(m[row]); column++ {
			m[row][column] = 0
		}
	}

	for column := range columnsToClear {
		for row := 0; row < len(m); row++ {
			if column < len(m[row]) {
				m[row][column] = 0
			}
		}
	}
}
