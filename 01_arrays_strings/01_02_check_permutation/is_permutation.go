package permutation

// IsPermutation returns true if and only if the string parameters have the exact same characters,
// possibly in a different order, that is, if s is a permutation of t
func IsPermutation(s, t string) bool {
	if len(s) != len(t) {
		return false
	}

	counts := map[rune]int{}
	for _, r := range s {
		counts[r]++
	}

	for _, r := range t {
		if counts[r] == 0 {
			return false
		}
		counts[r]--
	}

	return true
}
