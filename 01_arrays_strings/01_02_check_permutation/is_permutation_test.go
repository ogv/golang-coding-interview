package permutation

import (
	"testing"
)

func TestIsPermutation(t *testing.T) {
	testCases := []struct {
		name    string
		inS     string
		inT     string
		wantOut bool
	}{
		{
			name:    "empty strings",
			inS:     "",
			inT:     "",
			wantOut: true,
		},
		{
			name:    "different length strings",
			inS:     "four",
			inT:     "fourr",
			wantOut: false,
		},
		{
			name:    "same single character, same order",
			inS:     "A",
			inT:     "A",
			wantOut: true,
		},
		{
			name:    "different case, same order",
			inS:     "pan",
			inT:     "pAn",
			wantOut: false,
		},
		{
			name:    "same unique characters, same order",
			inS:     "pan",
			inT:     "pan",
			wantOut: true,
		},
		{
			name:    "same unique lower case characters, different order",
			inS:     "pan",
			inT:     "nap",
			wantOut: true,
		},
		{
			name:    "same unique upper case characters, different order",
			inS:     "STREETS",
			inT:     "TREESTS",
			wantOut: true,
		},
		{
			name:    "same length, one-character difference",
			inS:     "four",
			inT:     "tour",
			wantOut: false,
		},
		{
			name:    "same characters, different counts",
			inS:     "foot",
			inT:     "fott",
			wantOut: false,
		},
		{
			name:    "same characters, smaller count",
			inS:     "foot",
			inT:     "fot",
			wantOut: false,
		},
		{
			name:    "same characters, larger count",
			inS:     "fot",
			inT:     "foot",
			wantOut: false,
		},
		{
			name:    "same length, missing and added characters",
			inS:     "font",
			inT:     "fist",
			wantOut: false,
		},
		{
			name:    "multi-byte same length, same characters",
			inS:     "4木Ω木4Ω",
			inT:     "4木Ω木4Ω",
			wantOut: true,
		},
		{
			name:    "multi-byte same length, different characters",
			inS:     "4木Ω",
			inT:     "3あα",
			wantOut: false,
		},

		{
			name:    "multi-byte same length, same characters, different order",
			inS:     "4木Ω木4Ω",
			inT:     "Ω木4Ω4木",
			wantOut: true,
		},
		{
			name:    "same whitespace, same order",
			inS:     " \t \n \n \t",
			inT:     " \t \n \n \t",
			wantOut: true,
		},
		{
			name:    "same whitespace, different order",
			inS:     " \t \n \n \t",
			inT:     "\n\t    \t\n",
			wantOut: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := IsPermutation(testCase.inS, testCase.inT)
			if actualOut != testCase.wantOut {
				t.Errorf("for %q and %q expected %t but got %t", testCase.inS, testCase.inT, testCase.wantOut, actualOut)
			}
		})
	}
}
