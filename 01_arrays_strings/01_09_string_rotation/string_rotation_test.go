package stringrot

import (
	"testing"
)

func TestIsRotation(t *testing.T) {
	testCases := []struct {
		name    string
		inS     string
		inT     string
		wantOut bool
	}{
		{
			name:    "empty strings",
			inS:     "",
			inT:     "",
			wantOut: true,
		},
		{
			name:    "different length strings",
			inS:     "four",
			inT:     "fourr",
			wantOut: false,
		},
		{
			name:    "same single character, same order",
			inS:     "A",
			inT:     "A",
			wantOut: true,
		},
		{
			name:    "different case, same order",
			inS:     "pan",
			inT:     "pAn",
			wantOut: false,
		},
		{
			name:    "same unique characters, same order",
			inS:     "pan",
			inT:     "pan",
			wantOut: true,
		},
		{
			name:    "same characters, left shift",
			inS:     "pan",
			inT:     "npa",
			wantOut: true,
		},
		{
			name:    "same characters, right shift",
			inS:     "STREETS",
			inT:     "REETSST",
			wantOut: true,
		},
		{
			name:    "same length, one-character difference",
			inS:     "four",
			inT:     "tour",
			wantOut: false,
		},
		{
			name:    "same characters, smaller count",
			inS:     "foot",
			inT:     "fot",
			wantOut: false,
		},
		{
			name:    "same characters, larger count",
			inS:     "fot",
			inT:     "foot",
			wantOut: false,
		},
		{
			name:    "first is a substring of the second",
			inS:     "fo",
			inT:     "foot",
			wantOut: false,
		},
		{
			name:    "second is a substring of the first",
			inS:     "foot",
			inT:     "fo",
			wantOut: false,
		},
		{
			name:    "multi-byte same length, same characters",
			inS:     "4木Ω木4Ω",
			inT:     "4木Ω木4Ω",
			wantOut: true,
		},
		{
			name:    "multi-byte same length, different characters",
			inS:     "4木Ω",
			inT:     "3あα",
			wantOut: false,
		},

		{
			name:    "multi-byte same length, same characters, shifted",
			inS:     "4木Ω木4Ω",
			inT:     "木4Ω4木Ω",
			wantOut: true,
		},
		{
			name:    "same whitespace, same order",
			inS:     " \t \n \n \t",
			inT:     " \t \n \n \t",
			wantOut: true,
		},
		{
			name:    "same whitespace, different order",
			inS:     " \t \n \n \t",
			inT:     "\n\t    \t\n",
			wantOut: false,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			t.Parallel()
			actualOut := IsRotation(testCase.inS, testCase.inT)
			if actualOut != testCase.wantOut {
				t.Errorf("for %q and %q expected %t but got %t", testCase.inS, testCase.inT, testCase.wantOut, actualOut)
			}
		})
	}
}
