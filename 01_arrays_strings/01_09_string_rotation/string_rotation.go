package stringrot

import "strings"

// IsRotation returns true if s is a rotation of t
func IsRotation(s, t string) bool {
	/*
		t is a rotation of s if s = xy and t = yx
		s + s = xyxy
		So if t is a substring of xyxy AND s and t have the same length,
		then t is a rotation of s
	*/
	return len(s) == len(t) && strings.Contains(s+s, t)
}
